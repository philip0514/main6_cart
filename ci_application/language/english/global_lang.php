<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['global'] = array(
	//A
	'action'								=>	'Action',
	'all'									=>	'All',
	'account'								=>	'Account',
	'address'								=>	'Address',
	'authority_read'						=>	'Read',
	'authority_insert'						=>	'Insert',
	'authority_update'						=>	'Update',
	'authority_delete'						=>	'Delete',
	'account'								=>	'Account',
	'account_help'							=>	'Required',
	'account_placeholder'					=>	'Require Account',
	
	//B
	'backend_title'							=>	' Control Center',
	
	//C
	'cancel'								=>	'Cancel',
	'column'								=>	'Column',
	'content'								=>	'Content',
	'cost_price'							=>	'Cost',
	
	//D
	'display'								=>	'Display',
	'display_on'							=>	'On',
	'display_off'							=>	'Off',
	'delete'								=>	'Delete',
	'delete_all'							=>	'Delete All',
	'datatable_info'						=>	' _START_ ~ _END_ / _TOTAL_ rows ',
	'datatable_info_filtered'				=>	'(All _MAX_ rows)',
	'datatable_first'						=>	'First',
	'datatable_last'						=>	'Last',
	'datatable_zero'						=>	'Nothing Found',
	'datatable_search_placeholder'			=>	'Search...',
	'description'							=>	'Description',
	'description_help'						=>	'Required',
	'description_placeholder'				=>	'Require Description',
	
	//E
	'edit'									=>	'Edit',
	
	//F
	
	//G
	
	//H
	
	//I
	'id'									=>	'ID',
	'insert'								=>	'New',
	'insert_time'							=>	'Insert Time',
	'image'									=>	'Image',
	
	//J
	
	//K

	//L
	'login'									=>	'Login',
	'login_error'							=>	'<b>Failed！</b>Please check you Account and Password',
	'logout'								=>	'Logout',
	'list_price'							=>	'List Price',

	//M
	'modify_time'							=>	'Modify Time',
	'meta'									=>	'Socail Data',
	'mobile'								=>	'Mobile',

	//N
	'name'									=>	'Name',
	'name_help'								=>	'Required',
	'name_placeholder'						=>	'Require Name',
	
	//O
	'ogimage'								=>	'Share Image',
	'ogimage_help'							=>	'Images is draggable and sorable, and the size within 「600px * 315px」 ～ 「1200px * 630px」',

	//P
	'price'									=>	'Price',
	'publish'								=>	'Publish',
	'publish_and_return'					=>	'Publish and Return',
	'password'								=>	'Password',
	'password_help'							=>	'Keep it blank, if you do not want to change it. ',
	'password_placeholder'					=>	'Password',
	'password_confirm'						=>	'Confirm Password',
	
	//Q
	
	//R

	//S
	'sort'									=>	'Sort',
	'search'								=>	'Search',
	'search_placeholder'					=>	'Name、Content',
	'search_btn'							=>	'Search',
	'search_reset_btn'						=>	'Reset',
	'setting'								=>	'Setting',
	'submit'								=>	'Submit',
	'submit_and_return'						=>	'Submit and Return',
	'saved'									=>	'Saved',
	'send_time'								=>	'Send Time',
	'status'								=>	'Status',
	'status_success'						=>	'Success',
	'status_failed'							=>	'Failed',

	//T
	
	//U
	
	//V

	//W
	
	//X
	
	//Y
	
	//Z
);



$lang['error']	= array(
	'name'			=>	array(
		'required'		=>	'Require Name',
	),
	'description'	=>	array(
		'required'		=>	'Require Description',
	),
	'account'		=>	array(
		'required'		=>	'Require Account',
	),
	'password'		=>	array(
		'required'		=>	'Require Password',
	),
	'member'		=>	array(
		'name'			=>	array(
			'required'		=>	'Require Name',
		),
		'account'		=>	array(
			'required'		=>	'Require Account',
			'valid_email'	=>	'The format has to be email, ex:abc@gmail.com',
			'remote'		=>	'Registered',
		),
		'password'		=>	array(
			'min_length'	=>	'At least 6 chars',
		),
	),
	'category'		=>	array(
		'name'			=>	array(
			'required'		=>	'名稱 必填',
		),
		'url_name'	=>	array(
			'url_name'		=>	'網址名稱含有不可使用的字元',
		),
	),
	'product'		=>	array(
		'name'			=>	array(
			'required'		=>	'名稱 必填',
		),
		'description'	=>	array(
			'required'		=>	'敘述 必填',
		),
		'list_price'	=>	array(
			'required'		=>	'定價 必填',
		),
		'price'	=>	array(
			'required'		=>	'售價 必填',
		),
	),
	'news'		=>	array(
		'name'			=>	array(
			'required'		=>	'名稱 必填',
		),
		'description'	=>	array(
			'required'		=>	'敘述 必填',
		),
		'news_time'	=>	array(
			'required'		=>	'請選擇時間 YYYY/mm/dd',
		),
	),
	'user'		=>	array(
		'name'			=>	array(
			'required'		=>	'姓名 必填',
		),
		'account'		=>	array(
			'required'		=>	'帳號 必填',
		),
	),
	'user_info'		=>	array(
		'name'			=>	array(
			'required'		=>	'姓名 必填',
		),
	),
	'tag'		=>	array(
		'name'			=>	array(
			'required'		=>	'Require Tag',
		),
	),
	
);


?>