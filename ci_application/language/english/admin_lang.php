<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['admin'] = array(
	'about_list'							=>	'About',
	'about_item'							=>	'New About',
	
	'category_list'							=>	'Category',
	'category_item'							=>	'New Category',
	
	'dashboard'								=>	'Dashboard',
	
	'logout'								=>	'Logout',

	'mail'									=>	'Mail',
	'mail_template_list'					=>	'Mail Template',
	'mail_history_list'						=>	'Mail History',
	'media_list'							=>	'Media',
	'media_item'							=>	'Media Edit',
	'member_list'							=>	'Member',
	'member_item'							=>	'Member Edit',
	
	'news_list'								=>	'News',
	'news_item'								=>	'News Edit',
	
	'order_list'							=>	'Order',
	'order_item'							=>	'Order Edit',
	
	'product_list'							=>	'Product',
	'product_item'							=>	'Product Edit',
	
	'system'								=>	'System',
	
	'tag_list'								=>	'Tag',
	'tag_item'								=>	'Tag',
	'theme_list'							=>	'Theme',
	'theme_item'							=>	'Theme',
	
	'user_info_list'						=>	'User Info',
	'user_list'								=>	'Manager',
	'user_item'								=>	'Manager Edit',
	'user_group_list'						=>	'Manager Group',
	'user_group_item'						=>	'Manager Group Edit',
	
	'website_setting'						=>	'Setting',
	
	'product_setting'						=>	'Product Setting',
	
	'color_type_list'						=>	'Color Group',
	'color_type_item'						=>	'Edit Color Group',
	
	'color_list'							=>	'Color',
	'color_item'							=>	'Color Edit',
	
	'spec_type_list'						=>	'Spec Group',
	'spec_type_item'						=>	'Edit Spec Group',
	
	'spec_list'								=>	'Spec',
	'spec_item'								=>	'Spec Edit',
	
	'inventory_list'						=>	'Inventory',
	'inventory_item'						=>	'Inventory Edit',
	
	'collection_list'						=>	'Collection',
	'collection_item'						=>	'Edit Collection',
	
	'coupon_list'							=>	'Coupon',
	'coupon_item'							=>	'Coupon Edit',
	
);

//menu
$lang['admin_menu'] = array(
	'about'									=>	$lang['admin']['about_list'],
	'category'								=>	$lang['admin']['category_list'],
	'dashboard'								=>	$lang['admin']['dashboard'],
	'mail'									=>	$lang['admin']['mail'],
	'mail_template'							=>	$lang['admin']['mail_template_list'],
	'mail_history'							=>	$lang['admin']['mail_history_list'],
	'media'									=>	$lang['admin']['media_list'],
	'member'								=>	$lang['admin']['member_list'],
	'news'									=>	$lang['admin']['news_list'],
	'product'								=>	$lang['admin']['product_list'],
	'tag'									=>	$lang['admin']['tag_list'],
	'theme'									=>	$lang['admin']['theme_list'],
	'system'								=>	$lang['admin']['system'],
	'website_setting'						=>	$lang['admin']['website_setting'],
	'user_info'								=>	$lang['admin']['user_info_list'],
	'manager'								=>	$lang['admin']['user_list'],
	'manage_group'							=>	$lang['admin']['user_group_list'],
	'logout'								=>	$lang['admin']['logout'],
	
	'product_setting'						=>	$lang['admin']['product_setting'],
	'color_type'							=>	$lang['admin']['color_type_list'],
	'color'									=>	$lang['admin']['color_list'],
	'spec_type'								=>	$lang['admin']['spec_type_list'],
	'spec'									=>	$lang['admin']['spec_list'],
	'inventory'								=>	$lang['admin']['inventory_list'],
	'collection'							=>	$lang['admin']['collection_list'],
	'coupon'								=>	$lang['admin']['coupon_list'],
);

$lang['admin_news'] = array(
	'date'									=>	'News Date',
	'date_help'								=>	'Required',
	'date_placeholder'						=>	'Require News Date',
);

$lang['admin_category'] = array(
	'insert'								=>	'New',
	'expanded'								=>	'Expand',
	'collapsed'								=>	'Collapse',
	'modify'								=>	'Edit Category',
	'url_name'								=>	'Url String',
	'url_name_help'							=>	'Please use English letter, number, and  「- 」,「 _ 」. And do not use 「category」and 「product」',
	'url_name_placeholder'					=>	'Url String',
);

$lang['admin_product'] = array(
	'search_placeholder'					=>	'ID、Name、Description',
	'type'									=>	'Type',
	'type0'									=>	'Merchandise',
	'type1'									=>	'Gift',
	'display_time'							=>	'Display Time',
	'display_start_time'					=>	'Starting',
	'display_end_time'						=>	'End',
	'weight'								=>	'Weight',
	'weight_unit'							=>	'kg',
	'box_size'								=>	'Volume',
	'box_size_unit'							=>	'cm',
	'tag'									=>	'Tag',
	'tag_help'								=>	'press ENTER or 「,」 to sperate tags.',
	'tag_placeholder'						=>	'Tag',
	'category'								=>	'Category',
	'price_setting'							=>	'Price',
	'list_price_help'						=>	'Required',
	'list_price_placeholder'				=>	'Require List Price',
	'price_help'							=>	'Required',
	'price_placeholder'						=>	'Require Price',
	
	'preorder_setting'						=>	'Preorder',
	'preorder'								=>	'Preorder',
	'preorder_start'						=>	'Starting',
	'preorder_end'							=>	'End',
	'preorder_price'						=>	'Preorder Price',
	'preorder_shipping'						=>	'Shipping',
	'preorder_shipping_time'				=>	'Preorder Shipping Time',
	
	'special_offer'							=>	'Special Offer',
	'special'								=>	'Special',
	'special_start'							=>	'Starting',
	'special_end'							=>	'End',
	'special_price'							=>	'Special Price',
	'special_max'							=>	'Max',
	'special_max_placeholder'				=>	'Max Selecting each time',
	
	'image_setting'							=>	'Images',
	'image'									=>	'Image',
	'image_help'							=>	'Images is draggable and sorable.',
);

$lang['admin_media'] = array(
	'insert'								=>	'New Files',
);

$lang['admin_member'] = array(
	'search_placeholder'					=>	'All Personal Data',
	'gender'								=>	'Gender',
	'gender0'								=>	'All Gender',
	'gender1'								=>	'Male',
	'gender2'								=>	'Female',
	'city'									=>	'City',
	'city_null'								=>	'None',
	'city0'									=>	'All',
	'area'									=>	'Area',
	'area_null'								=>	'None',
	'area0'									=>	'All',
	'birthday'								=>	'Birthday',
	'birthday_placeholder'					=>	'EX: 1990/01/31',
	'birthday_start'						=>	'Starting',
	'birthday_end'							=>	'End',
	'name'									=>	'Name',
	'name_help'								=>	'Required',
	'name_placeholder'						=>	'Require Name',
	'identity'								=>	'Use Identity',
	'insert_time'							=>	'Register Time',
	'facebook_id'							=>	'Facebook ID',
	'verified'								=>	'Verified',
	'account_help'							=>	'Required',
	'account_placeholder'					=>	'Require Account',
	'password_help'							=>	'Keep it blank, if you do not want to change it. ',
	'password_placeholder'					=>	'Password',
	'last_login_time'						=>	'Last Login',
	'personal_data'							=>	'Personal Data',
);

$lang['admin_mail_history'] = array(
	'name'									=>	'Title',
	'send_to'								=>	'Recipient',
	'send_time'								=>	'Send Time',
	'status'								=>	'Status',
	'error'									=>	'Failed Message',
);

$lang['admin_theme'] = array(
	'upload'								=>	'Upload',
);

$lang['admin_user_group'] = array(
	'name'									=>	'Group Name',
	'authority'								=>	'Authority',
	'group'									=>	'Group',
	'company'								=>	'Company',
	'company_placeholder'					=>	'Company',
	'phone'									=>	'Phone',
	'phone_placeholder'						=>	'Phone',
	'record'								=>	'Login Record',
	'record_time'							=>	'Time',
	'record_ip'								=>	'IP',
	'record_status'							=>	'Status',
	'record_zero'							=>	'No Record',
);

$lang['admin_user'] = array(
	'name'									=>	'Name',
	'authority'								=>	'Authority',
	'group'									=>	'Group',
	'company'								=>	'Company',
	'company_placeholder'					=>	'Company',
	'phone'									=>	'Phone',
	'phone_placeholder'						=>	'Phone',
	'record'								=>	'Login Record',
	'record_time'							=>	'Time',
	'record_ip'								=>	'IP',
	'record_status'							=>	'Status',
	'record_zero'							=>	'No Record',
);

$lang['admin_setting'] = array(
	'title'									=>	'Website Title',
	'title_help'							=>	'Required',
	'title_placeholder'						=>	'Require Website Title',
	'description'							=>	'Website Description',
	'description_help'						=>	'Required',
	'description_placeholder'				=>	'Require Description',
	'keyword'								=>	'Keyword',
	'keyword_help'							=>	'Required',
	'keyword_placeholder'					=>	'Require Keyword',
	'facebook'								=>	'Facebook',
	'facebook_login'						=>	'Facebook Login',
	'facebook_scope'						=>	'Facebook Scope',
	'facebook_scope_help'					=>	'Seperate with ,',
	'facebook_scope_placeholder'			=>	'Facebook Scope',
	'facebook_appid'						=>	'Facebook App id',
	'facebook_appid_placeholder'			=>	'Facebook App id',
	'google'								=>	'Google',
	'ga_code'								=>	'Google Analytics',
	'ga_code_placeholder'					=>	'Google Analytics',
	'page_js'								=>	'Other Javascript',
	'page_js_placeholder'					=>	'Other Javascript',
	
	'mail_setting'							=>	'Mail Settings',
	'mail_function'							=>	'MAIL',
	'mail_type'								=>	'Type',
	'mail_type0'							=>	'Custom',
	'mail_type1'							=>	'GMail',
	'mail_host'								=>	'Host',
	'mail_host_placeholder'					=>	'Host',
	'mail_port'								=>	'PORT',
	'mail_port_placeholder'					=>	'PORT',
	'mail_username'							=>	'Username',
	'mail_username_placeholder'				=>	'Username',
	'mail_password'							=>	'Password',
	'mail_password_placeholder'				=>	'Password',
	'mail_secure'							=>	'Secure',
	'mail_charset'							=>	'Charset',
	'mail_charset_placeholder'				=>	'Charset',
	'mail_encoding'							=>	'Encoding',
	'mail_encoding_placeholder'				=>	'Encoding',
	'mail_from'								=>	'Email Address',
	'mail_from_placeholder'					=>	'Email Address',
	'mail_fromname'							=>	'Email Name',
	'mail_fromname_placeholder'				=>	'Email Name',
	'mail_test'								=>	'Mail Test',
	
);

$lang['admin_tag'] = array(
	'name'									=>	'Tag',
);










