<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['admin'] = array(
	'about_list'							=>	'關於我們',
	'about_item'							=>	'編輯 關於我們',
	
	'category_list'							=>	'類別',
	'category_item'							=>	'編輯 類別',
	
	'dashboard'								=>	'主控台',
	
	'logout'								=>	'登出',

	'mail'									=>	'信件',
	'mail_template_list'					=>	'信件樣板',
	'mail_history_list'						=>	'歷史信件',
	'media_list'							=>	'媒體庫',
	'media_item'							=>	'編輯 媒體庫',
	'member_list'							=>	'會員',
	'member_item'							=>	'編輯 會員',
	
	'news_list'								=>	'最新消息',
	'news_item'								=>	'編輯 最新消息',
	
	'order_list'							=>	'訂單',
	'order_item'							=>	'編輯訂單',
	
	'product_list'							=>	'商品',
	'product_item'							=>	'編輯 商品',
	
	'system'								=>	'系統',
	
	'tag_list'								=>	'標籤',
	'tag_item'								=>	'標籤',
	'theme_list'							=>	'外觀',
	'theme_item'							=>	'編輯 外觀',
	
	'user_info_list'						=>	'個人設定',
	'user_list'								=>	'管理者',
	'user_item'								=>	'編輯 管理者',
	'user_group_list'						=>	'管理群組',
	'user_group_item'						=>	'編輯 管理群組',
	
	'website_setting'						=>	'網站設定',
	
	'product_setting'						=>	'商品設定',
	
	'color_type_list'						=>	'顏色類別',
	'color_type_item'						=>	'編輯 顏色類別',
	
	'color_list'							=>	'顏色',
	'color_item'							=>	'編輯 顏色',
	
	'spec_type_list'						=>	'規格類別',
	'spec_type_item'						=>	'編輯 規格類別',
	
	'spec_list'								=>	'規格',
	'spec_item'								=>	'編輯 規格',
	
	'inventory_list'						=>	'庫存',
	'inventory_item'						=>	'編輯 庫存',
	
	'collection_list'						=>	'商品集合',
	'collection_item'						=>	'編輯 商品集合',
	
	'coupon_list'							=>	'優惠卷',
	'coupon_item'							=>	'編輯 優惠卷',
	
);

//menu
$lang['admin_menu'] = array(
	'about'									=>	$lang['admin']['about_list'],
	'category'								=>	$lang['admin']['category_list'],
	'dashboard'								=>	$lang['admin']['dashboard'],
	'mail'									=>	$lang['admin']['mail'],
	'mail_template'							=>	$lang['admin']['mail_template_list'],
	'mail_history'							=>	$lang['admin']['mail_history_list'],
	'media'									=>	$lang['admin']['media_list'],
	'member'								=>	$lang['admin']['member_list'],
	'news'									=>	$lang['admin']['news_list'],
	'product'								=>	$lang['admin']['product_list'],
	'order'									=>	$lang['admin']['order_list'],
	'tag'									=>	$lang['admin']['tag_list'],
	'theme'									=>	$lang['admin']['theme_list'],
	'system'								=>	$lang['admin']['system'],
	'website_setting'						=>	$lang['admin']['website_setting'],
	'user_info'								=>	$lang['admin']['user_info_list'],
	'manager'								=>	$lang['admin']['user_list'],
	'manage_group'							=>	$lang['admin']['user_group_list'],
	'logout'								=>	$lang['admin']['logout'],
	
	'product_setting'						=>	$lang['admin']['product_setting'],
	'color_type'							=>	$lang['admin']['color_type_list'],
	'color'									=>	$lang['admin']['color_list'],
	'spec_type'								=>	$lang['admin']['spec_type_list'],
	'spec'									=>	$lang['admin']['spec_list'],
	'inventory'								=>	$lang['admin']['inventory_list'],
	'collection'							=>	$lang['admin']['collection_list'],
	'coupon'								=>	$lang['admin']['coupon_list'],
);

$lang['admin_news'] = array(
	'date'									=>	'時間',
	'date_help'								=>	'必填',
	'date_placeholder'						=>	'時間 必填',
);

$lang['admin_category'] = array(
	'insert'								=>	'新增類別',
	'expanded'								=>	'全部展開',
	'collapsed'								=>	'全部縮合',
	'modify'								=>	'編輯類別',
	'url_name'								=>	'網址名稱',
	'url_name_help'							=>	'請填寫英文、數字、符號只能使用「 - 」中間線、「 _ 」底線，禁止使用保留字，如：category, product',
	'url_name_placeholder'					=>	'網址名稱',
);

$lang['admin_product'] = array(
	'search_placeholder'					=>	'搜尋字詞：ID、品名、簡述',
	'type'									=>	'商品類別',
	'type0'									=>	'一般商品',
	'type1'									=>	'贈品',
	'display_time'							=>	'上架起迄時間',
	'display_start_time'					=>	'開始時間',
	'display_end_time'						=>	'結束時間',
	'weight'								=>	'重量',
	'weight_unit'							=>	'公斤',
	'box_size'								=>	'材積',
	'box_size_unit'							=>	'公分',
	'tag'									=>	'標籤',
	'tag_help'								=>	'可以按「enter鍵」或「小寫逗點鍵」分隔標籤',
	'tag_placeholder'						=>	'標籤',
	'category'								=>	'類別',
	'price_setting'							=>	'價格設定',
	'list_price_help'						=>	'必填',
	'list_price_placeholder'				=>	'定價 必填',
	'price_help'							=>	'必填',
	'price_placeholder'						=>	'售價 必填',
	
	'preorder_setting'						=>	'預購設定',
	'preorder'								=>	'開啟預購',
	'preorder_start'						=>	'預購開始時間',
	'preorder_end'							=>	'預購結束時間',
	'preorder_price'						=>	'預購價格',
	'preorder_shipping'						=>	'出貨日期',
	'preorder_shipping_time'				=>	'出貨日期',
	
	'special_offer'							=>	'特價設定',
	'special'								=>	'開啟特價',
	'special_start'							=>	'特價開始時間',
	'special_end'							=>	'特價結束時間',
	'special_price'							=>	'特價金額',
	'special_max'							=>	'最多可選',
	'special_max_placeholder'				=>	'每次最多可選數量，0為不限',
	
	'image_setting'							=>	'商品圖片',
	'image'									=>	'選擇商品圖片',
	'image_help'							=>	'圖片可以拖曳排序',
);

$lang['admin_media'] = array(
	'insert'								=>	'新增檔案',
);

$lang['admin_member'] = array(
	'search_placeholder'					=>	'所有個人資料欄位',
	'gender'								=>	'性別',
	'gender0'								=>	'全部',
	'gender1'								=>	'男性',
	'gender2'								=>	'女性',
	'city'									=>	'城市',
	'city_null'								=>	'-- 無 --',
	'city0'									=>	'全部',
	'area'									=>	'地區',
	'area_null'								=>	'-- 無 --',
	'area0'									=>	'全部',
	'birthday'								=>	'生日',
	'birthday_placeholder'					=>	'生日，例：1990/01/31',
	'birthday_start'						=>	'開始時間',
	'birthday_end'							=>	'結束時間',
	'name'									=>	'姓名',
	'name_help'								=>	'必填',
	'name_placeholder'						=>	'姓名 必填',
	'identity'								=>	'使用身份',
	'insert_time'							=>	'註冊時間',
	'facebook_id'							=>	'Facebook ID',
	'verified'								=>	'驗證通過',
	'account_help'							=>	'必填',
	'account_placeholder'					=>	'帳號 必填',
	'password_help'							=>	'如不修改，請留空白',
	'password_placeholder'					=>	'密碼',
	'last_login_time'						=>	'最後登入時間',
	'personal_data'							=>	'個人資料',
);

$lang['admin_mail_history'] = array(
	'name'									=>	'主旨',
	'send_to'								=>	'收件人',
	'send_time'								=>	'寄送時間',
	'status'								=>	'狀態',
	'error'									=>	'寄送狀態訊息',
);

$lang['admin_theme'] = array(
	'upload'								=>	'上傳外觀',
);

$lang['admin_user_group'] = array(
	'name'									=>	'群組名稱',
	'authority'								=>	'權限',
	'group'									=>	'群組',
	'company'								=>	'公司',
	'company_placeholder'					=>	'公司',
	'phone'									=>	'電話',
	'phone_placeholder'						=>	'電話',
	'record'								=>	'登入紀錄',
	'record_time'							=>	'時間',
	'record_ip'								=>	'IP',
	'record_status'							=>	'狀態',
	'record_zero'							=>	'尚無登入紀錄',
);

$lang['admin_user'] = array(
	'name'									=>	'名稱',
	'authority'								=>	'權限',
	'group'									=>	'群組',
	'company'								=>	'公司',
	'company_placeholder'					=>	'公司',
	'phone'									=>	'電話',
	'phone_placeholder'						=>	'電話',
	'record'								=>	'登入紀錄',
	'record_time'							=>	'時間',
	'record_ip'								=>	'IP',
	'record_status'							=>	'狀態',
	'record_zero'							=>	'尚無登入紀錄',
);

$lang['admin_setting'] = array(
	'title'									=>	'網站名稱',
	'title_help'							=>	'必填',
	'title_placeholder'						=>	'網站名稱 必填',
	'description'							=>	'網站敘述',
	'description_help'						=>	'必填',
	'description_placeholder'				=>	'網站敘述 必填',
	'keyword'								=>	'網站關鍵字',
	'keyword_help'							=>	'必填',
	'keyword_placeholder'					=>	'網站關鍵字 必填',
	'facebook'								=>	'Facebook',
	'facebook_login'						=>	'Facebook 登入功能',
	'facebook_scope'						=>	'Facebook 權限',
	'facebook_scope_help'					=>	'權限請用 , 分隔',
	'facebook_scope_placeholder'			=>	'Facebook 權限',
	'facebook_appid'						=>	'Facebook App id',
	'facebook_appid_placeholder'			=>	'Facebook App id',
	'google'								=>	'Google',
	'ga_code'								=>	'Google 分析碼',
	'ga_code_placeholder'					=>	'Google 分析碼',
	'page_js'								=>	'其他Javascript',
	'page_js_placeholder'					=>	'其他Javascript',
	
	'mail_setting'							=>	'信件設定',
	'mail_function'							=>	'寄信功能',
	'mail_type'								=>	'類型',
	'mail_type0'							=>	'自行輸入',
	'mail_type1'							=>	'GMail',
	'mail_host'								=>	'主機網址',
	'mail_host_placeholder'					=>	'主機網址',
	'mail_port'								=>	'主機PORT',
	'mail_port_placeholder'					=>	'主機PORT',
	'mail_username'							=>	'信件主機登入帳號',
	'mail_username_placeholder'				=>	'信件主機登入帳號',
	'mail_password'							=>	'信件主機登入密碼',
	'mail_password_placeholder'				=>	'信件主機登入密碼',
	'mail_secure'							=>	'信件加密傳遞',
	'mail_charset'							=>	'信件編碼',
	'mail_charset_placeholder'				=>	'信件編碼',
	'mail_encoding'							=>	'信件加密',
	'mail_encoding_placeholder'				=>	'信件加密',
	'mail_from'								=>	'寄件者信箱',
	'mail_from_placeholder'					=>	'寄件者信箱',
	'mail_fromname'							=>	'寄件者姓名',
	'mail_fromname_placeholder'				=>	'寄件者姓名',
	'mail_test'								=>	'測試發信',
	
);

$lang['admin_tag'] = array(
	'name'									=>	'標籤',
);




