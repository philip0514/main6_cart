<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['global'] = array(
	//A
	'action'								=>	'功能',
	'all'									=>	'全部',
	'account'								=>	'帳號',
	'address'								=>	'地址',
	'authority_read'						=>	'讀取',
	'authority_insert'						=>	'新增',
	'authority_update'						=>	'修改',
	'authority_delete'						=>	'刪除',
	'account'								=>	'帳號',
	'account_help'							=>	'必填',
	'account_placeholder'					=>	'帳號 必填',
	
	//B
	'backend_title'							=>	'管理系統',
	
	//C
	'cancel'								=>	'取消',
	'column'								=>	'欄位顯示',
	'content'								=>	'內容',
	'cost_price'							=>	'成本',

	//D
	'display'								=>	'上下架',
	'display_on'							=>	'上架',
	'display_off'							=>	'下架',
	'delete'								=>	'刪除',
	'delete_all'							=>	'全選刪除',
	'datatable_info'						=>	' _START_ ~ _END_ / 共 _TOTAL_ 筆 ',
	'datatable_info_filtered'				=>	'(全部 _MAX_ 筆)',
	'datatable_first'						=>	'第一頁',
	'datatable_last'						=>	'最終頁',
	'datatable_zero'						=>	'無資料',
	'datatable_search_placeholder'			=>	'搜尋...',
	'description'							=>	'敘述',
	'description_help'						=>	'必填',
	'description_placeholder'				=>	'敘述 必填',
	
	//E
	'edit'									=>	'編輯',
	
	//F
	
	//G
	
	//H
	
	//I
	'id'									=>	'ID',
	'insert'								=>	'新增',
	'insert_time'							=>	'新增時間',
	'image'									=>	'圖片',
	
	//J
	
	//K

	//L
	'login'									=>	'登入',
	'login_error'							=>	'<b>登入失敗！</b>帳號或密碼錯誤，或是嘗試錯誤次數過多，請與後台管理員確認，謝謝。',
	'logout'								=>	'登出',
	'list_price'							=>	'定價',

	//M
	'modify_time'							=>	'更新時間',
	'meta'									=>	'社群分享',
	'mobile'								=>	'手機',

	//N
	'name'									=>	'名稱',
	'name_help'								=>	'必填',
	'name_placeholder'						=>	'名稱 必填',
	
	//O
	'ogimage'								=>	'選擇社群分享圖片',
	'ogimage_help'							=>	'圖片可以拖曳排序，尺寸：寬 * 高 => 「600px * 315px」 ～ 「1200px * 630px」',

	//P
	'price'									=>	'售價',
	'publish'								=>	'儲存並發佈',
	'publish_and_return'					=>	'儲存發佈回列表',
	'password'								=>	'密碼',
	'password_help'							=>	'如不修改，請留空白',
	'password_placeholder'					=>	'密碼',
	'password_confirm'						=>	'再次輸入密碼',
	
	//Q
	
	//R

	//S
	'sort'									=>	'排序',
	'search'								=>	'搜尋',
	'search_placeholder'					=>	'搜尋字詞：標題、內容',
	'search_btn'							=>	'搜尋',
	'search_reset_btn'						=>	'重設搜尋',
	'setting'								=>	'一般設定',
	'submit'								=>	'儲存',
	'submit_and_return'						=>	'儲存回列表',
	'saved'									=>	'已儲存',
	'send_time'								=>	'寄送時間',
	'status'								=>	'狀態',
	'status_success'						=>	'成功',
	'status_failed'							=>	'失敗',

	//T
	
	//U
	
	//V

	//W
	
	//X
	
	//Y
	
	//Z
);

$lang['error']	= array(
	'name'			=>	array(
		'required'		=>	'名稱 必填',
	),
	'description'	=>	array(
		'required'		=>	'敘述 必填',
	),
	'account'		=>	array(
		'required'		=>	'帳號 必填',
	),
	'password'		=>	array(
		'required'		=>	'密碼 必填',
	),
	'member'		=>	array(
		'name'			=>	array(
			'required'		=>	'姓名 必填',
		),
		'account'		=>	array(
			'required'		=>	'帳號 必填',
			'valid_email'	=>	'格式須為電子郵件，例：abc@gmail.com',
			'remote'		=>	'已被註冊',
		),
		'password'		=>	array(
			'min_length'	=>	'至少輸入6碼',
		),
		'mobile'		=>	array(
			'numeric'		=>	'請輸入數字',
			'is_mobile'		=>	'格式須為手機，例：0987654321',
			'remote'		=>	'此行動電話已被註冊使用',
		),
	),
	'category'		=>	array(
		'name'			=>	array(
			'required'		=>	'名稱 必填',
		),
		'url_name'	=>	array(
			'url_name'		=>	'網址名稱含有不可使用的字元',
		),
	),
	'product'		=>	array(
		'name'			=>	array(
			'required'		=>	'名稱 必填',
		),
		'description'	=>	array(
			'required'		=>	'敘述 必填',
		),
		'list_price'	=>	array(
			'required'		=>	'定價 必填',
		),
		'price'	=>	array(
			'required'		=>	'售價 必填',
		),
	),
	'news'		=>	array(
		'name'			=>	array(
			'required'		=>	'名稱 必填',
		),
		'description'	=>	array(
			'required'		=>	'敘述 必填',
		),
		'news_time'	=>	array(
			'required'		=>	'請選擇時間 YYYY/mm/dd',
		),
	),
	'user'		=>	array(
		'name'			=>	array(
			'required'		=>	'姓名 必填',
		),
		'account'		=>	array(
			'required'		=>	'帳號 必填',
		),
	),
	'user_info'		=>	array(
		'name'			=>	array(
			'required'		=>	'姓名 必填',
		),
	),
	'tag'		=>	array(
		'name'			=>	array(
			'required'		=>	'標籤 必填',
		),
	),
	
);


?>