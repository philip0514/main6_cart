<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends CI_Controller {

	function __construct(){
		parent::__construct();
    }
	
	public function index(){
		$this->preload->main();
		$Website = $this->Website;
		
		$nation = $_POST['nation'];
		$admin = $_POST['admin'];
		
		$language = "zh_tw";
		for($i=0; $i<sizeof($Website['language']); $i++){
			if($Website['language'][$i]['code']==$nation){
				$language = $Website['language'][$i]['language'];
			}
		}
		
		if($admin){
			$_SESSION['user_info']['site_lang'] = $language;
		}
	}
}
?>