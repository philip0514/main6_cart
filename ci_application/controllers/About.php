<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->preload->main();
    }
	
	public function index($page){
		$Website = $this->Website;
		$this->preload->current_location();
		
		$title = '關於我們';
		
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		//分頁
		$sql = sprintf("SELECT count(A.id) total
						FROM %s A
						WHERE A.display=1
						",
						$Website['table']['about']
						);//echo $sql;exit;
		$rows1 = $this->query->select($sql, 1);
		$total_rows = $rows1['total'];
		
		$Website['pagination']['base_url'] = base_url().'about/';
		$Website['pagination']['total_rows'] = $total_rows;
		$Website['pagination']['per_page'] = 9;
		$Website['pagination']['num_links'] = 3;
		$Website['pagination']['uri_segment'] = 2;
		$this->pagination->initialize($Website['pagination']);//echo '<pre>';print_r($Website['pagination']);
		$pagination = $this->pagination->create_links();//echo $pagination;exit;
		$page_total = ceil($total_rows/$Website['pagination']['per_page']);
		$page = (int)$page ? (int)$page : 1;//echo $page;exit;
		$skip = ($page-1) * $Website['pagination']['per_page'];
		$limit = $Website['pagination']['per_page'];
		
		$sql = sprintf("SELECT *
						FROM %s A
						WHERE A.display=1
						ORDER BY A.sort asc
						LIMIT %s, %s
						",
						$Website['table']['about'],
					   	sql_string($skip, 'int'),
					   	sql_string($limit, 'int')
						);//echo $sql;exit;
		$rows1 = $this->query->select($sql);
		
		$data = array(
			'Website'		=>	$Website,
			'page_title'	=>	$title,
			'pagination'	=>	$pagination,
			'rows1'			=>	$rows1,
		);
		$this->theme->views('about/index', $data);
	}
	
	public function content($id){
		$Website = $this->Website;
		$this->preload->current_location();
		
		if(!$id){
			show_404();
		}
		
		$sql = sprintf("SELECT * 
						FROM %s 
						WHERE id=%s and display=1", 
					   	$Website['table']['about'],
					  	sql_string($id, 'int')
					  );//echo $sql;exit;
		$rows1 = $this->query->select($sql, 1);
		
		if(!$rows1['id']){
			show_404();
		}
		
		$data = array(
			'title'			=>	$rows1['name'],
			'description'	=>	$rows1['description'],
			'ogimage'		=>	array(
				'id'			=>	$rows1['id'],
				'type'			=>	'about_ogimage'
			),
		);
		$this->theme->config($data);
		
		
		$data = array(
			'Website'	=>	$Website,
			'rows1'		=>	$rows1,
		);
		$this->theme->views('about/content', $data);
	}
	
	
}
?>