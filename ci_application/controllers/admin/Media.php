<?
class Media extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		$this->settings = array(
								'btn'				=>	array(			//列表下方按鈕
									'insert'			=>	0,
									'delete'			=>	1,
									'display'			=>	0,
									'sort'				=>	0,
								),
								'btn_txt'			=>	array(			//自行定義按鈕名稱
									'insert'			=>	'',
									'display'			=>	'',
									'sort'				=>	'',
								),
								'icon'				=>	array(			//自行增加icon數量，但須於datatable中的actions中新增項目
									'edit'				=>	0,
									'delete'			=>	1,
									'display'			=>	0,
									'sort'				=>	0,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									'ajax'				=>	base_url().'admin/'.$this->current_class.'/ajax/',
									'item'				=>	base_url().'admin/'.$this->current_class.'/item/',
								),
								'orderby'			=>	array(array(1, "desc")),
								'order_columns'		=>	array('', 'id', '', 'name', 'content'),
								'search_columns'	=>	array('title', 'content'),
								'table_columns'		=>	array(my_lang('action'), my_lang('id'), '', my_lang('name'), my_lang('content')),
								'column_visible'	=>	array(1, 1, 1, 1, 1),
								'column'			=>	array(
									array('width'		=>'130px', 	'targets'	=>	0),
									array('width'		=>'60px', 	'targets'	=>	1),
									array('orderable'	=>false, 	'targets'	=>	array(0, 2)),
								),
								
								'html'				=>	array(
									'list'				=>	$this->current_class.'/list',
									'item'				=>	$this->current_class.'/item',
									'adv_search'		=>	'admin/adv_search/'.$this->current_class,
								),
								'model'				=>	'global_query',
								
								//datatable
								'adv_search_status'	=>	0,
								'adv_search'		=>	array(
									'vars'			=>	array(
										'display',
										'search'
									),
								),
							);
		$this->Website['tables'] = array(
			$this->Website['table']['media'],
		);
		
		//確定編輯權限
		$this->settings = $this->menu->check_editable($this->settings);
		
		//進階搜尋設定
		$this->settings['adv_search']['html'] = $this->adv_search($this->settings);
		
		//下方btn設定
		$this->settings['button']  = $this->datatable->button($this->settings);
	}
	
	public function index(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_list');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			$id = $_POST['id'];
			$show = $_POST['display'];
			$delete = $_POST['delete'];
			
			switch($here){
				case 1:
					/*
						上下架顯示
					*/
					$this->$model_query->display_update($id, $show);
					
					/*
						刪除
					*/
					$this->$model_query->delete_update($id, $delete);
					break;
				case 2:
					/*
						排序
					*/
					$this->$model_query->sort_update();
					break;
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		//echo '<pre>';print_r($this->settings);exit;
		$data = array(
			'Website'	=>	$Website,
			'settings'	=>	$this->settings,
			'rows1'		=>	$rows1,
		);
		
		$this->template->views_admin($this->settings['html']['list'], $data);
	}
	
	public function ajax(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		$q = $this->datatable->query(array(
											$this->settings['order_columns'], 
											$this->settings['search_columns'], 
											$this->settings
										), 
										$_GET);
		
		$q = $this->adv_search_sql($q, $_GET);
		$iTotalRecords = $this->$model_query->table_total();
		$iTotalDisplayRecords = $this->$model_query->display_total($q);
		$rows1 = $this->$model_query->datatable_select($q);
		
		$rows2 = array();
		for($i=0; $i<sizeof($rows1); $i++){
			
			$icon_setting = $this->settings['icon'];
			
			if(in_array($rows1[$i]['id'], array())){
				//設定部份icon項目不顯示
				//$icon_setting['delete'] = 0;
			}
			if($q['sWhere']){
				//當有搜尋項目時，不予排序
				$icon_setting['sort'] = 0;
			}
			
			$action_array = array(
								'id'				=>	$rows1[$i]['id'],
								'checked'			=>	$Website['checked'][(int)$rows1[$i]['display']],
								'display_class'		=>	$Website['display_class'][(int)$rows1[$i]['display']],
								'edit'				=>	'admin/'.$this->current_class.'/item/'.$rows1[$i]['id'].'/',
								'icon'				=>	$icon_setting,
							);
			$rows2[] = array(
				$this->datatable->actions($action_array),
				$rows1[$i]['id'],
				'<img src="'.$Website['media']['upload_folder'].$Website['media']['thumb_folder'].$rows1[$i]['name'].'" width="50" class="img-rounded" />',
				$rows1[$i]['title'],
				$rows1[$i]['content']
			);
		}
		
		$output = array(
			"aaData"					=> $rows2,
			"iTotalRecords" 			=> $iTotalRecords,
			"iTotalDisplayRecords"		=> $iTotalDisplayRecords,
		);
		
		echo json_encode($output);
		exit;
	}
	
	public function item($id){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_item');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			//Save
			$here = $_POST['here'];
			$id = $_POST['id'];
			$name = $_POST['name'];
			$content = $_POST['content'];
			$display = $_POST['display'] ? 1 : 0;
			
			if(!$id){
				$rows1 = array(
					'name'		=>	ci_sql_string($name, 'text'),
					'content'	=>	ci_sql_string($content, 'text'),
					'display'	=>	ci_sql_string($display, 'int'),
					'sort'		=>	0,
				);
				$id = $this->$model_query->insert($this->settings, $rows1);
			}else{
				$rows1 = array(
					'name'		=>	ci_sql_string($name, 'text'),
					'content'	=>	ci_sql_string($content, 'text'),
					'display'	=>	ci_sql_string($display, 'int'),
				);
				$this->$model_query->update($rows1, $id);
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		
		$data = array(
			'id'			=>	$id,
		);
		$rows1 = $this->$model_query->select($data);
		if($id){
			$Website['page_header'] = $rows1['name'];
		}
		
		$data = array(
			'Website'	=>	$Website,
			'settings'	=>	$this->settings,
			'rows1'		=>	$rows1,
		);
		$this->template->views_admin($this->settings['html']['item'], $data);
	}
	
	private function adv_search($setting){
		$Website = $this->Website;
		
		if(!$this->settings['adv_search_status']){
			return 0;
		}
		
		$data = array(
			'Website'	=>	$Website,
			'rows1'		=>	$_SESSION['adv_search'][$this->current_class],
		);
		return $this->load->view($this->settings['html']['adv_search'], $data, true);
	}
	
	private function adv_search_sql($q, $g){
		//進階搜尋
		
		$_SESSION['adv_search'][$this->current_class] = $g;
		
		if($g['search']){
			$s[] = sprintf(' (A.name like %s or A.content like %s)',
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text')
			);
		}
		switch($g['display']){
			case 1:
				$s[] = sprintf(' A.display=0 ');
				break;
			case 2:
				$s[] = sprintf(' A.display=1 ');
				break;
			case 3:
				break;
		}
		if(sizeof($s)){
			if($q['sWhere']){
				$q['sWhere'] .= sprintf(' and %s', implode(' and ', $s));
			}else{
				$q['sWhere'] = sprintf(' WHERE %s', implode(' and ', $s));
			}
		}
		return $q;
	}
	
}
?>