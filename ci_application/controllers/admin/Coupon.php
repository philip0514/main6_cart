<?
class Coupon extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		$this->settings = array(
								'btn'				=>	array(			//列表下方按鈕
									'insert'			=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'btn_txt'			=>	array(			//自行定義按鈕名稱
									'insert'			=>	'',
									'display'			=>	'',
									'sort'				=>	'',
								),
								'icon'				=>	array(			//自行增加icon數量，但須於datatable中的actions中新增項目
									'edit'				=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									'ajax'				=>	base_url().'admin/'.$this->current_class.'/ajax/',
									'item'				=>	base_url().'admin/'.$this->current_class.'/item/',
								),
								'orderby'			=>	array(array(1, "asc")),
								'order_columns'		=>	array('', 'id', 'name', 'discount', 'discount_gate', 'start_time', 'end_time', 'used_count'),
								'search_columns'	=>	array('name'),
								'table_columns'		=>	array(my_lang('action'), my_lang('id'), my_lang('name'), '折扣金額', '折扣門檻', '開始時間', '結束時間', '已使用次數'),
								'column_visible'	=>	array(1, 1, 1, 1, 1, 1, 1, 1),
								'column'			=>	array(
									array('width'		=>	'130px', 	'targets'	=>	0),
									array('width'		=>	'60px', 	'targets'	=>	1),
									array('orderable'	=>	false, 		'targets'	=>	array(0)),
								),
								
								'html'				=>	array(
									'list'				=>	'template/list',
									'item'				=>	$this->current_class.'/item',
									'insert'			=>	$this->current_class.'/insert',
									'adv_search'		=>	'admin/adv_search/'.$this->current_class,
								),
								'model'				=>	'coupon_query',
								
								//datatable
								'adv_search_status'	=>	0,
								'adv_search'		=>	array(
									'vars'			=>	array(
										'display',
										'search'
									),
								),
								
								//image relation
								'item_type'			=>	array(
									'about_ogimage'
								),
								
								//validation rule group
								'validation_rule_group'	=>	array(
									'insert'	=>	'admin/'.$this->current_class.'/insert',
									'update'	=>	'admin/'.$this->current_class.'/item',
								),
							);
		$this->Website['tables'] = array(
			$this->Website['table']['coupon'],
		);
		
		//確定編輯權限
		$this->settings = $this->menu->check_editable($this->settings);
		
		//進階搜尋設定
		$this->settings['adv_search']['html'] = $this->adv_search();
		
		//下方btn設定
		$this->settings['button']  = $this->datatable->button($this->settings);
	}
	
	public function index(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_list');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			$id = $_POST['id'];
			$show = $_POST['display'];
			$delete = $_POST['delete'];
			
			switch($here){
				case 1:
					/*
						display or not
					*/
					$this->$model_query->display_update($id, $show);
					
					/*
						delete
					*/
					$this->$model_query->delete_update($id, $delete);
					break;
				case 2:
					/*
						sort
					*/
					$this->$model_query->sort_update();
					break;
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$data = array(
			'Website'	=>	$Website,
			'settings'	=>	$this->settings,
			'rows1'		=>	$rows1,
		);
		$this->template->views_admin($this->settings['html']['list'], $data);
	}
	
	public function ajax(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		$q = $this->datatable->query(array(
											$this->settings['order_columns'], 
											$this->settings['search_columns'], 
											$this->settings
										), 
										$_GET);
		
		$q = $this->adv_search_sql($q, $_GET);
		$iTotalRecords = $this->$model_query->table_total();
		$iTotalDisplayRecords = $this->$model_query->display_total($q);
		$rows1 = $this->$model_query->datatable_select($q);
		
		$rows2 = array();
		for($i=0; $i<sizeof($rows1); $i++){
			
			$icon_setting = $this->settings['icon'];
			
			if(in_array($rows1[$i]['id'], array())){
				//設定部份icon項目不顯示
				//$icon_setting['delete'] = 0;
			}
			if($q['sWhere']){
				//當有搜尋項目時，不予排序
				$icon_setting['sort'] = 0;
			}
			
			$action_array = array(
								'id'				=>	$rows1[$i]['id'],
								'checked'			=>	$Website['checked'][(int)$rows1[$i]['display']],
								'display_class'		=>	$Website['display_class'][(int)$rows1[$i]['display']],
								'edit'				=>	'admin/'.$this->current_class.'/item/'.$rows1[$i]['id'].'/',
								'icon'				=>	$icon_setting,
							);
			$rows2[] = array(
				$this->datatable->actions($action_array),
				$rows1[$i]['id'],
				$rows1[$i]['name'],
				$rows1[$i]['discount'].($rows1[$i]['discount_type']?' ％':' 元'),
				$rows1[$i]['discount_gate'].' 元',
				$rows1[$i]['start_time']?date('Y/m/d H:i', $rows1[$i]['start_time']):'無期限',
				$rows1[$i]['end_time']?date('Y/m/d H:i', $rows1[$i]['end_time']):'無期限',
				(int)$rows1[$i]['used_count'],
			);
		}
		
		$output = array(
			"aaData"					=> $rows2,
			"iTotalRecords" 			=> $iTotalRecords,
			"iTotalDisplayRecords"		=> $iTotalDisplayRecords,
		);
		
		echo json_encode($output);
		exit;
	}
	
	public function item($id){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_item');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){//exit;
			//Save
			$id = $_POST['id'];
			$name = $_POST['name'];
			
			if(!$id){
				//echo $_POST['discount'];exit;
				if($this->form_validation->run($this->settings['validation_rule_group']['insert']) == FALSE){
					$data = array(
						'Website'			=>	$Website,
						'error_message' 	=>	validation_errors('<li>', '</li>'),
					);
					$this->template->views_admin('template/error', $data, true);
					exit;
				}
				
				$type = $_POST['type'];
				$prefix = $_POST['prefix'];
				$length = $_POST['length'];
				$amount = $_POST['amount'];
				$number_start = $_POST['number_start'];
				$serial = $_POST['serial'];
				$limit_count = $_POST['limit_count'];
				$discount = $_POST['discount'];
				$discount_type = $_POST['discount_type'];
				$discount_gate = $_POST['discount_gate'];
				$start_time = $_POST['start_date'] ? strtotime($_POST['start_date']) : '';
				$end_time = $_POST['end_date'] ? strtotime($_POST['end_date']) : '';
				$note = $_POST['note'];
				
				
				if($type==2){
					$amount = 1;
				}
				
				$j=0;
				for($i=0; $i<$amount; $i++){
					
					switch($type){
						case 1:
							//固定順序
							$number = $number_start+$i;
							$number_length = strlen($number);
							$name_length = $length - $number_length;
							$name = str_pad($prefix, $name_length, 0, STR_PAD_RIGHT).$number;
							$limit_count = 1;			//限定使用一次
							break;
						case 2:
							//單一
							$name = $serial;
							break;
						default:
						case 0:
							//亂數
							$name = $prefix.$this->coupon_query->code_create($length-strlen($prefix));
							$limit_count = 1;			//限定使用一次
							break;
					}
					
					$rows1 = array(
						'name'				=>	ci_sql_string($name, 'text'),
						'type'				=>	ci_sql_string($type, 'int'),
						'discount'			=>	ci_sql_string($discount, 'int'),
						'discount_type'		=>	ci_sql_string($discount_type, 'int'),
						'discount_gate'		=>	ci_sql_string($discount_gate, 'int'),
						'limit_count'		=>	ci_sql_string($limit_count, 'int'),
						'start_time'		=>	ci_sql_string($start_time, 'int'),
						'end_time'			=>	ci_sql_string($end_time, 'int'),
						'note'				=>	ci_sql_string($note, 'text'),
						'insert_time'		=>	ci_sql_string(time(), 'int'),
						'modify_time'		=>	ci_sql_string(time(), 'int'),
						'display'			=>	ci_sql_string(1, 'int'),
					);
					$coupon_id = $this->$model_query->insert($rows1);
					
					if(!$coupon_id){
						$i--;
						$j++;
					}
					
					if($j>$amount){
						//相同的序號一直跑 所以直接取消產生
						exit;
					}
				}
			}else{
				if($this->form_validation->run($this->settings['validation_rule_group']['update']) == FALSE){
					$data = array(
						'Website'			=>	$Website,
						'error_message' 	=>	validation_errors('<li>', '</li>'),
					);
					$this->template->views_admin('template/error', $data, true);
					exit;
				}
				
				
				$name = $_POST['name'];
				$discount = $_POST['discount'];
				$discount_type = $_POST['discount_type'];
				$discount_gate = $_POST['discount_gate'];
				$start_time = $_POST['start_date'] ? strtotime($_POST['start_date']) : '';
				$end_time = $_POST['end_date'] ? strtotime($_POST['end_date']) : '';
				$note = $_POST['note'];
				$display = $_POST['display'] ? 1 : 0;
				
				
				$rows1 = array(
					'name'				=>	ci_sql_string($name, 'text'),
					'discount'			=>	ci_sql_string($discount, 'int'),
					'discount_type'		=>	ci_sql_string($discount_type, 'int'),
					'discount_gate'		=>	ci_sql_string($discount_gate, 'int'),
					'start_time'		=>	ci_sql_string($start_time, 'int'),
					'end_time'			=>	ci_sql_string($end_time, 'int'),
					'note'				=>	ci_sql_string($note, 'text'),
					'display'			=>	ci_sql_string($display, 'int'),
					'modify_time'		=>	ci_sql_string(time(), 'int'),
				);
				$this->$model_query->update($rows1, $id);
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		
		$data = array(
			'id'			=>	$id,
		);
		$rows1 = $this->$model_query->select($data);
		
		if($rows1['id']){
			$Website['page_header'] = $rows1['name'];
			
			$validation_rule = $this->config->item($this->settings['validation_rule_group']['update']);
			$this->jquery_validation->set_rules($validation_rule);
			$jquery_validation = $this->jquery_validation->run('#form1');
			
			$sql = sprintf("SELECT A.*, B.name member_name, C.name used_member_name
							FROM %s A
							LEFT JOIN %s B ON A.member_id=B.id
							LEFT JOIN %s C ON A.used_by=C.id
							WHERE A.coupon_id=%s
							",
							$Website['table']['member_coupon_relation'],
							$Website['table']['member'],
							$Website['table']['member'],
							sql_string($id, 'text')
							);//echo $sql;exit;
			$rows2 = $this->query->select($sql);
			
		}else{
			$validation_rule = $this->config->item($this->settings['validation_rule_group']['insert']);
			$this->jquery_validation->set_rules($validation_rule);
			$jquery_validation = $this->jquery_validation->run('#form1');
			
			$this->settings['html']['item'] = $this->settings['html']['insert'];
		}
		
		$data = array(
			'Website'		=>	$Website,
			'settings'		=>	$this->settings,
			'validation'	=>	$jquery_validation,
			'rows1'			=>	$rows1,
			'rows2'			=>	$rows2,
		);
		$this->template->views_admin($this->settings['html']['item'], $data);
	}
	
	public function column_visible(){
		$Website = $this->Website;
		
		$column = $_GET['column'];
		$column_visible = $this->settings['column_visible'];
		for($i=1; $i<sizeof($column_visible); $i++){
			if(in_array($i, $column)){
				$column_visible[$i] = 1;
			}else{
				$column_visible[$i] = 0;
			}
		}
		$_SESSION['column_visible'][$this->current_class] = $this->settings['column_visible'] = $column_visible;
	}
	
	private function adv_search(){
		$Website = $this->Website;
		
		if(!$this->settings['adv_search_status']){
			return 0;
		}
		
		if($_SESSION['column_visible'][$this->current_class]){
			$this->settings['column_visible'] = $_SESSION['column_visible'][$this->current_class];
		}
		
		$data = array(
			'Website'	=>	$Website,
			'setting'	=>	$this->settings,
			'rows1'		=>	$_SESSION['adv_search'][$this->current_class],
		);
		return $this->load->view($this->settings['html']['adv_search'], $data, true);
	}
	
	private function adv_search_sql($q, $g){
		//進階搜尋
		
		$_SESSION['adv_search'][$this->current_class] = $g;
		
		if($g['search']){
			$s[] = sprintf(' (A.name like %s or A.content like %s)',
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text')
			);
		}
		switch($g['display']){
			case 1:
				$s[] = sprintf(' A.display=0 ');
				break;
			case 2:
				$s[] = sprintf(' A.display=1 ');
				break;
			case 3:
				break;
		}
		if(sizeof($s)){
			if($q['sWhere']){
				$q['sWhere'] .= sprintf(' and %s', implode(' and ', $s));
			}else{
				$q['sWhere'] = sprintf(' WHERE %s', implode(' and ', $s));
			}
		}
		return $q;
	}
	
	public function ajax_insert_relationship(){
		$Website = $this->Website;
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$email = explode(';', $_POST['email']);
			$coupon_id = (int)$_POST['coupon_id'];
			
			for($i=0; $i<sizeof($email); $i++){
				$single_mail = ci_sql_string($email[$i], 'text');
				if(!filter_var($single_mail, FILTER_VALIDATE_EMAIL)){
					continue;
				}
				$email_array[] = "'".$single_mail."'";
			}
			
			$sql = sprintf("SELECT A.id, A.name, A.account
							FROM %s A
							WHERE A.account in (%s)
							",
							$Website['table']['member'],
							implode(",", $email_array)
							);//echo $sql;
			$rows1 = $this->query->select($sql);

			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.id=%s
							",
							$Website['table']['coupon'],
							sql_string($coupon_id, 'int')
							);//echo $sql;exit;
			$rows2 = $this->query->select($sql, 1);

			$coupon_start_end_time = date('Y/m/d H:i:s', $rows2['start_time']).' ~ '.date('Y/m/d H:i:s', $rows2['end_time']);
			if(!$rows2['discount_type']){
				$price = $rows2['price'].' 元';
			}else{
				$price = $rows2['price'].' ％';
			}

			if($rows2['id']){
				for($i=0; $i<sizeof($rows1); $i++){

					if(!$rows1[$i]['id']){
						continue;
					}

					$rows_data = array(
						'member_id'		=>	$rows1[$i]['id'],
						'coupon_id'		=>	$coupon_id,
						'sent_time'		=>	time(),
					);
					$this->db->set($rows_data);
					$this->db->insert($Website['table']['member_coupon_relation']);

					$content .= '<tr>'.
									'<td><a href="admin/member/item/'.$rows1[$i]['id'].'">'.$rows1[$i]['name'].'</a></td>'.
									'<td>'.date('Y/m/d H:i:s', time()).'</td>'.
									'<td>--</td>'.
									'<td>尚未使用</td>'.
									'<td>--</td>'.
								'</tr>';

					/*
					$data = array(
						'type'				=>	'coupon_group',
						'mailto'			=>	$rows1[$i]['account'],
						'member_name'		=>	$rows1[$i]['name'],
						'member_id'			=>	$rows1[$i]['id'],
						'rows1'				=>	array(
							'member_name'			=>	$rows1[$i]['name'],
							'member_account'		=>	$rows1[$i]['id'],
							'coupon_number'			=>	$rows2['name'],
							'coupon_price'			=>	$price,
							'coupon_start_end_time'	=>	$coupon_start_end_time,
						),
					);
					$this->mailer->to($data);
					*/
				}
			}

			$data = array(
				'limit_count'	=>	$rows2['limit_count'],
				'content'		=>	$content,
			);
			echo json_encode($data);
		}
	}
	
	public function ajax_serial_type1_check(){
		$Website = $this->Website;
		
		$code = $_POST['code'];
		$serial = "'".implode("','", $code)."'";
		
		//print_r($code);
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.name in (%s)
						",
						$Website['table']['coupon'],
						$serial
						);//echo $sql;exit;
		$rows1 = $this->query->select($sql);
		
		if(!sizeof($rows1)){
			$result = array(
				'status'		=>	1,
			);
		}else{
			$code = array();
			for($i=0; $i<sizeof($rows1); $i++){
				$conflict[] = $rows1[$i]['name'];
			}
			
			
			$result = array(
				'status'		=>	0,
				'size'			=>	sizeof($rows1),
				'conflict'		=>	$conflict
			);
		}
		
		echo json_encode($result);
		
	}
	
	public function ajax_coupon_check(){
		$Website = $this->Website;
		
		$id = $_POST['id'];
		$name = $_POST['name'];
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.name=%s
						",
						$Website['table']['coupon'],
						sql_string($name, 'text')
						);//echo $sql;exit;
		$rows1 = $this->query->select($sql, 1);
		
		if(!$id){

			if(!sizeof($rows1)){
				//不存在
				echo 'true';
			}else{
				//存在
				echo 'false';
			}
		}else{
			
			if(!sizeof($rows1)){
				echo 'true';
			}else{
				if($rows1['id']==$id){
					echo 'true';
				}else{
					echo 'false';
				}
			}
		}
	}
	
	private function test(){
		/*
		Serial			Type		limit_count		member		used_by		status		result
		b000000001		2			0				0			0			usable		yes
		b000000001		2			0				1			0			usable		yes
		b000000001		2			0				1			1			disable		yes
		b000000001		2			0				2			0			usable		yes
		b000000001		2			0				2			1			usable		yes
		
		b000000002		2			1				0			0			usable		yes
		b000000002		2			1				1			0			usable		yes
		b000000002		2			1				1			1			disable		yes
		b000000002		2			1				2			1			disable		yes
		
		abc_d1a55D		0			1				0			0			usable		yes
		abc_d1a55D		0			1				1			0			usable		yes
		abc_d1a55D		0			1				1			1			disable		yes
		abc_d1a55D		0			1				2			1			disable		yes
		abc_d1a55D		0			1				2			0			disable		yes
		
		*/
		
		
		
		$coupon = 'b000000001';
		$member_id = 2;
		
		$this->coupon_query->usable($coupon, $member_id);
	}
	
}
?>