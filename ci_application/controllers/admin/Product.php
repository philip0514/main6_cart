<?
class Product extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		$this->settings = array(
								'function'			=>	$this->current_class,
								'btn'				=>	array(
									'insert'			=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	1,
								),
								'icon'				=>	array(
									'edit'				=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	1,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									'ajax'				=>	base_url().'admin/'.$this->current_class.'/ajax/',
									'item'				=>	base_url().'admin/'.$this->current_class.'/item/',
									'inventory'			=>	base_url().'admin/'.$this->current_class.'/ajax_inventory/',
								),
								'orderby'			=>	array(array(1, "desc")),
								'order_columns'		=>	array('', 'B.sort', 'A.id', '', 'A.name', 'A.price', 'A.list_price', 'A.cost_price'),
								'search_columns'	=>	array('A.name'),
								'table_columns'		=>	array(my_lang('action'), my_lang('sort'), my_lang('id'), my_lang('image'), my_lang('name'), my_lang('list_price'), my_lang('price'), my_lang('cost_price')),
								'column_visible'	=>	array(1, 1, 1, 1, 1, 1, 1, 1),
								'column'			=>	array(
									array('width'		=>'130px', 	'targets'	=>	0),
									array('width'		=>'60px', 	'targets'	=>	1),
									array('width'		=>'60px', 	'targets'	=>	2),
									array('orderable'	=>false, 	'targets'	=>	array(0, 3)),
								),
								
								'html'				=>	array(
									'list'				=>	$this->current_class.'/list',
									'item'				=>	$this->current_class.'/item',
									'inventory'			=>	'admin/'.$this->current_class.'/inventory',
									'inventory_item'	=>	'admin/'.$this->current_class.'/inventory_item',
									'adv_search'		=>	'admin/adv_search/'.$this->current_class,
								),
								'model'				=>	'product_query',
								
								//datatable
								//'sdom'				=>	"<'row'<'col-md-6'<'adv_search'>><'col-md-6'l<'clear_both'p>>r>t<'row'<'col-md-4'i><'col-md-8'p>>",
								'adv_search_status'	=>	1,
								'adv_search'		=>	array(
									'vars'				=>	array(
										'display',
										'search'
									),
								),
								'item_type'			=>	array(
									'product',
									'product_ogimage'
								),
								
								//validation rule group
								'validation_rule_group'	=>	'admin/'.$this->current_class.'/item',
							);
							
		$this->Website['tables'] = array(
			$this->Website['table']['product'],
			$this->Website['table']['category']
		);
		
		//確定編輯權限
		$this->settings = $this->menu->check_editable($this->settings);
		
		//進階搜尋設定
		$this->settings['adv_search']['html'] = $this->adv_search();
		
		//下方btn設定
		$this->settings['button'] = $this->datatable->button($this->settings);
		
	}
	
	public function index($id){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//進階搜尋設定
		$this->settings['adv_search']['html'] = $this->adv_search($this->settings, $id);
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_list');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			$pid = $_POST['id'];
			$show = $_POST['display'];
			$delete = $_POST['delete'];
			
			switch($here){
				case 1:
					/*
						上下架顯示
					*/
					$this->$model_query->display_update($pid, $show);
					
					/*
						刪除
					*/
					$this->$model_query->delete_update($pid, $delete);
					break;
				case 2:
					/*
						排序
					*/
					$this->$model_query->sort_update($id);
					break;
			}
			
			$list = $this->settings['path']['list'];
			if($id){
				$list = $this->settings['path']['list'].'index/'.$id.'/';
			}
			
			redirect($list, 'refresh');
			exit;
		}
		
		
		if($id){
			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.id=%s
							LIMIT 1",
							$Website['tables'][1],
							sql_string($id, 'int')
							);//echo $sql;
			$rows1 = $this->query->select($sql, 1);
			
			$this->settings['add_vars'][] = array(
				'name'	=>	'type[]',
				'value'	=>	$id
			);
			
			for($i=1; $i<sizeof($nested_child); $i++){
				$this->settings['add_vars'][] = array(
					'name'	=>	'type[]',
					'value'	=>	$nested_child[$i]
				);
			}
			
			if(sizeof($nested_child)>1){
				$this->settings['btn']['sort'] = 0;
				$this->settings['icon']['sort'] = 0;
			}
			$Website['page_header'] = $rows1['name'];
		}else{
			$this->settings['btn']['sort'] = 0;
			$this->settings['icon']['sort'] = 0;
		}
		
		//下方btn設定
		$this->settings['button'] = $this->datatable->button($this->settings);
		
		$data = array(
			'Website'	=>	$Website,
			'settings'	=>	$this->settings,
			'rows1'		=>	$rows1,
			'nav'		=>	$this->menu->backend_category_nav(),
		);
		$this->template->views_admin($this->settings['html']['list'], $data);
	}
	
	public function ajax(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		if(sizeof($_GET['type'])!=1){
			$this->settings['icon']['sort'] = 0;
		}
		
		$q = $this->datatable->query(array(
											$this->settings['order_columns'], 
											$this->settings['search_columns'], 
											$this->settings
										), 
										$_GET);
		$q = $this->adv_search_sql($q, $_GET);
		$iTotalRecords = $this->$model_query->table_total();
		$iTotalDisplayRecords = $this->$model_query->display_total($q, $_GET);
		$rows1 = $this->$model_query->datatable_select($q, $_GET);//echo 5;exit;
		
		if($_GET['console']){
			echo '<pre>';
			print_r($_GET);
			print_r($q);
			exit;
		}
		
		$rows2 = array();
		for($i=0; $i<sizeof($rows1); $i++){
			
			$action_array = array(
				'id'				=>	$rows1[$i]['id'],
				'checked'			=>	$Website['checked'][(int)$rows1[$i]['display']],
				'display_class'		=>	$Website['display_class'][(int)$rows1[$i]['display']],
				'edit'				=>	'admin/'.$this->current_class.'/item/'.$rows1[$i]['id'].'/',
				'icon'				=>	$this->settings['icon'],
			);
		
			$image = '';
			if($rows1[$i]['main_media_id']){
				$image = 
					'<a href="product/content/'.$rows1[$i]['id'].'/" target="_blank">'.
					'<img src="'.
					$rows1[$i]['file_path'].
					$Website['media']['thumb_folder'].
					$rows1[$i]['file_name'].
					'" width="50" class="img-rounded" />'.
					'</a>'
					;
			}
			
			
			$rows2[] = array(
				$this->datatable->actions($action_array),
				$rows1[$i]['sort'],
				$rows1[$i]['id'],
				$image,
				'<a href="product/content/'.$rows1[$i]['id'].'/" target="_blank">'.$rows1[$i]['name'].'</a>',
				$rows1[$i]['list_price'],
				$rows1[$i]['price'],
				$rows1[$i]['cost_price'],
			);
		}
		
		$output = array(
			"aaData"					=> $rows2,
			"iTotalRecords" 			=> $iTotalRecords,
			"iTotalDisplayRecords"		=> $iTotalDisplayRecords,
		);
		
		echo json_encode($output);
		exit;
	}
	
	public function item($id){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_item');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			//Save
			$here = $_POST['here'];
			$id = (int)$_POST['id'];
			
			if($this->form_validation->run($this->settings['validation_rule_group']) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				$this->template->views_admin('template/error', $data, true);
				exit;
			}
			
			$inserted = 0;
			if(!$id){
				$inserted = 1;
			}
			
			$id = $this->product_query->item_post();
			
			//存檔 發佈 預覽
			if($here==1 || $here==2 || $here==4){
				echo json_encode(array(
					'id'			=>	$id,
					'inserted'		=>	$inserted,
					'file_id'		=>	$file_id,
				));
			}
			
			//存檔發佈回列表頁
			if($here==3){
				if(!$_SESSION['product_reffer']){
					$this->settings['path']['list'] .= 'index/';
				}else{
					$this->settings['path']['list'] = $_SESSION['product_reffer'];
				}
				
				redirect($this->settings['path']['list'], 'refresh');
			}
			exit;
		}
		
		$validation_rule = $this->config->item($this->settings['validation_rule_group']);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1', array('submit_handler'	=>	'handler_admin_product',));
		
		//列表頁來源
		$_SESSION['product_reffer'] = $_SERVER['HTTP_REFERER'];
		
		$data1 = $this->product_query->item_select($id);
		
		if($id){
			$Website['page_header'] = $data1['rows1']['name'];
		}
		
		$data2 = array(
			'Website'			=>	$Website,
			'validation'		=>	$jquery_validation,
			'settings'			=>	$this->settings,
			'nav'				=>	$this->menu->backend_category_nav(),
		);
		
		$data = array_merge($data1, $data2);
		
		$this->template->views_admin($this->settings['html']['item'], $data);
	}
	
	public function column_visible(){
		$Website = $this->Website;
		
		$column = $_GET['column'];
		$column_visible = $this->settings['column_visible'];
		for($i=1; $i<sizeof($column_visible); $i++){
			if(in_array($i, $column)){
				$column_visible[$i] = 1;
			}else{
				$column_visible[$i] = 0;
			}
		}
		$_SESSION['column_visible'][$this->current_class] = $this->settings['column_visible'] = $column_visible;
	}
	
	private function adv_search($category_id=0){
		$Website = $this->Website;
		
		if(!$this->settings['adv_search_status']){
			return 0;
		}
		
		if($_SESSION['column_visible'][$this->current_class]){
			$this->settings['column_visible'] = $_SESSION['column_visible'][$this->current_class];
		}
		
		$data = array(
			'Website'		=>	$Website,
			'setting'		=>	$this->settings,
			'rows1'			=>	$_SESSION['adv_search'][$this->current_class],
			'category_id'		=>	$category_id,
		);
		return $this->load->view($this->settings['html']['adv_search'], $data, true);
	}
	
	private function adv_search_sql($q, $g){
		//進階搜尋
		
		$_SESSION['adv_search'][$this->current_class] = $g;
		
		$s[] = sprintf(' 1=1 ');
		
		if($g['search']){
			$s[] = sprintf(' (A.id=%s or A.name like %s or A.content like %s)',
				sql_string($g['search'], 'int'),
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text')
			);
		}
		switch($g['display']){
			case 1:
				$s[] = sprintf(' A.display=0 ');
				break;
			case 2:
				$s[] = sprintf(' A.display=1 ');
				break;
			case 3:
				break;
		}
		
		if(sizeof($s)>1){
			//當有搜尋項目時，不予排序
			$this->settings['icon']['sort'] = 0;
		}
		
		if(sizeof($s)){
			if($q['sWhere']){
				$q['sWhere'] .= sprintf(' and %s', implode(' and ', $s));
			}else{
				$q['sWhere'] = sprintf(' WHERE %s', implode(' and ', $s));
			}
		}
		//echo '<pre>';print_r($q);exit;
		return $q;
	}
	
	public function ajax_export(){
		$Website = $this->Website;
		$ids = explode(',', $_GET['ids']);
		//echo $_SESSION['sql']['export']['product'];exit;
		
		$sql = $_SESSION['sql']['export']['product'];
		
		if(sizeof($ids) && $ids[0]){
			//echo $sql;
			$pos_1 = strpos($sql, 'WHERE');
			$pos_2 = strpos($sql, 'GROUP BY');
			
			$sql_before_where = substr($sql, 0, $pos_1);//echo $pos_1;exit;
			$sql_where = substr($sql, $pos_1, $pos_2-$pos_1).' and A.id in ('.$_GET['ids'].')';
			$sql_after_groupby = ' '.substr($sql, $pos_2);
			
			$sql = $sql_before_where.$sql_where.$sql_after_groupby;
			//echo '<pre>';echo $sql;exit;
		}
		
		$rows1 = $this->query->select($sql);
		//$rows1 = $this->query->select($_SESSION['sql']['export']['product']);
		//echo '<pre>';print_r($rows1);exit;
		
		$rows2[] = array(
			'id',
			'廠商',
			'品牌',
			'商品名稱',
			'商品編號',
			'狀態',
			'成本價',
			'售價',
			'原價',
			'顏色',
			'尺寸',
			'庫存數',
			'說明',
		);
		//exit;
		for($i=0; $i<sizeof($rows1); $i++){
			
			$display_text = $rows1[$i]['display']?'上架':'下架';
			
			//color
			$sql = sprintf("SELECT B.*
							FROM %s A
							LEFT JOIN %s B ON A.color_id=B.id
							WHERE A.product_id=%s
							",
							$Website['table']['product_color_relation'],
							$Website['table']['product_color'],
							sql_string($rows1[$i]['id'], "int")
							);
			$rows3 = $this->query->select($sql, 1);
			
			//size
			$sql = sprintf("SELECT B.*, C.quantity
							FROM %s A
							LEFT JOIN %s B ON A.size_id=B.id
							LEFT JOIN %s C ON B.id=C.size_id and C.product_id=A.product_id and C.type=0
							WHERE A.product_id=%s
							",
							$Website['table']['product_size_relation'],
							$Website['table']['product_size'],
							$Website['table']['inventory'],
							sql_string($rows1[$i]['id'], "int")
							);//echo $sql;exit;
			$rows4 = $this->query->select($sql);
			
			if(!sizeof($rows4)){
				$rows2[] = array(
					$rows1[$i]['id'],
					$rows1[$i]['supplier_name'],
					$rows1[$i]['brand_name'],
					$rows1[$i]['name'],
					$rows1[$i]['product_num'],
					$display_text,
					$rows1[$i]['cost_price'],
					$rows1[$i]['price'],
					$rows1[$i]['list_price'],
					$rows3['name'],
					'', //$rows4[$j]['name'],
					'', //(int)$rows4[$j]['quantity'],
					strip_tags(htmlspecialchars_decode($rows1[$i]['description']))
				);
			}else{
				for($j=0; $j<sizeof($rows4); $j++){
					$rows2[] = array(
						$rows1[$i]['id'],
						$rows1[$i]['supplier_name'],
						$rows1[$i]['brand_name'],
						$rows1[$i]['name'],
						$rows1[$i]['product_num'],
						$display_text,
						$rows1[$i]['cost_price'],
						$rows1[$i]['price'],
						$rows1[$i]['list_price'],
						$rows3['name'],
						$rows4[$j]['name'],
						(int)$rows4[$j]['quantity'],
						strip_tags(htmlspecialchars_decode($rows1[$i]['description']))
					);
				}
			}
		}
		
		//echo '<pre>';print_r($rows2);exit;
		
		$file = '商品資料 - '.date('Y.m.d H.i.s', time());
		$filename = $file.'.csv';
		$this->csv->array_to_csv($filename, $rows2);
	}
	
	public function ajax_inventory(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$product_id = $_POST['product_id'];
			
			$inventory_id = $_POST['inventory_id'];
			//$color_id = $_POST['color_id'];
			$spec_id = $_POST['spec_id'];
			$quantity = $_POST['quantity'];
			$cost_price = $_POST['cost_price'];
			$delete = $_POST['delete'];
			$display = $_POST['display'];
			/*
			//delete relation
			$this->db->where('product_id', $product_id);
			$this->db->delete($Website['table']['product_color_relation']);

			$this->db->where('product_id', $product_id);
			$this->db->delete($Website['table']['product_spec_relation']);
			*/
			//print_r($_POST);exit;
			for($i=0; $i<sizeof($inventory_id); $i++){
				if(!$inventory_id[$i]){
					//新增
					$data = array(
						'product_id'	=>	$product_id,
						//'color_id'		=>	$color_id[$i],
						'spec_id'		=>	$spec_id[$i],
						'quantity'		=>	$quantity[$i],
						'cost_price'	=>	$cost_price[$i],
						'type'			=>	0,
						'display'		=>	1,
						'insert_time'	=>	time(),
						'modify_time'	=>	time(),
					);
					$this->db->set($data);
					$this->db->insert($Website['table']['inventory']);
				}else{
					
					if(in_array($inventory_id[$i], $delete)){
						//刪除
						$this->db->where('id', $inventory_id[$i]);
						$this->db->delete($Website['table']['inventory']);
					}else{
						//修改
						$data = array(
							'product_id'	=>	$product_id,
							//'color_id'		=>	$color_id[$i],
							'spec_id'		=>	$spec_id[$i],
							'quantity'		=>	$quantity[$i],
							'cost_price'	=>	$cost_price[$i],
							'display'		=>	$display[$i]?1:0,
							'insert_time'	=>	time(),
							'modify_time'	=>	time(),
						);
						$this->db->set($data);
						$this->db->where('id', $inventory_id[$i]);
						$this->db->update($Website['table']['inventory']);
					}
				}
				
				
				//relation
				/*
				$sql = sprintf("INSERT IGNORE INTO %s (product_id, color_id) VALUES (%s, %s)", 
							  	$Website['table']['product_color_relation'],
							   	sql_string($product_id, 'int'),
							   	sql_string($color_id[$i], 'int')
							  );
				$this->query->sql($sql);
				
				$sql = sprintf("INSERT IGNORE INTO %s (product_id, spec_id) VALUES (%s, %s)", 
							  	$Website['table']['product_spec_relation'],
							   	sql_string($product_id, 'int'),
							   	sql_string($spec_id[$i], 'int')
							  );
				$this->query->sql($sql);
				*/
			}
			exit;
		}
		
		$product_id = $_GET['product_id'];
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.product_id=%s and A.type=0
						ORDER BY A.id asc
						",
						$Website['table']['inventory'],
						sql_string($product_id, 'int')
						);
		$rows1 = $this->query->select($sql);
		
		/*
		$sql = sprintf("SELECT B.*, A.name type_name, A.id type_id
						FROM %s A
						LEFT JOIN %s B ON A.id=B.type
						ORDER BY A.sort asc, B.sort asc, B.id asc
						",
						$Website['table']['color_type'],
						$Website['table']['color']
						);
		$rows2 = $this->query->select($sql);
		
		for($i=0; $i<sizeof($rows2); $i++){
			if(!$rows2_1[$rows2[$i]['type_id']]['name']){
				$rows2_1[$rows2[$i]['type_id']] = array(
					'name'		=>	$rows2[$i]['type_name']
				);
			}
			
			if($rows2[$i]['id']){
				$rows2_1[$rows2[$i]['type_id']]['data'][] = array(
					'id'		=>	$rows2[$i]['id'],
					'name'		=>	$rows2[$i]['name']
				);
			}
		}
		*/
		
		$sql = sprintf("SELECT B.*, A.name type_name, A.id type_id
						FROM %s A
						LEFT JOIN %s B ON A.id=B.type
						ORDER BY A.sort asc, B.sort asc, B.id asc
						",
						$Website['table']['spec_type'],
						$Website['table']['spec']
						);
		$rows3 = $this->query->select($sql);
		
		for($i=0; $i<sizeof($rows3); $i++){
			if(!$rows3_1[$rows3[$i]['type_id']]['name']){
				$rows3_1[$rows3[$i]['type_id']] = array(
					'name'		=>	$rows3[$i]['type_name']
				);
			}
			
			if($rows3[$i]['id']){
				$rows3_1[$rows3[$i]['type_id']]['data'][] = array(
					'id'		=>	$rows3[$i]['id'],
					'name'		=>	$rows3[$i]['name']
				);
			}
		}
		
		$data = array(
			'Website'		=>	$Website,
			'product_id'	=>	$product_id,
			'rows1'			=>	$rows1,
			//'rows2'			=>	$rows2_1,
			'rows3'			=>	$rows3_1,
		);
		echo $this->load->view($this->settings['html']['inventory'], $data, true);
	}
	
	public function ajax_inventory_item(){
		$Website = $this->Website;
		$this->preload->check_domain();
		/*
		$sql = sprintf("SELECT B.*, A.name type_name, A.id type_id
						FROM %s A
						LEFT JOIN %s B ON A.id=B.type
						ORDER BY A.sort asc, B.sort asc, B.id asc
						",
						$Website['table']['color_type'],
						$Website['table']['color']
						);
		$rows2 = $this->query->select($sql);
		
		for($i=0; $i<sizeof($rows2); $i++){
			if(!$rows2_1[$rows2[$i]['type_id']]['name']){
				$rows2_1[$rows2[$i]['type_id']] = array(
					'name'		=>	$rows2[$i]['type_name']
				);
			}
			
			if($rows2[$i]['id']){
				$rows2_1[$rows2[$i]['type_id']]['data'][] = array(
					'id'		=>	$rows2[$i]['id'],
					'name'		=>	$rows2[$i]['name']
				);
			}
		}
		*/
		$sql = sprintf("SELECT B.*, A.name type_name, A.id type_id
						FROM %s A
						LEFT JOIN %s B ON A.id=B.type
						ORDER BY A.sort asc, B.sort asc, B.id asc
						",
						$Website['table']['spec_type'],
						$Website['table']['spec']
						);
		$rows3 = $this->query->select($sql);
		
		for($i=0; $i<sizeof($rows3); $i++){
			if(!$rows3_1[$rows3[$i]['type_id']]['name']){
				$rows3_1[$rows3[$i]['type_id']] = array(
					'name'		=>	$rows3[$i]['type_name']
				);
			}
			
			if($rows3[$i]['id']){
				$rows3_1[$rows3[$i]['type_id']]['data'][] = array(
					'id'		=>	$rows3[$i]['id'],
					'name'		=>	$rows3[$i]['name']
				);
			}
		}
		$data = array(
			'Website'		=>	$Website,
			//'rows2'			=>	$rows2_1,
			'rows3'			=>	$rows3_1,
		);
		echo $this->load->view($this->settings['html']['inventory_item'], $data, true);
	}
}
?>