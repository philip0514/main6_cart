<?
class User_group extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		
		$this->settings = array(
								'btn'				=>	array(			//列表下方按鈕
									'insert'			=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'btn_txt'			=>	array(			//自行定義按鈕名稱
									'insert'			=>	'',
									'display'			=>	'',
									'sort'				=>	'',
								),
								'icon'				=>	array(			//自行增加icon數量，但須於datatable中的actions中新增項目
									'edit'				=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									'ajax'				=>	base_url().'admin/'.$this->current_class.'/ajax/',
									'item'				=>	base_url().'admin/'.$this->current_class.'/item/',
								),
								'orderby'			=>	array(array(1, "asc")),
								'order_columns'		=>	array('', 'id', 'name', 'description'),
								'search_columns'	=>	array('name', 'description'),
								'table_columns'		=>	array(my_lang('action'), my_lang('id'), my_lang('admin_user_group.name'), my_lang('description')),
								'column_visible'	=>	array(1, 1, 1, 1),
								'column'			=>	array(
									array('width'		=>'130px', 	'targets'	=>	0),
									array('width'		=>'60px', 	'targets'	=>	1),
									array('orderable'	=>false, 	'targets'	=>	array(0)),
								),
								
								'html'				=>	array(
									'list'				=>	'template/list',
									'item'				=>	$this->current_class.'/item',
									'adv_search'		=>	'admin/adv_search/'.$this->current_class,
								),
								'model'				=>	'global_query',
								
								//datatable
								'adv_search_status'	=>	0,
								'adv_search'		=>	array(
									'vars'			=>	array(
										'display',
										'search'
									),
								),
							);
		$this->Website['tables'] = array(
			$this->Website['table']['group'],
		);
		
		//確定編輯權限
		$this->settings = $this->menu->check_editable($this->settings);
		
		//進階搜尋設定
		$this->settings['adv_search']['html'] = $this->adv_search($this->settings);
		
		//下方btn設定
		$this->settings['button']  = $this->datatable->button($this->settings);
		
	}
	
	public function index(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_list');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			$id = $_POST['id'];
			$show = $_POST['display'];
			$delete = $_POST['delete'];
			
			switch($here){
				case 1:
					/*
						上下架顯示
					*/
					$this->$model_query->display_update($id, $show);
					
					/*
						刪除
					*/
					$this->$model_query->delete_update($id, $delete);
					break;
				
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		$data = array(
			'Website'	=>	$Website,
			'settings'	=>	$this->settings,
			'rows1'		=>	$rows1,
		);
		
		$this->template->views_admin($this->settings['html']['list'], $data);
	}
	
	public function ajax(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		$q = $this->datatable->query(array(
											$this->settings['order_columns'], 
											$this->settings['search_columns'], 
											$this->settings
										), 
										$_GET);
		//echo '<pre>';print_r($q);exit;
		$iTotalRecords = $this->$model_query->table_total();
		$iTotalDisplayRecords = $this->$model_query->display_total($q);
		$rows1 = $this->$model_query->datatable_select($q);//echo 5;exit;
		
		$rows2 = array();
		for($i=0; $i<sizeof($rows1); $i++){
			$action_array = array(
								'id'				=>	$rows1[$i]['id'],
								'checked'			=>	$Website['checked'][(int)$rows1[$i]['display']],
								'display_class'		=>	$Website['display_class'][(int)$rows1[$i]['display']],
								'edit'				=>	'admin/'.$this->current_class.'/item/'.$rows1[$i]['id'].'/',
								'icon'				=>	$this->settings['icon'],
							);
			$rows2[] = array(
				$this->datatable->actions($action_array),
				$rows1[$i]['id'],
				$rows1[$i]['name'],
				$rows1[$i]['description'],
			);
		}
		
		$output = array(
			"aaData"					=> $rows2,
			"iTotalRecords" 			=> $iTotalRecords,
			"iTotalDisplayRecords"	=> $iTotalDisplayRecords,
		);
		
		echo json_encode($output);
		exit;
	}
	
	public function item($id){
		
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_item');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			//Save
			$here = $_POST['here'];
			$name = $this->input->post('name');
			$description = $this->input->post('description');
			$display = $_POST['display'] ? 1 : 0;
			$rights = $this->input->post('rights');
			
			for($i=0; $i<sizeof($rights); $i++){
				$editable[] = $_POST['rights_'.$rights[$i]];
			}
			
			$data = array(
				'display'	=>	$display,
			);
			
			if(!$id){
				$id = $this->ion_auth->create_group($name, $description, $data);
				$this->ion_auth->add_group_structure($rights, $editable, $id);
				
			}else{
				$this->ion_auth->update_group($id, $name, $description, $data);
				$this->ion_auth->remove_group_structure($id);
				$this->ion_auth->add_group_structure($rights, $editable, $id);
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$data = array(
			'id'			=>	$id,
		);
		$rows1 = $this->$model_query->select($data);
		if($id){
			$Website['page_header'] = $rows1['name'];
		}
		
		
		$groups=$this->ion_auth->groups()->result_array();
		if($id){
			$currentGroups = $this->ion_auth->get_users_groups($id)->result();
			$sql = sprintf("SELECT A.structure_id id, A.editable
							FROM %s A
							WHERE A.group_id=%s
							",
							$Website['table']['group_structure'],
							$id
							);
			$rows5 = $this->query->select($sql);
			
			for($i=0; $i<sizeof($rows5); $i++){
				$rights[] = $rows5[$i]['id'];
				$rights_edit[$rows5[$i]['id']] = explode(',', $rows5[$i]['editable']);
			}
		}
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.display=1
						ORDER BY A.sort asc
						",
						$Website['table']['structure']
						);
		$rows4 = $this->query->select($sql);
		
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement("ul");
		$node->setAttribute("class", 'user_rights');
		$parnode = $dom->appendChild($node);
		
		for($i=0; $i<sizeof($rows4); $i++){
			$id = $rows4[$i]['id'];
			$fid = $rows4[$i]['fid'];
			$parent = $parnode;
			
			if($fid){
				if(!$node_ol[$fid]){
					$node_ol[$fid] = $dom->createElement("ul");						//start ol
				}
				$parent = $node_ol[$fid];
			}
			
			$nodes[$id] = $dom->createElement("li");								//start li
			$nodes[$id]->setAttribute("id", 'list_'.($i+1));						//set li attr
			
			$div = $dom->createElement('div');										//start div
			$div->setAttribute('class', 'checkbox check-primary');
				
				$input_rights = $dom->createElement('input');
				$input_rights->setAttribute('id', 'rights'.$id);
				$input_rights->setAttribute('type', 'checkbox');
				$input_rights->setAttribute('name', 'rights[]');
				$input_rights->setAttribute('class', 'change_rights');
				$input_rights->setAttribute('value', $id);
				if(in_array($rows4[$i]['id'], $rights)){
					$input_rights->setAttribute('checked', 'checked');
				}
				$div->appendChild($input_rights);
				
				$label = $dom->createElement('label', ' '.my_lang('admin_menu.'.$rows4[$i]['name']));
				$label->setAttribute('for', 'rights'.$id);
				$div->appendChild($label);
					
					
			$div2 = $dom->createElement('div');										//start div
			$div2->setAttribute('class', 'checkbox check-success');
				
				//編輯權限
				if($rows4[$i]['editable']){
					$input_edit1 = $dom->createElement('input');
					$input_edit1->setAttribute('id', 'rights_read_'.$id);
					$input_edit1->setAttribute('type', 'checkbox');
					$input_edit1->setAttribute('name', 'rights_'.$id.'[]');
					$input_edit1->setAttribute('class', 'rights_edit');
					$input_edit1->setAttribute('value', 1);
					if(in_array(1, $rights_edit[$id])){
						$input_edit1->setAttribute('checked', 'checked');
					}
					$div2->appendChild($input_edit1);
					
					$edit1 = $dom->createElement('label', ' '.my_lang('authority_read'));
					$edit1->setAttribute('for', 'rights_read_'.$id);
					$div2->appendChild($edit1);

					
					
					$input_edit2 = $dom->createElement('input');
					$input_edit2->setAttribute('id', 'rights_insert_'.$id);
					$input_edit2->setAttribute('type', 'checkbox');
					$input_edit2->setAttribute('name', 'rights_'.$id.'[]');
					$input_edit2->setAttribute('class', 'rights_edit');
					$input_edit2->setAttribute('value', 2);
					if(in_array(2, $rights_edit[$id])){
						$input_edit2->setAttribute('checked', 'checked');
					}
					$div2->appendChild($input_edit2);
					
					$edit2 = $dom->createElement('label', ' '.my_lang('authority_insert'));
					$edit2->setAttribute('for', 'rights_insert_'.$id);
					$div2->appendChild($edit2);
					
					$input_edit3 = $dom->createElement('input');
					$input_edit3->setAttribute('id', 'rights_edit_'.$id);
					$input_edit3->setAttribute('type', 'checkbox');
					$input_edit3->setAttribute('name', 'rights_'.$id.'[]');
					$input_edit3->setAttribute('class', 'rights_edit');
					$input_edit3->setAttribute('value', 3);
					if(in_array(3, $rights_edit[$id])){
						$input_edit3->setAttribute('checked', 'checked');
					}
					$div2->appendChild($input_edit3);
					
					$edit3 = $dom->createElement('label', ' '.my_lang('authority_update'));
					$edit3->setAttribute('for', 'rights_edit_'.$id);
					$div2->appendChild($edit3);
					
					$input_edit4 = $dom->createElement('input');
					$input_edit4->setAttribute('id', 'rights_delete_'.$id);
					$input_edit4->setAttribute('type', 'checkbox');
					$input_edit4->setAttribute('name', 'rights_'.$id.'[]');
					$input_edit4->setAttribute('class', 'rights_edit');
					$input_edit4->setAttribute('value', 4);
					if(in_array(4, $rights_edit[$id])){
						$input_edit4->setAttribute('checked', 'checked');
					}
					$div2->appendChild($input_edit4);
					
					$edit4 = $dom->createElement('label', ' '.my_lang('authority_delete'));
					$edit4->setAttribute('for', 'rights_delete_'.$id);
					$div2->appendChild($edit4);
				}
			
			$nodes[$id]->appendChild($div);											//end div
			$nodes[$id]->appendChild($div2);										//end div
			
			
			$parent->appendChild($nodes[$id]);										//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);									//end ol
			}
			
		}
		$nestedHTML = $dom->saveHTML();
		
		
		$data = array(
			'Website'	=>	$Website,
			'settings'	=>	$this->settings,
			'rows2'		=>	$groups,
			'rows1'		=>	$rows1,
			'rows3'		=>	$currentGroups,
			'nestedHTML'	=>	$nestedHTML,
		);
		$this->template->views_admin($this->settings['html']['item'], $data);
	}
	
	private function adv_search($setting){
		$Website = $this->Website;
		
		if(!$this->settings['adv_search_status']){
			return 0;
		}
		
		$data = array(
			'Website'	=>	$Website,
			'rows1'		=>	$_SESSION['adv_search'][$this->current_class],
		);
		return $this->load->view($this->settings['html']['adv_search'], $data, true);
	}
	
	private function adv_search_sql($q, $g){
		//進階搜尋
		
		$_SESSION['adv_search'][$this->current_class] = $g;
		
		if($g['search']){
			$s[] = sprintf(' (A.name like %s)',
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text')
			);
		}
		switch($g['display']){
			case 1:
				$s[] = sprintf(' A.display=0 ');
				break;
			case 2:
				$s[] = sprintf(' A.display=1 ');
				break;
			case 3:
				break;
		}
		if(sizeof($s)){
			if($q['sWhere']){
				$q['sWhere'] .= sprintf(' and %s', implode(' and ', $s));
			}else{
				$q['sWhere'] = sprintf(' WHERE %s', implode(' and ', $s));
			}
		}
		return $q;
	}
	
	public function structure(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = $this->lang->line($this->current_class."_list");
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			$rows1 = objectToArray(json_decode($_POST['ssort']));
			for($i=1; $i<sizeof($rows1); $i++){
				$id = $rows1[$i]['item_id'];
				$fid = (int)$rows1[$i]['parent_id'];
				
				$sql = sprintf("UPDATE %s SET fid=%s, sort=%s WHERE id=%s",
								$Website['table']['structure'],
								sql_string($fid, "int"),
								sql_string($i, "int"),
								sql_string($id, "int")
								);//echo $sql.'<br>';
				$this->query->sql($sql);
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$sql = sprintf("SELECT A.* 
						FROM %s A
						ORDER BY A.sort asc
						",
						$Website['table']['structure']);
		$rows1 = $this->db->query($sql)->result_array();
		//echo '<pre>';print_r($rows1);
		
		$nested = $this->menu->nested_sortable($rows1);
		
		$data = array(
			'Website'		=>	$Website,
			'settings'		=>	$settings,
			'rows1'			=>	$rows1,
			'nested'		=>	$nested,
		);
		$this->template->views_admin('category/structure', $data);
	}
}
?>