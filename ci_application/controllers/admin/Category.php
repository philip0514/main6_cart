<?
class Category extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		$this->settings = array(
								'function'			=>	$this->current_class,
								'btn'				=>	array(
									'insert'			=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	1,
								),
								'icon'				=>	array(
									'edit'				=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	1,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									'ajax'				=>	base_url().'admin/'.$this->current_class.'/ajax/',
									'item'				=>	base_url().'admin/'.$this->current_class.'/ajax_item/',
								),
								'unsort'			=>	array(0, 1, 2, 3),
								'orderby'			=>	array(array(1, "asc")),
								'order_columns'		=>	array('', 'sort', 'id', 'name'),
								'search_columns'	=>	array('name'),
								'html'				=>	array(
									'list'				=>	$this->current_class.'/list',
									'item'				=>	$this->current_class.'/item',
								),
								'model'				=>	'category_query',
								
								//image relation
								'item_type'			=>	array(
									'category_ogimage'
								),
								
								//validation rule group
								'validation_rule_group'	=>	'admin/'.$this->current_class.'/item',
							);
							
							
		$this->Website['tables'] = array(
			$this->Website['table']['category'],
		);
		
		
	}
	
	public function index(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_list');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			$delete = $_POST['delete_id'];
			$sort = $_POST['ssort'];
			$rows1 = objectToArray(json_decode($sort));
			//echo '<pre>';print_r($_POST);exit;
			for($i=0; $i<sizeof($delete); $i++){
				$d_id = substr($delete[$i], 5);
				
				if($d_id && strlen($id)<10){
					$sql = sprintf("DELETE FROM %s 
									WHERE id = '%s'",
									$Website['tables'][0],
									$d_id
					);
					$this->query->sql($sql);
				}
			}
			
			for($i=1; $i<sizeof($rows1); $i++){
				$id = $rows1[$i]['item_id'];
				$fid = (int)$rows1[$i]['parent_id'];
				
				if(strlen($id)>10){
					$data = array(
						'fid'			=>	ci_sql_string($fid, 'text'),
						'sort'			=>	ci_sql_string($i, 'int'),
						'layer'			=>	ci_sql_string($rows1[$i]['depth'], 'int'),
						'insert_time'	=>	time(),
						'modify_time'	=>	time(),
					);
					$id = $this->$model_query->insert($this->settings, $data);
					
				}else{
					$data = array(
						'fid'			=>	ci_sql_string($fid, 'text'),
						'sort'			=>	$i,
						'layer'			=>	ci_sql_string($rows1[$i]['depth'], 'int'),
						'modify_time'	=>	time(),
					);
					$this->$model_query->update($data, $id);
				}
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$sql = sprintf("SELECT A.* 
						FROM %s A
						ORDER BY A.sort asc
						",
						$Website['tables'][0]);
		$rows1 = $this->db->query($sql)->result_array();
		
		$nested = $this->menu->backend_category_sortable($rows1);
		
		$data = array(
			'Website'		=>	$Website,
			'settings'		=>	$settings,
			'rows1'			=>	$rows1,
			'nested'		=>	$nested,
		);
		$this->template->views_admin($this->settings['html']['list'], $data);
	}
	
	public function ajax_item($id){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_item');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			$id = $_POST['id'];
			$new_id = $_POST['new_id'];
			$name = $_POST['name'];
			$url_name = str_replace(' ', '', preg_replace($Website['url_allow_chars'] , '' , $_POST['url_name']));
			$display = $_POST['display'];
			$ogimage_input = $_POST['ogimage_input'];
			$ogimage_type = $this->settings['item_type'][0];
			
			if($this->form_validation->run($this->settings['validation_rule_group']) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				$this->template->views_admin('template/error', $data, true);
				exit;
			}
			
			if(!$id){
				$rows1 = array(
					'name'			=>	ci_sql_string($name, 'text'),
					'url_name'		=>	ci_sql_string($url_name, 'text'),
					'fid'			=>	ci_sql_string(0, 'text'),
					'display'		=>	ci_sql_string($display, 'int'),
					'sort'			=>	0,
					'insert_time'	=>	time(),
					'modify_time'	=>	time(),
				);
				$id = $this->$model_query->insert($this->settings, $rows1);
				echo json_encode(array('id'	=>	$id, 'new_id'	=>	$new_id));
			}else{
				$rows1 = array(
					'name'			=>	ci_sql_string($name, 'text'),
					'url_name'		=>	ci_sql_string($url_name, 'text'),
					'display'		=>	ci_sql_string($display, 'int'),
					'modify_time'	=>	time(),
				);
				$this->$model_query->update($rows1, $id);
				echo json_encode(array('id'	=>	$id));
			}
			
			
			//媒體 ogimage
			$this->media_query->media_relation($id, $ogimage_input, $ogimage_type);
			
			exit;
		}
		
		$validation_rule = $this->config->item($this->settings['validation_rule_group']);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form2', array('submit_handler'	=>	'handler_admin_category',));
		
		$id = substr($_GET['id'],5);
		if($id && strlen($id)<=10){
			$sql = sprintf("SELECT A.* 
							FROM %s A
							WHERE A.id=%s
							",
							$Website['tables'][0],
							$id
						  );
			$rows1 = $this->query->select($sql, 1);
			
			
			/*
				社群分享圖片
			*/
			$ogimage = $this->media_query->media_select($id, $this->settings['item_type'][0]);
			$rows1['ogimage_input'] = $ogimage['input'];
			$rows1['ogimage_area'] = $ogimage['area'];
			
		}else{
			$rows1['new_id'] = $id;
			$rows1['name'] = 'New Item';
			$rows1['display'] = 1;
		}
		
		$data = array(
			'Website'		=>	$Website,
			'validation'	=>	$jquery_validation,
			'rows1'			=>	$rows1,
		);
		echo $this->template->view('admin/'.$this->current_class.'/item', $data);
		exit;
	}
	
}
?>