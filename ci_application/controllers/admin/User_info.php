<?
class User_info extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		$this->settings = array(
								'btn'				=>	array(		//列表下方按鈕
									'insert'			=>	0,
									'delete'			=>	0,
									'display'			=>	0,
									'sort'				=>	0,
								),
								'btn_txt'			=>	array(	//自行定義按鈕名稱
									'insert'			=>	'',
									'display'			=>	'',
									'sort'				=>	'',
								),
								'icon'				=>	array(		//自行增加icon數量，但須於datatable中的actions中新增項目
									'edit'				=>	0,
									'delete'			=>	0,
									'display'			=>	0,
									'sort'				=>	0,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									'ajax'				=>	base_url().'admin/'.$this->current_class.'/ajax/',
									'item'				=>	base_url().'admin/'.$this->current_class.'/item/',
								),
								'unsort'			=>	array(0, 1, 2, 3),
								'orderby'			=>	array(array(1, "asc")),
								'order_columns'		=>	array('', 'sort', 'id', 'name'),
								'search_columns'	=>	array('name', 'content'),
								
								'html'				=>	array(
									'list'				=>	$this->current_class.'/list',
									'item'				=>	$this->current_class.'/item',
									'adv_search'		=>	'admin/adv_search/'.$this->current_class,
								),
								'model'				=>	'global_query',
								
								//datatable
								'sdom'				=>	"<'row'<'col-md-6'<'adv_search'>><'col-md-6'l<'clear_both'p>>r>t<'row'<'col-md-4'i><'col-md-8'p>>",
								'adv_search_status'	=>	0,
								'adv_search'		=>	array(
									'vars'				=>	array(
										'display',
										'search'
									),
								),
								'validation_rule_group'	=>	'admin/'.$this->current_class.'/list',
							);
		$this->Website['tables'] = array(
			$this->Website['table']['user'],
		);
		
		//確定編輯權限
		$this->settings = $this->menu->check_editable($this->settings);
	}
	
	public function index(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		$id = $_SESSION['user_info']['id'];
		
		//vars
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			//Save
			$here = $_POST['here'];
			$_POST = $this->input->post(NULL, TRUE);
			
			if($this->form_validation->run($this->settings['validation_rule_group']) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				$this->template->views_admin('template/error', $data, true);
				exit;
			}
			
			$password = $_POST['password'];
			$rows1 = array(
				'username'			=> $this->input->post('name'),
			);
			if($this->input->post('password')){
				$rows1['password'] = $this->ion_auth->hash_password($this->input->post('password'), $this->ion_auth->salt());
			}
			$this->db->set($rows1);
			$this->db->where('id', $id);
			$this->db->update($Website['table']['user']);
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$validation_rule = $this->config->item($this->settings['validation_rule_group']);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1');
		
		$data = array(
			'id'			=>	$id,
		);
		$rows1 = $this->$model_query->select($data);
		$Website['page_header'] = $this->lang->line($this->current_class."_list");
		
		$sql = sprintf("SELECT A.* 
						FROM %s A
						WHERE A.user_id=%s or A.login=%s
						ORDER BY A.id desc
						LIMIT 30
						",
						$Website['table']['login_attempts'],
						sql_string($id, 'int'),
						sql_string($rows1['email'], 'text')
						);//echo $sql;exit;
		$rows4 = $this->query->select($sql);
		//echo $sql;
		$data = array(
			'Website'		=>	$Website,
			'settings'		=>	$this->settings,
			'validation'	=>	$jquery_validation,
			'rows1'			=>	$rows1,
			'rows4'			=>	$rows4,
		);
		$this->template->views_admin($this->settings['html']['item'], $data);	
	}
	
	private function adv_search($setting){
		$Website = $this->Website;
		
		$data = array(
			'Website'	=>	$Website,
			'rows1'		=>	$_SESSION['adv_search'][$this->current_class],
		);
		return $this->load->view($this->settings['html']['adv_search'], $data, true);
	}
	
	private function adv_search_sql($q, $g){
		//進階搜尋
		
		$_SESSION['adv_search'][$this->current_class] = $g;
		
		if($g['search']){
			$s[] = sprintf(' (A.name like %s or A.content like %s)',
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text')
			);
		}
		switch($g['display']){
			case 1:
				$s[] = sprintf(' A.display=0 ');
				break;
			case 2:
				$s[] = sprintf(' A.display=1 ');
				break;
			case 3:
				break;
		}
		if(sizeof($s)){
			if($q['sWhere']){
				$q['sWhere'] .= sprintf(' and %s', implode(' and ', $s));
			}else{
				$q['sWhere'] = sprintf(' WHERE %s', implode(' and ', $s));
			}
		}
		return $q;
	}
	
}
?>