<?
class Member extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		$this->settings = array(
								'function'			=>	$this->current_class,
								'btn'				=>	array(		//列表下方按鈕
									'insert'			=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'btn_txt'			=>	array(	//自行定義按鈕名稱
									'insert'			=>	'',
									'display'			=>	'',
									'sort'				=>	'',
								),
								'icon'				=>	array(		//自行增加icon數量，但須於datatable中的actions中新增項目
									'edit'				=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									'ajax'				=>	base_url().'admin/'.$this->current_class.'/ajax/',
									'item'				=>	base_url().'admin/'.$this->current_class.'/item/',
								),
								'unsort'			=>	array(0, 3),
								'orderby'			=>	array(array(1, "desc")),
								'order_columns'		=>	array('', 'id', 'name', '', 'gender', 'mobile', 'insert_time'),
								'search_columns'	=>	array('name'),
								'table_columns'		=>	array(my_lang('action'), my_lang('id'), my_lang('admin_member.name'), my_lang('admin_member.identity'), my_lang('admin_member.gender'), my_lang('mobile'), my_lang('admin_member.insert_time')),
								'column_visible'	=>	array(1, 1, 1, 1, 1, 1, 1),
								'column'		=>	array(
									array('width'		=>'130px', 	'targets'	=>	0),
									array('width'		=>'60px', 	'targets'	=>	1),
									array('orderable'	=>false, 	'targets'	=>	array(0, 3)),
								),
								
								'html'				=>	array(
									'list'				=>	'template/list',
									'item'				=>	$this->current_class.'/item',
									'adv_search'		=>	'admin/adv_search/'.$this->current_class,
								),
								'model'				=>	'member_query',
								
								//datatable
								'adv_search_status'	=>	1,
								'adv_search'		=>	array(
									'vars'				=>	array(
										'search',
										'gender',
										'member_group',
										'city_id',
										'area_id',
										'start_date',
										'end_date',
									),
								),
								
								//validation rule group
								'validation_rule_group'	=>	'admin/'.$this->current_class.'/item',
							);
		$this->Website['tables'] = array(
			$this->Website['table']['member'],
		);
		
		//確定編輯權限
		$this->settings = $this->menu->check_editable($this->settings);
		
		//進階搜尋設定
		$this->settings['adv_search']['html'] = $this->adv_search();
		
		//下方btn設定
		$this->settings['button'] = $this->datatable->button($this->settings);
		
		
	}
	
	public function index(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_list');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			$id = $_POST['id'];
			$show = $_POST['display'];
			$delete = $_POST['delete'];
			
			switch($here){
				case 1:
					/*
						上下架顯示
					*/
					$this->$model_query->display_update($id, $show);
					
					/*
						刪除
					*/
					$this->$model_query->delete_update($id, $delete);
					break;
				case 2:
					/*
						排序
					*/
					//$this->$model_query->sort_update();
					break;
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$data = array(
			'Website'	=>	$Website,
			'settings'	=>	$this->settings,
			'rows1'		=>	$rows1,
		);
		$this->template->views_admin($this->settings['html']['list'], $data);	
	}
	
	public function ajax(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		
		$q = $this->datatable->query(array(
											$this->settings['order_columns'], 
											$this->settings['search_columns'], 
											$this->settings
										), 
										$_GET);
										
		$q = $this->adv_search_sql($q, $_GET);
		$iTotalRecords = $this->$model_query->table_total();
		$iTotalDisplayRecords = $this->$model_query->display_total($q);
		$rows1 = $this->$model_query->datatable_select($q);
		
		$rows2 = array();
		for($i=0; $i<sizeof($rows1); $i++){
			
			$icon_setting = $this->settings['icon'];
			
			if(in_array($rows1[$i]['id'], array())){
				//設定部份icon項目不顯示
				//$icon_setting['delete'] = 0;
			}
			if($q['sWhere']){
				//當有搜尋項目時，不予排序
				$icon_setting['sort'] = 0;
			}
			
			$action_array = array(
								'id'				=>	$rows1[$i]['id'],
								'checked'			=>	$Website['checked'][(int)$rows1[$i]['display']],
								'display_class'		=>	$Website['display_class'][(int)$rows1[$i]['display']],
								'edit'				=>	'admin/'.$this->current_class.'/item/'.$rows1[$i]['id'].'/',
								'icon'				=>	$icon_setting,
							);
							
			if($rows1[$i]['gender']==1){
				$rows1[$i]['gender_text'] = '先生';
			}elseif($rows1[$i]['gender']==2){
				$rows1[$i]['gender_text'] = '小姐';
			}
			
			$rows2[] = array(
				$this->datatable->actions($action_array),
				$rows1[$i]['id'],
				$rows1[$i]['name'],
				'<a class="btn btn-primary use_member" href="javascript:;" value="'.$rows1[$i]['id'].'">使用身份</a>',
				$rows1[$i]['gender_text'],
				$rows1[$i]['mobile'],
				$rows1[$i]['insert_time'] ? date('Y/m/d H:i:s', $rows1[$i]['insert_time']) : '',
			);
		}
		
		$output = array(
			"aaData"					=> $rows2,
			"iTotalRecords" 			=> $iTotalRecords,
			"iTotalDisplayRecords"		=> $iTotalDisplayRecords,
		);
		
		echo json_encode($output);
		exit;
	}
	
	public function item($id){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_item');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			//Save
			$here = $_POST['here'];
			$id = $_POST['id'];
			$name = $_POST['name'];
			$account = strtolower($_POST['account']);
			//$password = $_POST['password'] ? md5($_POST['password']) : '';
			//$password = $this->ion_auth_model->hash_password($_POST['password'], FALSE);
			$password = $_POST['password'];
			$gender = $_POST['gender'];
			$birth = $_POST['birth'] ? strtotime($_POST['birth']) : '';
			$display = $_POST['display'] ? 1 : 0;
			$checked = $_POST['checked'] ? 1 : 0;
			
			$city_id = $_POST['city_id'];
			$area_id = $_POST['area_id'];
			$address = $_POST['address'];
			$mobile = $_POST['mobile'];
			
			if($this->form_validation->run($this->settings['validation_rule_group']) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				$this->template->views_admin('template/error', $data, true);
				exit;
			}
			
			if(!$id){
				//新增
				
				$rows1 = array(
					'name'			=>	ci_sql_string($name, 'text'),
					'account'		=>	ci_sql_string($account, 'text'),
					'password'		=>	ci_sql_string($password, 'text'),
					'gender'		=>	ci_sql_string($gender, 'text'),
					'birth'			=>	ci_sql_string($birth, 'text'),
					
					'display'		=>	ci_sql_string($display, 'text'),
					'checked'		=>	ci_sql_string($checked, 'text'),
					'city_id'		=>	ci_sql_string($city_id, 'text'),
					'area_id'		=>	ci_sql_string($area_id, 'text'),
					'address'		=>	ci_sql_string($address, 'text'),
					
					'mobile'		=>	ci_sql_string($mobile, 'text'),
					'insert_time'	=>	time(),
					'modify_time'	=>	time()
				);
				$id = $this->member_query->register($rows1);
				
			}else{
				//修改
				
				$rows1 = array(
					'name'			=>	ci_sql_string($name, 'text'),
					'account'		=>	ci_sql_string($account, 'text'),
					'password'		=>	ci_sql_string($password, 'text'),
					'gender'		=>	ci_sql_string($gender, 'text'),
					'birth'			=>	ci_sql_string($birth, 'text'),
					
					'display'		=>	ci_sql_string($display, 'text'),
					'checked'		=>	ci_sql_string($checked, 'text'),
					'city_id'		=>	ci_sql_string($city_id, 'text'),
					'area_id'		=>	ci_sql_string($area_id, 'text'),
					'address'		=>	ci_sql_string($address, 'text'),
					
					'mobile'		=>	ci_sql_string($mobile, 'text'),
					'modify_time'	=>	time(),
				);
				if(!$rows1['password']){
					unset($rows1['password']);
				}
				$this->member_query->update($rows1, $id);
				
			}
			if($here==3){
				redirect($this->settings['path']['list'], 'refresh');
			}else{
				$data = array(
					'id'		=>	$id,
				);
				echo json_encode($data);
			}
			
			exit;
		}
		
		$validation_rule = $this->config->item($this->settings['validation_rule_group']);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1', array('submit_handler'	=>	'handler_admin_member',));
		
		$data = array(
			'id'			=>	$id,
		);
		$rows1 = $this->$model_query->select($data);
		
		if($rows1['birth']){
			$rows1['birth_text'] = date('Y/m/d', $rows1['birth']);
		}
		
		if($rows1['id']){
			$Website['page_header'] = $rows1['name'];
		}
		
		//city
		$sql = sprintf("SELECT A.*
						FROM %s A 
						ORDER BY A.sort asc
						",
						$Website['table']['zip_city']
						);
		$rows2 = $this->query->select($sql);
		
		if($rows1['city_id']){
			$sql = sprintf("SELECT A.*
							FROM %s A 
							WHERE A.city_id=%s
							ORDER BY A.sort asc
							",
							$Website['table']['zip_area'],
							sql_string($rows1['city_id'], "text")
							);
			$rows3 = $this->query->select($sql);
		}
		
		
		$data = array(
			'Website'		=>	$Website,
			'settings'		=>	$this->settings,
			'validation'	=>	$jquery_validation,
			'rows1'			=>	$rows1,
			'rows2'			=>	$rows2,
			'rows3'			=>	$rows3,
		);
		$this->template->views_admin($this->settings['html']['item'], $data);
	}
	
	public function column_visible(){
		$Website = $this->Website;
		//print_r($_GET);
		$column = $_GET['column'];
		$column_visible = $this->settings['column_visible'];
		for($i=1; $i<sizeof($column_visible); $i++){
			if(in_array($i, $column)){
				$column_visible[$i] = 1;
			}else{
				$column_visible[$i] = 0;
			}
		}
		$_SESSION['column_visible'][$this->current_class] = $this->settings['column_visible'] = $column_visible;
	}
	
	private function adv_search(){
		$Website = $this->Website;
		
		if(!$this->settings['adv_search_status']){
			return 0;
		}
		
		if($_SESSION['column_visible'][$this->current_class]){
			$this->settings['column_visible'] = $_SESSION['column_visible'][$this->current_class];
		}
		
		//city
		$sql = sprintf("SELECT A.*
						FROM %s A 
						ORDER BY A.sort asc
						",
						$Website['table']['zip_city']
						);
		$rows2 = $this->query->select($sql);
		
		$data = array(
			'Website'	=>	$Website,
			'setting'	=>	$this->settings,
			'rows1'		=>	$_SESSION['adv_search'][$this->current_class],
			'rows2'		=>	$rows2,
		);
		return $this->load->view($this->settings['html']['adv_search'], $data, true);
	}
	
	private function adv_search_sql($q, $g){
		//進階搜尋
		
		$_SESSION['adv_search'][$this->current_class] = $g;
		
		if($g['search']){
			$s[] = sprintf(' (A.id=%s or 
								A.account like %s or
								A.name like %s or 
								A.address like %s or
								A.mobile like %s
								)',
				sql_string($g['search'], 'int'),
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text')
			);
		}
		
		switch($g['gender']){
			case 1:
				$s[] = sprintf(' A.gender=1 ');
				break;
			case 2:
				$s[] = sprintf(' A.gender=2 ');
				break;
		}
		
		if($g['city_id']){
			$s[] = sprintf(' A.city_id=%s ', $g['city_id']);
		}
		
		if($g['area_id']){
			$s[] = sprintf(' A.area_id=%s ', $g['area_id']);
		}
		
		//生日
		if($g['start_date'] && $g['end_date']){
			$s[] = sprintf(' A.birth >= %s and A.birth<=%s ',
				strtotime($g['start_date']),
				strtotime($g['end_date'])+86400
			);
		}else{
			if($g['start_date']){
				$s[] = sprintf(' A.birth >= %s and A.birth<=%s ',
					strtotime($g['start_date']),
					strtotime($g['start_date'])+86400
				);
			}
			if($g['end_date']){
				$s[] = sprintf(' A.birth >= %s and A.birth<=%s ',
					strtotime($g['end_date'])-86400,
					strtotime($g['end_date'])
				);
			}
		}
		
		if(sizeof($s)){
			if($q['sWhere']){
				$q['sWhere'] .= sprintf(' and %s', implode(' and ', $s));
			}else{
				$q['sWhere'] = sprintf(' WHERE %s', implode(' and ', $s));
			}
		}
		return $q;
	}
	
	public function ajax_account_check(){
		//確認註冊
		$Website = $this->Website;
		$account = strtolower($_POST['account']);
		$member_id = $_POST['member_id'];
		
		if(!$account){
			echo 'false';
			exit;
		}
		
		if($member_id){
			$sql = sprintf("SELECT A.id
							FROM %s A
							WHERE A.account=%s and A.id!=%s
							LIMIT 1",
							$Website['table']['member'],
							sql_string($account, "text"),
							sql_string($member_id, "text")
							);
		}else{
			$sql = sprintf("SELECT A.id
							FROM %s A
							WHERE A.account=%s
							LIMIT 1",
							$Website['table']['member'],
							sql_string($account, "text")
							);
		}
		$rows1 = $this->query->select($sql, 1);
		
		if(!sizeof($rows1)){
			//不存在
			echo 'true';
		}else{
			//存在
			echo 'false';
		}
		exit;
	}
	
	public function ajax_mobile_check(){
		//確認註冊
		$Website = $this->Website;
		$mobile = $_POST['mobile'];
		$member_id = $_POST['member_id'];
		
		if(!$mobile){
			echo 'false';
			exit;
		}
		
		if($member_id){
			$sql = sprintf("SELECT A.id
							FROM %s A
							WHERE A.mobile=%s and A.id!=%s
							LIMIT 1",
							$Website['table']['member'],
							sql_string($mobile, "text"),
							sql_string($member_id, "text")
							);
		}else{
			$sql = sprintf("SELECT A.id
							FROM %s A
							WHERE A.mobile=%s
							LIMIT 1",
							$Website['table']['member'],
							sql_string($mobile, "text")
							);
		}
		$rows1 = $this->query->select($sql, 1);
		
		if(!sizeof($rows1)){
			//不存在
			echo 'true';
		}else{
			//存在
			echo 'false';
		}
		exit;
	}
	
	public function ajax_export(){
		$Website = $this->Website;
		//echo $_SESSION['sql']['export']['member'];exit;
		
		$rows1 = $this->query->select($_SESSION['sql']['export']['member']);
		//echo '<pre>';print_r($rows1);exit;
		
		$rows2[] = array(
			'ID',
			'姓名',
			'帳號',
			'性別',
			'生日',
			'城市',
			'地區',
			'地址',
			'手機',
			'註冊時間',
		);
		
		//exit;
		for($i=0; $i<sizeof($rows1); $i++){
			
			
			$sql = sprintf("SELECT A.name city_text, B.name area_text
							FROM %s A
							LEFT JOIN %s B ON B.id=%s
							WHERE A.id=%s
							
							",
							$Website['table']['zip_city'],
							$Website['table']['zip_area'],
							sql_string($rows1[$i]['city_id'], "int"),
							sql_string($rows1[$i]['area_id'], "int")
							);
			$rows3 = $this->query->select($sql, 1);
			$city_text = $rows3['city_text'];
			$area_text = $rows3['area_text'];
			
			
			
			switch($rows1[$i]['gender']){
				default:
					$gender_text = '';
					break;
				case 1:
					$gender_text = '先生';
					break;
				case 2:
					$gender_text = '小姐';
					break;
			}
			
			
			$rows2[] = array(
				$rows1[$i]['id'], 				//'ID',
				$rows1[$i]['name'], 				//'姓名',
				$rows1[$i]['account'], 			//'帳號',
				$gender_text, 					//'性別',
				$rows1[$i]['birth'] ? date('Y/m/d', $rows1[$i]['birth']) : '', 					//'生日',
				$city_text, 					//'城市',
				$area_text, 					//'地區',
				$rows1[$i]['address'], 			//'地址',
				$rows1[$i]['mobile'], 			//'手機',
				$rows1[$i]['insert_time'] ? date('Y/m/d H:i:s') : '', 							//'註冊時間',
			);
		}
		
		//echo '<pre>';print_r($rows2);exit;
		
		$file = '會員資料 - '.date('Y.m.d H.i.s', time());
		$filename = $file.'.csv';
		$this->csv->array_to_csv($filename, $rows2);
	}
	
}
?>