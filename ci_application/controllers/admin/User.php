<?
class User extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		$this->settings = array(
								'btn'				=>	array(			//列表下方按鈕
									'insert'			=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'btn_txt'			=>	array(			//自行定義按鈕名稱
									'insert'			=>	'',
									'display'			=>	'',
									'sort'				=>	'',
								),
								'icon'				=>	array(			//自行增加icon數量，但須於datatable中的actions中新增項目
									'edit'				=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									'ajax'				=>	base_url().'admin/'.$this->current_class.'/ajax/',
									'item'				=>	base_url().'admin/'.$this->current_class.'/item/',
								),
			
								'orderby'			=>	array(array(1, "asc")),
								'order_columns'		=>	array('', 'id', 'username', 'email'),
								'search_columns'	=>	array('username', 'email'),
								//'table_columns'		=>	array('actions', 'id', 'name', '帳號'),
								'table_columns'		=>	array(my_lang('action'), my_lang('id'), my_lang('name'), my_lang('account')),
								'column_visible'	=>	array(1, 1, 1, 1),
								'column'		=>	array(
									array('width'		=>'130px', 	'targets'	=>	0),
									array('width'		=>'60px', 	'targets'	=>	1),
									array('orderable'	=>false, 	'targets'	=>	array(0, 3)),
								),
								
								'html'				=>	array(
									'list'				=>	'template/list',
									'item'				=>	$this->current_class.'/item',
									'adv_search'		=>	'admin/adv_search/'.$this->current_class,
								),
								'model'				=>	'user_query',
								
								//datatable
								'adv_search_status'	=>	0,
								'adv_search'		=>	array(
									'vars'			=>	array(
										'display',
										'search'
									),
								),
								
								//validation rule group
								'validation_rule_group'	=>	'admin/'.$this->current_class.'/item',
							);
		$this->Website['tables'] = array(
			$this->Website['table']['user'],
		);
			
		//確定編輯權限
		$this->settings = $this->menu->check_editable($this->settings);
		
		//進階搜尋設定
		$this->settings['adv_search']['html'] = $this->adv_search($this->settings);
		
		//下方btn設定
		$this->settings['button'] = $this->datatable->button($this->settings);
		
		
	}
	
	public function index(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$here = $_POST['here'];
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_list');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$id = $_POST['id'];
			$show = $_POST['display'];
			$delete = $_POST['delete'];
			
			switch($here){
				case 1:
					/*
						上下架顯示
					*/
					$this->$model_query->display_update($id, $show);
					
					/*
						刪除
					*/
					$this->$model_query->delete_update($id, $delete);
					break;
				case 2:
					/*
						排序
					*/
					$this->$model_query->sort_update();
					break;
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		$data = array(
			'Website'	=>	$Website,
			'settings'	=>	$this->settings,
			'rows1'		=>	$rows1,
		);
		
		$this->template->views_admin($this->settings['html']['list'], $data);
	}
	
	public function ajax(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		$q = $this->datatable->query(array(
											$this->settings['order_columns'], 
											$this->settings['search_columns'], 
											$this->settings
										), 
										$_GET);
		
		$iTotalRecords = $this->$model_query->table_total();
		$iTotalDisplayRecords = $this->$model_query->display_total($q);
		$rows1 = $this->$model_query->datatable_select($q);
		
		$rows2 = array();
		for($i=0; $i<sizeof($rows1); $i++){
			unset($group_name);
			
			$sql = sprintf("SELECT B.name
							FROM %s A 
							LEFT JOIN %s B ON A.group_id=B.id
							WHERE A.user_id=%s
							",
							$Website['table']['user_group'],
							$Website['table']['group'],
							$rows1[$i]['id']
							);
			$rows3 = $this->query->select($sql);
			//print_r($rows3);
			for($j=0; $j<sizeof($rows3); $j++){
				$group_name[] = $rows3[$j]['name'];
			}
			
			$action_array = array(
								'id'				=>	$rows1[$i]['id'],
								'checked'			=>	$Website['checked'][(int)$rows1[$i]['active']],
								'display_class'		=>	$Website['display_class'][(int)$rows1[$i]['active']],
								'edit'				=>	'admin/'.$this->current_class.'/item/'.$rows1[$i]['id'].'/',
								'icon'				=>	$this->settings['icon'],
							);
			$rows2[] = array(
				$this->datatable->actions($action_array),
				$rows1[$i]['id'],
				$rows1[$i]['username'],
				$rows1[$i]['email'],
				implode(', ', $group_name)
			);
		}
		
		$output = array(
			"aaData"					=> $rows2,
			"iTotalRecords" 			=> $iTotalRecords,
			"iTotalDisplayRecords"	=> $iTotalDisplayRecords,
		);
		
		echo json_encode($output);
		exit;
	}
	
	public function item($id){
		
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$here = $_POST['here'];
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_item');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$_POST = $this->input->post(NULL, TRUE);
			//Save
			$name = $_POST['name'];
			$password = $_POST['password'];
			$email = strtolower($_POST['account']);
			$display = $_POST['display'] ? 1 : 0;
			$additional_data = array(
				'active'     => $display,
				'company'    => $this->input->post('company'),
				'phone'      => $this->input->post('phone'),
			);
			
			if($this->form_validation->run($this->settings['validation_rule_group']) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				$this->template->views_admin('template/error', $data, true);
				exit;
			}
			
			
			//print_r($additional_data);exit;
			if(!$id){
				$id = $this->ion_auth->register($name, $password, $email, $additional_data);
			}else{
				
				if($this->input->post('password')){
					$additional_data['password'] = $this->input->post('password');
				}
				$additional_data['username'] = $name;
				$additional_data['email'] = $email;
				//print_r($additional_data);
				$this->ion_auth->update($id, $additional_data);
				//echo $this->db->last_query();exit;
			}
			
			$groupData = $this->input->post('groups');
			if (isset($groupData) && !empty($groupData)) {
				$this->ion_auth->remove_from_group('', $id);
				foreach ($groupData as $grp) {
					$this->ion_auth->add_to_group($grp, $id);
				}
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$validation_rule = $this->config->item($this->settings['validation_rule_group']);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1');
		
		$data = array(
			'id'			=>	$id,
		);
		$rows1 = $this->$model_query->select($data);
		$groups=$this->ion_auth->groups()->result_array();
		if($id){
			$currentGroups = $this->ion_auth->get_users_groups($id)->result();
			$Website['page_header'] = $rows1['username'];
			
			$sql = sprintf("SELECT A.* 
							FROM %s A
							WHERE A.user_id=%s or A.login=%s
							ORDER BY A.id desc
							LIMIT 30
							",
							$Website['table']['login_attempts'],
							sql_string($id, 'int'),
							sql_string($rows1['email'], 'text')
							);//echo $sql;exit;
			$rows4 = $this->query->select($sql);
		}
		//$settings['breadcrumbs_item'] = $this->breadcrumbs_item($rows1, $this->settings);
		
		
		
		$data = array(
			'Website'		=>	$Website,
			'settings'		=>	$this->settings,
			'rows2'			=>	$groups,
			'rows1'			=>	$rows1,
			'rows3'			=>	$currentGroups,
			'rows4'			=>	$rows4,
			'validation'	=>	$jquery_validation
		);
		$this->template->views_admin($this->settings['html']['item'], $data);
	}
	
	private function adv_search($setting){
		$Website = $this->Website;
		
		if(!$this->settings['adv_search_status']){
			return 0;
		}
		
		$data = array(
			'Website'	=>	$Website,
			'rows1'		=>	$_SESSION['adv_search'][$this->current_class],
		);
		return $this->load->view($this->settings['html']['adv_search'], $data, true);
	}
	
	private function adv_search_sql($q, $g){
		//進階搜尋
		
		$_SESSION['adv_search'][$this->current_class] = $g;
		
		if($g['search']){
			$s[] = sprintf(' (A.name like %s or A.content like %s)',
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text')
			);
		}
		switch($g['display']){
			case 1:
				$s[] = sprintf(' A.display=0 ');
				break;
			case 2:
				$s[] = sprintf(' A.display=1 ');
				break;
			case 3:
				break;
		}
		if(sizeof($s)){
			if($q['sWhere']){
				$q['sWhere'] .= sprintf(' and %s', implode(' and ', $s));
			}else{
				$q['sWhere'] = sprintf(' WHERE %s', implode(' and ', $s));
			}
		}
		return $q;
	}
}
?>