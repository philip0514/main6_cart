<?
class Order extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		$this->settings = array(
								'btn'				=>	array(			//列表下方按鈕
									'insert'			=>	0,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'btn_txt'			=>	array(			//自行定義按鈕名稱
									'insert'			=>	'',
									'display'			=>	'',
									'sort'				=>	'',
								),
								'icon'				=>	array(			//自行增加icon數量，但須於datatable中的actions中新增項目
									'edit'				=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									'ajax'				=>	base_url().'admin/'.$this->current_class.'/ajax/',
									'item'				=>	base_url().'admin/'.$this->current_class.'/item/',
								),
								'orderby'			=>	array(array(1, "desc")),
								'order_columns'		=>	array('', 'id', 'insert_time', 'buyer_name', 'total_price', 'payment_type', 'payment_status', 'shipping_type', 'shipping_status'),
								'search_columns'	=>	array('buyer_name'),
								'table_columns'		=>	array(my_lang('action'), my_lang('id'), '訂單時間', '購買人姓名', '總金額', '付款方式', '付款狀態', '寄送方式', '寄送狀態'),
								'column_visible'	=>	array(1, 1, 1, 1, 1, 1, 1, 1, 1),
								'column'			=>	array(
									array('width'		=>	'130px', 	'targets'	=>	0),
									array('width'		=>	'60px', 	'targets'	=>	1),
									array('orderable'	=>	false, 		'targets'	=>	array(0)),
								),
								
								'html'				=>	array(
									'list'				=>	'template/list',
									'item'				=>	$this->current_class.'/item',
									'adv_search'		=>	'admin/adv_search/'.$this->current_class,
								),
								'model'				=>	'order_query',
								
								//datatable
								'adv_search_status'	=>	1,
								'adv_search'		=>	array(
									'vars'			=>	array(
										'search',
										'payment_type',
										'payment_status',
										'start_insert_date',
										'end_insert_date',
										'start_payment_date',
										'end_payment_date',
									),
								),
								
								//image relation
								'item_type'			=>	array(
									//'about_ogimage'
								),
								
								//validation rule group
								'validation_rule_group'	=>	'admin/'.$this->current_class.'/item',
							);
		$this->Website['tables'] = array(
			$this->Website['table']['orders'],
		);
		
		//確定編輯權限
		$this->settings = $this->menu->check_editable($this->settings);
		
		//進階搜尋設定
		$this->settings['adv_search']['html'] = $this->adv_search();
		
		//下方btn設定
		$this->settings['button']  = $this->datatable->button($this->settings);
	}
	
	public function index(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_list');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			$id = $_POST['id'];
			$show = $_POST['display'];
			$delete = $_POST['delete'];
			
			switch($here){
				case 1:
					/*
						display or not
					*/
					$this->$model_query->display_update($id, $show);
					
					/*
						delete
					*/
					$this->$model_query->delete_update($id, $delete);
					break;
				case 2:
					/*
						sort
					*/
					$this->$model_query->sort_update();
					break;
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$data = array(
			'Website'	=>	$Website,
			'settings'	=>	$this->settings,
			'rows1'		=>	$rows1,
		);
		$this->template->views_admin($this->settings['html']['list'], $data);
	}
	
	public function ajax(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		$q = $this->datatable->query(array(
											$this->settings['order_columns'], 
											$this->settings['search_columns'], 
											$this->settings
										), 
										$_GET);
		
		$q = $this->adv_search_sql($q, $_GET);
		$iTotalRecords = $this->$model_query->table_total();
		$iTotalDisplayRecords = $this->$model_query->display_total($q);
		$rows1 = $this->$model_query->datatable_select($q);
		
		$rows2 = array();
		for($i=0; $i<sizeof($rows1); $i++){
			
			$icon_setting = $this->settings['icon'];
			
			if(in_array($rows1[$i]['id'], array())){
				//設定部份icon項目不顯示
				//$icon_setting['delete'] = 0;
			}
			if($q['sWhere']){
				//當有搜尋項目時，不予排序
				$icon_setting['sort'] = 0;
			}
			
			$action_array = array(
								'id'				=>	$rows1[$i]['id'],
								'checked'			=>	$Website['checked'][(int)$rows1[$i]['display']],
								'display_class'		=>	$Website['display_class'][(int)$rows1[$i]['display']],
								'edit'				=>	'admin/'.$this->current_class.'/item/'.$rows1[$i]['id'].'/',
								'icon'				=>	$icon_setting,
							);
			$rows2[] = array(
				$this->datatable->actions($action_array),
				$rows1[$i]['id'],
				date('Y/m/d H:i:s', $rows1[$i]['insert_time']),
				$rows1[$i]['buyer_name'],
				$rows1[$i]['total_price'],
				$Website['payment_type'][$rows1[$i]['payment_type']]['name'],
				$Website['payment_status'][$rows1[$i]['payment_status']]['name'],
				$Website['shipping_type'][$rows1[$i]['shipping_type']]['name'],
				$Website['shipping_status'][$rows1[$i]['shipping_status']]['name'],
			);
		}
		
		$output = array(
			"aaData"					=> $rows2,
			"iTotalRecords" 			=> $iTotalRecords,
			"iTotalDisplayRecords"		=> $iTotalDisplayRecords,
		);
		
		echo json_encode($output);
		exit;
	}
	
	public function item($id){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_item');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			//Save
			$id = $_POST['id'];
			$display = $_POST['display'] ? 1 : 0;
			

			//如果是下拉式選地址，則重新取得地址名稱，並寫入資料庫
			$buyer_city_id = $_POST['buyer_city_id'];
			if($buyer_city_id){
				$sql = sprintf("SELECT A.*
								FROM %s A 
								WHERE A.id=%s
								ORDER BY A.sort asc
								LIMIT 1
								",
								$Website['table']['zip_city'],
								sql_string($buyer_city_id, 'int')
								);
				$rows2 = $this->query->select($sql, 1);
				$_POST['buyer_city_id'] = $rows2['name'];
				
				//area
				$buyer_area_id = $_POST['buyer_area_id']?$_POST['buyer_area_id']:$rows2[0]['id'];
				$sql = sprintf("SELECT A.*
								FROM %s A 
								WHERE A.id=%s
								ORDER BY A.sort asc
								LIMIT 1
								",
								$Website['table']['zip_area'],
								sql_string($buyer_area_id, "text")
								);
				$rows3 = $this->query->select($sql, 1);
				$_POST['buyer_area'] = $rows3['name'];
				$_POST['buyer_zip'] = $rows3['code'];
			}

			$recipient_city_id = $_POST['recipient_city_id'];
			if($recipient_city_id){
				$sql = sprintf("SELECT A.*
								FROM %s A 
								WHERE A.id=%s
								ORDER BY A.sort asc
								LIMIT 1
								",
								$Website['table']['zip_city'],
								sql_string($recipient_city_id, 'int')
								);
				$rows2 = $this->query->select($sql, 1);
				$_POST['recipient_city'] = $rows2['name'];

				//area
				$recipient_area_id = $_POST['recipient_area_id']?$_POST['recipient_area_id']:$rows2[0]['id'];
				$sql = sprintf("SELECT A.*
								FROM %s A 
								WHERE A.id=%s
								ORDER BY A.sort asc
								LIMIT 1
								",
								$Website['table']['zip_area'],
								sql_string($recipient_area_id, "text")
								);
				$rows3 = $this->query->select($sql, 1);
				$_POST['recipient_area'] = $rows3['name'];
				$_POST['recipient_zip'] = $rows3['code'];
			}

			$data = array(
				'payment_type'			=>	ci_sql_string($_POST['payment_type'], "text"),
				'payment_status'		=>	ci_sql_string($_POST['payment_status'], "text"),
				'display'				=>	ci_sql_string($display, "text"),
				'payment_date'			=>	ci_sql_string(strtotime($_POST['payment_date']), "text"),
				'discount_price'		=>	ci_sql_string($_POST['discount_price'], "text"),
				'freight'				=>	ci_sql_string($_POST['freight'], "text"),
				'total_price'			=>	ci_sql_string($_POST['total_price'], "text"),
				'invoice'				=>	ci_sql_string($_POST['invoice'], "text"),
				'invoice_donate'		=>	ci_sql_string($_POST['invoice_donate'], "text"),
				'invoice_duplicate'		=>	ci_sql_string($_POST['invoice_duplicate'], "text"),
				'invoice_mobile'		=>	ci_sql_string($_POST['invoice_mobile'], "text"),
				'invoice_emcard'		=>	ci_sql_string($_POST['invoice_emcard'], "text"),
				'invoice_guid'			=>	ci_sql_string($_POST['invoice_guid'], "text"),
				'invoice_title'			=>	ci_sql_string($_POST['invoice_title'], "text"),
				
				'buyer_name'			=>	ci_sql_string($_POST['buyer_name'], "text"),
				'buyer_zip'				=>	ci_sql_string($_POST['buyer_zip'], "text"),
				'buyer_city_id'			=>	ci_sql_string($buyer_city_id, "text"),
				'buyer_city'			=>	ci_sql_string($_POST['buyer_city'], "text"),
				'buyer_area_id'			=>	ci_sql_string($buyer_area_id, "text"),
				'buyer_area'			=>	ci_sql_string($_POST['buyer_area'], "text"),
				'buyer_address'			=>	ci_sql_string($_POST['buyer_address'], "text"),
				'buyer_mobile'			=>	ci_sql_string($_POST['buyer_mobile'], "text"),
				'buyer_phone'			=>	ci_sql_string($_POST['buyer_phone'], "text"),
				
				'recipient_name'		=>	ci_sql_string($_POST['recipient_name'], "text"),
				'recipient_zip'			=>	ci_sql_string($_POST['recipient_zip'], "text"),
				'recipient_city_id'		=>	ci_sql_string($recipient_city_id, "text"),
				'recipient_city'		=>	ci_sql_string($_POST['recipient_city'], "text"),
				'recipient_area_id'		=>	ci_sql_string($recipient_area_id, "text"),
				'recipient_area'		=>	ci_sql_string($_POST['recipient_area'], "text"),
				'recipient_address'		=>	ci_sql_string($_POST['recipient_address'], "text"),
				'recipient_mobile'		=>	ci_sql_string($_POST['recipient_mobile'], "text"),
				'recipient_phone'		=>	ci_sql_string($_POST['recipient_phone'], "text"),
				'recipient_note'		=>	ci_sql_string($_POST['recipient_note'], "text"),
				
				'shipping_type'			=>	ci_sql_string($_POST['shipping_type'], "text"),
				'shipping_day'			=>	ci_sql_string($_POST['shipping_day'], "text"),
				'shipping_time'			=>	ci_sql_string($_POST['shipping_time'], "text"),
				'shipping_status'		=>	ci_sql_string($_POST['shipping_status'], "text"),
				'shipping_date'			=>	ci_sql_string(strtotime($_POST['shipping_date']), "text"),
				'restart_payment'		=>	ci_sql_string($_POST['restart_payment'], "text"),
				'restart_payment_end'	=>	ci_sql_string(strtotime($_POST['restart_payment_end']), "text"),
				'atm_number'			=>	ci_sql_string($_POST['atm_number'], "text"),
				'note_inside'			=>	ci_sql_string($_POST['note_inside'], "text"),
				
				'modify_time'			=>	time(),
			);
			$this->db->where('id', $id);
			$this->db->update($Website['table']['orders'], $data);
			
			//寄送出貨信件
			if(($_POST['shipping_status_original'] < $_POST['shipping_status']) && $_POST['shipping_status']==3){
			}
			
			//寄送取消訂單處理完成信件
			if(($_POST['payment_status_original'] < $_POST['payment_status']) && $_POST['payment_status']==8){
			}
			
			//訂單商品
			$quantity = $_POST['quantity'];
			$product_id = $_POST['product_id'];
			$size_id = $_POST['size_id'];
			
			//加回庫存
			if($_POST['payment_status']!=$_POST['payment_status_original'] && in_array($_POST['payment_status'], array(2, 5, 6, 11, 12))  &&  !in_array($_POST['payment_status_original'], array(2, 5, 6, 11, 12)) ){

				for($i=0; $i<sizeof($product_id); $i++){
					$sql = sprintf('UPDATE %s SET
									quantity=quantity+%s
									WHERE product_id=%s and spec_id=%s and type=0
									',
									$Website['table']['inventory'],
									sql_string($quantity[$i], "int"),
									sql_string($product_id[$i], "text"),
									sql_string($spec_id[$i], "int")
									);
					$this->query->sql($sql);
				}
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		
		
		
		$validation_rule = $this->config->item($this->settings['validation_rule_group']);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1');
		
		$data = array(
			'id'			=>	$id,
		);
		$rows1 = $this->$model_query->select($data);
		
		$Website['page_header'] = $rows1['buyer_name'].' '.date('Y/m/d H:i', $rows1['insert_time']);

		$sql = sprintf("SELECT A.*, B.name, C.name spec_name
						FROM %s A 
						LEFT JOIN %s B ON B.id=A.product_id
						LEFT JOIN %s C ON C.id=A.spec_id
						WHERE A.order_id=%s
						ORDER BY A.id asc
						",
						$Website['table']['inventory'],
						$Website['table']['product'],
						$Website['table']['spec'],
						sql_string($id, "text")
						);
		$rows2 = $this->query->select($sql);

		$sql = sprintf("SELECT A.*
						FROM %s A 
						WHERE A.order_id=%s
						ORDER BY A.id asc
						",
						$Website['table']['orders_payment'],
						sql_string($id, "text")
						);
		$rows3 = $this->query->select($sql);

		if($rows1['buyer_city_id'] || !$rows1['buyer_city']){
			//city
			$sql = sprintf("SELECT A.*
							FROM %s A 
							ORDER BY A.sort asc
							",
							$Website['table']['zip_city']
							);
			$rows4 = $this->query->select($sql);

			//area
			$rows1['buyer_city_id'] = $rows1['buyer_city_id'] ? $rows1['buyer_city_id'] : $rows4[0]['id'];
			$sql = sprintf("SELECT A.*
							FROM %s A 
							WHERE A.city_id=%s
							ORDER BY A.sort asc
							",
							$Website['table']['zip_area'],
							sql_string($rows1['buyer_city_id'], "text")
							);
			$rows5 = $this->query->select($sql);
		}

		if($rows1['recipient_city_id'] || !$rows1['recipient_city']){
			//city
			$sql = sprintf("SELECT A.*
							FROM %s A 
							ORDER BY A.sort asc
							",
							$Website['table']['zip_city']
							);
			$rows6 = $this->query->select($sql);

			//area
			$rows1['recipient_city_id'] = $rows1['recipient_city_id'] ? $rows1['recipient_city_id'] : $rows6[0]['id'];
			$sql = sprintf("SELECT A.*
							FROM %s A 
							WHERE A.city_id=%s
							ORDER BY A.sort asc
							",
							$Website['table']['zip_area'],
							sql_string($rows1['recipient_city_id'], "text")
							);
			$rows7 = $this->query->select($sql);
		}

		//coupon
		$sql = sprintf("SELECT B.name, B.discount, B.discount_type
						FROM %s A 
						LEFT JOIN %s B ON A.coupon_id=B.id
						WHERE A.order_id=%s
						",
						$Website['table']['member_coupon_relation'],
						$Website['table']['coupon'],
						sql_string($rows1['id'], "text")
						);
		$rows8 = $this->query->select($sql, 1);
		
		$payment_result = (array)json_decode($rows1['payment_result']);
		
		
		$data = array(
			'Website'					=>	$Website,
			'settings'					=>	$this->settings,
			'validation'				=>	$jquery_validation,
			'rows1'						=>	$rows1,
			'rows2'						=>	$rows2,
			'rows3'						=>	$rows3,
			'rows4'						=>	$rows4,
			'rows5'						=>	$rows5,
			'rows6'						=>	$rows6,
			'rows7'						=>	$rows7,
			'rows8'						=>	$rows8,
			'payment_result'			=>	$payment_result,
		);
		$this->template->views_admin($this->settings['html']['item'], $data);
	}
	
	public function column_visible(){
		$Website = $this->Website;
		
		$column = $_GET['column'];
		$column_visible = $this->settings['column_visible'];
		for($i=1; $i<sizeof($column_visible); $i++){
			if(in_array($i, $column)){
				$column_visible[$i] = 1;
			}else{
				$column_visible[$i] = 0;
			}
		}
		$_SESSION['column_visible'][$this->current_class] = $this->settings['column_visible'] = $column_visible;
	}
	
	private function adv_search(){
		$Website = $this->Website;
		
		if(!$this->settings['adv_search_status']){
			return 0;
		}
		
		if($_SESSION['column_visible'][$this->current_class]){
			$this->settings['column_visible'] = $_SESSION['column_visible'][$this->current_class];
		}
		
		$data = array(
			'Website'	=>	$Website,
			'setting'	=>	$this->settings,
			'rows1'		=>	$_SESSION['adv_search'][$this->current_class],
		);
		return $this->load->view($this->settings['html']['adv_search'], $data, true);
	}
	
	private function adv_search_sql($q, $g){
		//進階搜尋
		
		$_SESSION['adv_search'][$this->current_class] = $g;
		
		if($g['search']){
			$s[] = sprintf(' (A.buyer_name like %s)',
				sql_string('%'.$g['search'].'%', 'text')
			);
		}
		
		$g['payment_type'] = explode(',', $g['payment_type']);
		if(in_array(0, $g['payment_type']) || !sizeof($g['payment_type']) || !$g['payment_type']){
			
		}else{
			for($i=0; $i<sizeof($g['payment_type']); $i++){
				$payment_type[] = $g['payment_type'][$i]-1;
			}
			
			$s[] = sprintf(' (A.payment_type in (%s))',
				implode(',', $payment_type)
			);
		}
		
		$g['payment_status'] = explode(',', $g['payment_status']);
		if(in_array(0, $g['payment_status']) || !sizeof($g['payment_status']) || !$g['payment_status']){
			
		}else{
			for($i=0; $i<sizeof($g['payment_status']); $i++){
				$payment_status[] = $g['payment_status'][$i]-1;
			}
			
			$s[] = sprintf(' (A.payment_status in (%s))',
				implode(',', $payment_status)
			);
		}
		
		if($g['start_insert_date'] && $g['end_insert_date']){
			$s[] = sprintf(' A.insert_time >= %s and A.insert_time<=%s ',
				strtotime($g['start_insert_date']),
				strtotime($g['end_insert_date'])+86400
			);
		}else{
			if($g['start_insert_date']){
				$s[] = sprintf(' A.insert_time >= %s and A.insert_time<=%s ',
					strtotime($g['start_insert_date']),
					strtotime($g['start_insert_date'])+86400
				);
			}
			if($g['end_insert_date']){
				$s[] = sprintf(' A.insert_time >= %s and A.insert_time<=%s ',
					strtotime($g['end_insert_date'])-86400,
					strtotime($g['end_insert_date'])
				);
			}
		}
		
		if($g['start_payment_date'] && $g['end_payment_date']){
			$s[] = sprintf(' A.payment_date >= %s and A.payment_date<=%s ',
				strtotime($g['start_payment_date']),
				strtotime($g['end_payment_date'])+86400
			);
		}else{
			if($g['start_payment_date']){
				$s[] = sprintf(' A.payment_date >= %s and A.payment_date<=%s ',
					strtotime($g['start_payment_date']),
					strtotime($g['start_payment_date'])+86400
				);
			}
			if($g['end_payment_date']){
				$s[] = sprintf(' A.payment_date >= %s and A.payment_date<=%s ',
					strtotime($g['end_payment_date'])-86400,
					strtotime($g['end_payment_date'])
				);
			}
		}
		
		if(sizeof($s)){
			if($q['sWhere']){
				$q['sWhere'] .= sprintf(' and %s', implode(' and ', $s));
			}else{
				$q['sWhere'] = sprintf(' WHERE %s', implode(' and ', $s));
			}
		}
		return $q;
	}
	
}
?>