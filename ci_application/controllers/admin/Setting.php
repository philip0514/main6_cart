<?
class Setting extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		$this->settings = array(
								'btn'				=>	array(		//列表下方按鈕
									'insert'			=>	0,
									'delete'			=>	0,
									'display'			=>	0,
									'sort'				=>	0,
								),
								'btn_txt'			=>	array(	//自行定義按鈕名稱
									'insert'			=>	'',
									'display'			=>	'',
									'sort'				=>	'',
								),
								'icon'				=>	array(		//自行增加icon數量，但須於datatable中的actions中新增項目
									'edit'				=>	0,
									'delete'			=>	0,
									'display'			=>	0,
									'sort'				=>	0,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									//'ajax'	=>	base_url().'admin/'.$this->current_class.'/ajax/',
									//'item'	=>	base_url().'admin/'.$this->current_class.'/item/',
								),
								'unsort'			=>	array(0, 1, 2, 3),
								'orderby'			=>	array(array(1, "asc")),
								'order_columns'		=>	array('', 'sort', 'id', 'name'),
								'search_columns'	=>	array('name', 'content'),
								
								'html'				=>	array(
									'list'				=>	$this->current_class.'/list',
									'item'				=>	$this->current_class.'/item',
									'adv_search'		=>	'admin/adv_search/'.$this->current_class,
								),
								'model'				=>	'global_query',
								
								//datatable
								'sdom'				=>	"<'row'<'col-md-6'<'adv_search'>><'col-md-6'l<'clear_both'p>>r>t<'row'<'col-md-4'i><'col-md-8'p>>",
								'adv_search_status'	=>	1,
								'adv_search'		=>	array(
									'vars'				=>	array(
										'display',
										'search'
									),
								),
								'item_type'			=>	array(
									'site_ogimage'
								),
							);
		$this->Website['tables'] = array(
			$this->Website['table']['setting'],
		);
		
		//確定編輯權限
		$this->settings = $this->menu->check_editable($this->settings);
		
		//下方btn設定
		//$this->settings['button'] = $this->datatable->button($this->settings);
		
		//進階搜尋設定
		//$this->settings['adv_search']['html'] = $this->adv_search($this->settings);
		
		
		
	}
	
	public function index(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		$id = 1;
			
		//vars
		$Website['page_header'] = $this->lang->line($this->current_class."_list");
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here 			= $_POST['here'];
			$title 			= $_POST['title'];
			$keyword 		= $_POST['keyword'];
			$description 	= $_POST['description'];
			
			$ogimage_input 	= $_POST['ogimage_input'];
			$ogimage_type 	= $this->settings['item_type'][0];
			
			
			$logo_input 	= $_POST['logo_input'];
			$logo2x_input 	= $_POST['logo2x_input'];
			$icon16_input 	= $_POST['icon16_input'];
			$icon57_input 	= $_POST['icon57_input'];
			$icon72_input 	= $_POST['icon72_input'];
			$icon114_input 	= $_POST['icon114_input'];
			$icon144_input 	= $_POST['icon144_input'];
			
			
			$facebook_login = $_POST['facebook_login'];
			$facebook_scope = $_POST['facebook_scope'];
			$facebook_appid = $_POST['facebook_appid'];
			
			$ga 			= $_POST['ga'];
			$page_js 		= $_POST['page_js'];
			
			$mail_on 		= $_POST['mail_on'];
			$mail_type 		= $_POST['mail_type'];
			$mail_host 		= $_POST['mail_host'];
			$mail_port 		= $_POST['mail_port'];
			$mail_username 	= $_POST['mail_username'];
			$mail_password 	= $_POST['mail_password'];
			$mail_charset 	= $_POST['mail_charset'];
			$mail_encoding 	= $_POST['mail_encoding'];
			$mail_secure 	= $_POST['mail_secure'];
			$mail_from 		= $_POST['mail_from'];
			$mail_fromname 	= $_POST['mail_fromname'];
			
			//媒體 ogimage
			$this->media_query->media_relation($id, $ogimage_input, $ogimage_type);
			
			//LOGO
			$this->media_query->media_relation($id, $logo_input, 'logo');
			$this->media_query->media_relation($id, $logo2x_input, 'logo2x');
			$this->media_query->media_relation($id, $icon16_input, 'icon16');
			$this->media_query->media_relation($id, $icon57_input, 'icon57');
			$this->media_query->media_relation($id, $icon72_input, 'icon72');
			$this->media_query->media_relation($id, $icon114_input, 'icon114');
			$this->media_query->media_relation($id, $icon144_input, 'icon144');
			
			
			
			
			
			
			
			$rows1 = array(
				'title'					=>	ci_sql_string($title, 'text'),
				'keyword'				=>	ci_sql_string($keyword, 'text'),
				'description'			=>	ci_sql_string($description, 'text'),
				'facebook_login'		=>	ci_sql_string($facebook_login, 'text'),
				'facebook_scope'		=>	ci_sql_string($facebook_scope, 'text'),
				'facebook_appid'		=>	ci_sql_string($facebook_appid, 'text'),
				'ga'					=>	ci_sql_string($ga, 'text'),
				'page_js'				=>	ci_sql_string($page_js, 'text'),
				'mail_on'				=>	ci_sql_string($mail_on, 'text'),
				'mail_type'				=>	ci_sql_string($mail_type, 'text'),
				'mail_host'				=>	ci_sql_string($mail_host, 'text'),
				'mail_port'				=>	ci_sql_string($mail_port, 'text'),
				'mail_username'			=>	ci_sql_string($mail_username, 'text'),
				'mail_password'			=>	ci_sql_string(base64_encode($mail_password), 'text'),
				'mail_charset'			=>	ci_sql_string($mail_charset, 'text'),
				'mail_encoding'			=>	ci_sql_string($mail_encoding, 'text'),
				'mail_secure'			=>	ci_sql_string($mail_secure, 'text'),
				'mail_from'				=>	ci_sql_string($mail_from, 'text'),
				'mail_fromname'			=>	ci_sql_string($mail_fromname, 'text'),
			);//print_r($rows1);exit;
			if(!$mail_password){
				unset($rows1['mail_password']);
			}
			
			$rows2 = $rows1;
			
			//GET OGIMAGE AND LOGO URL
			$rows2['ogimage'] = $this->media_query->image_url($id, $ogimage_type);
			$rows2['logo'] = $this->media_query->image_url($id, 'logo', 1);
			$rows2['logo2x'] = $this->media_query->image_url($id, 'logo2x', 1);
			$rows2['icon16'] = $this->media_query->image_url($id, 'icon16', 1);
			$rows2['icon57'] = $this->media_query->image_url($id, 'icon57', 1);
			$rows2['icon72'] = $this->media_query->image_url($id, 'icon72', 1);
			$rows2['icon114'] = $this->media_query->image_url($id, 'icon114', 1);
			$rows2['icon144'] = $this->media_query->image_url($id, 'icon144', 1);
			
			$rows1['json'] = json_encode($rows2);
			
			//$rows1['json'] = $rows2;
			//echo '<pre>';print_r($rows1);exit;
			
			$this->$model_query->update($rows1, $id);
			
			redirect('admin/setting/', 'refresh');
			exit;
		}
		
		
		$Website['page_header'] = '網站設定';
		$data = array(
			'id'			=>	$id,
		);
		$rows1 = $this->$model_query->select($data);
		
		/*
			社群分享圖片
		*/
		$ogimage = $this->media_query->media_select($id, $this->settings['item_type'][0]);
		$rows1['ogimage_input'] = $ogimage['input'];
		$rows1['ogimage_area'] = $ogimage['area'];
		
		$logo = $this->media_query->media_select($id, 'logo');
		$rows1['logo_input'] = $logo['input'];
		$rows1['logo_area'] = $logo['area'];
		
		$logo2x = $this->media_query->media_select($id, 'logo2x');
		$rows1['logo2x_input'] = $logo2x['input'];
		$rows1['logo2x_area'] = $logo2x['area'];
		
		$icon16 = $this->media_query->media_select($id, 'icon16');
		$rows1['icon16_input'] = $icon16['input'];
		$rows1['icon16_area'] = $icon16['area'];
		
		$icon57 = $this->media_query->media_select($id, 'icon57');
		$rows1['icon57_input'] = $icon57['input'];
		$rows1['icon57_area'] = $icon57['area'];
		
		$icon72 = $this->media_query->media_select($id, 'icon72');
		$rows1['icon72_input'] = $icon72['input'];
		$rows1['icon72_area'] = $icon72['area'];
		
		$icon114 = $this->media_query->media_select($id, 'icon114');
		$rows1['icon114_input'] = $icon114['input'];
		$rows1['icon114_area'] = $icon114['area'];
		
		$icon144 = $this->media_query->media_select($id, 'icon144');
		$rows1['icon144_input'] = $icon144['input'];
		$rows1['icon144_area'] = $icon144['area'];
		
		
		$data = array(
			'Website'	=>	$Website,
			'rows1'		=>	$rows1,
		);
		$this->template->views_admin($this->settings['html']['list'], $data);
	}
	
	public function mailtest(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		
		$id 			= 1;
		//$mail_on 		= $_POST['mail_on'];
		$mail_host 		= $_POST['mail_host'];
		$mail_port 		= $_POST['mail_port'];
		$mail_username 	= $_POST['mail_username'];
		$mail_password 	= $_POST['mail_password'];
		$mail_charset 	= $_POST['mail_charset'];
		$mail_encoding 	= $_POST['mail_encoding'];
		$mail_secure 	= $_POST['mail_secure'];
		$mail_from 		= $_POST['mail_from'];
		$mail_fromname 	= $_POST['mail_fromname'];
		$rows1 = array(
			//'mail_on'				=>	ci_sql_string($mail_on, 'text'),
			'mail_host'				=>	ci_sql_string($mail_host, 'text'),
			'mail_port'				=>	ci_sql_string($mail_port, 'text'),
			'mail_username'			=>	ci_sql_string($mail_username, 'text'),
			'mail_password'			=>	ci_sql_string(base64_encode($mail_password), 'text'),
			'mail_charset'			=>	ci_sql_string($mail_charset, 'text'),
			'mail_encoding'			=>	ci_sql_string($mail_encoding, 'text'),
			'mail_secure'			=>	ci_sql_string($mail_secure, 'text'),
			'mail_from'				=>	ci_sql_string($mail_from, 'text'),
			'mail_fromname'			=>	ci_sql_string($mail_fromname, 'text'),
		);//print_r($rows1);exit;
		if(!$mail_password){
			unset($rows1['mail_password']);
		}
		$this->$model_query->update($rows1, $id);
		
		$data = array(
			'type'			=>	'test',
			'mailto'		=>	$mail_from,
			'member_name'	=>	$mail_fromname,
			'rows1'			=>	array(
				'member_name'			=>	$mail_fromname,
				'current_time'			=>	time(),
			),
		);
		$this->mailer->to($data);
		
	}
	
	
}
?>