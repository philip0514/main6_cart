<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct(){
		parent::__construct();
		$this->preload->admin();
	}
	
	public function index(){
		$Website = $this->Website;
		$Website['page_header']	= '首頁';
		
		$data = array(
			'Website'	=>	$Website,
			'rows1'		=>	$Website['admin_srtucture'],
		);
		$this->template->views_admin('welcome/index', $data);
	}
	
	public function login($error){
		$Website = $this->Website;
		$Website['page_header']	=	'登入';
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			
			if($this->form_validation->run('admin/login') == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				$this->template->views_admin('template/error', $data, true);
				exit;
			}
			
			$MM_fldUserAuthorization = "";
			$MM_redirectLoginSuccess = 'admin/';
			$MM_redirectLoginFailed = "admin/login/error/";
			$MM_redirecttoReferrer = false;
			
			$status = $this->ion_auth->login($this->input->post('account'), $this->input->post('password'), $remember);
			//echo $status;exit;
			if($status){
				$list_url = $MM_redirectLoginSuccess;
				if($_SESSION['current_location']['backend']){
					$list_url = $_SESSION['current_location']['backend'];
				}
			}else{
				$list_url = $MM_redirectLoginFailed;
			}
			
			$nation = $_POST['language_setting'];
			$language = "zh_tw";
			for($i=0; $i<sizeof($Website['language']); $i++){
				if($Website['language'][$i]['code']==$nation){
					$language = $Website['language'][$i]['language'];
				}
			}
			$_SESSION['user_info']['site_lang'] = $language;
			
			
			redirect($list_url, 'refresh');
			
			exit;
		}
		
		$validation_rule = $this->config->item('admin/login');
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1');
		
		$csrf = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
		
		$data = array(
			'Website'		=>	$Website,
			'validation'	=>	$jquery_validation,
			'error'			=>	$error,
			'csrf'			=>	$csrf,
		);
		$this->template->views_admin_login('login/index', $data);
	}
	
	public function logout(){
		$Website = $this->Website;
		
		$list_url = "admin/login/";
		unset($_SESSION['user_info']);
		redirect($list_url, 'refresh');
	}
}
