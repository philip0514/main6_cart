<?
class Collection extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		$this->settings = array(
								'btn'				=>	array(			//列表下方按鈕
									'insert'			=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'btn_txt'			=>	array(			//自行定義按鈕名稱
									'insert'			=>	'',
									'display'			=>	'',
									'sort'				=>	'',
								),
								'icon'				=>	array(			//自行增加icon數量，但須於datatable中的actions中新增項目
									'edit'				=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									'ajax'				=>	base_url().'admin/'.$this->current_class.'/ajax/',
									'item'				=>	base_url().'admin/'.$this->current_class.'/item/',
								),
								'orderby'			=>	array(array(1, "desc")),
								'order_columns'		=>	array('', 'id', 'name'),
								'search_columns'	=>	array('name'),
								'table_columns'		=>	array(my_lang('action'), my_lang('id'), my_lang('name')),
								'column_visible'	=>	array(1, 1, 1),
								'column'			=>	array(
									array('width'		=>	'130px', 	'targets'	=>	0),
									array('width'		=>	'60px', 	'targets'	=>	1),
									array('orderable'	=>	false, 		'targets'	=>	array(0)),
								),
								
								'html'				=>	array(
									'list'				=>	'template/list',
									'item'				=>	$this->current_class.'/item',
									'adv_search'		=>	'admin/adv_search/'.$this->current_class,
								),
								'model'				=>	'collection_query',
								
								//datatable
								'adv_search_status'	=>	0,
								'adv_search'		=>	array(
									'vars'			=>	array(
										'display',
										'search'
									),
								),
								
								//image relation
								'item_type'			=>	array(
									'collection_ogimage'
								),
								
								//validation rule group
								'validation_rule_group'	=>	'admin/'.$this->current_class.'/item',
							);
		$this->Website['tables'] = array(
			$this->Website['table']['collection'],
		);
		
		//確定編輯權限
		$this->settings = $this->menu->check_editable($this->settings);
		
		//進階搜尋設定
		$this->settings['adv_search']['html'] = $this->adv_search();
		
		//下方btn設定
		$this->settings['button']  = $this->datatable->button($this->settings);
	}
	
	public function index(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_list');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			$id = $_POST['id'];
			$show = $_POST['display'];
			$delete = $_POST['delete'];
			
			switch($here){
				case 1:
					/*
						display or not
					*/
					$this->$model_query->display_update($id, $show);
					
					/*
						delete
					*/
					$this->$model_query->delete_update($id, $delete);
					break;
				case 2:
					/*
						sort
					*/
					$this->$model_query->sort_update();
					break;
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$data = array(
			'Website'	=>	$Website,
			'settings'	=>	$this->settings,
			'rows1'		=>	$rows1,
		);
		$this->template->views_admin($this->settings['html']['list'], $data);
	}
	
	public function ajax(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		$q = $this->datatable->query(array(
											$this->settings['order_columns'], 
											$this->settings['search_columns'], 
											$this->settings
										), 
										$_GET);
		
		$q = $this->adv_search_sql($q, $_GET);
		$iTotalRecords = $this->$model_query->table_total();
		$iTotalDisplayRecords = $this->$model_query->display_total($q);
		$rows1 = $this->$model_query->datatable_select($q);
		
		$rows2 = array();
		for($i=0; $i<sizeof($rows1); $i++){
			
			$icon_setting = $this->settings['icon'];
			
			if(in_array($rows1[$i]['id'], array())){
				//設定部份icon項目不顯示
				//$icon_setting['delete'] = 0;
			}
			if($q['sWhere']){
				//當有搜尋項目時，不予排序
				$icon_setting['sort'] = 0;
			}
			
			$action_array = array(
								'id'				=>	$rows1[$i]['id'],
								'checked'			=>	$Website['checked'][(int)$rows1[$i]['display']],
								'display_class'		=>	$Website['display_class'][(int)$rows1[$i]['display']],
								'edit'				=>	'admin/'.$this->current_class.'/item/'.$rows1[$i]['id'].'/',
								'icon'				=>	$icon_setting,
							);
			$rows2[] = array(
				$this->datatable->actions($action_array),
				$rows1[$i]['id'],
				$rows1[$i]['name'],
			);
		}
		
		$output = array(
			"aaData"					=> $rows2,
			"iTotalRecords" 			=> $iTotalRecords,
			"iTotalDisplayRecords"		=> $iTotalDisplayRecords,
		);
		
		echo json_encode($output);
		exit;
	}
	
	public function item($id){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_item');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			//Save
			$here = $_POST['here'];
			$id = $_POST['id'];
			$name = $_POST['name'];
			$start_time = strtotime($_POST['start_date']);
			$end_time = strtotime($_POST['end_date']);
			$url = $_POST['url'];
			$description = $_POST['description'];
			
			$display = $_POST['display'] ? 1 : 0;
			$ogimage_input = $_POST['ogimage_input'];
			$ogimage_type = $this->settings['item_type'][0];
			
			if($this->form_validation->run($this->settings['validation_rule_group']) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				$this->template->views_admin('template/error', $data, true);
				exit;
			}
			
			
			if(!$id){
				$rows1 = array(
					'name'			=>	ci_sql_string($name, 'text'),
					'description'	=>	ci_sql_string($description, 'text'),
					'start_time'	=>	ci_sql_string($start_time, 'text'),
					'end_time'		=>	ci_sql_string($end_time, 'text'),
					'url'			=>	ci_sql_string($url, 'text'),
					'display'		=>	ci_sql_string($display, 'int'),
					'insert_time'	=>	time(),
					'modify_time'	=>	time(),
				);
				$id = $this->$model_query->insert($this->settings, $rows1);
			}else{
				$rows1 = array(
					'name'			=>	ci_sql_string($name, 'text'),
					'description'	=>	ci_sql_string($description, 'text'),
					'start_time'	=>	ci_sql_string($start_time, 'text'),
					'end_time'		=>	ci_sql_string($end_time, 'text'),
					'url'			=>	ci_sql_string($url, 'text'),
					'display'		=>	ci_sql_string($display, 'int'),
					'modify_time'	=>	time(),
				);
				$this->$model_query->update($rows1, $id);
			}
			
			//媒體 ogimage
			$this->media_query->media_relation($id, $ogimage_input, $ogimage_type);
			
			
			$product_id = $_POST['product_id'];
			$sort = $_POST['sort'];
			
			for($i=0; $i<sizeof($product_id); $i++){
				$sql = sprintf("UPDATE %s SET sort=%s WHERE collection_id=%s and product_id=%s",
							  	$Website['table']['product_collection_relation'],
							   	sql_string($sort[$i], 'int'),
							   	sql_string($id, 'int'),
							   	sql_string($product_id[$i], 'int')
							  );
				$this->query->sql($sql);
			}
			
			
			
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$validation_rule = $this->config->item($this->settings['validation_rule_group']);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1');
		//$jquery_validation = $this->jquery_validation->run('#form1', array('submit_handler'	=>	'handler_admin_about',));
		
		$data = array(
			'id'			=>	$id,
		);
		$rows1 = $this->$model_query->select($data);
		if($id){
			$Website['page_header'] = $rows1['name'];
		
			/*
				社群分享圖片
			*/
			$ogimage = $this->media_query->media_select($id, $this->settings['item_type'][0]);
			$rows1['ogimage_input'] = $ogimage['input'];
			$rows1['ogimage_area'] = $ogimage['area'];
			
			$sql = sprintf("SELECT P.*, A.sort, B.file_url, B.name file_name
							FROM %s A
							LEFT JOIN %s P ON A.product_id=P.id
							LEFT JOIN %s B ON B.id = (SELECT C.media_id FROM %s C WHERE C.item_id=P.id and C.item_type=%s ORDER BY C.sort asc limit 1)
							WHERE 
								A.collection_id=%s
							ORDER BY A.sort asc
							",
							$Website['table']['product_collection_relation'],
							$Website['table']['product'],

							$Website['table']['media'],
							$Website['table']['media_relation'],
							sql_string('product', 'text'),
						   	
						   	$id
							);
			$rows2 = $this->query->select($sql);
			
			for($i=0; $i<sizeof($rows2); $i++){
				$data = array(
					'rows1'		=>	array(
						'product_id'	=>	$rows2[$i]['id'],
						'file_url'		=>	$rows2[$i]['file_url'],
						'name'			=>	$rows2[$i]['name'],
						'sort'			=>	$rows2[$i]['sort'],
					)
				);
				$html .= $this->load->view('admin/collection/box', $data, true);
			}
			
		}
		
		$data = array(
			'Website'		=>	$Website,
			'settings'		=>	$this->settings,
			'validation'	=>	$jquery_validation,
			'rows1'			=>	$rows1,
			'html'			=>	$html,
		);
		$this->template->views_admin($this->settings['html']['item'], $data);
	}
	
	public function column_visible(){
		$Website = $this->Website;
		
		$column = $_GET['column'];
		$column_visible = $this->settings['column_visible'];
		for($i=1; $i<sizeof($column_visible); $i++){
			if(in_array($i, $column)){
				$column_visible[$i] = 1;
			}else{
				$column_visible[$i] = 0;
			}
		}
		$_SESSION['column_visible'][$this->current_class] = $this->settings['column_visible'] = $column_visible;
	}
	
	private function adv_search(){
		$Website = $this->Website;
		
		if(!$this->settings['adv_search_status']){
			return 0;
		}
		
		if($_SESSION['column_visible'][$this->current_class]){
			$this->settings['column_visible'] = $_SESSION['column_visible'][$this->current_class];
		}
		
		$data = array(
			'Website'	=>	$Website,
			'setting'	=>	$this->settings,
			'rows1'		=>	$_SESSION['adv_search'][$this->current_class],
		);
		return $this->load->view($this->settings['html']['adv_search'], $data, true);
	}
	
	private function adv_search_sql($q, $g){
		//進階搜尋
		
		$_SESSION['adv_search'][$this->current_class] = $g;
		
		if($g['search']){
			$s[] = sprintf(' (A.name like %s or A.content like %s)',
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text')
			);
		}
		switch($g['display']){
			case 1:
				$s[] = sprintf(' A.display=0 ');
				break;
			case 2:
				$s[] = sprintf(' A.display=1 ');
				break;
			case 3:
				break;
		}
		if(sizeof($s)){
			if($q['sWhere']){
				$q['sWhere'] .= sprintf(' and %s', implode(' and ', $s));
			}else{
				$q['sWhere'] = sprintf(' WHERE %s', implode(' and ', $s));
			}
		}
		return $q;
	}
	
	
	public function product_insert(){
		$Website = $this->Website;
		
		$collection_id = $_POST['collection_id'];
		$product_id = explode(',', $_POST['product_id']);
		
		if($collection_id && sizeof($product_id)){
			
			$sql = sprintf("SELECT P.*, B.file_url, B.name file_name, C.collection_id
							FROM %s P
							LEFT JOIN %s B ON B.id = (SELECT C.media_id FROM %s C WHERE C.item_id=P.id and C.item_type=%s ORDER BY C.sort asc limit 1)
							LEFT JOIN %s C ON C.product_id=P.id and C.collection_id=%s
							WHERE 
								P.id in (%s)
							ORDER BY FIELD (P.id, %s)
							",
							$Website['table']['product'],
						   	
							$Website['table']['media'],
							$Website['table']['media_relation'],
							sql_string('product', 'text'),
						   	
							$Website['table']['product_collection_relation'],
							sql_string($collection_id, 'int'),
						   	
						   	implode(',', $product_id),
						   	implode(',', $product_id)
							);
			$rows1 = $this->query->select($sql);
			
			for($i=0; $i<sizeof($rows1); $i++){
				
				if($rows1[$i]['collection_id']){
					continue;
				}
				
				$data = array(
					'collection_id'	=>	ci_sql_string($collection_id, 'int'),
					'product_id'	=>	ci_sql_string($rows1[$i]['id'], 'int'),
					'sort'			=>	0,
				);
				$this->db->set($data);
				$this->db->insert($Website['table']['product_collection_relation']);
				
				$data = array(
					'rows1'		=>	array(
						'product_id'	=>	$rows1[$i]['id'],
						'file_url'		=>	$rows1[$i]['file_url'],
						'name'			=>	$rows1[$i]['name'].$id,
						'sort'			=>	0,
					)
				);
				$html .= $this->load->view('admin/collection/box', $data, true);
			}
		}
		
		echo json_encode(array(
			'html'	=>	$html,
		));
		
	}
	
	public function product_delete(){
		$Website = $this->Website;
		
		$collection_id = $_POST['collection_id'];
		$product_id = $_POST['product_id'];
		
		$sql = sprintf("DELETE FROM %s WHERE collection_id=%s and product_id=%s",
					  	$Website['table']['product_collection_relation'],
					   	sql_string($collection_id, 'int'),
					   	sql_string($product_id, 'int')
					  );
		$this->query->sql($sql);
		
	}
	
	public function product_delete_all(){
		$Website = $this->Website;
		
		$collection_id = $_POST['collection_id'];
		
		$sql = sprintf("DELETE FROM %s WHERE collection_id=%s",
					  	$Website['table']['product_collection_relation'],
					   	sql_string($collection_id, 'int')
					  );
		$this->query->sql($sql);
		
	}
	
}
?>