<?
class Theme extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->preload->admin();
		$this->current_class = strtolower($this->router->fetch_class());
		$this->settings = array(
								'btn'				=>	array(			//列表下方按鈕
									'insert'			=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'btn_txt'			=>	array(			//自行定義按鈕名稱
									'insert'			=>	'',
									'display'			=>	'',
									'sort'				=>	'',
								),
								'icon'				=>	array(			//自行增加icon數量，但須於datatable中的actions中新增項目
									'edit'				=>	1,
									'delete'			=>	1,
									'display'			=>	1,
									'sort'				=>	0,
								),
								'path'				=>	array(
									'list'				=>	base_url().'admin/'.$this->current_class.'/',
									'ajax'				=>	base_url().'admin/'.$this->current_class.'/ajax/',
									'item'				=>	base_url().'admin/'.$this->current_class.'/item/',
								),
								'orderby'			=>	array(array(1, "desc")),
								'order_columns'		=>	array('', 'id', 'name'),
								'search_columns'	=>	array('name', 'content'),
								'table_columns'		=>	array(my_lang('action'), my_lang('id'), my_lang('name')),
								'column_visible'	=>	array(1, 1, 1),
								'column'			=>	array(
									array('width'		=>	'130px', 	'targets'	=>	0),
									array('width'		=>	'60px', 	'targets'	=>	1),
									array('orderable'	=>	false, 		'targets'	=>	array(0)),
								),
								
								'html'				=>	array(
									'list'				=>	$this->current_class.'/list',
									'item'				=>	$this->current_class.'/item',
									'adv_search'		=>	'admin/adv_search/'.$this->current_class,
								),
								'model'				=>	'global_query',
								
								//datatable
								'adv_search_status'	=>	0,
								'adv_search'		=>	array(
									'vars'			=>	array(
										'display',
										'search'
									),
								),
								/*
								//image relation
								'item_type'			=>	array(
									'about_ogimage'
								),
								*/
								//validation rule group
								'validation_rule_group'	=>	'admin/'.$this->current_class.'/item',
							);
		$this->Website['tables'] = array(
			$this->Website['table']['theme'],
		);
		
		//確定編輯權限
		$this->settings = $this->menu->check_editable($this->settings);
		
		//進階搜尋設定
		$this->settings['adv_search']['html'] = $this->adv_search();
		
		//下方btn設定
		$this->settings['button']  = $this->datatable->button($this->settings);
	}
	
	public function index(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = my_lang('admin.'.$this->current_class.'_list');
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$here = $_POST['here'];
			$id = $_POST['id'];
			$show = $_POST['display'];
			$delete = $_POST['delete'];
			
			switch($here){
				case 1:
					/*
						display or not
					*/
					$this->$model_query->display_update($id, $show);
					
					/*
						delete
					*/
					$this->$model_query->delete_update($id, $delete);
					break;
				case 2:
					/*
						sort
					*/
					$this->$model_query->sort_update();
					break;
			}
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$sql = sprintf("SELECT * FROM %s ORDER BY id desc", $Website['table']['theme']);
		$rows1 = $this->query->select($sql);
		
		for($i=0; $i<sizeof($rows1); $i++){
			$data = array(
				'rows1'		=>	$rows1[$i]
			);
			$content[] = $this->load->view('admin/theme/block', $data, true);
		}
		
		$data = array(
			'Website'	=>	$Website,
			'settings'	=>	$this->settings,
			'rows1'		=>	$rows1,
			'content'	=>	implode('', $content),
		);
		$this->template->views_admin($this->settings['html']['list'], $data);
	}
	
	
	
	
	public function upload(){
		$Website = $this->Website;
		
		//vars
		$file_headers = array(
			'Name'        => 'Theme Name',
			'ThemeURI'    => 'Theme URI',
			'Description' => 'Description',
			'Author'      => 'Author',
			'AuthorURI'   => 'Author URI',
			'Version'     => 'Version',
			'Template'    => 'Template',
			'Status'      => 'Status',
			'Tags'        => 'Tags',
			'TextDomain'  => 'Text Domain',
			'DomainPath'  => 'Domain Path',
		);
		
		$theme_root_folder = 'ci_theme';
		$theme_file_folder = 'themes';
		$theme_upload_folder = 'uploads';
		$upload_folder = FCPATH.$theme_root_folder.'/'.$theme_upload_folder.'/';
		
		//echo (int)chmod($upload_folder, 0777);
		
		//get upload file
		//$file_name = 'default1.zip';
		//$file_name = basename($_FILES['name']);
		//print_r($_FILES);
		
		$data = array();
		$files = array();
		$error = false;
		
		foreach($_FILES as $file){
			if(move_uploaded_file($file['tmp_name'], $upload_folder .basename($file['name']))){
				$file_path = $upload_folder.$file['name'];
			}else{
				$error = true;
			}
		}
		
		//unzip
		$zip = new ZipArchive;
		$res = $zip->open($file_path);
		
		$zip_dir = trim($zip->getNameIndex(0), '/');
		$zip_folder = FCPATH.$theme_root_folder.'/'.$theme_file_folder.'/'.$zip_dir;
		
		if(!file_exists($zip_folder)){
			//can insert
			$zip->extractTo(FCPATH.$theme_root_folder.'/'.$theme_file_folder.'/');
			
		}else{
			//can not insert
			$rows1 = array(
				'status'	=>	false,
				'dir'		=>	$zip_dir,
			);
			echo json_encode($rows1);exit;
		}
		
		for ($i = 0; $i < $zip->numFiles; $i++) {
			$entry = $zip->getNameIndex($i);

			if(strpos($entry, 'assets/css/style.css')){
				$theme_data = $this->get_file_data( FCPATH.$theme_root_folder.'/'.$theme_file_folder.'/'.$entry, $file_headers );
			}
		}
		
		$zip->close();
		
		//remove zip
		unlink($file_path);
		
		//set folder permission to 0777
		chmod(FCPATH.$theme_root_folder.'/'.$theme_file_folder.'/'.$zip_dir.'/assets/js/', 0777);
		chmod(FCPATH.$theme_root_folder.'/'.$theme_file_folder.'/'.$zip_dir.'/assets/css/', 0777);
		
		//database
		$sql = sprintf("INSERT IGNORE INTO %s 
						(
						name, folder, theme_uri, description, author, 
						author_uri, version, template, status, tags, 
						text_domain, domain_path, display, insert_time, modify_time
						) VALUES (
						%s, %s, %s, %s, %s,
						%s, %s, %s, %s, %s,
						%s, %s, %s, %s, %s
						)",
					  	$Website['table']['theme'],
					   
					   	sql_string($theme_data['Name'], 'text'),
					   	sql_string($zip_dir, 'text'),
					   	sql_string($theme_data['ThemeURI'], 'text'),
					   	sql_string($theme_data['Description'], 'text'),
					   	sql_string($theme_data['Author'], 'text'),
					   
					   	sql_string($theme_data['AuthorURI'], 'text'),
					   	sql_string($theme_data['Version'], 'text'),
					   	sql_string($theme_data['Template'], 'text'),
					   	sql_string($theme_data['Status'], 'text'),
					   	sql_string($theme_data['Tags'], 'text'),
					   
					   	sql_string($theme_data['TextDomain'], 'text'),
					   	sql_string($theme_data['DomainPath'], 'text'),
					   	sql_string(1, 'text'),
					   	sql_string(time(), 'text'),
					   	sql_string(time(), 'text')
					  );
		$id = $this->query->sql($sql);
		
		$data = array(
			'rows1'		=>	array(
				'id'		=>	id,
				'name'		=>	$theme_data['Name'],
				'folder'	=>	$zip_dir,
				'active'	=>	0
			),
		);
		
		$rows1 = array(
			'id'		=>	$id, 
			'name'		=>	$theme_data['Name'],
			'status'	=>	true,
			'dir'		=>	$zip_dir,
			'content'	=>	$this->load->view('admin/theme/block', $data, true),
		);
		echo json_encode($rows1);
	}
	
	
	
	
	
	private function get_file_data( $file, $default_headers) {
		// We don't need to write to the file, so just open for reading.
		$fp = fopen( $file, 'r' );

		// Pull only the first 8kiB of the file in.
		$file_data = fread( $fp, 8192 );

		// PHP will close file handle, but we are good citizens.
		fclose( $fp );

		// Make sure we catch CR-only line endings.
		$file_data = str_replace( "\r", "\n", $file_data );
		
		$all_headers = $default_headers;

		foreach ( $all_headers as $field => $regex ) {
			if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( $regex, '/' ) . ':(.*)$/mi', $file_data, $match ) && $match[1] ){
				$all_headers[ $field ] = $match[1];
			}else{
				$all_headers[ $field ] = '';
			}
		}
		
		return $all_headers;
	}
	
	
	
	
	public function ajax_active(){
		$Website = $this->Website;
		$id = (int)$_POST['id'];
		
		$sql = sprintf("UPDATE %s SET active=0", $Website['table']['theme']);
		$this->query->sql($sql);
		
		
		$sql = sprintf("UPDATE %s SET active=1 WHERE id=%s", $Website['table']['theme'], sql_string($id, 'int'));
		$this->query->sql($sql);
	}
	
	public function ajax(){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		$q = $this->datatable->query(array(
											$this->settings['order_columns'], 
											$this->settings['search_columns'], 
											$this->settings
										), 
										$_GET);
		
		$q = $this->adv_search_sql($q, $_GET);
		$iTotalRecords = $this->$model_query->table_total();
		$iTotalDisplayRecords = $this->$model_query->display_total($q);
		$rows1 = $this->$model_query->datatable_select($q);
		
		$rows2 = array();
		for($i=0; $i<sizeof($rows1); $i++){
			
			$icon_setting = $this->settings['icon'];
			
			if(in_array($rows1[$i]['id'], array())){
				//設定部份icon項目不顯示
				//$icon_setting['delete'] = 0;
			}
			if($q['sWhere']){
				//當有搜尋項目時，不予排序
				$icon_setting['sort'] = 0;
			}
			
			$action_array = array(
								'id'				=>	$rows1[$i]['id'],
								'checked'			=>	$Website['checked'][(int)$rows1[$i]['display']],
								'display_class'		=>	$Website['display_class'][(int)$rows1[$i]['display']],
								'edit'				=>	'admin/'.$this->current_class.'/item/'.$rows1[$i]['id'].'/',
								'icon'				=>	$icon_setting,
							);
			$rows2[] = array(
				$this->datatable->actions($action_array),
				$rows1[$i]['sort'],
				$rows1[$i]['id'],
				$rows1[$i]['name'],
			);
		}
		
		$output = array(
			"aaData"					=> $rows2,
			"iTotalRecords" 			=> $iTotalRecords,
			"iTotalDisplayRecords"		=> $iTotalDisplayRecords,
		);
		
		echo json_encode($output);
		exit;
	}
	
	public function item($id){
		$Website = $this->Website;
		$model_query = $this->settings['model'];
		
		//vars
		$Website['page_header'] = $this->lang->line($this->current_class."_item");
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			//Save
			$here = $_POST['here'];
			$id = $_POST['id'];
			$name = $_POST['name'];
			$description = $_POST['description'];
			$content = $_POST['content'];
			$display = $_POST['display'] ? 1 : 0;
			$ogimage_input = $_POST['ogimage_input'];
			$ogimage_type = $this->settings['item_type'][0];
			
			if($this->form_validation->run($this->settings['validation_rule_group']) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				$this->template->views_admin('template/error', $data, true);
				exit;
			}
			
			
			if(!$id){
				$rows1 = array(
					'name'			=>	ci_sql_string($name, 'text'),
					'description'	=>	ci_sql_string($description, 'text'),
					'content'		=>	ci_sql_string($content, 'text'),
					'display'		=>	ci_sql_string($display, 'int'),
					'sort'			=>	0,
					'insert_time'	=>	time(),
					'modify_time'	=>	time(),
				);
				$id = $this->$model_query->insert($this->settings, $rows1);
			}else{
				$rows1 = array(
					'name'			=>	ci_sql_string($name, 'text'),
					'description'	=>	ci_sql_string($description, 'text'),
					'content'		=>	ci_sql_string($content, 'text'),
					'display'		=>	ci_sql_string($display, 'int'),
					'modify_time'	=>	time(),
				);
				$this->$model_query->update($rows1, $id);
			}
			
			//媒體 ogimage
			$this->media_query->media_relation($id, $ogimage_input, $ogimage_type);
			
			redirect($this->settings['path']['list'], 'refresh');
			exit;
		}
		
		$validation_rule = $this->config->item($this->settings['validation_rule_group']);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1');
		//$jquery_validation = $this->jquery_validation->run('#form1', array('submit_handler'	=>	'handler_admin_about',));
		
		
		$data = array(
			'id'			=>	$id,
		);
		$rows1 = $this->$model_query->select($data);
		if($id){
			$Website['page_header'] = $rows1['name'];
		
			/*
				社群分享圖片
			*/
			$ogimage = $this->media_query->media_select($id, $this->settings['item_type'][0]);
			$rows1['ogimage_input'] = $ogimage['input'];
			$rows1['ogimage_area'] = $ogimage['area'];
		}
		
		$data = array(
			'Website'		=>	$Website,
			'settings'		=>	$this->settings,
			'validation'	=>	$jquery_validation,
			'rows1'			=>	$rows1,
		);
		$this->template->views_admin($this->settings['html']['item'], $data);
	}
	
	public function column_visible(){
		$Website = $this->Website;
		
		$column = $_GET['column'];
		$column_visible = $this->settings['column_visible'];
		for($i=1; $i<sizeof($column_visible); $i++){
			if(in_array($i, $column)){
				$column_visible[$i] = 1;
			}else{
				$column_visible[$i] = 0;
			}
		}
		$_SESSION['column_visible'][$this->current_class] = $this->settings['column_visible'] = $column_visible;
	}
	
	private function adv_search(){
		$Website = $this->Website;
		
		if(!$this->settings['adv_search_status']){
			return 0;
		}
		
		if($_SESSION['column_visible'][$this->current_class]){
			$this->settings['column_visible'] = $_SESSION['column_visible'][$this->current_class];
		}
		
		$data = array(
			'Website'	=>	$Website,
			'setting'	=>	$this->settings,
			'rows1'		=>	$_SESSION['adv_search'][$this->current_class],
		);
		return $this->load->view($this->settings['html']['adv_search'], $data, true);
	}
	
	private function adv_search_sql($q, $g){
		//進階搜尋
		
		$_SESSION['adv_search'][$this->current_class] = $g;
		
		if($g['search']){
			$s[] = sprintf(' (A.name like %s or A.content like %s)',
				sql_string('%'.$g['search'].'%', 'text'),
				sql_string('%'.$g['search'].'%', 'text')
			);
		}
		switch($g['display']){
			case 1:
				$s[] = sprintf(' A.display=0 ');
				break;
			case 2:
				$s[] = sprintf(' A.display=1 ');
				break;
			case 3:
				break;
		}
		if(sizeof($s)){
			if($q['sWhere']){
				$q['sWhere'] .= sprintf(' and %s', implode(' and ', $s));
			}else{
				$q['sWhere'] = sprintf(' WHERE %s', implode(' and ', $s));
			}
		}
		return $q;
	}
	
}
?>