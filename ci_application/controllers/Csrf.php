<?
class Csrf extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->preload->main();
		$this->preload->check_domain();
    }
	
	public function index(){
		$Website = $this->Website;
		
		$csrf = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
		echo json_encode($csrf);
	}
}
?>