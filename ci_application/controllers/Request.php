<?
class Request extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->preload->main();
		$this->preload->check_domain();
    }
	
	public function layout_toggle(){//echo $_POST['status'];
		$status = $_POST['status']=='true'?0:1;//echo $status;
		
		$_SESSION['layout_toggle'] = $status;
		
		echo $status;
	}
	
	public function member_account(){
		//確認註冊
		$Website = $this->Website;
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$account = strtolower($_POST['account']);
			$member_id = $_POST['member_id'];

			if(!$account){
				echo 'false';
				exit;
			}

			$sql = sprintf("SELECT A.id
							FROM %s A
							WHERE A.account=%s
							LIMIT 1",
							$Website['table']['member'],
							sql_string($account, "text")
							);
			$rows1 = $this->query->select($sql, 1);
			
			if($rows1['id']==$member_id && sizeof($rows1)){
				unset($rows1);
			}

			if(!sizeof($rows1)){
				//不存在
				echo 'true';
			}else{
				//存在
				echo 'false';
			}
		}
		exit;
	}
	
	public function member_password(){
		//確認註冊
		$Website = $this->Website;
		$password = $_POST['password'];
		
		if(!$password){
			echo 'true';
			exit;
		}
		
		$sql = sprintf("SELECT A.id
						FROM %s A
						WHERE A.account=%s and A.password=%s
						LIMIT 1",
						$Website['table']['member'],
						sql_string($_SESSION['member_info']['account'], "text"),
						sql_string(md5($password), "text")
						);
		$rows1 = $this->query->select($sql, 1);
		
		if(sizeof($rows1)){
			//正確
			echo 'true';
		}else{
			//錯誤
			echo 'false';
		}
		exit;
	}
	
	public function member_mobile(){
		//確認註冊
		$Website = $this->Website;
		$mobile = $_POST['mobile'];
		
		if(!$mobile){
			echo 'false';
			exit;
		}
		
		if($mobile=='0919312265'){
			echo 'true';exit;
		}
		
		if($_SESSION['member_info']['id']){
			$sql = sprintf("SELECT A.id
							FROM %s A
							WHERE A.mobile=%s and A.id!=%s
							LIMIT 1",
							$Website['table']['member'],
							sql_string($mobile, "text"),
							sql_string($_SESSION['member_info']['id'], "int")
							);
		}else{
			$sql = sprintf("SELECT A.id
							FROM %s A
							WHERE A.mobile=%s
							LIMIT 1",
							$Website['table']['member'],
							sql_string($mobile, "text")
							);
		}
		$rows1 = $this->query->select($sql, 1);
		
		if(!sizeof($rows1)){
			//不存在
			echo 'true';
		}else{
			//存在
			echo 'false';
		}
		exit;
	}
	
	public function use_member($id){
		//使用會員身份
		$Website = $this->Website;
		
		if($_SESSION['user_info']['id']){
			//必須以登入後台管理員 才可以使用會員身份
			$sql = sprintf("SELECT A.*
							FROM %s A 
							WHERE A.id=%s
							LIMIT 1
							",
							$Website['table']['member'],
							sql_string($id, 'int')
							);
			$rows1 = $this->query->select($sql, 1);//print_r($rows1);exit;
			
			$rows1['age'] = date('Y', time()) - date('Y', $rows1['birth']?$rows1['birth']:time());
			
			$rows1['from_admin'] = 1;
			
			$_SESSION['member_info'] = $rows1;
			
		}
		
	}
	
	public function member_search(){
		$Website = $this->Website;
		$name = $_GET['q'];
		//echo $name;
		if(!$name){
			//echo 'false';
			exit;
		}
		
		$sql = sprintf("SELECT A.id, A.name, A.code
						FROM %s A
						WHERE A.name like %s or A.code like %s
						ORDER BY A.id asc
						LIMIT 10
						",
						$Website['table']['member'],
						sql_string('%'.$name.'%', "text"),
						sql_string('%'.strtoupper($name).'%', "text")
						);//echo $sql;
		$rows1 = $this->query->select($sql);
		
		for($i=0; $i<sizeof($rows1); $i++){
			$rows1[$i]['name'] = $rows1[$i]['code'].' | '.$rows1[$i]['name'];
			unset($rows1[$i]['code']);
		}
		
		echo json_encode($rows1);
		exit;
	}
	
	public function user_check(){
		//確認註冊
		$Website = $this->Website;
		
		$status = $this->ion_auth->login_check($this->input->post('account'), $this->input->post('password'), $remember);
	}
	
	public function zip_code(){
		//地址，郵遞區號
		$Website = $this->Website;
		
		$id = $_POST['id'];
		
		$sql = sprintf("SELECT A.* FROM %s A WHERE A.city_id=%s
						",
						$Website['table']['zip_area'],
						sql_string($id, "int")
						);
		$rows1 = $this->query->select($sql);
		/*
		for($i=0; $i<sizeof($rows1); $i++){
			$rows1[$i]['name'] = $rows1[$i]['name'].' '.$rows1[$i]['code'];
		}
		*/
		echo json_encode($rows1);
	}
	
	public function tag_search(){
		$Website = $this->Website;
		$name = $_GET['q']; 
		//echo $name;
		if(!$name){
			//echo 'false';
			exit;
		}
		
		$sql = sprintf("SELECT A.id, A.name
						FROM %s A
						WHERE A.name like %s
						ORDER BY A.id asc
						LIMIT 10
						",
						$Website['table']['tag'],
						sql_string('%'.$name.'%', "text")
						);//echo $sql;
		$rows1 = $this->query->select($sql);
		
		for($i=0; $i<sizeof($rows1); $i++){
			$rows2[] = $rows1[$i]['name'];
		}
		
		if(sizeof($rows2)){
			echo json_encode($rows2);
		}
		exit;
	}
	
	public function tag_insert(){
		$Website = $this->Website;
		$name = substr($_POST['q'], 1);
		$charset[1]=substr($name,0,1);
		$charset[2]=substr($name,1,1);
		$charset[3]=substr($name,2,1);
		if(ord($charset[1])==239 && ord($charset[2])==187 && ord($charset[3])==191){
			$name=substr($name,3);
		}
		if(!$name){
			//echo 'false';
			exit;
		}
		
		$sql = sprintf("INSERT IGNORE INTO %s 
						(name) 
						VALUES 
						(%s)",
						$Website['table']['tag'],
						sql_string($name, "text")
						);//echo $sql.'<br>';//exit;
		$tag_id = $this->query->sql($sql);
		
		if(!$tag_id){
			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.name=%s
							LIMIT 1
							",
							$Website['table']['tag'],
							sql_string($name, "text")
							);//echo $sql;
			$rows1 = $this->query->select($sql, 1);
			
			$tag_id = $rows1['id'];
		}
		echo $tag_id;
		//echo json_encode($rows2);
		exit;
	}
	
}

?>