<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Example extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->preload->main();
		//$this->load->library(array('theme'));
    }
	
	public function index(){	
		// Adding a CSS file
		$this->theme->add_css('style');

		// Add a partial view as sidebar
		//$this->theme->add_partial('sidebar');
		
		// Load the template
		$data = array(
			'rows1'		=>	1
		);
		$this->theme->views('example/index', $data);
		//exit;
	}
	
	public function primary(){
		
		$layout = $_GET['layout'];
		$header = $_GET['header'];
		
		if($layout=='sidebar'){
			$data = array(
				'title'			=>	'Primary',
				'site_name'		=>	'Demo Site',
				'description'	=>	'theme manager demo',
				'keywords'		=>	'theme, demo, ec',
				'theme'			=>	'primary',
				'has_header'	=>	true,
				'has_footer'	=>	true,
				'layout'		=>	'default',
			);
			$this->theme->config($data);

			// Adding a CSS file
			$this->theme->add_css('style');
			
			// Add a partial view as sidebar
			$this->theme->add_partial('sidebar');

			// Load the template
			$data = array(
				'rows1'		=>	1,
			);
			$this->theme->views('example/index1', $data);
			
		}else{
			$data = array(
				'title'			=>	'Primary',
				'site_name'		=>	'Demo Site',
				'description'	=>	'theme manager demo',
				'keywords'		=>	'theme, demo, ec',
				'theme'			=>	'primary',
				'header_config' =>	array(
					'display'		=>	true,
					'theme'			=>	$header ? $header : 'default',
					'width'			=>	'within_fullwidth',
					'stickup'		=>	true,
				),
				'footer_config' =>	array(
					'display'		=>	true,
					'theme'			=>	$footer ? $footer : 'default',
					'width'			=>	'fullwidth',
				),
			);
			$this->theme->config($data);
			/*
			$header_config = array(
				//'line'				=>	1,
				'stickup'				=>	true,
				//'style'					=>	'default',
				//'navbar_width'		=>	'boxed',
				//'navbar_width'		=>	'within_fullwidth',
				'width'			=>	'fullwidth',
				//'category1_position'	=>	'left',
				//'category2_position'	=>	'right',
			);
			$this->theme->header_config($header_config);
			
			/*
				//line：1 or 2
				stickup：true false
				width：fullwidth within_fullwidth boxed
				//logo： left, middle
				//category1：left middle right
				//category2：left right
				//menu: menu, megamenu
				//class：custom
				
				
					logo: left
						category1：left
							category2：left
					
					logo: left
						category1：left
							category2：right

					logo: left
						category1：middle 
							category2：right

					logo: left
						category1：right
							category2：right

					logo: middle
						category1&2： combine middle
			*/

			// Adding a CSS file
			$this->theme->add_css('style');
			
			// Add a partial view as sidebar
			//$this->theme->add_partial('sidebar');
			
			// Load the template
			$data = array(
				'rows1'		=>	1,
			);
			$this->theme->views('example/index', $data);
			
		}
	}
}
