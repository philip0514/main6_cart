<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Assets Handler Controller
 *
 * This controller handles all assets: JS, CSS & image files
 *
 * @package 	CodeIgniter
 * @category 	Controllers
 * @author 	Kader Bouyakoub <bkader@mail.com>, Chuang I Hao <philip0514@gmail.com>
 * @link 	https://github.com/bkader
 * @link 	https://twitter.com/KaderBouyakoub
 */

class Asset extends CI_Controller
{
	/**
	 * Cache files
	 * @var integer
	 */
	// protected $cache_life = 60;

	/**
	 * Constructor
	 * @param 	none
	 * @return 	void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->preload->main();

		$this->config->load('assets', NULL, FALSE);
		$this->minify_assets = $this->config->item('minify_assets');
		$this->minify_assets OR $this->minify_assets = (ENVIRONMENT === 'production');
		$this->cache_assets = $this->config->item('cache_assets');
		$this->cache_assets OR $this->cache_assets = (ENVIRONMENT === 'production') ? 60 : 0;

		// Load our file & url helpers
		function_exists('uri_string') OR $this->load->helper('url');

		$class = get_class($this);
		$file_path = ltrim($this->uri->uri_string(), '/');

		$args = explode('/', $file_path);

		// If there are not enough params, we show_404
		if (count($args) < 2) {
			die('not enough arguments');
		}

		// Prevent the user from accessing class name ;)
		if (strtolower($args[0]) === strtolower($class)) {
			show_404();
		}

		// Grab theme file from Theme library
		$theme = class_exists('Theme') ? $this->theme_query->get_active() : 'default';
		
		// Prepare full path to requested file
		$this->full_path = array(FCPATH, 'ci_theme');
		
		// Append 'assets' OR 'uploads'
		if ($args[0] === 'assets') {
			$this->full_path = array_merge($this->full_path, array('themes', $theme, 'assets'));
		} else {
			$this->full_path = array_merge($this->full_path, array('uploads'));
		}
		
		

		// Append path to file
		unset($args[0]);
		$this->full_path = array_merge($this->full_path, $args);
		$this->full_path = rtrim(implode(DIRECTORY_SEPARATOR, $this->full_path));
		unset($class, $file_path, $args, $the);
	}

	/**
	 * Remap the controller and try to guess the requested file
	 * smartly without creating a method for each file type.
	 * @access 	public
	 * @param 	none
	 * @return 	void
	 */
	public function _remap($method, $args = NULL)
	{
        // Load the file content with its mime if it's found
        if (file_exists($this->full_path) && ! empty($pathinfo = pathinfo($this->full_path))) {

        	if ($this->cache_assets > 0) {
        		$this->output->cache($this->cache_assets);
        	}
        	// Prepare our output
        	$output = @ (string) file_get_contents($this->full_path);

        	// Set content type according to file extension
        	$this->output->set_content_type($pathinfo['extension'], 'utf-8');
			
        	// We minify and cache files if we are in production mode
        	if (ENVIRONMENT === 'production' OR (bool) $this->minify_assets === TRUE) {
        		$output = $this->_minify($output, $pathinfo['extension']);
        		$this->output->cache(15);
        	}
			
			// Output the file
			$this->output->set_header("HTTP/1.0 200 OK");
			$this->output->set_header("HTTP/1.1 200 OK");
			$this->output->set_header('Last-Modified: '.gmdate('D，d M Y H:i:s', time()).' GMT');
			$this->output->set_header('Expires: '.gmdate('D，d M Y H:i:s', time() + (60*60)).' GMT');
			$this->output->set_header('Cache-Control: max-age='. 60*60);
			
			switch($pathinfo['extension']){
				case 'css':
					$output = gzencode($output." \n//compressed",9);
					$this->output->set_header('Content-type: text/css');
					$this->output->set_header('Content-Length: ' . strlen($output));
					$this->output->set_header("Content-Encoding: gzip"); 
					$this->output->set_header("Vary: Accept-Encoding");
					break;
				case 'js':
					$output = gzencode($output." \n//compressed",9);
					$this->output->set_header('Content-type: application/javascript');
					$this->output->set_header('Content-Length: ' . strlen($output));
					$this->output->set_header("Content-Encoding: gzip"); 
					$this->output->set_header("Vary: Accept-Encoding");
					break;
			}
			
        	$this->output->set_output($output);
        	return;
        }

        // If the find is not found, we display a 404 error
        show_error('The file you are looking for does not exist or has been deleted.', 404, anchor('', 'Back Home'));
	}

	/**
	 * This method minifies CSS & JS files while it returns full images output
	 * @access 	protected
	 * @param 	string 	$output 	file content
	 * @param 	string 	$type 		file type
	 * @return 	string 	minified version of the file
	 */
	protected function _minify($output, $type = 'css')
	{
		switch (strtolower($type)) {
				
			// In case of a CSS file
			case 'css':
				require_once APPPATH.'third_party/minifiers/cssminify.php';
				$cssminify = new cssminify();
				return $cssminify->compress($output);
				break;

			// In case of a JS file
			case 'js':
				require_once APPPATH.'third_party/minifiers/JSMinPlus.php';
				return JSMinPlus::minify($output);
				break;
			
			// In case of other files
			default:
				return $output;
				break;
		}
	}
}

/* End of file Asset.php */
/* Location: ./application/controllers/Asset.php */