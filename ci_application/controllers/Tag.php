<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->preload->main();
    }
	
	public function index($text=null, $page=1){
		$Website = $this->Website;
		$this->preload->current_location();
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.name=%s and A.display=1
						LIMIT 1
						",
						$Website['table']['tag'],
						sql_string($text, 'text')
						);
		$rows1 = $this->query->select($sql, 1);
		
		if(!$rows1['id']){
			show_404();
		}
		
		//商品
		$sql = sprintf("SELECT P.*, B.file_url
						FROM %s A
						LEFT JOIN %s P ON A.product_id=P.id
						LEFT JOIN %s B ON B.id = (SELECT C.media_id FROM %s C WHERE C.item_id=P.id and C.item_type=%s ORDER BY C.sort asc limit 1)
						WHERE A.tag_id=%s and 
								P.display=1 and
								(P.display_start_time<=%s or P.display_start_time is null or P.display_start_time=0) and
								(P.display_end_time>%s or P.display_end_time is null or P.display_end_time=0) 
						ORDER BY P.id desc
						",
						$Website['table']['product_tag_relation'],
						$Website['table']['product'],
					   
						$Website['table']['media'],
						$Website['table']['media_relation'],
					   	sql_string('product', 'text'),
					   
						sql_string($rows1['id'], 'int'),
						time(),
						time()
						);
		$product = $this->query->select($sql);
		
		$data = array(
			'title'			=>	$rows1['name'],
		);
		$this->theme->config($data);
		
		$data = array(
			'Website'		=>	$Website,
			'page_title'	=>	$rows1['name'],
			'pagination'	=>	$pagination,
			'product'		=>	$product,
		);
		$this->theme->views('tag/index', $data);
	}
}
?>