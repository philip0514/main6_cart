<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {
	
	function __construct(){
        parent::__construct();
		$this->preload->main();
    }
	
	/*
		登入
	*/
	public function login($status='success'){
		$Website = $this->Website;
		
		$title = '會員登入';
		$validation_rule_group = 'member/login';
		
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$this->preload->check_domain();
			
			if($this->form_validation->run($validation_rule_group) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				echo $this->theme->error_views('validation', $data, true);
				exit;
			}
			
			$account = strtolower($_POST['account']);
			$password = $_POST['password'];
			$remember = $_POST['remember'];
			
			$data = array(
				'account'		=>	ci_sql_string($account, 'text'),
				'password'		=>	ci_sql_string($password, 'text'),
				'remember'		=>	ci_sql_string($remember, 'int'),
			);
			$status = $this->member_query->login($data);
			
			switch($status){
				case 1:
					//正常登入
					$location = $_SESSION['current_location']['frontend'];
					break;
				case 2:
					//帳號被停用
					$location = 'member/message/deactive/';
					break;
				default:
				case 0:
					//失敗
					$location = 'member/login/error/';
					break;
			}
			
			redirect($location, 'refresh');
			exit;
		}
		
		$validation_rule = $this->config->item($validation_rule_group);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1');
		
		$csrf = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
		
		$data = array(
			'Website'		=>	$Website,
			'page_title'	=>	$title,
			'validation'	=>	$jquery_validation,
			'csrf'			=>	$csrf,
		);
		$this->theme->views('member/login', $data);
	}
	
	/*
		一般註冊
	*/
	public function register(){
		$Website = $this->Website;
		
		$title = '會員註冊';
		$validation_rule_group = 'member/register';
		
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$this->preload->check_domain();
			
			if($this->form_validation->run($validation_rule_group) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'page_title'		=>	$title,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				echo $this->theme->error_views('validation', $data, true);
				exit;
			}
			
			//Save
			$remember = $_POST['remember'];
			$name = $_POST['name'];
			$account = strtolower($_POST['account']);
			$password = $_POST['password'];
			$display = 1;
			$checked = 0;
			$success_url = 'member/message/register_success/';
			$error_url = 'member/message/register_error/';
			
			$data = array(
				'name'			=>	ci_sql_string($name, 'text'),
				'account'		=>	ci_sql_string($account, 'text'),
				'password'		=>	ci_sql_string($password, 'text'),
				'display'		=>	ci_sql_string($display, 'int'),
				'checked'		=>	ci_sql_string($checked, 'int'),
				'insert_time'	=>	time(),
				'modify_time'	=>	time()
			);
			$id = $this->member_query->register($data);
			
			if($id){
				//登入
				$this->member_query->login_by_id($id, $remember);

				redirect($success_url, 'refresh');
			}else{
				redirect($error_url, 'refresh');
			}
			exit;
		}
		
		$validation_rule = $this->config->item($validation_rule_group);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1');
		
		$csrf = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
		
		$data = array(
			'Website'		=>	$Website,
			'page_title'	=>	$title,
			'validation'	=>	$jquery_validation,
			'csrf'			=>	$csrf,
		);
		$this->theme->views('member/register', $data);
	}
	
	/*
		Facebook 登入
	*/
	public function facebook_login(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		$rows1 = $_POST['response'];
		
		$status = $this->member_query->facebook_login($rows1['id']);
		
		switch($status){
			case 1:
				//正常登入
				$location = $_SESSION['current_location']['frontend']?$_SESSION['current_location']['frontend']:'/';
				
				break;
			case 2:
				//帳號被停用
				$location = 'member/message/deactive/';
				
				break;
			default:
			case 0:
				//新會員
				$rows1['gender'] = ($rows1['gender']=='female') ? 2 : 1;
				
				if($rows1['email']){
					//可以直接註冊
					$display = 1;
					$checked = 0;
					
					$rows2 = array(
						'name'			=>	ci_sql_string($rows1['name'], 'text'),
						'account'		=>	ci_sql_string(strtolower($rows1['email']), 'text'),
						'password'		=>	ci_sql_string(strtolower($rows1['email'].time()), 'text'),
						'gender'		=>	ci_sql_string($rows1['gender'], 'text'),
						'facebook_id'	=>	ci_sql_string($rows1['id'], 'text'),
						'display'		=>	ci_sql_string($display, 'int'),
						'checked'		=>	ci_sql_string($checked, 'int'),
						'insert_time'	=>	time(),
						'modify_time'	=>	time()
					);
					$id = $this->member_query->register($rows2);
					
					$location = 'member/message/register_success/';
				}else{
					//無法直接註冊 因為缺少EMAIL
					$_SESSION['member_info']['temp'] = $rows1;
					$location = 'member/facebook_register/';
				}
				break;
		}
		
		echo json_encode(array(
			'status'	=>	$status,
			'location'	=>	$location,
		));
	}
	
	/*
		Facebook 註冊 - 無法取得facebook email時
	*/
	public function facebook_register(){
		$Website = $this->Website;
		
		$title = 'Facebook 註冊';
		$validation_rule_group = 'member/facebook_register';
		
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$this->preload->check_domain();
			
			if($this->form_validation->run($validation_rule_group) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'page_title'		=>	$title,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				echo $this->theme->error_views('validation', $data, true);
				exit;
			}
			
			//Save
			$remember = $_POST['remember'];
			$name = $_POST['name'];
			$account = strtolower($_POST['account']);
			$facebook_id = $_SESSION['member_info']['temp']['id'];
			$display = 1;
			$checked = 0;
			$success_url = 'member/message/register_success/';
			$error_url = 'member/message/register_error/';
			
			$data = array(
				'name'			=>	ci_sql_string($name, 'text'),
				'account'		=>	ci_sql_string($account, 'text'),
				'password'		=>	ci_sql_string(strtolower($account.time()), 'text'),
				'facebook_id'	=>	ci_sql_string($facebook_id, 'text'),
				'display'		=>	ci_sql_string($display, 'int'),
				'checked'		=>	ci_sql_string($checked, 'int'),
				'insert_time'	=>	time(),
				'modify_time'	=>	time()
			);
			$id = $this->member_query->register($data);
			
			unset($_SESSION['member_info']['temp']);
			
			if($id){
				//登入
				$this->member_query->login_by_id($id);

				redirect($success_url, 'refresh');
			}else{
				redirect($error_url, 'refresh');
			}
			exit;
		}
		
		$validation_rule = $this->config->item($validation_rule_group);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1');
		
		$csrf = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
		
		$rows1 = $_SESSION['member_info']['temp'];
		//echo '<pre>';print_r($rows1);exit;
		
		$data = array(
			'Website'		=>	$Website,
			'page_title'	=>	$title,
			'validation'	=>	$jquery_validation,
			'rows1'			=>	$rows1,
			//'csrf'			=>	$csrf,
		);
		$this->theme->views('member/facebook_register', $data);
	}
	
	/*
		modal 登入
	*/
	public function ajax_modal_login(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		$status = FALSE;
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$validation_rule_group = 'member/login';
			
			$account = $_POST['account'] = strtolower($_POST['modal_login_account']);
			$password = $_POST['password'] = $_POST['modal_login_password'];
			$remember = $_POST['remember'] = $_POST['modal_login_remember'];
			/*
			if($this->form_validation->run($validation_rule_group) == FALSE){
				$result = array(
					'status'	=>	FALSE
				);
				echo json_encode($result);
				exit;
			}
			*/
			
			$data = array(
				'account'		=>	ci_sql_string($account, 'text'),
				'password'		=>	ci_sql_string($password, 'text'),
				'remember'		=>	ci_sql_string($remember, 'int'),
			);
			$status = $this->member_query->login($data);
			
			switch($status){
				case 1:
					//正常登入
					$message = '';
					break;
				case 2:
					//帳號被停用
					$message = '很抱歉，您的帳號已被停用，請洽網站管理員，謝謝。';
					break;
				default:
				case 0:
					//失敗
					$message = '很抱歉，您的帳號或密碼錯誤，請重新輸入，謝謝。';
					break;
			}
		}
		
		$result = array(
			'status'	=>	$status,
			'message'	=>	$message,
		);
		echo json_encode($result);
	}
	
	/*
		modal 註冊
	*/
	public function ajax_modal_register(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		$status = 0;
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$validation_rule_group = 'member/register';
			
			$name = $_POST['name'] = $_POST['modal_register_name'];
			$account = $_POST['account'] = strtolower($_POST['modal_register_account']);
			$password = $_POST['password'] = $_POST['modal_register_password'];
			$remember = $_POST['remember'] = $_POST['modal_register_remember'];
			$display = 1;
			$checked = 0;
			
			/*
			if($this->form_validation->run($validation_rule_group) == FALSE){
				$result = array(
					'status'	=>	FALSE
				);
				echo json_encode($result);
				exit;
			}
			*/
			
			$data = array(
				'name'			=>	ci_sql_string($name, 'text'),
				'account'		=>	ci_sql_string($account, 'text'),
				'password'		=>	ci_sql_string($password, 'text'),
				'display'		=>	ci_sql_string($display, 'int'),
				'checked'		=>	ci_sql_string($checked, 'int'),
				'insert_time'	=>	time(),
				'modify_time'	=>	time()
			);
			$id = $this->member_query->register($data);
			
			if($id){
				//登入
				$this->member_query->login_by_id($id, $remember);
				$status = 1;
				$message = '';
			}else{
				$status = 0;
				$message = '抱歉，帳號已存在。';
			}
		}
		
		$result = array(
			'status'	=>	$status,
			'message'	=>	$message,
		);
		
		echo json_encode($result);
	}
	
	/*
		帳號管理
	*/
	public function info(){
		$Website = $this->Website;
		$this->preload->current_location();
		$this->check_login();
		$validation_rule_group = 'member/info';
		
		$title = '帳號管理';
		
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$this->preload->check_domain();
			$id = $_SESSION['member_info']['id'];
			$name = $_POST['name'];
			$password = $_POST['password'];
			$gender = $_POST['gender'];
			$birth = $_POST['birth'] ? strtotime($_POST['birth']) : '';
			
			$city_id = $_POST['city_id'];
			$area_id = $_POST['area_id'];
			$address = $_POST['address'];
			$mobile = $_POST['mobile'];
			
			if($this->form_validation->run($validation_rule_group) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'page_title'		=>	$title,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				echo $this->theme->error_views('validation', $data, true);
				exit;
			}
			//修改

			$rows1 = array(
				'name'			=>	ci_sql_string($name, 'text'),
				'password'		=>	ci_sql_string($password, 'text'),
				'gender'		=>	ci_sql_string($gender, 'text'),
				'birth'			=>	ci_sql_string($birth, 'text'),

				'city_id'		=>	ci_sql_string($city_id, 'text'),
				'area_id'		=>	ci_sql_string($area_id, 'text'),
				'address'		=>	ci_sql_string($address, 'text'),

				'mobile'		=>	ci_sql_string($mobile, 'text'),
				'modify_time'	=>	time(),
			);
			if(!$rows1['password']){
				unset($rows1['password']);
			}
			$this->member_query->update($rows1, $id);

			redirect('member/message/modify_success/', 'refresh');
			exit;
		}
		
		$sql = sprintf("SELECT A.* 
						FROM %s A 
						WHERE A.id=%s",
					  	$Website['table']['member'],
					   	sql_string($_SESSION['member_info']['id'], 'int')
					  );
		$rows1 = $this->query->select($sql, 1);
		$rows1['birth_text'] = $rows1['birth']?date('Y/m/d', $rows1['birth']):'';
		
		//city
		$sql = sprintf("SELECT A.*
						FROM %s A 
						ORDER BY A.sort asc
						",
						$Website['table']['zip_city']
						);
		$rows2 = $this->query->select($sql);
		
		if($rows1['city_id']){
			$sql = sprintf("SELECT A.*
							FROM %s A 
							WHERE A.city_id=%s
							ORDER BY A.sort asc
							",
							$Website['table']['zip_area'],
							sql_string($rows1['city_id'], "text")
							);
			$rows3 = $this->query->select($sql);
		}
		
		$validation_rule = $this->config->item($validation_rule_group);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1');
		
		$csrf = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
		
		$data = array(
			'Website'		=>	$Website,
			'page_title'	=>	$title,
			'validation'	=>	$jquery_validation,
			'rows1'			=>	$rows1,
			'rows2'			=>	$rows2,
			'rows3'			=>	$rows3,
			'csrf'			=>	$csrf,
		);
		$this->theme->views('member/info', $data);
	}
	
	/*
		忘記密碼
	*/
	public function forgot_password(){
		$Website = $this->Website;
		$validation_rule_group = 'member/forgot_password';
		
		$title = '忘記密碼';
		
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$this->preload->check_domain();
			
			$account = strtolower($_POST['account']);
			
			if($this->form_validation->run($validation_rule_group) == FALSE){
				$data = array(
					'Website'			=>	$Website,
					'page_title'		=>	$title,
					'error_message' 	=>	validation_errors('<li>', '</li>'),
				);
				echo $this->theme->error_views('validation', $data, true);
				exit;
			}
			
			$sql = sprintf("SELECT A.*
							FROM %s A 
							WHERE A.account=%s",
							$Website['table']['member'],
							sql_string($account, "text")
							);
			$rows1 = $this->query->select($sql, 1);
			
			if($rows1['id']){
				//成功
				$password = uniqid().time();
				$password_hash = $this->member_query->encrypt_password($password);
				
				$sql = sprintf("UPDATE %s SET password=%s WHERE id=%s",
								$Website['table']['member'],
								sql_string($password_hash, "text"),
								sql_string($rows1['id'], "int")
								);
				$this->query->sql($sql);
				
				//寄信
				$data = array(
					'type'			=>	'forget_password',
					'mailto'		=>	$rows1['account'],
					'member_name'	=>	$rows1['name'],
					'member_id'		=>	$rows1['id'],
					'rows1'			=>	array(
						'member_name'			=>	$rows1['name'],
						'member_account'		=>	$rows1['account'],
						'member_password'		=>	$password,
					),
				);
				$this->mailer->to($data);
				
				$list_url = 'member/message/forgot_password_success/';
			}else{
				$list_url = 'member/message/forgot_password_fail/';
			}
			
			redirect($list_url, 'refresh');
			exit;
		}
		
		$validation_rule = $this->config->item($validation_rule_group);
		$this->jquery_validation->set_rules($validation_rule);
		$jquery_validation = $this->jquery_validation->run('#form1');
		
		$csrf = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
		
		$data = array(
			'Website'		=>	$Website,
			'page_title'	=>	$title,
			'validation'	=>	$jquery_validation,
			'csrf'			=>	$csrf,
		);
		$this->theme->views('member/forgot_password', $data);
	}
	
	/*
		登出
	*/
	public function logout(){
		unset($_SESSION['member_info']);
		//echo 'session logout';exit;
		delete_cookie("member_id");
		delete_cookie("member_account");
		//echo 'session and cookie logout';exit;
		$list_url = '/';
		redirect($list_url, 'refresh');
	}
	
	/*
		訊息回覆
	*/
	public function message($status){
		$Website = $this->Website;
		
		switch($status){
			case 'register_success':
				$title = '註冊帳號成功';
				break;
			case 'modify_success':
				$title = '修改資料完成';
				break;
			case 'deactive':
				$title = '帳號已停用';
				break;
			case 'change_password_success':
				$title = '修改密碼成功';
				break;
			case 'change_password_error':
				$title = '修改密碼失敗';
				break;
			case 'forgot_password_success':
				$title = '已寄送新密碼';
				break;
			case 'forgot_password_fail':
				$title = '帳號輸入錯誤';
				break;
			default:
			case '':
				show_404();
				exit;
				break;
		}
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		
		$data = array(
			'Website'		=>	$Website,
			'page_title'	=>	$title,
		);
		$this->theme->views('member/message/'.$status, $data);
	}
	
	/*
		確認帳號有無重複
	*/
	public function ajax_account_check(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$account = strtolower($_POST['account']);

			if(!$account){
				echo 'false';
				exit;
			}

			$sql = sprintf("SELECT A.id
							FROM %s A
							WHERE A.account=%s
							LIMIT 1",
							$Website['table']['member'],
							sql_string($account, "text")
							);
			$rows1 = $this->query->select($sql, 1);
			
			if(!sizeof($rows1)){
				//不存在
				echo 'true';
			}else{
				//存在
				echo 'false';
			}
		}
	}
	
	/*
		確認登入狀態
	*/
	public function ajax_login_status(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		$status = 0;
		if($_SESSION['member_info']['id']){
			$status = 1;
		}
		echo $status;
	}
	
	/*
		訂單管理
	*/
	public function order($id=0){
		$Website = $this->Website;
		$this->preload->current_location();
		$this->check_login();
		
		
		if(!$id){
			$sql = sprintf("SELECT A.* 
							FROM %s A 
							WHERE A.member_id=%s
							ORDER BY A.id desc
							",
							$Website['table']['orders'],
							sql_string($_SESSION['member_info']['id'], 'int')
						  );
			$rows1 = $this->query->select($sql);
			
			
			$title = '訂單管理';
			$data = array(
				'title'			=>	$title,
			);
			$this->theme->config($data);

			$data = array(
				'Website'		=>	$Website,
				'page_title'	=>	$title,
				'rows1'			=>	$rows1,
			);
			$this->theme->views('member/order/index', $data);
			
		}else{
			$sql = sprintf("SELECT A.* 
							FROM %s A 
							WHERE A.id=%s",
							$Website['table']['orders'],
							sql_string($id, 'int')
						  );
			$rows1 = $this->query->select($sql, 1);
			
			$sql = sprintf("SELECT A.*, B.name product_name
							FROM %s A 
							LEFT JOIN %s B ON A.product_id=B.id
							WHERE A.order_id=%s
							ORDER BY A.id asc
							",
							$Website['table']['inventory'],
							$Website['table']['product'],
							sql_string($id, 'int')
							);
			$rows2 = $this->query->select($sql);
			
			
			$title = date('Y/m/d H:i', $rows1['insert_time']);
			$data = array(
				'title'			=>	$title,
			);
			$this->theme->config($data);

			$data = array(
				'Website'		=>	$Website,
				'page_title'	=>	$title,
				'rows1'			=>	$rows1,
				'rows2'			=>	$rows2,
			);
			$this->theme->views('member/order/content', $data);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/*
		是否登入
	*/
	private function check_login(){
		if(!$_SESSION['member_info']['id']){
			redirect('member/login/', 'refresh');
		}
	}
	
	private function read_cookie(){
		echo '<pre>';
		//echo $this->input->cookie('member_id');
		print_r($_COOKIE);
	}
	
	private function remove_cookie(){
		delete_cookie("member_info");
	}
}
