<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	function __construct(){
        parent::__construct();
		$this->preload->main();
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$Website = $this->Website;
		
		switch($_SERVER['REDIRECT_URL']){
			case 'admin/':
			case '/admin':
				//當只有輸入admin時 自動導到後台首頁
				header('Location: admin/index/');
				exit;
				break;
			case '/admin/':
				//當只有輸入admin時 自動導到後台首頁
				header('Location: index/');
				exit;
				break;
		}
		
		$this->preload->current_location();
		/*
		$data = array(
			'title'			=>	'Welcome',
			'site_name'		=>	'Demo Site',
			'description'	=>	'theme manager demo',
			'keywords'		=>	'theme, demo, ec',
			'theme'			=>	'default',
			
			'header_config' =>	array(
				'display'		=>	true,
				'theme'			=>	$header ? $header : 'default',
				'width'			=>	'within_fullwidth',
				'stickup'		=>	false,
			),
			'footer_config' =>	array(
				'display'		=>	true,
				'theme'			=>	$footer ? $footer : 'default',
				'width'			=>	'fullwidth',
			),
			'ogimage'		=>	array(
				'id'			=>	1,
				'type'			=>	'site_ogimage'
			),
		);
		*/
		$this->theme->config(array());
		
		$sql = sprintf("SELECT P.*, B.file_url, B.name file_name
						FROM %s P
						LEFT JOIN %s B ON B.id = (SELECT C.media_id FROM %s C WHERE C.item_id=P.id and C.item_type=%s ORDER BY C.sort asc limit 1)
						WHERE P.display=1 and
							(P.display_start_time is null or P.display_start_time=0 or P.display_start_time<=%s) and 
							(P.display_end_time is null or P.display_end_time=0 or P.display_end_time>%s)
						ORDER BY P.id desc
						LIMIT 6
						",
						$Website['table']['product'],
					   
						$Website['table']['media'],
						$Website['table']['media_relation'],
					   	sql_string('product', 'text'),
					   
					   	time(),
					   	time()
						);
		$product = $this->query->select($sql);
		
		for($i=0; $i<sizeof($product); $i++){
			$product[$i] = $this->product_query->price_calculate($product[$i]);
		}
		
		
		$sql = sprintf("SELECT *
						FROM %s A
						WHERE A.display=1
						ORDER BY A.id desc
						LIMIT 5
						",
						$Website['table']['news']
						);//echo $sql;exit;
		$news = $this->query->select($sql);
		
		
		$data = array(
			'Website'		=>	$Website,
			'product'		=>	$product,
			'news'			=>	$news,
		);
		$this->theme->views('welcome/index', $data);
	}
	
	public function show_404(){
		$Website = $this->Website;
		
		$title = '404 Page Not Found';
		
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		//	core/MY_Exceptions 也要一起
		$data = array(
			'Website'	=>	$Website,
		);
		$this->theme->error_views('404', $data);
	}
}
