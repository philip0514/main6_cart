<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->preload->main();
    }
	
	public function index($page){
		$Website = $this->Website;
		
		$root = base_url();
		$rows0 = array();
		$rows0[] = array(
			'url'		=>	$root,
			'lastmod'	=>	date('Y-m-d', time()),
		);
		$rows0[] = array(
			'url'		=>	$root.'member/register/',
			'lastmod'	=>	date('Y-m-d', time()),
		);
		$rows0[] = array(
			'url'		=>	$root.'member/login/',
			'lastmod'	=>	date('Y-m-d', time()),
		);
		$rows0[] = array(
			'url'		=>	$root.'member/forget_password/',
			'lastmod'	=>	date('Y-m-d', time()),
		);
		
		//分類
		$sql = sprintf("SELECT A.id, A.url_name, A.modify_time
						FROM %s A
						WHERE A.display=1
						ORDER BY A.sort asc
						",
						$Website['table']['category']
						);
		$rows1 = $this->query->select($sql);
		for($i=0; $i<sizeof($rows1); $i++){
			if($rows1[$i]['url_name']){
				$rows0[] = array(
					'url'		=>	$root.'category/'.$rows1[$i]['url_name'].'/',
					'lastmod'	=>	$rows1[$i]['modify_time']?date('Y-m-d\TH:i:s', $rows1[$i]['modify_time']):'',
				);
			}else{
				$rows0[] = array(
					'url'		=>	$root.'category/'.$rows1[$i]['id'].'/',
					'lastmod'	=>	$rows1[$i]['modify_time']?date('Y-m-d\TH:i:s', $rows1[$i]['modify_time']):'',
				);
			}
		}
		
		//商品
		$sql = sprintf("SELECT P.id, P.modify_time
						FROM %s P
						WHERE P.display=1 and
								(P.display_start_time<=%s or P.display_start_time is null or P.display_start_time=0) and
								(P.display_end_time>%s or P.display_end_time is null or P.display_end_time=0) 
						ORDER BY P.id asc
						",
						$Website['table']['product'],
						time(),
					   	time()
						);
		$rows2 = $this->query->select($sql);
		for($i=0; $i<sizeof($rows2); $i++){
			$rows0[] = array(
				'url'		=>	$root.'product/content/'.$rows2[$i]['id'].'/',
				'lastmod'	=>	$rows2[$i]['modify_time']?date('Y-m-d\TH:i:s', $rows2[$i]['modify_time']):'',
			);
		}
		
		//關於我們
		$rows0[] = array(
			'url'		=>	$root.'about/',
			'lastmod'	=>	date('Y-m-d', time()),
		);
		$sql = sprintf("SELECT A.id, A.modify_time
						FROM %s A
						WHERE A.display=1
						ORDER BY A.sort asc
						",
						$Website['table']['about']
						);
		$rows3 = $this->query->select($sql);
		for($i=0; $i<sizeof($rows3); $i++){
			$rows0[] = array(
				'url'		=>	$root.'about/content/'.$rows3[$i]['id'].'/',
				'lastmod'	=>	$rows3[$i]['modify_time']?date('Y-m-d\TH:i:s', $rows3[$i]['modify_time']):'',
			);
		}
		
		//最新消息
		$rows0[] = array(
			'url'		=>	$root.'news/',
			'lastmod'	=>	date('Y-m-d', time()),
		);
		$sql = sprintf("SELECT A.id, A.modify_time
						FROM %s A
						WHERE A.display=1
						ORDER BY A.id asc
						",
						$Website['table']['news']
						);
		$rows4 = $this->query->select($sql);
		for($i=0; $i<sizeof($rows4); $i++){
			$rows0[] = array(
				'url'		=>	$root.'news/content/'.$rows4[$i]['id'].'/',
				'lastmod'	=>	$rows4[$i]['modify_time']?date('Y-m-d\TH:i:s', $rows4[$i]['modify_time']):'',
			);
		}
		
		$data = array(
			'Website'			=>	$Website,
			'rows0'				=>	$rows0,
		);
		$this->load->view('sitemap/index', $data);
	}
}
?>