<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->preload->main();
    }
	
	public function index($text=null, $page=1){
		$Website = $this->Website;
		$this->preload->current_location();
		
		$q = $_GET['q'];
		if($q){
			$q = preg_replace($Website['url_allow_chars'] , '' , $q);
			header("Location: /search/".$q.'/');
			exit;
		}
		
		$text = urldecode($text);
		$text_array = explode(' ', $text);
		
		$title = '搜尋：'.implode(' + ', $text_array);
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		//product
		$column = array(
			'P.name', 'P.content'
		);
		$search_query = $this->get_query($column, $text);
		
		$sql = sprintf("SELECT P.*, B.file_url
						FROM %s A
						LEFT JOIN %s P ON A.product_id=P.id
						LEFT JOIN %s B ON B.id = (SELECT C.media_id FROM %s C WHERE C.item_id=P.id and C.item_type=%s ORDER BY C.sort asc limit 1)
						WHERE P.display=1 and
							(P.display_start_time is null or P.display_start_time=0 or P.display_start_time<=%s) and 
							(P.display_end_time is null or P.display_end_time=0 or P.display_end_time>%s) 
							%s
						ORDER BY P.id desc
						",
						$Website['table']['product_category_relation'],
						$Website['table']['product'],
					   
						$Website['table']['media'],
						$Website['table']['media_relation'],
					   	sql_string('product', 'text'),
					   	
					   	time(),
					   	time(),
					   	$search_query
						);//echo $sql;exit;
		$product = $this->query->select($sql);
		
		for($i=0; $i<sizeof($product); $i++){
			$product[$i] = $this->product_query->price_calculate($product[$i]);
		}
		
		//tag
		$column = array(
			'A.name'
		);
		$search_query = $this->get_query($column, $text);
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.display=1 %s
						ORDER BY A.id desc
						",
						$Website['table']['tag'],
					   	$search_query
						);
		$tag = $this->query->select($sql);
		
		//tag
		$column = array(
			'A.name', 'A.content'
		);
		$search_query = $this->get_query($column, $text);
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.display=1 %s
						ORDER BY A.id desc
						",
						$Website['table']['news'],
					   	$search_query
						);
		$news = $this->query->select($sql);
		
		$item_count = sizeof($product) + sizeof($tag) + sizeof($news);
		
		$data = array(
			'Website'			=>	$Website,
			'page_title'		=>	$title,
			'text'				=>	$text,
			'pagination'		=>	$pagination,
			'item_count'		=>	$item_count,
			'product'			=>	$product,
			'tag'				=>	$tag,
			'news'				=>	$news,
		);
		$this->theme->views('search/index', $data);
	}
	
	private function get_query($column, $string){
		
		$string = explode(' or ', strtolower($string));
		for($i=0; $i<sizeof($string); $i++){
			preg_match_all('~(?|"([^"]+)"|(\S+))~', $string[$i], $matched);
			$data = $matched[1];
			for($j=0; $j<sizeof($data); $j++){
				for($k=0; $k<sizeof($column); $k++){
					if(substr($data[$j], 0, 1)=='-' && !is_int($data[$j])){
						if(!in_array(substr($data[$j], 1), $data_exclude)){
							$data_exclude[] = substr($data[$j], 1);
						}
					}else{
						$query_3[] = sprintf('%s like %s', $column[$k], "'%".$data[$j]."%'");
					}
				}
				if(sizeof($query_3)){
					$query_2[] = '('.implode(' or ', $query_3).')';
				}
				unset($query_3);
			}
			
			if(sizeof($query_2)){
				$query_1[] = implode(' and ', $query_2);
			}
			unset($query_2);
		}
		
		$query[] = implode(' or ', $query_1);
		
		for($j=0; $j<sizeof($data_exclude); $j++){
			for($k=0; $k<sizeof($column); $k++){
				$query_3[] = sprintf('%s not like %s', $column[$k], "'%".$data_exclude[$j]."%'");
			}
		}
		if(sizeof($query_3)){
			$query[] = '('.implode(' or ', $query_3).')';
		}
		
		$where_query = implode(' and ', $query);
		
		return $where_query ? ' and '.$where_query : '';
	}
}
?>