<?
class Media extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->preload->main();
		//$this->preload->check_domain();
    }
	
	public function do_upload($start=0, $limit=0){
		$this->media_query->do_upload($start, $limit);
	}
	
	public function manager(){
		$Website 			= $this->Website;
		$is_image 			= $_POST['is_image'];
		$selectable_limit 	= $_POST['selectable_limit'];
		//$multiple 		= $_POST['multiple'];
		$input_field 		= explode(',', $_POST['input_field']);
		$file_original 			= $_POST['file_original'];
		$per_page = 50;
		
		//全部的圖片
		$rows1 = $this->media_query->file_list('array', $is_image, 0, $per_page);
		
		//已經選擇的圖片
		if(sizeof($input_field) && $input_field[0]){
			$sql = sprintf("SELECT A.* 
							FROM %s A 
							WHERE A.id in (%s)
							ORDER BY FIELD(A.id, %s)
							",
							$Website['table']['media'],
							implode(',', $input_field),
							implode(',', $input_field)
			);
			$rows2 = $this->query->select($sql);
		}
		
		//自己上傳的圖片
		$rows3 = $this->media_query->my_file_list('array', $is_image, 0, $per_page);
		
		$data = array(
			'Website'			=>	$Website,
			'rows1'				=>	$rows1,
			'rows2'				=>	$rows2,
			'rows3'				=>	$rows3,
			'selectable_limit'	=>	(int)$selectable_limit,
			'input_field'		=>	$input_field,
			'file_original'			=>	$file_original,
		);
		echo $this->load->view('media/manager', $data, true);	
	}
	
	public function file_list_infinite($type=1, $page=0){
		$Website = $this->Website;
		
		$page--;
		if($page<0){
			$page=0;
		}
		$per_page = 50;
		$start = $page*$per_page;
		$search = $_GET['search'];
		$file_original = $_GET['file_original'];
		
		switch($type){
			default:
			case 1:
				$rows1 = $this->media_query->file_list('array', false, $start, $per_page);
				break;
			case 2:
				break;
			case 3:
				$rows1 = $this->media_query->my_file_list('array', false, $start, $per_page);
				break;
			case 4:
				$rows1 = $this->media_query->file_list('array', false, $start, $per_page, $search);
				break;
		}
		
		//echo '<pre>';print_r($rows1);exit;
		
		$data = array(
			'Website'	=>	$Website,
			'rows1'		=>	$rows1,
			'file_original'	=>	$file_original,
			'next'		=>	$page++,
		);
		echo $this->load->view('media/file_list_infinite', $data, true);
	}
	
	public function edit_modal(){
		$Website = $this->Website;
		$id = $_POST['id'];
		$type = $_POST['type'];
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.id=%s
						LIMIT 1
						",
						$Website['table']['media'],
						sql_string($id, "int")
						);//echo $sql;
		$rows1 = $this->query->select($sql, 1);
		
		switch($type){
			default:
			case 'square':
				$rows2 = array(
					'box_data'	=>	json_encode(
						array(
							'top'		=>	0,
							'width'		=>	500,
							'height'	=>	500,
						)
					),
					'canvas_data'	=>	json_encode(
						array(
							'top'		=>	0,
							'width'		=>	500,
							'height'	=>	500,
						)
					),
					'min_box_width'		=>	(int)$Website['media']['thumb_width'],
					'min_box_height'	=>	(int)$Website['media']['thumb_height'],
					'crop_data'			=>	htmlspecialchars_decode($rows1['crop_data']),
					'aspectRatio'		=>	1,
				);
				break;
			case 'facebook':
				$rows2 = array(
					'box_data'	=>	json_encode(
						array(
							'top'		=>	0,
							'width'		=>	560,
							'height'	=>	292,
						)
					),
					'canvas_data'	=>	json_encode(
						array(
							'top'		=>	0,
							'width'		=>	560,
							'height'	=>	292,
						)
					),
					'min_box_width'		=>	560,
					'min_box_height'	=>	292,
					'crop_data'			=>	htmlspecialchars_decode($rows1['facebook_crop_data']),
					'aspectRatio'		=>	560/292,
				);
				break;
		}
		
		$data = array(
			'Website'		=>	$Website,
			'rows1'			=>	$rows1,
			'rows2'			=>	$rows2,
			'id'			=>	$id,
			'type'			=>	$type,
		);
		echo $this->load->view('media/edit_modal', $data, true);	
	}
	
	public function crop($id){
		$Website = $this->Website;
		$this->load->library(array('cropper'));
		//print_r($_POST['avatar_data']);
		//print_r($_POST);exit;
		//echo 555;exit;
		//echo 55;exit;
		$avatar_data = $_POST['avatar_data'];
		$type = $_POST['type'];
		
		switch($type){
			default:
			case 'square':
				$type = 'square';
		
				$data = array(
					'src'		=>	$_POST['avatar_src'],
					'data'		=>	$_POST['avatar_data'],
					'name'		=>	$_POST['avatar_name'],
					'path'		=>	$_POST['avatar_path'],
					'file'		=>	$_POST['avatar_file'],
					'id'		=>	$id,
				);
				$this->cropper->main($data);
				
				$rows1 = array(
					'crop_data'	=>	$avatar_data,
					'id'		=>	$id,
					
					'title'		=>	$_POST['title'],
					'content'	=>	$_POST['content'],
					'excerpt'	=>	$_POST['excerpt'],
					'color_id'	=>	$_POST['color_id'],
				);
				$this->media_query->update_crop_data($rows1);
				//echo 5;exit;
				
				break;
			case 'facebook':
				
				$data = array(
					'src'		=>	$_POST['avatar_src'],
					'data'		=>	$_POST['avatar_data'],
					'name'		=>	$_POST['avatar_name'],
					'path'		=>	$_POST['avatar_path'],
					'file'		=>	$_POST['avatar_file'],
					'id'		=>	$id,
				);
				$this->cropper->facebook($data);
				
				$rows1 = array(
					'crop_data'	=>	$avatar_data,
					'id'		=>	$id,
					
					'title'		=>	$_POST['title'],
					'content'	=>	$_POST['content'],
					'excerpt'	=>	$_POST['excerpt'],
				);
				$this->media_query->update_facebook_crop_data($rows1);
				
				
				
				
				break;
		}
		
		
		
		$response = array(
			'state'  		=> 200,
			'message' 		=> $this->cropper->getMsg(),
			'result'		=> $this->cropper->getResult(),
			'id'			=> $id,
			'type'			=> $type,
		);
		//print_r($response);exit;
		echo json_encode($response);
		
		
	}
	
	public function image($type=640, $name){
		$Website = $this->Website;
		
		$path = $Website['media']['upload_folder'].$Website['media']['thumb_size'][$type]['folder'];
		$file = $path.$name;
		
		$ext_array = explode('.', $name);
		$extension = end($ext_array);
		
		switch($extension){
			case 'jpg':
			case 'jpeg':
				$content_type = 'jpeg';
				break;
			case 'png':
				$content_type = 'png';
				break;
			case 'gif':
				$content_type = 'gif';
				break;
			default:
				$content_type = 'jpeg';
				break;
		}
		
		$data = file_get_contents($file);
		
		$this->output->set_header("HTTP/1.0 200 OK");
		$this->output->set_header("HTTP/1.1 200 OK");
		$this->output->set_header('Last-Modified: '.gmdate('D，d M Y H:i:s', time()).' GMT');
		$this->output->set_header('Expires: '.gmdate('D，d M Y H:i:s', time() + (60*60)).' GMT');
		$this->output->set_header('Cache-Control: max-age='. 60*60);
		$this->output->set_header('Content-Length: ' . strlen($data));
		
		$this->output->set_content_type($content_type)->set_output($data);
	}
}
?>