<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->preload->main();
    }
	
	public function index($type, $page=1){
		$Website = $this->Website;
		$this->preload->current_location();
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.display=1 and (A.id=%s or A.url_name=%s)
						LIMIT 1
						",
						$Website['table']['category'],
					   	sql_string($type, 'int'),
					   	sql_string($type, 'text')
						);
		$rows1 = $this->query->select($sql, 1);
		
		if(!$rows1['id']){
			show_404();
		}
		
		$title = $rows1['name'];
		
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		//pagination
		$sql = sprintf("SELECT count(P.id) total
						FROM %s A
						LEFT JOIN %s P ON A.product_id=P.id
						WHERE A.category_id=%s and P.display=1 and 
							(P.display_start_time is null or P.display_start_time=0 or P.display_start_time<=%s) and 
							(P.display_end_time is null or P.display_end_time=0 or P.display_end_time>%s)
						",
						$Website['table']['product_category_relation'],
						$Website['table']['product'],
					   	sql_string($rows1['id'], 'int'),
					   	time(),
					   	time()
						);
		$rows2 = $this->query->select($sql, 1);
		$total_rows = $rows2['total'];
		
		$Website['pagination']['base_url'] = base_url().'category/'.$type.'/';
		$Website['pagination']['total_rows'] = $total_rows;
		$Website['pagination']['per_page'] = 9;
		$Website['pagination']['num_links'] = 3;
		$Website['pagination']['uri_segment'] = 3;
		$this->pagination->initialize($Website['pagination']);
		$pagination = $this->pagination->create_links();
		$page_total = ceil($total_rows/$Website['pagination']['per_page']);
		$page = (int)$page ? (int)$page : 1;
		$skip = ($page-1) * $Website['pagination']['per_page'];
		$limit = $Website['pagination']['per_page'];
		
		$sql = sprintf("SELECT P.*, B.file_url
						FROM %s A
						LEFT JOIN %s P ON A.product_id=P.id
						LEFT JOIN %s B ON B.id = (SELECT C.media_id FROM %s C WHERE C.item_id=P.id and C.item_type=%s ORDER BY C.sort asc limit 1)
						WHERE A.category_id=%s and P.display=1 and
							(P.display_start_time is null or P.display_start_time=0 or P.display_start_time<=%s) and 
							(P.display_end_time is null or P.display_end_time=0 or P.display_end_time>%s)
						ORDER BY A.sort asc
						LIMIT %s, %s
						",
						$Website['table']['product_category_relation'],
						$Website['table']['product'],
					   
						$Website['table']['media'],
						$Website['table']['media_relation'],
					   	sql_string('product', 'text'),
					   
					   	sql_string($rows1['id'], 'int'),
					   	time(),
					   	time(),
					   	sql_string($skip, 'int'),
					   	sql_string($limit, 'int')
						);
		$product = $this->query->select($sql);
		
		for($i=0; $i<sizeof($product); $i++){
			$product[$i] = $this->product_query->price_calculate($product[$i]);
		}
		
		$data = array(
			'Website'		=>	$Website,
			'page_title'	=>	$title,
			'pagination'	=>	$pagination,
			'product'		=>	$product,
		);
		$this->theme->views('category/index', $data);
	}
	
	
}
?>