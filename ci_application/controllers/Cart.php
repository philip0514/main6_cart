<?
class Cart extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->preload->main();
    }
	
	public function index(){
		$Website = $this->Website;
		$this->preload->current_location();
		
		if($_GET['step']){
			header('Location: /cart/');
			exit;
		}
		
		$cart = $this->cart->contents();
		//print_r($cart);exit;
		foreach($cart as $key => $value){
			$sql = sprintf("SELECT A.*
							FROM %s A 
							WHERE 	
								A.id=%s and A.display=1
							LIMIT 1
							", 
							$Website['table']['product'],
							sql_string($value['id'], 'int')
						  );
			$product = $this->query->select($sql, 1);
			
			if($product['display']){
				$product = $this->product_query->price_calculate($product);
				$value['price'] = $product['price'];

				$data = array(
					'rowid'		=>	$key,
					'price'		=>	$value['price'],
				);
				$this->cart->update($data);

				$rows1[] = $value;
			}else{
				$this->cart->remove($key);
			}
		}
		
		$subtotal_price = $this->cart->total();
		$total_price = $subtotal_price-$discount_price+$freight;
		
		$title = '購物車';
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		$data = array(
			'Website'			=>	$Website,
			'page_title'		=>	$title,
			'rows1'				=>	$rows1,
			'subtotal_price'	=>	$subtotal_price,
			'total_price'		=>	$total_price
		);
		$this->theme->views('cart/index', $data);
	}
	
	public function insert(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		if($_SERVER['REQUEST_METHOD']=='POST'){

			$product_id = $_POST['product_id'];
			$spec_id = $_POST['spec'];
			$quantity = $_POST['quantity'];
			
			$sql = sprintf("SELECT A.*, B.quantity, C.name spec_name 
							FROM %s A 
							LEFT JOIN %s B ON A.id=B.product_id
							LEFT JOIN %s C ON B.spec_id=C.id
							WHERE 	
								A.id=%s and B.spec_id=%s and A.display=1 and 
								B.quantity>0 and B.display=1 and B.type=0 and B.plus_type=0
							LIMIT 1
							", 
							$Website['table']['product'],
							$Website['table']['inventory'],
							$Website['table']['spec'],
							sql_string($product_id, 'int'),
							sql_string($spec_id, 'int')
						  );
			$rows1 = $this->query->select($sql, 1);
			
			if(!$rows1['id']){
				$result = array(
					'status'		=>	'false'
				);
			}else{
				
				$sql = sprintf("SELECT B.*
								FROM %s A
								LEFT JOIN %s B ON A.media_id=B.id
								WHERE A.item_id=%s and A.item_type=%s and B.display=1
								ORDER BY A.sort asc
								LIMIT 1
								", 
								$Website['table']['media_relation'],
								$Website['table']['media'],
								sql_string($rows1['id'], 'int'),
								sql_string('product', 'text')
							  );
				$media = $this->query->select($sql, 1);
				
				$rows1 = $this->product_query->price_calculate($rows1);
				
				$data = array(
					'id'		=>	$rows1['id'],
					'qty'		=>	$quantity>$rows1['quantity'] ? $rows1['quantity'] : $quantity,
					'price'		=>	$rows1['price'],
					'name'		=>	$rows1['name'],
					'options'	=>	array(
						'spec'		=>	(int)$spec_id,
					),
					'info'		=>	array(
						'media'			=>	$media['file_url'],
						'spec'			=>	$rows1['spec_name'],
						'list_price'	=>	$rows1['list_price'],
						'description'	=>	htmlspecialchars_decode($rows1['description']),
					),
					/*
					'plus'		=>	array(
						array(
							'id'	=>	'101',
							'qty'		=> 1,
							'price'		=> 39.95,
							'name'		=> 'T-Shirt',

						),
					),
					*/
				);
				
				if(array_key_exists($this->cart->rowid($data), $this->cart->contents())){
					$result = array(
						'status'		=>	'false'
					);
				}else{
					$cart_hash = $this->cart->insert($data);
					
					$result = array(
						'status'		=>	'true',
						'cart_hash'		=>	$cart_hash,
					);
				}
			}
		}
		
		echo json_encode($result);
	}
	
	public function calculate(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			
			//$product_id = $_POST['product_id'];
			//$spec_id = $_POST['spec_id'];
			
			$row_id = $_POST['row_id'];
			$quantity = $_POST['quantity'];
			$payment_type = $_POST['payment_type'];
			$shipping_type = $_POST['shipping_type'];
			$discount_type = $_POST['discount_type'];
			$coupon_select = $_POST['coupon_select'];
			$coupon_manual = $_POST['coupon_manual'];
			
			
			//更改數量
			for($i=0; $i<sizeof($row_id); $i++){
				$data[] = array(
					'rowid'		=>	$row_id[$i],
					'qty'		=>	$quantity[$i],
				);
			}
			$this->cart->update($data);
			
			//移除商品
			$cart_key = array_keys($this->cart->contents());
			for($i=0; $i<sizeof($cart_key); $i++){
				if(!in_array($cart_key[$i], $row_id)){
					$this->cart->remove($cart_key[$i]);
				}
			}
			
			//商品小計
			$subtotal_price = $this->cart->total();
			
			//折扣
			switch($discount_type){
				default:
				case 0:
					$discount_price = 0;
					break;
				case 1:
					//購物金
					break;
				case 2:
				case 3:
					//coupon
					if($discount_type==2){
						$sql = sprintf("SELECT * FROM %s WHERE id=%s LIMIT 1", $Website['table']['coupon'], sql_string($coupon_select, 'int'));
						$rows_coupon = $this->query->select($sql, 1);
						
						$coupon_code = $rows_coupon['name'];
					}elseif($discount_type==3){
						$coupon_code = $coupon_manual;
					}
					
					$discount_price = 0;
					if($coupon_code){
						
						$coupon_result = $this->coupon_query->usable($coupon_code, $_SESSION['member_info']['id']);
						
						if($coupon_result['usable'] && $subtotal_price >= $coupon_result['discount_gate']){
							if(!$coupon_result['discount_type']){
								//元
								$discount_price = $coupon_result['discount'];
							}else{
								//％
								$discount_price = $subtotal_price*$coupon_result['discount']/100;
							}
							
							$coupon_id = $coupon_result['coupon_id'];
						}
					}
					
					break;
			}
			
			//總計
			$total_price = $subtotal_price-$discount_price+$freight;
			
			
			
			
			
			$result = array(
				'payment_type'			=>	$payment_type,
				'shipping_type'			=>	$shipping_type,
				'discount_type'			=>	$discount_type,
				'coupon_select'			=>	$coupon_select,
				'coupon_manual'			=>	$coupon_manual,
				'coupon_id'				=>	$coupon_id,
				
				'discount_price'		=>	(int)$discount_price,
				'freight'				=>	(int)$freight,
				'subtotal_price'		=>	(int)$subtotal_price,
				'total_price'			=>	(int)$total_price,
			);
			
			$_SESSION['cart']['payment'] = $result;
			
			echo json_encode($result);
		}
	}
	
	public function set_info(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$same = $_POST['same'];
			$buyer = $_POST['buyer'];
			$recipient= $_POST['recipient'];
			
			$_SESSION['cart']['info'] = array(
				'same'			=>	$same,
				'buyer'			=>	$buyer,
				'recipient'		=>	$recipient,
			);
		}
		
	}
	
	public function get_info(){
		$Website = $this->Website;
		
	}
	
	public function shipping(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			
			$sql = sprintf("SELECT * 
							FROM %s 
							WHERE id=%s",
						  	$Website['table']['member'],
						   	sql_string($_SESSION['member_info']['id'], 'int')
						  );
			$rows1 = $this->query->select($sql, 1);
			
			$sql = sprintf("SELECT * 
							FROM %s 
							ORDER BY sort asc
							",
						  	$Website['table']['zip_city']
						  );
			$rows2 = $this->query->select($sql);
			
			$sql = sprintf("SELECT * 
							FROM %s 
							WHERE city_id=%s
							ORDER BY sort asc
							",
						  	$Website['table']['zip_area'],
						   	sql_string($rows1['city_id']?$rows1['city_id']:$rows2[0]['id'], 'int')
						  );
			$rows3 = $this->query->select($sql);
			
			$data = array();
			$this->theme->config($data);
			
			$data = array(
				'Website'			=>	$Website,
				'rows1'				=>	$rows1,
				'rows2'				=>	$rows2,
				'rows3'				=>	$rows3,
			);
			echo $this->theme->view('cart/shipping', $data, true);
			
		}
		
	}
	
	public function discount(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			
			
			$sql = sprintf("SELECT B.* 
							FROM %s A
							LEFT JOIN %s B ON A.coupon_id=B.id
							WHERE A.member_id=%s and A.used_by is null
							",
						  	$Website['table']['member_coupon_relation'],
						  	$Website['table']['coupon'],
						   	sql_string($_SESSION['member_info']['id'], 'int')
						  );
			$rows1 = $this->query->select($sql);
			
			
			$data = array();
			$this->theme->config($data);
			
			$data = array(
				'Website'			=>	$Website,
				'rows1'				=>	$rows1,
			);
			echo $this->theme->view('cart/discount', $data, true);
		}
		
	}
	
	public function submit(){
		$Website = $this->Website;
		$this->preload->check_domain();
		
		if($_SERVER['REQUEST_METHOD']=='POST'){
			
			$cart = $_SESSION['cart'];
			$cart_payment = $cart['payment'];
			$cart_info = $cart['info'];
			
			$invoice = $_POST['invoice'];						//發票
			$invoice_donate = $_POST['invoice_donate'];			//捐贈愛心碼
			$invoice_duplicate = $_POST['invoice_duplicate'];	//二聯式發票類型
			$invoice_mobile = $_POST['invoice_mobile'];			//手機載具
			$invoice_emcard = $_POST['invoice_emcard'];			//自然人憑證
			$invoice_guid = $_POST['invoice_guid'];				//統編
			$invoice_title = $_POST['invoice_title'];			//公司抬頭
			
			$recipient_note = $_POST['recipient_note'];
			
			//購買人與收件人
			$cart_info['buyer']['city_id'] = $cart_info['buyer']['city'];
			$cart_info['buyer']['area_id'] = $cart_info['buyer']['area'];
			
			$sql = sprintf("SELECT * FROM %s WHERE id=%s LIMIT 1", $Website['table']['zip_city'], $cart_info['buyer']['city']);
			$row_buyer_city = $this->query->select($sql, 1);
			$cart_info['buyer']['city_text'] = $row_buyer_city['name'];
			
			$sql = sprintf("SELECT * FROM %s WHERE id=%s LIMIT 1", $Website['table']['zip_area'], $cart_info['buyer']['area']);
			$row_buyer_area = $this->query->select($sql, 1);
			$cart_info['buyer']['area_text'] = $row_buyer_area['name'];
			$cart_info['buyer']['zip'] = $row_buyer_area['code'];
			
			if($cart_info['same']){
				$cart_info['recipient'] = $cart_info['buyer'];
			}else{
				
				$cart_info['recipient']['city_id'] = $cart_info['recipient']['city'];
				$cart_info['recipient']['area_id'] = $cart_info['recipient']['area'];
				
				$sql = sprintf("SELECT * FROM %s WHERE id=%s LIMIT 1", $Website['table']['zip_city'], $cart_info['recipient']['city']);
				$row_recipient_city = $this->query->select($sql, 1);
				$cart_info['recipient']['city_text'] = $row_recipient_city['name'];
				
				$sql = sprintf("SELECT * FROM %s WHERE id=%s LIMIT 1", $Website['table']['zip_area'], $cart_info['recipient']['area']);
				$row_recipient_area = $this->query->select($sql, 1);
				$cart_info['recipient']['area_text'] = $row_recipient_area['name'];
				$cart_info['recipient']['zip'] = $row_recipient_area['code'];
			}
			
			//訂單
			$rows1 = array(
				'member_id'					=>	ci_sql_string($_SESSION['member_info']['id'], 'int'),
				'payment_type'				=>	ci_sql_string($cart_payment['payment_type'], 'int'),
				'payment_status'			=>	0,
				'discount_type'				=>	ci_sql_string($cart_payment['discount_type'], 'int'),
				'discount_price'			=>	ci_sql_string($cart_payment['discount_price'], 'int'),
				'coupon_id'					=>	ci_sql_string($cart_payment['coupon_id'], 'int'),
				'freight'					=>	ci_sql_string($cart_payment['freight'], 'int'),
				'subprice'					=>	ci_sql_string($cart_payment['subtotal_price'], 'int'),
				'total_price'				=>	ci_sql_string($cart_payment['total_price'], 'int'),
				
				//發票
				'invoice'					=>	ci_sql_string($invoice, 'int'),
				'invoice_donate'			=>	ci_sql_string($invoice_donate, 'text'),
				'invoice_duplicate'			=>	ci_sql_string($invoice_duplicate, 'text'),
				'invoice_mobile'			=>	ci_sql_string($invoice_mobile, 'text'),
				'invoice_emcard'			=>	ci_sql_string($invoice_emcard, 'text'),
				'invoice_guid'				=>	ci_sql_string($invoice_guid, 'text'),
				'invoice_title'				=>	ci_sql_string($invoice_title, 'text'),
				
				//購買人
				'buyer_name'				=>	ci_sql_string($cart_info['buyer']['name'], 'text'),
				'buyer_zip'					=>	ci_sql_string($cart_info['buyer']['zip'], 'text'),
				'buyer_city'				=>	ci_sql_string($cart_info['buyer']['city_text'], 'text'),
				'buyer_city_id'				=>	ci_sql_string($cart_info['buyer']['city_id'], 'text'),
				'buyer_area'				=>	ci_sql_string($cart_info['buyer']['area_text'], 'text'),
				'buyer_area_id'				=>	ci_sql_string($cart_info['buyer']['area_id'], 'text'),
				'buyer_address'				=>	ci_sql_string($cart_info['buyer']['address'], 'text'),
				'buyer_phone'				=>	ci_sql_string($cart_info['buyer']['phone'], 'text'),
				'buyer_mobile'				=>	ci_sql_string($cart_info['buyer']['mobile'], 'text'),
				
				//收件人
				'recipient_name'			=>	ci_sql_string($cart_info['recipient']['name'], 'text'),
				'recipient_zip'				=>	ci_sql_string($cart_info['recipient']['zip'], 'text'),
				'recipient_city'			=>	ci_sql_string($cart_info['recipient']['city_text'], 'text'),
				'recipient_city_id'			=>	ci_sql_string($cart_info['recipient']['city_id'], 'text'),
				'recipient_area'			=>	ci_sql_string($cart_info['recipient']['area_text'], 'text'),
				'recipient_area_id'			=>	ci_sql_string($cart_info['recipient']['area_id'], 'text'),
				'recipient_address'			=>	ci_sql_string($cart_info['recipient']['address'], 'text'),
				'recipient_phone'			=>	ci_sql_string($cart_info['recipient']['phone'], 'text'),
				'recipient_mobile'			=>	ci_sql_string($cart_info['recipient']['mobile'], 'text'),
				'recipient_note'			=>	ci_sql_string($recipient_note, 'text'),
				
				'shipping_type'				=>	ci_sql_string($cart_payment['shipping_type'], 'int'),
				'shipping_day'				=>	ci_sql_string($cart_payment['shipping_day'], 'int'),
				'shipping_time'				=>	ci_sql_string($cart_payment['shipping_time'], 'int'),
				'shipping_status'			=>	0,
				
				'ip'						=>	ci_sql_string($this->preload->ip(), 'text'),
				'display'					=>	1,
				'insert_time'				=>	time(),
				'modify_time'				=>	time(),
			);
			$this->db->set($rows1);
			$this->db->insert($Website['table']['order']);
			$order_id = $this->db->insert_id();
			
			//訂單商品
			$product = $this->cart->contents();
			foreach($product as $key => $value){
				$rows2 = array(
					'product_id'	=>	ci_sql_string($value['id'], 'int'),
					'color_id'		=>	ci_sql_string($value['options']['color'], 'int'),
					'spec_id'		=>	ci_sql_string($value['options']['spec'], 'int'),
					'order_id'		=>	ci_sql_string($order_id, 'int'),
					'type'			=>	2,
					'quantity'		=>	ci_sql_string((int)$value['qty'], 'int'),
					'price'			=>	ci_sql_string((int)$value['price'], 'int'),
					//'cost_price'	=>	$value['info']['cost_price'],
					'plus_type'		=>	ci_sql_string((int)$value['info']['plus_type'], 'int'),
					'insert_time'	=>	time(),
					'modify_time'	=>	time(),
					'display'		=>	1,
				);
				$this->db->set($rows2);
				$this->db->insert($Website['table']['inventory']);
				
				//扣庫存
				$sql = sprintf("UPDATE %s SET quantity=quantity-%s WHERE product_id=%s and spec_id=%s and type=0",
							  	$Website['table']['inventory'],
							   	sql_string($value['qty'], 'int'),
							   	sql_string($value['id'], 'int'),
							   	sql_string($value['options']['spec'], 'int')
							  );
				$this->query->sql($sql);
				//echo $this->db->last_query();
			}
			
			//discount
			switch($cart_payment['discount_type']){
				case 1:
					//購物金
					$rows2 = array(
						'member_id'		=>	$_SESSION['member_info']['id'],
						'price'			=>	$cart_payment['coin_deduct'],
						'display'		=>	1,
						'type'			=>	1,
						'insert_time'	=>	time(),
						'modify_time'	=>	time(),
					);
					$this->db->set($rows2);
					$this->db->insert($Website['table']['feedback_fund']);
					
					break;
				case 2:
				case 3:
					//coupon
					if($cart_payment['coupon_id']){
						$sql = sprintf("SELECT A.*
										FROM %s A
										WHERE A.coupon_id=%s and A.member_id=%s",
										$Website['table']['member_coupon_relation'],
										sql_string($cart_payment['coupon_id'], "int"),
										sql_string($_SESSION['member_info']['id'], "int")
						);
						$rows2 = $this->query->select($sql, 1);
						
						$sql = sprintf("UPDATE %s
										SET used_count=used_count+1
										WHERE A.id=%s",
										$Website['table']['coupon'],
										sql_string($cart_payment['coupon_id'], "int")
						);
						$this->query->sql($sql, 1);
						
						if($rows2['member_id']){
							//update
							$rows3 = array(
								'order_id'		=>	$order_id,
								'used_by'		=>	$_SESSION['member_info']['id'],
								'used_time'		=>	time(),
							);
							$this->db->set($rows3);
							$this->db->update($Website['table']['member_coupon_relation'], array('member_id'=>$_SESSION['member_info']['id'], 'coupon_id'=>$cart_payment['coupon_id']));
						}else{
							//insert
							$rows3 = array(
								'order_id'		=>	$order_id,
								'coupon_id'		=>	$cart_payment['coupon_id'],
								'member_id'		=>	$_SESSION['member_info']['id'],
								'used_by'		=>	$_SESSION['member_info']['id'],
								'used_time'		=>	time(),
							);
							$this->db->set($rows3);
							$this->db->insert($Website['table']['member_coupon_relation']);
						}
					}
					break;
			}
			
			
			//金流id
			if($order_id){
				$payment_order = array(
					'order_id'		=>	$order_id,
				);
				$this->db->set($payment_order);
				$this->db->insert($Website['table']['orders_payment']);
				$payment_id = $this->db->insert_id();
				
				$payment_id = date('Ymd').str_pad($payment_id, 11, '0', STR_PAD_LEFT);			//永豐一次性付款支援19字元，豐掌櫃支援60字元，nccc支援40字元，以永豐為主
				$this->db->set(array(
					'payment_id'			=>	$payment_id,
				));
				$this->db->where('id', $order_id);
				$this->db->update($Website['table']['orders']);
			}
			
			
			/*
			
			購物車流程結束
			---------------------------------------------------------------------------
			結帳流程開始
			
			*/
			
			
			$this->theme->config(array());
			
			//根據付款方式不同，走不同的路
			switch($cart_payment['payment_type']){
				case 0:
					//atm
					$result_data = array(
						'payment_type'			=>	0,
						'redirect'				=>	1,
						'location'				=>	'/cart/success/',
					);
					break;
				case 1:
					//信用卡 一次付清
					$payment_setting = $Website['payment_type'][1]['setting']['main'];
					
					$data = array(
						'Website'				=>	$Website,
						'payment'				=>	$payment_setting,
						'payment_id'			=>	$payment_id,
						'total_price'			=>	$cart_payment['total_price'],
					);
					$html = $this->theme->view($payment_setting['view_page'], $data, true);
					
					$result_data = array(
						'payment_type'			=>	1,
						'html'					=>	$html,
					);
					break;
				case 2:
					//信用卡 三期
					$payment_setting = $Website['payment_type'][2]['setting']['main'];
					
					$data = array(
						'Website'				=>	$Website,
						'payment'				=>	$payment_setting,
						'payment_id'			=>	$payment_id,
						'total_price'			=>	$cart_payment['total_price'],
					);
					$html = $this->theme->view($payment_setting['view_page'], $data, true);
					
					$result_data = array(
						'payment_type'			=>	2,
						'html'					=>	$html,
					);
					break;
				case 3:
					//信用卡 六期
					$payment_setting = $Website['payment_type'][3]['setting']['main'];
					
					$data = array(
						'Website'				=>	$Website,
						'payment'				=>	$payment_setting,
						'payment_id'			=>	$payment_id,
						'total_price'			=>	$cart_payment['total_price'],
					);
					$html = $this->theme->view($payment_setting['view_page'], $data, true);
					
					$result_data = array(
						'payment_type'			=>	3,
						'html'					=>	$html,
					);
					break;
			}
			
			echo json_encode($result_data);
		}
	}
	
	public function payment_fisc(){
		$Website = $this->Website;
		
		/*
			測試碼開始
		*/
		$payment_content = '{
			"authCode":"005460",
			"lastPan4":"7208",
			"status":"0",
			"merID":"54151074",
			"xid":"O-OBJECT-20170704184320.107-2147",
			"lidm":"'.$_POST['lidm'].'",
			"cardBrand":"VISA",
			"pan":"468236******7208",
			"errcode":"00",
			"currency":"",
			"errDesc":"",
			"authRespTime":"'.date('Ymdhis').'",
			"amtExp":"0",
			"authAmt":"8225"
		}';
		$_POST = objectToArray(json_decode($payment_content));
		/*
			print_r($_POST);exit;
			測試碼結束
		*/
		
		
		
		$payment_id 	= $_POST["lidm"];
		$status 		= $_POST['status'];
		$merID 			= $_POST['merID'];
		$authCode 		= $_POST["authCode"];
		$xid 			= $_POST["xid"];
		$authAmt 		= $_POST["authAmt"];
		$errcode 		= $_POST["errcode"];
		$errDesc 		= $_POST["errDesc"];
		
		if(!$status){
			$result_page = 'cart/success/';
			
			$this->payment_success($lidm);
			
			$data = array(
				'payment_status'			=>	1,
				'payment_result'			=>	json_encode($_POST),
				'payment_date'				=>	time(),
				'modify_date'				=>	time(),
			);
		}else{
			$result_page = 'cart/error/';
			
			$this->payment_fail($lidm);
			
			$data = array(
				'payment_status'			=>	2,
				'payment_result'			=>	json_encode($_POST),
				'payment_date'				=>	time(),
				'modify_date'				=>	time(),
			);
		}
		
		$this->db->set($data);
		$this->db->where('payment_id', $payment_id);
		$this->db->update($Website['table']['orders']);
		
		redirect($result_page, 'refresh');
	}
	
	public function payment_nccc(){
		$Website = $this->Website;
		$payment_id 	= $_POST['OrderID'];
		$response_code 	= $_POST['ResponseCode'];
		
		if(in_array($response_code, array('00', '08', '11'))){
			$result_page = 'cart/success/';
			
			$this->payment_success($payment_id);
			
			$data = array(
				'payment_status'			=>	1,
				'payment_result'			=>	json_encode($_POST),
				'payment_date'				=>	time(),
				'modify_date'				=>	time(),
			);
		}else{
			$result_page = 'cart/error/';
			
			$this->payment_fail($payment_id);
			
			$data = array(
				'payment_status'			=>	2,
				'payment_result'			=>	json_encode($_POST),
				'payment_date'				=>	time(),
				'modify_date'				=>	time(),
			);
		}
		
		$this->db->set($data);
		$this->db->where('payment_id', $payment_id);
		$this->db->update($Website['table']['orders']);
		
		redirect($result_page, 'refresh');
	}
	
	private function payment_success($payment_id, $payment=0){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT A.*, B.account, B.name member_name
						FROM %s A
						LEFT JOIN %s B ON A.member_id=B.id
						WHERE A.payment_id=%s",
						$Website['table']['orders'],
						$Website['table']['member'],
						sql_string($payment_id, "text")
		);
		$rows1 = $this->query->select($sql, 1);
		
		if(!$rows1['id']){
			return 0;
		}
		
		/*
		
		//寄信
		$sql = sprintf("SELECT A.*, B.name product_name, B.url_name, C.name spec_name, D.name media_name
						FROM %s A
						LEFT JOIN %s B ON A.product_id=B.id
						LEFT JOIN %s C ON A.spec_id=C.id
						LEFT JOIN %s D ON B.main_media_id=E.id
						WHERE A.order_id=%s",
						$Website['table']['inventory'],
						$Website['table']['product'],
						$Website['table']['spec'],
						$Website['table']['media'],
						sql_string($rows1['id'], "int")
		);//echo $sql;
		$rows2 = $this->query->select($sql);
		
		$r_address = str_repeat('*', strlen($rows1['r_address']));
		$r_mobile = substr($rows1['r_mobile'], 0, 6).str_repeat('*', strlen(substr($rows1['r_mobile'], 6)));
		
		if(!$payment){
			//atm
			$data = array(
				'type'				=>	'atm',
				'mailto'			=>	$rows1['account'],
				'rows1'				=>	array(
					'order_total_price'		=>	$rows1['total_price'],
					'order_product_price'	=>	$rows1['product_total_price'],
					'order_discount'		=>	$rows1['discount_price'],
					'freight'				=>	(int)$rows1['freight'],
					'member_name'			=>	$rows1['member_name'],
					'payment_order_id'		=>	$rows1['payment_order_id'],
					'order_product_table'	=>	$this->load->view('order/product_table', array('Website'=>$Website, 'rows0'=>$rows1, 'rows1'=>$rows2), true),
					'addressee_name'		=>	$rows1['r_name'],
					'addressee_mobile'		=>	$r_mobile,
					'addressee_address'		=>	$rows1['r_city'].$rows1['r_area'].$r_address,
					'payment'				=>	$Website['payment'][$rows1['payment']]['name'],
					'shipping_type'			=>	$Website['shipping_type'][$rows1['shipping_type']]['name'],
				),
			);
		}else{
			$data = array(
				'type'				=>	'order',
				'mailto'			=>	$rows1['account'],
				'rows1'				=>	array(
					'order_total_price'		=>	$rows1['total_price'],
					'order_product_price'	=>	$rows1['product_total_price'],
					'order_discount'		=>	$rows1['discount_price'],
					'freight'				=>	(int)$rows1['freight'],
					'member_name'			=>	$rows1['member_name'],
					'payment_order_id'		=>	$rows1['payment_order_id'],
					'order_product_table'	=>	$this->load->view('order/product_table', array('Website'=>$Website, 'rows0'=>$rows1, 'rows1'=>$rows2), true),
					'addressee_name'		=>	$rows1['r_name'],
					'addressee_mobile'		=>	$r_mobile,
					'addressee_address'		=>	$rows1['r_city'].$rows1['r_area'].$r_address,
					'payment'				=>	$Website['payment'][$rows1['payment']]['name'],
					'shipping_type'			=>	$Website['shipping_type'][$rows1['shipping_type']]['name'],
				),
			);
		}
		$this->mailer->to($data);
		*/
	}
	
	private function payment_fail($payment_id=null){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT A.*, B.account, B.name member_name
						FROM %s A
						LEFT JOIN %s B ON A.member_id=B.id
						WHERE A.payment_id=%s",
						$Website['table']['orders'],
						$Website['table']['member'],
					   	sql_string($payment_id, "text")
		);
		$rows1 = $this->query->select($sql, 1);
		
		if(!$rows1['id']){
			//return 0;
		}else{
		
			//加回庫存
			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.order_id=%s",
							$Website['table']['inventory'],
							sql_string($rows1['id'], "int")
			);
			$rows2 = $this->query->select($sql);

			for($i=0; $i<sizeof($rows2); $i++){
				$sql = sprintf('UPDATE %s SET
								quantity=quantity+%s
								WHERE product_id=%s and spec_id=%s and type=0
								',
								$Website['table']['inventory'],
								sql_string($rows2[$i]['quantity'], "int"),
								sql_string($rows2[$i]['product_id'], "text"),
								sql_string($rows2[$i]['spec_id'], "int")
								);
				$this->query->sql($sql);
			}
		}
		/*
		//寄信
		$data = array(
			'type'				=>	'payment_fail',
			'mailto'			=>	$rows1['account'],
			'rows1'				=>	array(
				'member_name'		=>	$rows1['member_name'],
				'payment_id'		=>	$rows1['payment_id'],
				'order_time'		=>	date('Y/m/d H:i:s', time()),
			),
		);
		$this->mailer->to($data);
		*/
	}
	
	public function success(){
		$Website = $this->Website;
		
		//成功時才清空購物車
		unset($_SESSION['cart']);
		$this->cart->destroy();
		
		$title = '訂單成立';
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		$data = array(
			'Website'			=>	$Website,
			'title'				=>	$title,
		);
		$this->theme->views('cart/success', $data);
	}
	
	public function error(){
		$Website = $this->Website;
		
		$title = '訂單發生錯誤';
		$data = array(
			'title'			=>	$title,
		);
		$this->theme->config($data);
		
		$data = array(
			'Website'			=>	$Website,
			'title'				=>	$title,
		);
		$this->theme->views('cart/error', $data);
	}
	
	
	
	
	
	public function test(){
		$Website = $this->Website;
		echo '<pre>';
		print_r($_SESSION['cart']);
		print_r($this->cart->contents());
	}
	private function unset_cart(){
		$Website = $this->Website;
		unset($_SESSION['cart']);
	}
	
}
?>