<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->preload->main();
    }
	
	public function content($id=0){
		$Website = $this->Website;
		$this->preload->current_location();
		
		if(!$id){
			show_404();
		}
		
		$sql = sprintf("SELECT P.* 
						FROM %s P
						WHERE P.id=%s and P.display=1 and 
							(P.display_start_time is null or P.display_start_time=0 or P.display_start_time<=%s) and 
							(P.display_end_time is null or P.display_end_time=0 or P.display_end_time>%s)", 
					   	$Website['table']['product'],
					  	sql_string($id, 'int'),
					   	time(),
					   	time()
					  );
		$rows1 = $this->query->select($sql, 1);
		
		if(!$rows1['id']){
			show_404();
		}
		
		$product = $rows1 = $this->product_query->price_calculate($rows1);
		
		$sql = sprintf("SELECT B.*
						FROM %s A
						LEFT JOIN %s B ON A.media_id=B.id
						WHERE A.item_id=%s and A.item_type=%s and B.display=1
						ORDER BY A.sort asc
						", 
					   	$Website['table']['media_relation'],
					   	$Website['table']['media'],
					  	sql_string($rows1['id'], 'int'),
					  	sql_string('product', 'text')
					  );
		$media = $this->query->select($sql);
		
		$sql = sprintf("SELECT B.*
						FROM %s A
						LEFT JOIN %s B ON A.tag_id=B.id
						WHERE A.product_id=%s
						ORDER BY A.tag_id asc
						", 
					   	$Website['table']['product_tag_relation'],
					   	$Website['table']['tag'],
					  	sql_string($rows1['id'], 'int')
					  );
		$tag = $this->query->select($sql);
		
		$sql = sprintf("SELECT A.*, B.name spec_name
						FROM %s A
						LEFT JOIN %s B ON A.spec_id=B.id
						WHERE A.product_id=%s and A.type=0 and A.display=1 and A.quantity>0
						ORDER BY B.sort asc
						", 
					   	$Website['table']['inventory'],
					   	$Website['table']['spec'],
					  	sql_string($rows1['id'], 'int')
					  );
		$spec = $this->query->select($sql);
		
		
		
		
		
		$data = array(
			'title'			=>	$rows1['name'],
			'description'	=>	$rows1['description'],
			'ogimage'		=>	array(
				'id'			=>	$rows1['id'],
				'type'			=>	'product_ogimage'
			),
		);
		$this->theme->config($data);
		
		$data = array(
			'Website'		=>	$Website,
			'page_title'	=>	$product['name'],
			'product'		=>	$product,
			'media'			=>	$media,
			'tag'			=>	$tag,
			'spec'			=>	$spec,
		);
		$this->theme->views('product/content', $data);
	}
	
	public function collection($url){
		$Website = $this->Website;
		$this->preload->current_location();
		
		if(!$url){
			show_404();
		}
		
		$sql = sprintf("SELECT A.* 
						FROM %s A
						WHERE A.url=%s and A.display=1 and 
							(A.start_time is null or A.start_time=0 or A.start_time<=%s) and 
							(A.end_time is null or A.end_time=0 or A.end_time>%s)
							", 
					   	$Website['table']['collection'],
					  	sql_string($url, 'text'),
					   	time(),
					   	time()
					  );
		$rows1 = $this->query->select($sql, 1);
		
		if(!$rows1['id']){
			show_404();
		}
		
		//pagination
		$sql = sprintf("SELECT count(P.id) total
						FROM %s A
						LEFT JOIN %s P ON A.product_id=P.id
						WHERE A.collection_id=%s and P.display=1 and 
							(P.display_start_time is null or P.display_start_time=0 or P.display_start_time<=%s) and 
							(P.display_end_time is null or P.display_end_time=0 or P.display_end_time>%s)
						",
						$Website['table']['product_collection_relation'],
						$Website['table']['product'],
					   	sql_string($rows1['id'], 'int'),
					   	time(),
					   	time()
						);
		$rows2 = $this->query->select($sql, 1);
		$total_rows = $rows2['total'];
		
		$Website['pagination']['base_url'] = base_url().'product/collection/';
		$Website['pagination']['total_rows'] = $total_rows;
		$Website['pagination']['per_page'] = 9;
		$Website['pagination']['num_links'] = 3;
		$Website['pagination']['uri_segment'] = 3;
		$this->pagination->initialize($Website['pagination']);
		$pagination = $this->pagination->create_links();
		$page_total = ceil($total_rows/$Website['pagination']['per_page']);
		$page = (int)$page ? (int)$page : 1;
		$skip = ($page-1) * $Website['pagination']['per_page'];
		$limit = $Website['pagination']['per_page'];
		
		$sql = sprintf("SELECT P.*, B.file_url
						FROM %s A
						LEFT JOIN %s P ON A.product_id=P.id
						LEFT JOIN %s B ON B.id = (SELECT C.media_id FROM %s C WHERE C.item_id=P.id and C.item_type=%s ORDER BY C.sort asc limit 1)
						WHERE A.collection_id=%s and P.display=1 and
							(P.display_start_time is null or P.display_start_time=0 or P.display_start_time<=%s) and 
							(P.display_end_time is null or P.display_end_time=0 or P.display_end_time>%s)
						ORDER BY A.sort asc
						LIMIT %s, %s
						",
						$Website['table']['product_collection_relation'],
						$Website['table']['product'],
					   
						$Website['table']['media'],
						$Website['table']['media_relation'],
					   	sql_string('product', 'text'),
					   
					   	sql_string($rows1['id'], 'int'),
					   	time(),
					   	time(),
					   	sql_string($skip, 'int'),
					   	sql_string($limit, 'int')
						);
		$product = $this->query->select($sql);
		
		for($i=0; $i<sizeof($product); $i++){
			$product[$i] = $this->product_query->price_calculate($product[$i]);
		}
		
		
		
		$data = array(
			'title'			=>	$rows1['name'],
			'description'	=>	$rows1['description'],
			'ogimage'		=>	array(
				'id'			=>	$rows1['id'],
				'type'			=>	'collection_ogimage'
			),
		);
		$this->theme->config($data);
		
		$data = array(
			'Website'		=>	$Website,
			'page_title'	=>	$rows1['name'],
			'product'		=>	$product
		);
		$this->theme->views('product/collection', $data);
	}
}
?>