<?
class Query extends CI_Model{
	function __construct(){
        // 呼叫模型(Model)的建構函數
        parent::__construct();
    }
	
	public function select($sql, $single=0, $debug=FALSE){
		$this->db->db_debug = $debug;
		$value1 = $this->db->query($sql);
		$rows1 = $value1->result_array();
		
		if($single){
			return $rows1[0];
		}else{
			return $rows1;
		}
	}
	
	public function sql($sql, $debug=FALSE){
		$this->db->db_debug = $debug; //disable debugging for queries
		$this->db->query($sql);
		return $this->db->insert_id();
	}
	
	function query_select($sql, $single=0, $debug=FALSE){
		$this->db->db_debug = $debug; //disable debugging for queries
		$value1 = $this->db->query($sql);
		$rows1 = $value1->result_array();
		$total_rows1 = $value1->num_rows();
		
		if($single){
			return $rows1[0];
		}else{
			return $rows1;
		}
	}
	
	function query_select_single($sql, $debug=FALSE){
		$this->db->db_debug = $debug; //disable debugging for queries
		$value1 = $this->db->query($sql);
		$rows1 = $value1->result_array();
		$total_rows1 = $value1->num_rows();
		return $rows1[0];
	}
	
	function query($sql, $debug=FALSE){
		$this->db->db_debug = $debug; //disable debugging for queries
		$this->db->query($sql);
		return $this->db->insert_id();
	}
}
?>