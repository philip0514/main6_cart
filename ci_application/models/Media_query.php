<?php
class Media_query extends CI_Model{
	function __construct(){
		// 呼叫模型(Model)的建構函數
		parent::__construct();
    }
	
	//mutiple upload
	public function do_upload($start=0, $limit=0) {
		$Website 					= $this->Website;
		$upload_folder 				= $Website['media']['upload_folder'];
		$thumb_folder 				= $Website['media']['thumb_folder'];
		$upload_path_url 			= base_url() . $upload_folder;
		
		$config['upload_path'] 		= FCPATH . $upload_folder;
		$config['allowed_types'] 	= $Website['media']['allowed_types'];
		$config['max_size'] 			= $Website['media']['max_size'];
		$config['encrypt_name'] 		= TRUE;
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload()) {
			
			$data = $this->upload->data();
			if(!$data['file_name']){
				//非上傳，取得列表資料
				//$this->get_file_list($type='json', $is_image=false, $start, $limit);
			}
			
		} else {
			
			$data = $this->upload->data();
			if($data['is_image']){
				//縮圖與切圖
				$this->crop($data);
			}
			$data['file_path'] = $Website['media']['upload_folder'];
			$data['file_url'] = base_url().$data['file_path'].$data['file_name'];
			$data['title'] = $data['orig_name'];
			
			//set the data for the json array
			$info = new StdClass;
			$info->name = $data['file_name'];
			$info->orig_name = $data['orig_name'];
			$info->title = $data['orig_name'];
			$info->size = $data['file_size'] * 1024;
			$info->type = $data['file_type'];
			$info->url = $upload_path_url . $data['file_name'];
			//$info->url = $data['file_url'];
			// I set this to original file since I did not create thumbs.  change to thumbnail directory if you do = $upload_path_url .'/thumbs' .$data['file_name']
			$info->thumbnailUrl = $upload_path_url . $thumb_folder . $data['file_name'];
			$info->is_image = $data['is_image'];
		
			
			//寫入資料庫
			
			$thumb_id = $this->insert_db($data);
			$info->id = $thumb_id;
			
			$files[] = $info;
			
			
			//this is why we put this in the constants to pass only json data
			if (IS_AJAX) {
				echo json_encode(array("files" => $files));
				//this has to be the only data returned or you will get an error.
				//if you don't give this a json array it will give you a Empty file upload result error
				//it you set this without the if(IS_AJAX)...else... you get ERROR:TRUE (my experience anyway)
				// so that this will still work if javascript is not enabled
			} else {
				$file_data['upload_data'] = $this->upload->data();
				$this->load->view('upload/upload_success', $file_data);
			}
		}
	}
	
	public function file_list($type='json', $is_image=false, $start=0, $limit=10, $search=''){
		$Website = $this->Website;
		
		if($search){
			$where[] = sprintf(' A.title like %s or A.orig_name like %s', 
								sql_string('%'.$search.'%', 'text'), 
								sql_string('%'.$search.'%', 'text')
			);
		}
		
		if($is_image){
			$where[] = sprintf(' A.is_image=1', 
								sql_string('%'.$search.'%', 'text')
			);
		}
			
		if(sizeof($where)){
			$where_sql = 'WHERE '.implode(' and ', $where);
		}
		
		$sql = sprintf("SELECT A.* 
						FROM %s A 
						%s
						ORDER BY A.insert_time desc 
						limit %s, %s",
						$Website['table']['media'],
						$where_sql,
						(int)$start,
						(int)$limit
		);	
		$rows1 = $this->query->select($sql);
		
		for($i=0; $i<sizeof($rows1); $i++){
			//$local_path = $this->thumb_combination($rows1[$i]);
			$rows1[$i]['thumbnailUrl'] =  $rows1[$i]['url'];
			$rows1[$i]['url'] = $rows1[$i]['file_url'];
		}
		if($type=='json'){
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('files' => $rows1)));
		}elseif($type=='array'){
			return $rows1;
		}
	}
	
	public function my_file_list($type='json', $is_image=false, $start=0, $limit=10, $search=''){
		$Website = $this->Website;
		
		if($search){
			$where[] = sprintf(' A.title like %s or A.content like %s', 
								sql_string('%'.$search.'%', 'text'), 
								sql_string('%'.$search.'%', 'text')
			);
		}
		
		if($is_image){
			$where[] = sprintf(' A.is_image=1', 
								sql_string('%'.$search.'%', 'text')
			);
		}
		
		$where[] = sprintf(' A.user_id=%s',
						  	sql_string($_SESSION['user_info']['id'], 'int')
						  );
		
		if(sizeof($where)){
			$where_sql = 'WHERE '.implode(' and ', $where);
		}
		
		$sql = sprintf("SELECT A.* 
						FROM %s A 
						%s
						ORDER BY A.insert_time desc 
						limit %s, %s",
						$Website['table']['media'],
						$where_sql,
						(int)$start,
						(int)$limit
		);	
		$rows1 = $this->query->select($sql);
		
		for($i=0; $i<sizeof($rows1); $i++){
			//$local_path = $this->thumb_combination($rows1[$i]);
			$rows1[$i]['thumbnailUrl'] =  $rows1[$i]['url'];
			$rows1[$i]['url'] = $rows1[$i]['file_url'];
		}
		if($type=='json'){
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('files' => $rows1)));
		}elseif($type=='array'){
			return $rows1;
		}
	}
	
	/*
	private function thumb_combination($rows1, $size_type='square'){
		$Website = $this->Website;
		
		switch($size_type){
			default:
			case 'square':
				$thumb_folder = $Website['media']['thumb_folder'];
				break;
			case 'facebook':
				$thumb_folder = $Website['media']['thumb_size']['fb560']['folder'];
				$image = $rows1['file_path'].$thumb_folder.$rows1['name'];
				if(!is_file($image)){
					$thumb_folder = $Website['media']['thumb_folder'];
				}
				break;
		}
		
		$image = $rows1['file_path'].$thumb_folder.$rows1['name'];
		
		return $image;
	}
	*/
	
	public function insert_db($data, $query=true){
		$Website = $this->Website;
		
		//寫入資料庫
		$sql = sprintf("INSERT ignore INTO media
						(
							name, ext, file_type, file_path, file_url, 
							file_size, image_width, image_height, image_size_str, image_type, 
							is_image, orig_name, display, title, content, 
							excerpt, post_name, insert_time, modify_time, user_id
						)
						VALUES
						(
							%s, %s, %s, %s, %s,
							%s, %s, %s, %s, %s, 
							%s, %s, %s, %s, %s,
							%s, %s, %s, %s, %s
						);",
						
						sql_string($data['file_name'], 'text'),
						sql_string($data['file_ext'], 'text'),
						sql_string($data['file_type'], 'text'),
						sql_string($data['file_path'], 'text'),
						sql_string($data['file_url'], 'text'),
						
						sql_string($data['file_size']*1024, 'text'),
						sql_string($data['image_width'], 'text'),
						sql_string($data['image_height'], 'text'),
						sql_string($data['image_size_str'], 'text'),
						sql_string($data['image_type'], 'text'),
						
						sql_string($data['is_image'], 'text'),
						sql_string($data['orig_name'], 'text'),
						1,
						sql_string($data['title'], 'text'),
						sql_string($data['content'], 'text'),
						
						sql_string($data['excerpt'], 'text'),
						sql_string($data['post_name'], 'text'),
						sql_string(time(), 'text'),
						sql_string(time(), 'text'),
						sql_string($_SESSION['user_info']['id'], 'text')
						);
						//echo $sql;exit;
		if($query){
			$this->db->query($sql);
			$id = $this->db->insert_id();
		}else{
			return $sql;
		}
		
		return $id;
	}
	
	public function single_image($rows1, $size_type='square'){
		$Website = $this->Website;
		
		//$rows1['url'] = $Website['media']['upload_folder'].$Website['media']['thumb_folder'].$rows1['name'];
		//$rows1['url'] = $this->thumb_combination($rows1, $size_type);
		$data = array(
			'Website'	=>	$Website,
			'rows1'		=>	$rows1,
			'size_type'	=>	$size_type,
		);
		$html = $this->load->view('media/single_image', $data, true);	
		return $html;
	}
	
	public function media_select($id, $item_type){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT A.sort, B.*
						FROM %s A
						LEFT JOIN %s B ON A.media_id=B.id
						WHERE A.item_type=%s and A.item_id=%s
						ORDER BY A.sort asc
						",
						$Website['table']['media_relation'],
					   	$Website['table']['media'],
						sql_string($item_type, 'text'),
						sql_string($id, 'int')
						);//echo $sql;exit;
		$rows1 = $this->query->select($sql);
		
		for($i=0; $i<sizeof($rows1); $i++){
			$input_array[] = $rows1[$i]['id'];
			$area_html[] = $this->single_image($rows1[$i]);
		}
		$rows2['input'] = implode(',', $input_array);
		$rows2['area'] = implode('', $area_html);
		
		return $rows2;
	}
	
	public function image_url($id, $item_type, $is_single=0){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT B.file_url
						FROM %s A
						LEFT JOIN %s B ON A.media_id=B.id
						WHERE A.item_type=%s and A.item_id=%s
						ORDER BY A.sort asc
						",
						$Website['table']['media_relation'],
					   	$Website['table']['media'],
						sql_string($item_type, 'text'),
						sql_string($id, 'int')
						);//echo $sql;exit;
		if($is_single){
			$rows1 = $this->query->select($sql, 1);
			
			return $rows1['file_url'];
			
		}else{
			$rows1 = $this->query->select($sql);
			for($i=0; $i<sizeof($rows1); $i++){
				$url[] = $rows1[$i]['file_url'];
			}
			
			return $url;
		}
	}
	
	public function media_relation($item_id, $media_id, $item_type){
		$Website = $this->Website;
		//print_r($media_id);echo $item_type;
		$media_id = explode(',', $media_id);
		$sql = sprintf("DELETE FROM %s 
						WHERE item_id=%s and item_type=%s",
						$Website['table']['media_relation'],
						sql_string($item_id, "int"),
						sql_string($item_type, "text")
						);
		$this->query->sql($sql);
		for($i=0; $i<sizeof($media_id); $i++){
			if(!$media_id[$i]){
				continue;
			}
			
			$sql = sprintf("INSERT IGNORE INTO %s 
							(media_id, item_id, item_type, sort) 
							VALUES 
							(%s, %s, %s, %s)",
							$Website['table']['media_relation'],
							sql_string($media_id[$i], "int"),
							sql_string($item_id, "int"),
							sql_string($item_type, "text"),
							$i
							);
			$this->query->sql($sql);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//以下不一定會用到
	
	public function select($data){
		$Website = $this->Website;
		
		if(!$data['id']){
			$sql = sprintf("SELECT A.*
							FROM %s A
							",
							$Website['tables'][0]
							);
			$rows1 = $this->query->select($sql);
		}else{
			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.id=%s
							",
							$Website['tables'][0],
							(int)$data['id']
							);
			$rows1 = $this->query->select($sql, 1);
		}
		
		return $rows1;
	}
	
	public function table_total(){
		$Website = $this->Website;
		
		$total = $this->db->count_all_results($Website['tables'][0]);
		return $total;
	}
	
	public function display_total($q, $g){
		$Website = $this->Website;
		
		if(!$g['tag_search']){
			$sql = sprintf("SELECT count(A.id) total
							FROM %s A
							%s
							",
							$Website['tables'][0],
							$q['sWhere']
							);
		}else{
			$sql = sprintf("SELECT count(A.id) total
							FROM %s A
							LEFT JOIN %s B ON B.media_id=A.id
							LEFT JOIN %s C ON C.id=B.tag_id
							%s
							group by A.id
							",
							$Website['tables'][0],
							$Website['table']['media_tag'],
							$Website['table']['tag'],
							$q['sWhere']
							);//echo $sql;
		}
		
		$rows1 = $this->query->select($sql, 1);
		
		return $rows1['total'];
	}
	
	public function datatable_select($q, $g){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT A.* 
						FROM %s A
						%s
						%s
						%s
						",
						$Website['tables'][0],
						$q['sWhere'],
						$q['sOrder'],
						$q['sLimit']
						);//echo $sql;
		
		$sql1 = sprintf("SELECT A.*
						FROM %s A
						LEFT JOIN %s B ON B.media_id=A.id
						LEFT JOIN %s C ON C.id=B.tag_id
						%s
						group by A.id
						%s
						%s
						",
						$Website['tables'][0],
						$Website['table']['media_tag'],
						$Website['table']['tag'],
						$q['sWhere'],
						$q['sOrder'],
						$q['sLimit']
						);//echo $sql;
		$rows1 = $this->query->select($sql);
		
		return $rows1;
	}
	
	public function display_update($id, $show){
		$Website = $this->Website;
		
		for($i=0;$i<sizeof($id);$i++){
			if($id[$i]){
				if(in_array($id[$i],$show))	$display=1;
				else	$display=0;
				
				$data = array(
					'display'	=>	$display,
				);
				$this->db->where('id', $id[$i]);
				$this->db->update($Website['tables'][0], $data);
			}
		}
		return 0;
	}
	
	public function delete_update($id, $delete){
		$Website = $this->Website;
		
		if(sizeof($delete)){
			for($i=0;$i<sizeof($id);$i++){
				if($id[$i]){
					if(in_array($id[$i],$delete)){
						
						$sql = sprintf("SELECT A.* 
										FROM %s A
										WHERE id=%s
										limit 1
										",
										$Website['tables'][0],
										(int)$id[$i]
										);//echo $sql;
						$rows1 = $this->query->select($sql, 1);
						if($rows1['name']){
							foreach ($Website['media']['thumb_size'] as $thumb_size) {
								//echo "Value: $value<br />\n";
								$file_folder = $Website['media']['upload_folder'].$thumb_size['folder'];
								$file_path = $file_folder.$rows1['name'];
								//echo $file_path.'<br>';
								unlink($file_path);
							}
							
							$file_folder = $Website['media']['upload_folder'];
							$file_path = $file_folder.$rows1['name'];
							//echo $file_path.'<br>';
							unlink($file_path);
						}
						
						$this->db->where('id', $id[$i]);
						$this->db->delete($Website['tables'][0]);
					}
				}
			}
		}
		//exit;
		return 0;
	}
	
	public function sort_update(){
		$Website = $this->Website;
		
		$epp = $_POST['epp'];
		$page_no = $_POST['page_no']+1;
		
		$limit = ((int)$page_no-1)*$epp;
		$sql = 'SET @i:=0';
		$this->db->query($sql);
		
		$sql = sprintf("UPDATE %s 
						SET sort=(@i:=(@i+1)) 
						ORDER BY sort asc, id asc 
						LIMIT %s",
						$Website['tables'][0],
						$limit
						);
		$this->db->query($sql);
		
		$order_split = split(",",$_POST['orderby']);
		$order_size = sizeof($order_split);
		$limit_start = $_POST['limit_start'];
		
		for($i=0;$i<$order_size;$i++){
			$id = $order_split[$i];
			if($id!='' && $id!=0){
				$sql = sprintf("UPDATE %s 
								SET sort=%s 
								WHERE id=%s",
								$Website['tables'][0],
								sql_string(($limit_start+$i), "int"),
								sql_string($id, "int"));//echo $sql;
				$this->db->query($sql);
			}
		}
		//exit;
	}
	
	public function insert_($settings, $rows1){
		$Website = $this->Website;
		
		if($settings['btn']['sort']){
			$sql = sprintf("SELECT sort
							FROM %s
							WHERE type=%s
							ORDER BY sort desc
							LIMIT 1",
							$Website['tables'][0],
							sql_string($rows1['type'], 'int')
							);
			$rows2 = $this->query->select($sql, 1);
			
			$rows1['sort'] = $rows2['sort']+1;
		}
		
		$this->db->set($rows1);
		$this->db->insert($Website['tables'][0]);
		$id = $this->db->insert_id();
		
		return $id;
	}
	
	public function update_($rows1, $id){
		$Website = $this->Website;
		
		$this->db->set($rows1);
		$this->db->where('id', $id);
		$this->db->update($Website['tables'][0]);
	}
	
	public function edit_select($data){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.item_id=%s and A.type=%s
						",
						$Website['table']['item_image'],
						sql_string($data['id'], 'int'),
						sql_string($data['type'], 'text')
						);//echo $sql;exit;
		$rows1 = $this->query->select($sql);
		
		if(!$data['id'] || !sizeof($rows1)){
			$rows1[0] = array(
				'id'	=>	0,
				'name'	=>	'',
				'type'	=>	'',
				'path'	=>	'',
			);
		}
		
		for($i=0; $i<sizeof($rows1); $i++){
			$rows1[$i]['from'] = $from;
			$data = array(
				'rows1'		=>	$rows1[$i],
				'Website'	=>	$Website,
			);
			$html .= $this->load->view('main_template/image_edit', $data, true);	
		}
		
		return $html;
	}
	
	public function insert($data){
		$Website = $this->Website;
		
		$sql = sprintf("INSERT INTO %s 
						(name, item_id, type, path) 
						VALUES 
						(%s, %s, %s, %s)",
						$Website['table']['item_image'],
						sql_string($data['name'], "text"),
						sql_string($data['item_id'], "int"),
						sql_string($data['type'], "text"),
						sql_string($data['path'], "text")
						);//echo $sql.'<br>';//exit;
		$this->query->sql($sql);
	}
	
	public function update($data){
		$Website = $this->Website;
		
		$sql = sprintf("UPDATE %s
						name=%s
						WHERE id=%s",
						$Website['table']['item_image'],
						sql_string($data['name'], "text"),
						sql_string($data['id'], "int")
						);//echo $sql.'<br>';//exit;
		$this->query->sql($sql);
		
	}
	
	public function delete($data){
		$Website = $this->Website;
		
		$sql = sprintf("DELETE FROM %s
						WHERE id=%s",
						$Website['table']['item_image'],
						sql_string($data['id'], "int")
						);//echo $sql.'<br>';//exit;
		$this->query->sql($sql);
		if($data['name']){
			$file = $data['path'].$data['name'];
			unlink($file);
		}
	}
	
	
	
	//public function crop($config, $img_path, $img_thumb){
	public function crop($data){
		$Website = $this->Website;
		
		$this->load->library('image_lib');
		
		$config = array();
		$config['image_library'] = 'gd2';
		$config['source_image'] = $data['full_path'];
		$config['create_thumb'] = TRUE;
		$config['thumb_marker'] = '';
		$config['maintain_ratio'] = FALSE;
		
		
		
		
		foreach ($Website['media']['thumb_size'] as $thumb_size) {
			
			if($thumb_size['skip']){
				//不同步切圖
				continue;
			}
			
			//echo "Value: $value<br />\n";
			$config['new_image'] = $data['file_path'] . $thumb_size['folder'];
			
			$_width = $data['image_width'];
			$_height = $data['image_height'];
			
			$img_type = '';
			$thumb_width_size = $thumb_size['width'];
			$thumb_height_size = $thumb_size['height'];
			
			if(!$thumb_height_size){
				$thumb_height_size = intval(($_height/$_width)*$thumb_width_size);
			}
			
			if($_width > $_height){
				// wide image
				$config['width'] = intval(($_width / $_height) * $thumb_height_size);
				if($config['width'] % 2 != 0){
					$config['width']++;
				}
				$config['height'] = $thumb_height_size;
				$img_type = 'wide';
			}else if($_width < $_height){
				// landscape image
				$config['width'] = $thumb_width_size;
				$config['height'] = intval(($_height / $_width) * $thumb_width_size);
				if($config['height'] % 2 != 0){
					$config['height']++;
				}
				$img_type = 'landscape';
			}else{
				// square image
				$config['width'] = $thumb_width_size;
				$config['height'] = $thumb_height_size;
				$img_type = 'square';
			}
			//echo 5.5 . '<br>';
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			//echo 6 .'<br>';
			
			if(!$thumb_size['isometric']){
				//縮圖切割
				// reconfigure the image lib for cropping
				$config_thumb = array(
					'image_library' 		=> 'gd2',
					'source_image' 		=> $data['file_path'] . $thumb_size['folder'] . $data['file_name'],
					'create_thumb' 		=> FALSE,
					'maintain_ratio' 	=> FALSE,
					'width' 				=> $thumb_width_size,
					'height'				=> $thumb_height_size
				);
		
				if($img_type == 'wide'){
					$config_thumb['x_axis'] = ($config['width'] - $thumb_width_size) / 2 ;
					$config_thumb['y_axis'] = 0;
				}else if($img_type == 'landscape'){
					$config_thumb['x_axis'] = 0;
					$config_thumb['y_axis'] = ($config['height'] - $thumb_height_size) / 2;
				}else{
					$config_thumb['x_axis'] = 0;
					$config_thumb['y_axis'] = 0;
				}
				
				$this->image_lib->initialize($config_thumb);
				$this->image_lib->crop();
			}
			//echo 7 .'<br>';
		}
		
		//echo 8 . '<hr><br>';
		
		//exit;
		/*
		$config['new_image'] = $data['file_path'] . $Website['media']['thumb_folder'];
		
		$_width = $data['image_width'];
		$_height = $data['image_height'];

		$img_type = '';
		$thumb_width_size = $Website['media']['thumb_width'];
		$thumb_height_size = $Website['media']['thumb_height'];

		if($_width > $_height){
			// wide image
			$config['width'] = intval(($_width / $_height) * $thumb_width_size);
			if($config['width'] % 2 != 0){
				$config['width']++;
			}
			$config['height'] = $thumb_height_size;
			$img_type = 'wide';
		}else if($_width < $_height){
			// landscape image
			$config['width'] = $thumb_width_size;
			$config['height'] = intval(($_height / $_width) * $thumb_height_size);
			if($config['height'] % 2 != 0){
				$config['height']++;
			}
			$img_type = 'landscape';
		}else{
			// square image
			$config['width'] = $thumb_width_size;
			$config['height'] = $thumb_height_size;
			$img_type = 'square';
		}

		$this->image_lib->initialize($config);
		$this->image_lib->resize();


		//縮圖切割
		// reconfigure the image lib for cropping
		$config_thumb = array(
			'image_library' 	=> 'gd2',
			'source_image' 	=> $data['file_path'] . $Website['media']['thumb_folder'] . $data['file_name'],
			'create_thumb' 	=> FALSE,
			'maintain_ratio' 	=> FALSE,
			'width' 			=> $thumb_width_size,
			'height'			=> $thumb_height_size
		);

		if($img_type == 'wide'){
			$config_thumb['x_axis'] = ($config['width'] - $thumb_width_size) / 2 ;
			$config_thumb['y_axis'] = 0;
		}else if($img_type == 'landscape'){
			$config_thumb['x_axis'] = 0;
			$config_thumb['y_axis'] = ($config['height'] - $thumb_height_size) / 2;
		}else{
			$config_thumb['x_axis'] = 0;
			$config_thumb['y_axis'] = 0;
		}
		
		$this->image_lib->initialize($config_thumb);
		$this->image_lib->crop();
		
		*/
		
	}
	
	
	public function update_crop_data($data){
		//儲存切割後的資訊
		$Website = $this->Website;
		
		$sql = sprintf("UPDATE %s SET
						crop_data=%s, title=%s, content=%s, modify_time=%s
						WHERE id=%s",
						$Website['table']['media'],
						sql_string($data['crop_data'], "text"),
						sql_string($data['title'], "text"),
						sql_string($data['content'], "text"),
						sql_string(time(), 'text'),
						sql_string($data['id'], "int")
						);//echo $sql.'<br>';exit;
		$this->query->sql($sql);
		
	}
	
	public function update_facebook_crop_data($data){
		//儲存切割後的資訊
		$Website = $this->Website;
		
		$sql = sprintf("UPDATE %s SET
						facebook_crop_data=%s, title=%s, content=%s, modify_time=%s
						WHERE id=%s",
						$Website['table']['media'],
						sql_string($data['crop_data'], "text"),
						sql_string($data['title'], "text"),
						sql_string($data['content'], "text"),
						sql_string(time(), 'text'),
						sql_string($data['id'], "int")
						);//echo $sql.'<br>';//exit;
		$this->query->sql($sql);
		
	}
	
	
	public function new_filename($path, $filename){
		$Website = $this->Website;
		
		//echo $filename;
		$file_ext = '.'.end(split('\.', $filename));//echo $file_ext;exit;
		mt_srand();
		$filename = md5(uniqid(mt_rand())).$file_ext;
		//exit;
		if (!file_exists($path.$filename)){
			return $filename;
		}

		$filename = str_replace($file_ext, '', $filename);

		$new_filename = '';
		for($i = 1; $i < 100; $i++){
			if ( ! file_exists($path.$filename.$i.$file_ext))
			{
				$new_filename = $filename.$i.$file_ext;
				break;
			}
		}

		return $new_filename;
		
	}
	
	public function wp_image_copy($file){
		$Website = $this->Website;
		
		if(!file_exists($file)){
			return false;
		}
		$orig_file_path = $file;
		
		$file_path = $Website['media']['upload_folder'];
		$orig_file_name = end(split('/', $file));
		$file_name = $this->new_filename($file_path, $orig_file_name);
		copy($file, $file_path.$file_name);
			//$file_name = '88d732c6bca6c78bf9957c172540d1a0.jpg';
		//echo $file_path.$file_name;
		$property = $this->image_lib->get_image_properties($file_path.$file_name, true);
		$file_size = round(filesize($file_path.$file_name)/1024, 2);
		
		$img_mimes = array(
							'image/gif',
							'image/jpeg',
							'image/png',
							'image/x-png',
							'image/jpg', 'image/jpe', 'image/jpeg', 'image/pjpeg',
						);
		
		$is_image = (in_array($property['mime_type'], $img_mimes, TRUE)) ? TRUE : FALSE;
		
		$file_ext = '.'.end(split('\.', $file_name));
		$raw_name = str_replace($file_ext, '', $file_name);
		
		$data = array(
			'file_name'			=>	$file_name,
			'file_type'			=>	$property['mime_type'],
			'file_path'			=>	$file_path,
			'file_url'			=>	base_url().$file_path.$file_name,
			'full_path'			=>	$file_path.$file_name,
			
			'raw_name'			=>	$raw_name,
			'orig_name'			=>	$orig_file_name,
			'file_ext'			=>	$file_ext,
			'file_size'			=>	$file_size,
			'is_image'			=>	$is_image,
			
			'image_width'		=>	$property['width'],
			'image_height'		=>	$property['height'],
			'image_type'			=>	end(split('/',$property['mime_type'])),
			'image_size_str'	=>	$property['size_str'],
			'wp_file_path'		=>	$orig_file_path,
		);
		
		return $data;
	}
	
	
	public function media_insert_db($data){
		$Website = $this->Website;
		
		//寫入資料庫
		$sql = sprintf("INSERT ignore INTO media
						(
							id,
							name, ext, file_type, file_path, file_url, 
							file_size, image_width, image_height, image_size_str, image_type, 
							is_image, upload_time, orig_name, display, wp_file_path,
							title, content, excerpt, post_name, wp_id, 
							crop_later, insert_time, modify_time, user_id
						)
						VALUES
						(
							%s,
							%s, %s, %s, %s, %s,
							%s, %s, %s, %s, %s, 
							%s, %s, %s, %s, %s,
							%s, %s, %s, %s, %s, 
							%s, %s, %s, %s
						);",
						sql_string($data['wp_id'], 'text'),
						
						sql_string($data['file_name'], 'text'),
						sql_string($data['file_ext'], 'text'),
						sql_string($data['file_type'], 'text'),
						sql_string($data['file_path'], 'text'),
						sql_string($data['file_url'], 'text'),
						
						sql_string($data['file_size']*1024, 'text'),
						sql_string($data['image_width'], 'text'),
						sql_string($data['image_height'], 'text'),
						sql_string($data['image_size_str'], 'text'),
						sql_string($data['image_type'], 'text'),
						
						sql_string($data['is_image'], 'text'),
						sql_string(time(), 'text'),
						sql_string($data['orig_name'], 'text'),
						1,
						sql_string($data['wp_file_path'], 'text'),
						
						sql_string($data['title'], 'text'),
						sql_string($data['content'], 'text'),
						sql_string($data['excerpt'], 'text'),
						sql_string($data['post_name'], 'text'),
						sql_string($data['wp_id'], 'text'),
						sql_string($data['crop_later'], 'text'),
						sql_string(strtotime($data['post_date']), 'text'),
						sql_string(strtotime($data['post_modified']), 'text'),
						sql_string($_SESSION['user_info']['id'], 'text')
						);
		
			return $sql;
	}
	
	
	public function tag_select($data){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT B.*
						FROM %s A, %s B
						WHERE A.media_id=%s and A.tag_id=B.id
						",
						$Website['table']['media_tag'],
						$Website['table']['tag'],
						(int)$data['id']
						);//echo $sql;
		$rows1 = $this->query->select($sql);
		
		return $rows1;
	}
	
}
?>