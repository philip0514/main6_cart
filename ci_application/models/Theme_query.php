<?
class Theme_query extends CI_Model{
	function __construct(){
        // 呼叫模型(Model)的建構函數
        parent::__construct();
    }
	
	public function select($output='array'){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.display=1
						ORDER BY A.sort asc
						",
						$Website['table']['category']
						);
		$rows1 = $this->query->select($sql);
		
		switch($output){
			case 'tree':
				$rows0 = $this->build_tree($rows1, 'fid', 'id');
				break;
			default:
				$rows0 = $rows1;
				break;
		}
		return $rows0;
	}
	
	public function build_tree($flat, $pidKey, $idKey = null)
{
		$grouped = array();
		foreach ($flat as $sub){
			$grouped[$sub[$pidKey]][] = $sub;
		}

		$fnBuilder = function($siblings) use (&$fnBuilder, $grouped, $idKey) {
			foreach ($siblings as $k => $sibling) {
				$id = $sibling[$idKey];
				if(isset($grouped[$id])) {
					$sibling['children'] = $fnBuilder($grouped[$id]);
				}
				$siblings[$k] = $sibling;
			}

			return $siblings;
		};

		$tree = $fnBuilder($grouped[0]);

		return $tree;
	}
	
	public function meta($rows0=array()){
		$Website = $this->Website;
		
		/*
		$sql = sprintf("SELECT A.*, B.file_url
						FROM %s A
						LEFT JOIN %s B ON A.media_id=B.id
						WHERE A.item_id=%s and A.item_type=%s
						ORDER BY A.sort asc
						",
						$Website['table']['media_relation'],
						$Website['table']['media'],
						sql_string($rows0['id'], 'int'),
						sql_string($rows0['type'], 'text')
						);
		$rows1 = $this->query->select($sql);
		//print_r($rows1);exit;
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.id=1
						",
						$Website['table']['setting']
						);
		$rows2 = $this->query->select($sql, 1);
		
		$meta = array(
			'site_name'			=>	$rows2['title'],
			'description'		=>	$rows2['description'],
			'keywords'			=>	$rows2['keyword'],
			'facebook_appid'	=>	$rows2['facebook_appid'],
			'ogimage'			=>	$rows1,
			'ogurl'				=>	base_url().substr($_SERVER['REQUEST_URI'], 1),
		);
		*/
		$sql = sprintf("SELECT A.json
						FROM %s A
						WHERE A.id=1
						",
						$Website['table']['setting']
						);
		$rows1 = $this->query->select($sql, 1);
		
		$rows2 = objectToArray(json_decode($rows1['json']));
		//echo '<pre>';print_r($rows2);exit;
		$meta = array(
			'site_name'			=>	htmlspecialchars_decode($rows2['title']),
			'description'		=>	htmlspecialchars_decode($rows2['description']),
			'keywords'			=>	htmlspecialchars_decode($rows2['keyword']),
			'facebook_appid'	=>	htmlspecialchars_decode($rows2['facebook_appid']),
			'ogimage'			=>	$rows2['ogimage'],
			'logo'				=>	$rows2['logo'],
			'logo2x'			=>	$rows2['logo2x'],
			'icon16'			=>	$rows2['icon16'],
			'icon57'			=>	$rows2['icon57'],
			'icon72'			=>	$rows2['icon72'],
			'icon114'			=>	$rows2['icon114'],
			'icon144'			=>	$rows2['icon144'],
			'ogurl'				=>	base_url().substr($_SERVER['REQUEST_URI'], 1),
		);
		
		
		
		
		return $meta;
	}
	
	public function get_active(){
		$Website = $this->Website;
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.active=1
						",
						$Website['table']['theme']
						);
		$rows1 = $this->query->select($sql, 1);
		
		if(!$rows1['id']){
			$sql = sprintf("SELECT A.*
							FROM %s A
							ORDER BY A.id asc
							LIMIT 1
							",
							$Website['table']['theme']
							);
			$rows1 = $this->query->select($sql, 1);
		}
		
		return $rows1['folder'];
	}
	
}
?>