<?
class Coupon_query extends CI_Model{
	function __construct(){
        // 呼叫模型(Model)的建構函數
        parent::__construct();
    }
	
	public function select($data){
		$Website = $this->Website;
		
		if(!$data['id']){
			$sql = sprintf("SELECT A.*
							FROM %s A
							",
							$Website['tables'][0]
							);
			$rows1 = $this->query->select($sql);
		}else{
			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.id=%s
							",
							$Website['tables'][0],
							(int)$data['id']
							);//echo $sql;exit;
			$rows1 = $this->query->select($sql, 1);
		}
		
		return $rows1;
	}
	
	public function table_total(){
		$Website = $this->Website;
		
		$total = $this->db->count_all_results($Website['tables'][0]);
		return $total;
	}
	
	public function display_total($q){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT count(A.id) total
						FROM %s A
						%s
						",
						$Website['tables'][0],
						$q['sWhere']
						);
		$rows1 = $this->query->select($sql, 1);
		
		$total = $rows1['total'];
		//echo $total;
		return $total;
	}
	
	public function datatable_select($q){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT A.* 
						FROM %s A
						%s
						%s
						%s
						",
						$Website['tables'][0],
						$q['sWhere'],
						$q['sOrder'],
						$q['sLimit']
						);
		$rows1 = $this->query->select($sql);
		
		return $rows1;
	}
	
	public function display_update($id, $show){
		$Website = $this->Website;
		
		for($i=0;$i<sizeof($id);$i++){
			if($id[$i]){
				if(in_array($id[$i],$show))	$display=1;
				else	$display=0;
				
				$data = array(
					'display'	=>	$display,
				);
				$this->db->where('id', $id[$i]);
				$this->db->update($Website['tables'][0], $data);
			}
		}
		return 0;
	}
	
	public function delete_update($id, $delete){
		$Website = $this->Website;
		
		if(sizeof($delete)){
			for($i=0;$i<sizeof($id);$i++){
				if($id[$i]){
					if(in_array($id[$i],$delete)){
						
						$this->db->where('id', $id[$i]);
						$this->db->delete($Website['tables'][0]);
					}
				}
			}
		}
		return 0;
	}
	
	public function sort_update(){
		$Website = $this->Website;
		
		$epp = $_POST['epp'];
		$page_no = $_POST['page_no']+1;
		
		$limit = ((int)$page_no-1)*$epp;
		$sql = 'SET @i:=0';
		$this->db->query($sql);
		
		$sql = sprintf("UPDATE %s 
						SET sort=(@i:=(@i+1)) 
						ORDER BY sort asc, id asc 
						LIMIT %s",
						$Website['tables'][0],
						$limit
						);
		$this->db->query($sql);
		
		$order_split = explode(",",$_POST['orderby']);
		$order_size = sizeof($order_split);
		$limit_start = $_POST['limit_start'];
		
		for($i=0;$i<$order_size;$i++){
			$id = $order_split[$i];
			if($id!='' && $id!=0){
				$sql = sprintf("UPDATE %s 
								SET sort=%s 
								WHERE id=%s",
								$Website['tables'][0],
								sql_string(($limit_start+$i), "int"),
								sql_string($id, "int"));//echo $sql;
				$this->db->query($sql);
			}
		}
		//exit;
	}
	
	public function insert($rows1){
		$Website = $this->Website;
		
		$this->db->set($rows1);
		$this->db->insert($Website['tables'][0]);
		$id = $this->db->insert_id();
		
		return $id;
	}
	
	public function update($rows1, $id){
		$Website = $this->Website;
		
		$this->db->set($rows1);
		$this->db->where('id', $id);
		$this->db->update($Website['tables'][0]);
	}
	
	public function code_create($length){
		//產生隨機字串
		$code = str_split(substr(md5(uniqid(rand(), true)), 0, $length));
		
		for($i=0; $i<sizeof($code); $i++){
			if(rand()%2==1){
				$code[$i] = strtoupper($code[$i]);
			}
		}
		
		return implode('', $code);
	}
	
	public function usable($coupon_code, $member_id){
		$Website = $this->Website;
		/*
		
		input
			coupon_code,
			member_id,
		
		output
			usable
			discount			折扣金額
			discount_type		折扣方式 0:元 1:％
			discount_gate		折扣門檻
		*/
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.name=%s and A.display=1 and
							(A.start_time<=%s or A.start_time is NULL or A.start_time=0) and 
							(A.end_time>%s or A.end_time is NULL or A.end_time=0 )",
						$Website['table']['coupon'],
						sql_string($coupon_code, 'text'),
						time(),
						time()
						);
		$rows1 = $this->query->select($sql, 1);
		
		//預設coupon可被使用，以下只判斷是否不能被使用
		$usable = 1;
		
		if($rows1['id']){
			/*
				確認輸入的Coupon是否可被此使用者使用，如果此coupon卷已被指派，但使用者非被指派者，則不能使用
				
				若coupon沒有指定給誰，則誰都可以用，若有指定，則只有指定的人可以用，但每個人只能使用一次
				
				先確認coupon有沒有指定使用，根據rows1.limit_count判斷
				所以情況可能為
				limit_count =0 無使用限制
				limit_count =1 只能使用一次
				
				在看rows2的數量判斷
				rows2是找出未被使用的數量（未數）
				若 未數 >0 可以知道哪些人可以使用，這也代表此coupon卷有被指定
				若 未數 =0 無人可使用，代表coupon卷被使用完了

			*/
			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.coupon_id=%s",
							$Website['table']['member_coupon_relation'],
							sql_string($rows1['id'], 'text')
							);
			$rows2 = $this->query->select($sql);
			
			switch($rows1['type']){
				default:
				case 0:
				case 1:
					/*
						隨機亂數 與 固定順序 
						預設 rows1.limit_count = 1
						此處只會有一筆資料
						所以先判斷是否有被配發
						若未指定配發，則可使用
						
						反之則是已配發，則判斷是否可以被使用
						若被使用 或者 配發對象與使用人不同 則不可使用
					*/
					if(sizeof($rows2)){
						//已配發，有可能已被使用
						
						if($rows2[0]['used_by'] || ($rows2[0]['member_id']!=$member_id)){
							$usable = 0;
						}
					}

					break;
				case 2:
					//單一序號
					if(!$rows1['limit_count']){
						/*
							無數量限制
							預設可以使用
						*/
						
						for($i=0; $i<sizeof($rows2); $i++){
							//若使用者已使用過，則不可使用
							if($rows2[$i]['used_by']==$member_id){
								$usable = 0;
								break;
							}
						}
						
					}else{
						/*
							有數量限制
							則需要判斷目前已被使用的數量，是否已大於等於設定數量
							且目前使用者，是否有使用過
						*/
						
						$j=0;
						
						for($i=0; $i<sizeof($rows2); $i++){
							if($rows2[$i]['used_by']){
								//計算被使用次數
								$j++;
							}
							if($rows2[$i]['used_by']==$member_id){
								//若目前使用者使用過，則不可使用
								$usable = 0;
							}
						}
						
						if($j>=$rows1['limit_count']){
							//若被使用數量大於設定數量 則不可使用
							$usable = 0;
						}
					}
					break;
			}
			
		}else{
			$usable = 0;
		}
		
		if($usable){
			$coupon_id = $rows1['id'];
		}
		
		$result = array(
			'usable'			=>	$usable,
			'discount'			=>	$rows1['discount'],
			'discount_type'		=>	$rows1['discount_type'],
			'discount_gate'		=>	$rows1['discount_gate'],
			'coupon_id'			=>	$coupon_id,
		);
		return $result;
	}
	
}
?>