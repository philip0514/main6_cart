<?
class News_query extends CI_Model{
	function __construct(){
        // 呼叫模型(Model)的建構函數
        parent::__construct();
    }
	
	public function select($data){
		$Website = $data['Website'];
		
		if(!$data['id']){
			$sql = sprintf("SELECT A.*
							FROM %s A
							",
							$Website['tables'][0]
							);
			$rows1 = $this->query->select($sql);
		}else{
			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.id=%s
							",
							$Website['tables'][0],
							(int)$data['id']
							);
			$rows1 = $this->query->select($sql, 1);
		}
		
		return $rows1;
	}
	
	public function table_total($Website){
		$total = $this->db->count_all_results($Website['tables'][0]);
		return $total;
	}
	
	public function display_total($Website, $q){
		$sql = sprintf("SELECT count(A.id) total
						FROM %s A
						",
						$Website['tables'][0]
						);
		$rows1 = $this->query->select($sql, 1);
		return $rows1['total'];
	}
	
	public function datatable_select($Website, $q){
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						%s
						%s
						%s",
						$Website['tables'][0],
						$q['sWhere'],
						$q['sOrder'],
						$q['sLimit']
						);//echo $sql;exit;
		$rows1 = $this->query->select($sql);
		return $rows1;
	}
	
	public function display_update($id, $show, $Website){
		for($i=0;$i<sizeof($id);$i++){
			if($id[$i]){
				if(in_array($id[$i],$show))	$display=1;
				else	$display=0;
				
				$data = array(
					'display'	=>	$display,
				);
				$this->db->where('id', $id[$i]);
				$this->db->update($Website['tables'][0], $data);
			}
		}
		return 0;
	}
	
	public function delete_update($id, $delete, $Website){
		if(sizeof($delete)){
			for($i=0;$i<sizeof($id);$i++){
				if($id[$i]){
					if(in_array($id[$i],$delete)){
						
						$this->db->where('id', $id[$i]);
						$this->db->delete($Website['tables'][0]);
					}
				}
			}
		}
		return 0;
	}
	
	public function insert($Website, $settings, $rows1){
		$this->db->set($rows1);
		$this->db->insert($Website['tables'][0]);
		$id = $this->db->insert_id();
		
		return $id;
	}
	
	public function update($Website, $rows1, $id){
		$this->db->set($rows1);
		$this->db->where('id', $id);
		$this->db->update($Website['tables'][0]);
	}
	
}
?>