<?
class Menu_query extends CI_Model{
	function __construct(){
        parent::__construct();
    }
	
	public function backend_structure($rights){
		$Website = $this->Website;
		
		for($i=0; $i<sizeof($rights); $i++){
			$rights_array[] = 'A.id='.$rights[$i];
		}
		$rights_query = implode(' or ', $rights_array);
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.display=1 and (%s)
						ORDER BY A.sort asc
						",
						$Website['table']['structure'],
						$rights_query
						);
		$rows1 = $this->query->select($sql);
		
		return $rows1;
	}
	
}
?>