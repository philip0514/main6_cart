<?
class Inventory_query extends CI_Model{
	function __construct(){
        // 呼叫模型(Model)的建構函數
        parent::__construct();
    }
	
	public function select($data){
		$Website = $this->Website;
		
		if(!$data['id']){
			$sql = sprintf("SELECT A.*
							FROM %s A
							",
							$Website['tables'][0]
							);
			$rows1 = $this->query->select($sql);
		}else{
			$sql = sprintf("SELECT A.*, B.name product_name, C.name color_name, D.name spec_name
							FROM %s A
							LEFT JOIN %s B ON A.product_id=B.id
							LEFT JOIN %s C ON A.color_id=C.id
							LEFT JOIN %s D ON A.spec_id=D.id
							WHERE A.id=%s
							",
							$Website['tables'][0],
							$Website['tables'][1],
							$Website['tables'][2],
							$Website['tables'][3],
							(int)$data['id']
							);//echo $sql;exit;
			
			$sql = sprintf("SELECT A.*, B.name product_name, C.name spec_name
							FROM %s A
							LEFT JOIN %s B ON A.product_id=B.id
							LEFT JOIN %s C ON A.spec_id=C.id
							WHERE A.id=%s
							",
							$Website['tables'][0],
							$Website['tables'][1],
							$Website['tables'][2],
							(int)$data['id']
							);//echo $sql;exit;
			$rows1 = $this->query->select($sql, 1);
		}
		
		return $rows1;
	}
	
	public function table_total(){
		$Website = $this->Website;
		
		$total = $this->db->count_all_results($Website['tables'][0]);
		return $total;
	}
	
	public function display_total($q){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT count(A.id) total
						FROM %s A
						LEFT JOIN %s B ON A.product_id=B.id
						%s
						",
						$Website['tables'][0],
						$Website['tables'][1],
						$q['sWhere']
						);
		$rows1 = $this->query->select($sql, 1);
		
		$total = $rows1['total'];
		//echo $total;
		return $total;
	}
	
	public function datatable_select($q){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT A.*, B.name product_name, C.name color_name, D.name spec_name
						FROM %s A
						LEFT JOIN %s B ON A.product_id=B.id
						LEFT JOIN %s C ON A.color_id=C.id
						LEFT JOIN %s D ON A.spec_id=D.id
						%s
						%s
						%s
						",
						$Website['tables'][0],
						$Website['tables'][1],
						$Website['tables'][2],
						$Website['tables'][3],
						$q['sWhere'],
						$q['sOrder'],
						$q['sLimit']
						);//echo $sql;exit;
		$sql = sprintf("SELECT A.*, B.name product_name, C.name spec_name
						FROM %s A
						LEFT JOIN %s B ON A.product_id=B.id
						LEFT JOIN %s C ON A.spec_id=C.id
						%s
						%s
						%s
						",
						$Website['tables'][0],
						$Website['tables'][1],
						$Website['tables'][2],
						$q['sWhere'],
						$q['sOrder'],
						$q['sLimit']
						);//echo $sql;exit;
		$rows1 = $this->query->select($sql);
		
		return $rows1;
	}
	
	public function display_update($id, $show){
		$Website = $this->Website;
		
		for($i=0;$i<sizeof($id);$i++){
			if($id[$i]){
				if(in_array($id[$i],$show))	$display=1;
				else	$display=0;
				
				$data = array(
					'display'	=>	$display,
				);
				$this->db->where('id', $id[$i]);
				$this->db->update($Website['tables'][0], $data);
			}
		}
		return 0;
	}
	
	public function delete_update($id, $delete){
		$Website = $this->Website;
		
		if(sizeof($delete)){
			for($i=0;$i<sizeof($id);$i++){
				if($id[$i]){
					if(in_array($id[$i],$delete)){
						
						$this->db->where('id', $id[$i]);
						$this->db->delete($Website['tables'][0]);
					}
				}
			}
		}
		return 0;
	}
	
	public function sort_update(){
		$Website = $this->Website;
		
		$epp = $_POST['epp'];
		$page_no = $_POST['page_no']+1;
		
		$limit = ((int)$page_no-1)*$epp;
		$sql = 'SET @i:=0';
		$this->db->query($sql);
		
		$sql = sprintf("UPDATE %s 
						SET sort=(@i:=(@i+1)) 
						ORDER BY sort asc, id asc 
						LIMIT %s",
						$Website['tables'][0],
						$limit
						);
		$this->db->query($sql);
		
		$order_split = explode(",",$_POST['orderby']);
		$order_size = sizeof($order_split);
		$limit_start = $_POST['limit_start'];
		
		for($i=0;$i<$order_size;$i++){
			$id = $order_split[$i];
			if($id!='' && $id!=0){
				$sql = sprintf("UPDATE %s 
								SET sort=%s 
								WHERE id=%s",
								$Website['tables'][0],
								sql_string(($limit_start+$i), "int"),
								sql_string($id, "int"));//echo $sql;
				$this->db->query($sql);
			}
		}
		//exit;
	}
	
	public function insert($settings, $rows1){
		$Website = $this->Website;
		
		if($settings['btn']['sort']){
			$sql = sprintf("SELECT sort
							FROM %s
							ORDER BY sort desc
							LIMIT 1",
							$Website['tables'][0]
							);
			$rows2 = $this->query->select($sql, 1);
			
			$rows1['sort'] = $rows2['sort']+1;
		}
		
		$this->db->set($rows1);
		$this->db->insert($Website['tables'][0]);
		$id = $this->db->insert_id();
		
		return $id;
	}
	
	public function update($rows1, $id){
		$Website = $this->Website;
		
		$this->db->set($rows1);
		$this->db->where('id', $id);
		$this->db->update($Website['tables'][0]);
	}
	
}
?>