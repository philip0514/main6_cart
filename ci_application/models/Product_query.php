<?
class Product_query extends CI_Model{
	function __construct(){
        // 呼叫模型(Model)的建構函數
        parent::__construct();
    }
	
	public function select($data){
		$Website = $this->Website;
		
		if(!$data['id']){
			$sql = sprintf("SELECT A.*
							FROM %s A
							",
							$Website['tables'][0]
							);
			$rows1 = $this->query->select($sql);
		}else{
			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.id=%s
							",
							$Website['tables'][0],
							(int)$data['id']
							);
			$rows1 = $this->query->select($sql, 1);
		}
		
		
		return $rows1;
	}
	
	public function table_total(){
		$Website = $this->Website;
		
		$total = $this->db->count_all_results($Website['tables'][0]);
		return $total;
	}
	
	public function display_total($q, $g){
		$Website = $this->Website;
		//print_r($g);
		
		$type = $g['type'];
		if($type && $type[0]){
			if($q['sWhere']){
				$q['sWhere'] .= ' and B.category_id IN ('.implode(',',$type).')';
			}else{
				$q['sWhere'] = ' WHERE B.category_id IN ('.implode(',',$type).')';
			}
			
			
			$sql = sprintf("SELECT count(B.product_id) total
							FROM %s B
							LEFT JOIN %s A ON A.id=B.product_id
							%s",
							$Website['table']['product_category_relation'],
							$Website['table']['product'],
							$q['sWhere']
							);//echo $sql;exit;
			$rows1 = $this->query->select($sql, 1);
		}else{
			$sql = sprintf("SELECT count(A.id) total
							FROM %s A
							%s",
							$Website['table']['product'],
							$q['sWhere']
							);//echo $sql;exit;
			$rows1 = $this->query->select($sql, 1);
		}
		/*
		$sql = sprintf("SELECT count(A.product_id) total
						FROM %s A
						%s",
						$Website['table']['product_category_relation'],
						$q['sWhere']
						);echo $sql;exit;
						*/
		
		return $rows1['total'];
	}
	
	public function datatable_select($q, $g){
		$Website = $this->Website;
		
		$type = $g['type'];
		if($type && $type[0]){
			if($q['sWhere']){
				$q['sWhere'] .= ' and B.category_id IN ('.implode(',',$type).')';
			}else{
				$q['sWhere'] = ' WHERE B.category_id IN ('.implode(',',$type).')';
			}
		}
		
		$sql = sprintf("SELECT A.*, B.sort, C.file_path, C.name file_name
						FROM %s A
						LEFT JOIN %s B ON A.id=B.product_id
						LEFT JOIN %s C ON A.main_media_id=C.id
						%s
						GROUP BY A.id
						%s, A.id desc
						
						",
						$Website['table']['product'],
						$Website['table']['product_category_relation'],
						$Website['table']['media'],
						$q['sWhere'],
						$q['sOrder']
						);
		$_SESSION['sql']['export']['product'] = $sql;
		
		$sql = sprintf("SELECT A.*, B.sort, C.file_path, C.name file_name
						FROM %s A
						LEFT JOIN %s B ON A.id=B.product_id
						LEFT JOIN %s C ON A.main_media_id=C.id
						%s
						GROUP BY A.id
						%s, A.id desc
						%s
						
						",
						$Website['table']['product'],
						$Website['table']['product_category_relation'],
						$Website['table']['media'],
						$q['sWhere'],
						$q['sOrder'],
						$q['sLimit']
						);//echo $sql;exit;
		$rows1 = $this->query->select($sql);
		
		return $rows1;
	}
	
	public function display_update($id, $show){//echo 5;
		$Website = $this->Website;
		
		for($i=0;$i<sizeof($id);$i++){
			if($id[$i]){
				if(in_array($id[$i],$show))	$display=1;
				else	$display=0;
				
				if($display){
					$data = array(
						'display'	=>	$display,
						'draft'		=>	0,
					);
				}else{
					$data = array(
						'display'	=>	$display,
					);
				}
				$this->db->where('id', $id[$i]);
				$this->db->update($Website['tables'][0], $data);
				
				//發佈靜態檔
				//$this->static_data($id[$i]);
			}
		}
		return 0;
	}
	
	public function delete_update($id, $delete){
		$Website = $this->Website;
		
		if(sizeof($delete)){
			for($i=0;$i<sizeof($id);$i++){
				if($id[$i]){
					if(in_array($id[$i],$delete)){
						
						$this->db->where('id', $id[$i]);
						$this->db->delete($Website['tables'][0]);
					}
				}
			}
		}
		return 0;
	}
	
	public function sort_update($category_id){
		$Website = $this->Website;
		
		$epp = $_POST['epp'];
		$page_no = $_POST['page_no']+1;
		
		$limit = ((int)$page_no-1)*$epp;
		$sql = 'SET @i:=0';
		$this->db->query($sql);
		
		$sql = sprintf("UPDATE %s 
						SET sort=(@i:=(@i+1)) 
						WHERE category_id=%s
						ORDER BY sort asc, id asc 
						LIMIT %s",
						$Website['table']['product_category_relation'],
						(int)$category_id,
						$limit
						);
		$this->db->query($sql);
		
		
		$order_split = explode(",",$_POST['orderby']);
		$order_size = sizeof($order_split);
		$limit_start = $_POST['limit_start'];
		
		for($i=0;$i<$order_size;$i++){
			$id = $order_split[$i];
			if($id!='' && $id!=0){
				$sql = sprintf("UPDATE %s 
								SET sort=%s 
								WHERE category_id=%s and product_id=%s",
								$Website['table']['product_category_relation'],
								sql_string(($limit_start+$i), "int"),
								sql_string($category_id, "int"),
								sql_string($id, "int"));//echo $sql;
				$this->db->query($sql);
			}
		}
	}
	
	public function insert($settings, $rows1){
		$Website = $this->Website;
		
		$this->db->set($rows1);
		$this->db->insert($Website['tables'][0]);
		$id = $this->db->insert_id();
		
		return $id;
	}
	
	public function update($rows1, $id){
		$Website = $this->Website;
		
		$this->db->set($rows1);
		$this->db->where('id', $id);
		$this->db->update($Website['tables'][0]);
	}
	
	public function type_select($data){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT A.*, B.product_id
						FROM %s A
						LEFT JOIN %s B ON A.id=B.category_id and B.product_id=%s
						WHERE A.display=1
						ORDER BY A.sort asc
						",
						$Website['table']['category'],
						$Website['table']['product_category_relation'],
						(int)$data['id']
						);//echo $sql;
		$rows1 = $this->query->select($sql);
		//echo '<pre>';print_r($rows1);exit;
		return $rows1;
	}
	
	/*
		取得單一項目
	*/
	public function item_select($id=0){
		$Website = $this->Website;
		
		/*
			基本資料 rows1
		*/
		$data = array(
			'id'			=>	$id,
		);
		$rows1 = $this->select($data);
		
		/*
			分類 rows2
		*/
		$data = array(
			'id'		=>	(int)$id,
		);
		$rows2 = $this->type_select($data);
		$rows1['menu'] = $this->menu->product_type_checkbox($rows2);
		for($i=0; $i<sizeof($rows2); $i++){
			if($rows2[$i]['product_id']){
				$rows1['type_original'][] = $rows2[$i]['id'];
			}
		}
		
		if($id){
			
		
			
			/*
				商品圖片
			*/
			$media = $this->media_query->media_select($id, $this->settings['item_type'][0]);
			$rows1['media_input'] = $media['input'];
			$rows1['media_area'] = $media['area'];
			
			/*
				社群分享圖片
			*/
			$ogimage = $this->media_query->media_select($id, $this->settings['item_type'][1]);
			$rows1['ogimage_input'] = $ogimage['input'];
			$rows1['ogimage_area'] = $ogimage['area'];
			
			/*
				媒體 ogimage圖片 rows3
			* /
			$data = array(
				'id'		=>	$id,
			);
			$rows3 = $this->media_query->media_select($data, $this->settings['item_type'][1]);
			for($i=0; $i<sizeof($rows3); $i++){
				$rows1['ogimage_input'][] = $rows3[$i]['id'];
				$rows1['ogimage_area_html'][] = $this->media_query->single_image($rows3[$i]);
			}
			$rows1['ogimage_area'] = implode('', $rows1['ogimage_area_html']);
			
			/*
				TAG rows4
			*/
			$sql = sprintf("SELECT B.*
							FROM %s A, %s B
							WHERE A.product_id=%s and A.tag_id=B.id
							",
							$Website['table']['product_tag_relation'],
							$Website['table']['tag'],
							(int)$id
							);
			$rows4 = $this->query->select($sql);
			for($i=0; $i<sizeof($rows4); $i++){
				$rows1['tags'][] = $rows4[$i]['name'];
			}
			$rows1['tag_text'] = implode(',', $rows1['tags']);
			
			/*
				媒體 商品圖片 rows6
			* /
			$data = array(
				'id'		=>	$id,
			);
			$rows6 = $this->media_query->media_select($data, $this->settings['item_type'][0]);
			
			for($i=0; $i<sizeof($rows6); $i++){
				$rows1['media_input'][] = $rows6[$i]['id'];
				$rows1['media_area_html'][] = $this->media_query->single_image($rows6[$i]);
			}
			$rows1['media_area'] = implode('', $rows1['media_area_html']);
			*/
		}
		
		return array(
			'rows1'		=>	$rows1,
			'rows2'		=>	$rows2,
			//'rows3'		=>	$rows3,
			'rows4'		=>	$rows4,
		);
	}
	
	/*
		存檔
	*/
	public function item_post(){
		$Website = $this->Website;
		
		$id = (int)$_POST['id'];
		$display = (int)$_POST['display'];
		
		$name = $_POST['name'];
		$description = $_POST['description'];
		$kind = $_POST['kind'];
		$display_start_time = strtotime($_POST['display_start_time']);
		$display_end_time = strtotime($_POST['display_end_time']);
		$weight = $_POST['weight'];
		$box_size = $_POST['box_size'];
		$list_price = $_POST['list_price'];
		$price = $_POST['price'];
		$preorder = $_POST['preorder'] ? 1 : 0;
		$preorder_start = strtotime($_POST['preorder_start']);
		$preorder_end = strtotime($_POST['preorder_end']);
		$preorder_shippingtime = strtotime($_POST['preorder_shippingtime']);
		$preorder_price = $_POST['preorder_price'];
		$special = $_POST['special'];
		$special_start = strtotime($_POST['special_start']);
		$special_end = strtotime($_POST['special_end']);
		$special_price = $_POST['special_price'];
		$special_cost_price = $_POST['special_cost_price'];
		$special_select_max = $_POST['special_select_max'];
		$special_text = $_POST['special_text'];
		$today_special = $_POST['today_special'];
		$content = $_POST['content'];
		$product_gift = $_POST['product_gift'];
		$main_media_id = $_POST['main_media_id'];
		
		
		if(!$id){
			//新增
			
			$rows1 = array(
				'display'					=>	ci_sql_string($display, 'int'),
				'name'						=>	ci_sql_string($name, 'text'),
				'description'				=>	ci_sql_string($description, 'text'),
				'kind'						=>	ci_sql_string($kind, 'text'),
				'display_start_time'		=>	ci_sql_string($display_start_time, 'int'),
				'display_end_time'			=>	ci_sql_string($display_end_time, 'int'),
				'weight'					=>	ci_sql_string($weight, 'double'),
				'box_size'					=>	ci_sql_string($box_size, 'double'),
				'list_price'				=>	ci_sql_string($list_price, 'int'),
				'price'						=>	ci_sql_string($price, 'int'),
				'preorder'					=>	ci_sql_string($preorder, 'int'),
				'preorder_start'			=>	ci_sql_string($preorder_start, 'int'),
				'preorder_end'				=>	ci_sql_string($preorder_end, 'int'),
				'preorder_shippingtime'		=>	ci_sql_string($preorder_shippingtime, 'int'),
				'preorder_price'			=>	ci_sql_string($preorder_price, 'int'),
				'special'					=>	ci_sql_string($special, 'int'),
				'special_start'				=>	ci_sql_string($special_start, 'int'),
				'special_end'				=>	ci_sql_string($special_end, 'int'),
				'special_price'				=>	ci_sql_string($special_price, 'int'),
				'special_select_max'		=>	ci_sql_string($special_select_max, 'int'),
				'today_special'				=>	ci_sql_string($today_special, 'int'),
				'content'					=>	ci_sql_string($content, 'text'),
				'main_media_id'				=>	ci_sql_string($main_media_id, 'int'),
				
				'insert_time'				=>	time(),
				'modify_time'				=>	time(),
				'sort'						=>	0,
			);
			$id = $this->insert($this->settings, $rows1);
			
		}else{
			//更新
			
			$rows1 = array(
				'display'					=>	ci_sql_string($display, 'int'),
				'name'						=>	ci_sql_string($name, 'text'),
				'description'				=>	ci_sql_string($description, 'text'),
				'kind'						=>	ci_sql_string($kind, 'text'),
				'display_start_time'		=>	ci_sql_string($display_start_time, 'int'),
				'display_end_time'			=>	ci_sql_string($display_end_time, 'int'),
				'weight'					=>	ci_sql_string($weight, 'double'),
				'box_size'					=>	ci_sql_string($box_size, 'double'),
				'list_price'				=>	ci_sql_string($list_price, 'int'),
				'price'						=>	ci_sql_string($price, 'int'),
				'preorder'					=>	ci_sql_string($preorder, 'int'),
				'preorder_start'			=>	ci_sql_string($preorder_start, 'int'),
				'preorder_end'				=>	ci_sql_string($preorder_end, 'int'),
				'preorder_shippingtime'		=>	ci_sql_string($preorder_shippingtime, 'int'),
				'preorder_price'			=>	ci_sql_string($preorder_price, 'int'),
				'special'					=>	ci_sql_string($special, 'int'),
				'special_start'				=>	ci_sql_string($special_start, 'int'),
				'special_end'				=>	ci_sql_string($special_end, 'int'),
				'special_price'				=>	ci_sql_string($special_price, 'int'),
				'special_select_max'		=>	ci_sql_string($special_select_max, 'int'),
				'today_special'				=>	ci_sql_string($today_special, 'int'),
				'content'					=>	ci_sql_string($content, 'text'),
				'main_media_id'				=>	ci_sql_string($main_media_id, 'int'),
				
				'modify_time'				=>	time(),
				
			);
			$this->update($rows1, $id);
		}
		
		//分類
		$type = $_POST['type'];
		$type_original = explode(',', $_POST['type_original']);
		
		for($i=0; $i<sizeof($type); $i++){
			//新增
			if(!$type[$i]){
				continue;
			}
			
			$sql = sprintf("INSERT IGNORE INTO %s 
							(category_id, product_id, sort) 
							VALUES 
							(%s, %s, 0)",
							$Website['table']['product_category_relation'],
							sql_string($type[$i], "int"),
							sql_string($id, "int")
							);
			$this->query->sql($sql);
		}
		
		for($i=0; $i<sizeof($type_original); $i++){
			//刪除
			if(!in_array($type_original[$i], $type)){
				$sql = sprintf("DELETE FROM %s 
								WHERE product_id=%s and category_id=%s",
								$Website['table']['product_category_relation'],
								sql_string($id, "int"),
								sql_string($type_original[$i], "int")
								);
				$this->query->sql($sql);
			}
		}
		
		//tag
			//刪除所有跟商品有關的tag
		$sql = sprintf("DELETE FROM %s 
						WHERE product_id=%s",
						$Website['table']['product_tag_relation'],
						sql_string($id, "int")
						);//echo $sql.'<br>';exit;
		$this->query->sql($sql);
		
			//取得所有Tag id 沒有的就新增
		$tags = explode(',', $_POST['tag']);
		for($i=0; $i<sizeof($tags); $i++){
			if(!$tags[$i]){
				continue;
			}
			
			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.name=%s",
							$Website['table']['tag'],
							sql_string($tags[$i], "text")
							);
			$rows1 = $this->query->select($sql, 1);
			
			if($rows1['id']){
				$tag_id = $rows1['id'];
			}else{
				$sql = sprintf("INSERT IGNORE INTO %s 
								(name, display) 
								VALUES 
								(%s, 1)",
								$Website['table']['tag'],
								sql_string($tags[$i], "text")
								);//echo $sql.'<br>';exit;
				$tag_id = $this->query->sql($sql);
			}
			
			$sql = sprintf("INSERT IGNORE INTO %s 
							(tag_id, product_id) 
							VALUES 
							(%s, %s)",
							$Website['table']['product_tag_relation'],
							sql_string($tag_id, "int"),
							sql_string($id, "int")
							);//echo $sql.'<br>';//exit;
			$this->query->sql($sql);
		}
		
		//媒體 商品圖片
		$media_type = $this->settings['item_type'][0];
		$media_input = $_POST['media_input'];
		$this->media_query->media_relation($id, $media_input, $media_type);
		
		//媒體 ogimage
		$ogimage_type = $this->settings['item_type'][1];
		$ogimage_input = $_POST['ogimage_input'];
		$this->media_query->media_relation($id, $ogimage_input, $ogimage_type);
		
		return $id;
	}
	
	/*
		根據商品狀態（如：預購，限時特價）確認價格、庫存、與最大可選數量
	*/
	public function price_calculate($rows1){
		$Website = $this->Website;
		
		//原本價格
		$list_price = $rows1['list_price'];
		$price = $rows1['price'];
		
		//預購設定
		$preorder = 0;
		if($rows1['preorder'] && 
			(time()>=$rows1['preorder_start'] || !$rows1['preorder_start']) && (time()<=$rows1['preorder_end'] || !$rows1['preorder_end'])
		){
			$preorder = 1;
			if(!$rows1['list_price']){
				$list_price = $rows1['price'];
			}
			$price = $rows1['preorder_price'];
			$hint = '預購中';
		}
		
		//限時特價
		$special = 0;
		if(($rows1['special']) && 
			(time()>=$rows1['special_start'] || !$rows1['special_start']) && (time()<=$rows1['special_end'] || !$rows1['special_end'])
		){
			$special = 1;
			if(!$rows1['list_price']){
				$list_price = $rows1['price'];
			}
			$price = $rows1['special_price'];
			$hint = '限時特價';
			
			if($rows1['special_select_max'] < $quantity){
				//最大可選數量小於庫存數，則以最大可選數量為主，反之，則以庫存數為主
				$quantity = $rows1['special_select_max'];
			}
		}
		
		
		$rows1['preorder'] = $preorder;
		$rows1['special'] = $special;
		$rows1['price'] = $price;
		$rows1['list_price'] = $list_price;
		$rows1['hint'] = $hint;
		
		return $rows1;
	}
	
}
?>