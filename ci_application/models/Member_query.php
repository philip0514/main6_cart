<?
class Member_query extends CI_Model{
	function __construct(){
        parent::__construct();
		
		//$this->load->database();
		$this->load->config('ion_auth', TRUE);
		$this->load->helper('cookie');
		$this->load->helper('date');
		$this->lang->load('ion_auth');

		//initialize db tables data
		//$this->tables  = $this->config->item('tables', 'ion_auth');

		//initialize data
		$this->identity_column = $this->config->item('identity', 'ion_auth');
		$this->store_salt      = $this->config->item('store_salt', 'ion_auth');
		$this->salt_length     = $this->config->item('salt_length', 'ion_auth');
		$this->join			   = $this->config->item('join', 'ion_auth');


		//initialize hash method options (Bcrypt)
		$this->hash_method = $this->config->item('hash_method', 'ion_auth');
		$this->frontend_hash_method = $this->config->item('frontend_hash_method', 'ion_auth');
		$this->default_rounds = $this->config->item('default_rounds', 'ion_auth');
		$this->random_rounds = $this->config->item('random_rounds', 'ion_auth');
		$this->min_rounds = $this->config->item('min_rounds', 'ion_auth');
		$this->max_rounds = $this->config->item('max_rounds', 'ion_auth');
		
		//initialize messages and error
		$this->messages    = array();
		$this->errors      = array();
		$delimiters_source = $this->config->item('delimiters_source', 'ion_auth');
		
		/*
		//load the error delimeters either from the config file or use what's been supplied to form validation
		if ($delimiters_source === 'form_validation')
		{
			//load in delimiters from form_validation
			//to keep this simple we'll load the value using reflection since these properties are protected
			$this->load->library('form_validation');
			$form_validation_class = new ReflectionClass("CI_Form_validation");

			$error_prefix = $form_validation_class->getProperty("_error_prefix");
			$error_prefix->setAccessible(TRUE);
			$this->error_start_delimiter = $error_prefix->getValue($this->form_validation);
			$this->message_start_delimiter = $this->error_start_delimiter;

			$error_suffix = $form_validation_class->getProperty("_error_suffix");
			$error_suffix->setAccessible(TRUE);
			$this->error_end_delimiter = $error_suffix->getValue($this->form_validation);
			$this->message_end_delimiter = $this->error_end_delimiter;
		}
		else
		{
			//use delimiters from config
			$this->message_start_delimiter = $this->config->item('message_start_delimiter', 'ion_auth');
			$this->message_end_delimiter   = $this->config->item('message_end_delimiter', 'ion_auth');
			$this->error_start_delimiter   = $this->config->item('error_start_delimiter', 'ion_auth');
			$this->error_end_delimiter     = $this->config->item('error_end_delimiter', 'ion_auth');
		}

		*/
		//initialize our hooks object
		$this->_ion_hooks = new stdClass;

		//load the bcrypt class if needed
		if ($this->hash_method == 'bcrypt') {
			if ($this->random_rounds)
			{
				$rand = rand($this->min_rounds,$this->max_rounds);
				$rounds = array('rounds' => $rand);
			}
			else
			{
				$rounds = array('rounds' => $this->default_rounds);
			}

			$this->load->library('bcrypt',$rounds);
		}

		//$this->trigger_events('model_constructor');
		
    }
	
	public function select($data){
		$Website = $this->Website;
		
		if(!$data['id']){
			$sql = sprintf("SELECT A.*
							FROM %s A
							",
							$Website['tables'][0]
							);
			$rows1 = $this->query->select($sql);
		}else{
			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.id=%s
							",
							$Website['tables'][0],
							(int)$data['id']
							);
			$rows1 = $this->query->select($sql, 1);
		}
		
		return $rows1;
	}
	
	public function table_total(){
		$Website = $this->Website;
		
		$total = $this->db->count_all_results($Website['tables'][0]);
		return $total;
	}
	
	public function display_total($q){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT count(A.id) total
						FROM %s A
						%s
						",
						$Website['tables'][0],
						$q['sWhere']
						);
		$rows1 = $this->query->select($sql, 1);
		
		$total = $rows1['total'];
		//echo $total;
		return $total;
	}
	
	public function datatable_select($q){
		$Website = $this->Website;
		
		$sql = sprintf("SELECT A.* 
						FROM %s A
						%s
						%s
						",
						$Website['table']['member'],
						$q['sWhere'],
						$q['sOrder']
						);
		$_SESSION['sql']['export']['member'] = $sql;
		
		$sql = sprintf("SELECT A.* 
						FROM %s A
						%s
						%s
						%s
						",
						$Website['tables'][0],
						$q['sWhere'],
						$q['sOrder'],
						$q['sLimit']
						);//echo $sql;exit;
		$rows1 = $this->query->select($sql);
		
		return $rows1;
	}
	
	public function display_update($id, $show){
		$Website = $this->Website;
		
		for($i=0;$i<sizeof($id);$i++){
			if($id[$i]){
				if(in_array($id[$i],$show))	$display=1;
				else	$display=0;
				
				$data = array(
					'display'	=>	$display,
				);
				$this->db->where('id', $id[$i]);
				$this->db->update($Website['tables'][0], $data);
			}
		}
		return 0;
	}
	
	public function delete_update($id, $delete){
		$Website = $this->Website;
		
		if(sizeof($delete)){
			for($i=0;$i<sizeof($id);$i++){
				if($id[$i]){
					if(in_array($id[$i],$delete)){
						
						$this->db->where('id', $id[$i]);
						$this->db->delete($Website['tables'][0]);
					}
				}
			}
		}
		return 0;
	}
	
	public function sort_update(){
		$Website = $this->Website;
		
		$epp = $_POST['epp'];
		$page_no = $_POST['page_no']+1;
		
		$limit = ((int)$page_no-1)*$epp;
		$sql = 'SET @i:=0';
		$this->db->query($sql);
		
		$sql = sprintf("UPDATE %s 
						SET sort=(@i:=(@i+1)) 
						ORDER BY sort asc, id asc 
						LIMIT %s",
						$Website['tables'][0],
						$limit
						);
		$this->db->query($sql);
		
		$order_split = explode(",",$_POST['orderby']);
		$order_size = sizeof($order_split);
		$limit_start = $_POST['limit_start'];
		
		for($i=0;$i<$order_size;$i++){
			$id = $order_split[$i];
			if($id!='' && $id!=0){
				$sql = sprintf("UPDATE %s 
								SET sort=%s 
								WHERE id=%s",
								$Website['tables'][0],
								sql_string(($limit_start+$i), "int"),
								sql_string($id, "int"));//echo $sql;
				$this->db->query($sql);
			}
		}
		//exit;
	}
	
	public function insert($settings, $rows1){
		$Website = $this->Website;
		
		if($settings['btn']['sort']){
			$sql = sprintf("SELECT sort
							FROM %s
							ORDER BY sort desc
							LIMIT 1",
							$Website['tables'][0]
							);
			$rows2 = $this->query->select($sql, 1);
			
			$rows1['sort'] = $rows2['sort']+1;
		}
		
		$this->db->set($rows1);
		$this->db->insert($Website['tables'][0]);
		$id = $this->db->insert_id();
		
		return $id;
	}
	
	public function update($rows1, $id){
		$Website = $this->Website;
		
		if($rows1['password']){
			$rows1['password'] = $this->encrypt_password($rows1['password']);
		}
		
		$this->db->set($rows1);
		$this->db->where('id', $id);
		$this->db->update($Website['table']['member']);
	}
	
	public function register($rows1){
		$Website = $this->Website;
		
		if(sizeof($rows1) && $rows1['account']){
			$sql = sprintf("SELECT A.* FROM %s A WHERE A.account=%s LIMIT 1",
							$Website['table']['member'],
							sql_string($rows1['account'], 'text')
			);
			$rows0 = $this->query->select($sql, 1);
			
			if($rows0['id']){
				return 0;
			}
			
			$password = $rows1['password'];
			
			$rows1['password'] = $this->encrypt_password($rows1['password']);
			if(!$rows1['password']){
				unset($rows1['password']);
			}
			
			$this->db->set($rows1);
			$this->db->insert($Website['table']['member']);
			$member_id = $this->db->insert_id();
		}else{
			return 0;
		}
		
		if(!$member_id){
			return 0;
		}
		
		//寄信
		if(!$rows1['facebook_id']){
			//一般註冊
			$password_first_three = substr($password, 0, 3);
			$password_after_three = substr($password, 3);
			$password_hidden = $password_first_three.str_repeat("*", strlen($password_after_three)); 
			
			$data = array(
				'type'				=>	'register',
				'mailto'			=>	$rows1['account'],
				'member_name'		=>	$rows1['name'],
				'member_id'			=>	$member_id,
				'rows1'				=>	array(
					'member_name'			=>	$rows1['name'],
					'member_account'		=>	$rows1['account'],
					'member_password'		=>	$password_hidden,
				),
			);
			$this->mailer->to($data);
		}else{
			//Facebook註冊
			$data = array(
				'type'				=>	'facebook_register',
				'mailto'			=>	$rows1['account'],
				'member_name'		=>	$rows1['name'],
				'member_id'			=>	$member_id,
				'rows1'				=>	array(
					'member_name'			=>	$rows1['name'],
					'member_account'		=>	$rows1['account'],
				),
			);
			$this->mailer->to($data);
			
		}
		
		return $member_id;
	}
	
	public function login($data){
		$Website = $this->Website;
		
		if($data['account'] && $data['password']){
			$sql = sprintf("SELECT A.*
							FROM %s A 
							WHERE A.account=%s",
							$Website['table']['member'],
							sql_string($data['account'], "text")
							);
			$rows1 = $this->query->select($sql, 1);
			
			if($rows1['display']){
				$password_confirm = $this->encrypt_password_db($rows1['id'], $rows1['password'], $data['password']);
				
				if($password_confirm){
					$_SESSION['member_info'] = $rows1;
					
					if($rows1['id'] && $data['remember']){
						//更新cookie存在時間
						$this->remember_user($_SESSION['member_info']);
					}
					
					return 1;
				}
			}else{
				//帳號停用
				return 2;
			}
			
			
		}
		
		return 0;
	}
	
	public function login_by_id($id, $remember=0){
		$Website = $this->Website;
		
		if($id){
			//將使用者登入
			$sql = sprintf("SELECT A.*
							FROM %s A 
							WHERE A.id=%s and A.display=1",
							$Website['table']['member'],
							sql_string($id, "int")
							);
			$rows1 = $this->query->select($sql, 1);
			
			$this->login_update($rows1['id']);
			
			if($rows1['id']){
				$_SESSION['member_info'] = $rows1;
			}
		}
		
		if($rows1['id'] && $remember){
			//更新cookie存在時間
			$this->remember_user($_SESSION['member_info']);
		}
		
		return $rows1;
	}
	
	public function facebook_login($facebook_id=0){
		$Website = $this->Website;
		
		$status = 0;
		
		if($facebook_id){
			$sql = sprintf("SELECT A.*
							FROM %s A 
							WHERE A.facebook_id=%s
							LIMIT 1
							",
							$Website['table']['member'],
							sql_string($facebook_id, "int")
							);//echo $sql;
			$rows1 = $this->query->select($sql, 1);
			
			if($rows1['id'] && $rows1['display']){
				$_SESSION['member_info'] = $rows1;
				
				$status = 1;
				
				$this->remember_user($_SESSION['member_info']);
			}
			
			if($rows1['id'] && !$rows1['display']){
				$status = 2;
			}
			
			if($rows1['id']){
				$this->login_update($rows1['id']);
			}
		}
		
		return $status;
	}
	
	/*
		更新最後登入時間
	*/
	public function login_update($id){
		$Website = $this->Website;
		
		$rows1 = array(
			'login_time'		=>	time(),
		);
		$this->db->set($rows1);
		$this->db->where('id', $id);
		$this->db->update($Website['table']['member']);
	}
	
	/*
		確認是否有Cookie存在 有的話就預設登入
	*/
	public function cookie_login(){
		$Website = $this->Website;
		
		$id = $this->input->cookie('member_id');
		$account = $this->input->cookie('member_account');
		
		if(!$_SESSION['member_info']['id']){
			$sql = sprintf("SELECT A.*
							FROM %s A 
							WHERE A.account=%s and A.id=%s and A.display=1",
							$Website['table']['member'],
							sql_string($account, "text"),
							sql_string($id, "int")
							);
			$rows1 = $this->query->select($sql, 1);
			
			$this->login_update($rows1['id']);
			
			if($rows1['id']){
				$_SESSION['member_info'] = $rows1;
			}
		}
		
		if($id && $account){
			//更新cookie存在時間
			$this->remember_user($_SESSION['member_info']);
		}
		return $rows1;
	}
	
	/*
		記住我
	*/
	public function remember_user($rows1){
		$Website = $this->Website;
		
		if($_SESSION['member_info']['id'] && !$_SESSION['member_info']['from_admin']){
			$expire = time()+60*60*24*7;
			$this->input->set_cookie('member_id', $rows1['id'], $expire);
			$this->input->set_cookie('member_account', $rows1['account'], $expire);
		}
	}
	

	/**
	 * Misc functions
	 *
	 * Hash password : Hashes the password to be stored in the database.
	 * Hash password db : This function takes a password and validates it
	 * against an entry in the users table.
	 * Salt : Generates a random salt value.
	 *
	 * @author Mathew
	 */

	/**
	 * Encrypt the password
	 *
	 * @return void
	 * @author Philip
	 **/
	public function encrypt_password($password){
		
		if(empty($password)){
			return FALSE;
		}
		
		if($this->frontend_hash_method=='md5'){
			$password = md5($password);
		}else{
			$password = $this->hash_password($password, FALSE);
		}
		
		return $password;
	}
	
	public function encrypt_password_db($member_id, $password_db, $password_input){
		
		if(empty($password_input)){
			return FALSE;
		}
		
		if($this->frontend_hash_method=='md5'){
			if($password_db==md5($password_input)){
				return TRUE;
			}
		}else{
			$status = $this->hash_password_db($member_id, $password_db, $password_input, FALSE);
			return $status;
		}
		
		return FALSE;
	}
	
	/**
	 * Hashes the password to be stored in the database.
	 *
	 * @return void
	 * @author Mathew
	 **/
	public function hash_password($password, $salt=false, $use_sha1_override=FALSE){
		if (empty($password))
		{
			return FALSE;
		}

		//bcrypt
		if ($use_sha1_override === FALSE && $this->hash_method == 'bcrypt')
		{
			return $this->bcrypt->hash($password);
		}
	}

	/**
	 * This function takes a password and validates it
	 * against an entry in the users table.
	 *
	 * @return void
	 * @author Mathew
	 **/
	public function hash_password_db($id, $password_db, $password_input, $use_sha1_override=FALSE){
		if (empty($id) || empty($password_input)){
			return FALSE;
		}

		// bcrypt
		if ($use_sha1_override === FALSE && $this->frontend_hash_method == 'bcrypt')
		{
			if ($this->bcrypt->verify($password_input,$password_db))
			{
				return TRUE;
			}

			return FALSE;
		}
	}
}
?>