<?
$config = array(
	
	/*
		Admin
	*/
	'admin/login' => array(
        array(
			'field' 		=>	'account',
			'label' 		=>	'my_lang:account',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.account.required',
			),
		),
        array(
			'field' 		=>	'password',
			'label' 		=>	'my_lang:password',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.password.required',
			),
		),
    ),
	
    'admin/about/item' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.name.required',
			),
		),
        array(
			'field' 		=>	'description',
			'label' 		=>	'my_lang:description',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.description.required',
			),
		),
    ),
	
    'admin/color_type/item' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.name.required',
			),
		),
    ),
	
    'admin/color/item' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.name.required',
			),
		),
    ),
	
    'admin/spec_type/item' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.name.required',
			),
		),
    ),
	
    'admin/spec/item' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.name.required',
			),
		),
    ),
	
    'admin/coupon/insert' => array(
        array(
			'field' 		=>	'discount',
			'label' 		=>	'金額',
			'rules'			=>	array(
				'required',
				//'number',
			),
			'errors' 		=> array(
				'required'		=>	'請輸入折扣',
				//'number'		=>	'請輸入正整數'
			),
		),
        array(
			'field' 		=>	'serial',
			'label' 		=>	'自定義序號',
			'rules'			=>	array(),
			'errors' 		=>	array(
				'required'		=>	'請輸入自定義序號',
			),
			'remote'		=>	array(
				'url'			=>	'admin/coupon/ajax_coupon_check/',
				'type'			=>	'POST',
				'data'			=>	array(
					'name'		=>	'#serial',
				),
				'message'		=>	'此序號已存在',
			),
		),
        array(
			'field' 		=>	'limit_count',
			'label' 		=>	'可用次數',
			'rules'			=>	array(),
			'errors' 		=>	array(
				'required'		=>	'可用次數',
			),
		),
        array(
			'field' 		=>	'amount',
			'label' 		=>	'序號組數',
			'rules'			=>	array(),
			'errors' 		=>	array(
				'required'		=>	'序號組數',
			),
		),
        array(
			'field' 		=>	'length',
			'label' 		=>	'序號長度',
			'rules'			=>	array(),
			'errors' 		=>	array(
				'required'		=>	'序號長度',
			),
		),
    ),
	
    'admin/coupon/item' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.name.required',
			),
			'remote'		=>	array(
				'url'			=>	'admin/coupon/ajax_coupon_check/',
				'type'			=>	'POST',
				'data'			=>	array(
					'name'		=>	'#name',
					'id'		=>	'#id',
				),
				'message'		=>	'此序號已存在',
			),
		),
        array(
			'field' 		=>	'discount',
			'label' 		=>	'金額',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'請輸入折扣',
			),
		),
    ),
	
	'admin/member/item' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:admin_member.name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.member.name.required',
			),
		),
        array(
			'field' 		=>	'account',
			'label' 		=>	'my_lang:account',
			'rules'			=>	array(
				'required',
				'valid_email',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.member.account.required',
				'valid_email'	=>	'my_lang:error.member.account.valid_email',
			),
			'remote'		=>	array(
				'url'			=>	'request/member_account/',
				'type'			=>	'POST',
				'data'			=>	array(
					'account'		=>	'#account',
					'member_id'		=>	'#id',
				),
				'message'		=>	'my_lang:error.member.account.remote',
			),
		),
        array(
			'field' 		=>	'password',
			'label' 		=>	'my_lang:password',
			'rules'			=>	array(
				'min_length[6]',
			),
			'errors' 		=> array(
				'min_length'		=>	'my_lang:error.member.password.min_length',
			),
		),
		/*
        array(
			'field' 		=>	'mobile',
			'label' 		=>	'my_lang:mobile',
			'rules'			=>	array(
				'numeric',
				'is_mobile',
			),
			'errors' 		=> array(
				'numeric'		=>	'my_lang:error.member.mobile.numeric',
				'is_mobile'		=>	'my_lang:error.member.mobile.is_mobile',
			),
			'remote'		=>	array(
				'url'			=>	'admin/member/ajax_mobile_check/',
				'type'			=>	'POST',
				'data'			=>	array(
					'mobile'		=>	'#mobile',
					'member_id'		=>	'#id',
				),
				'message'		=>	'my_lang:error.member.mobile.remote',
			),
		),
		*/
    ),
	
	'admin/category/item' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.category.name.required',
			),
		),
		array(
			'field' 		=>	'url_name',
			'label' 		=>	'my_lang:admin_category.url_name',
			'rules'			=>	array(
				'url_name'
			),
			'errors' 		=> array(
				'url_name'		=>	'my_lang:error.category.url_name.url_name'
			),
		),
    ),
	
	'admin/inventory/item' => array(
        array(
			'field' 		=>	'quantity',
			'label' 		=>	'庫存',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:required',
			),
		),
    ),
	
	'admin/product/item' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.product.name.required',
			),
		),
        array(
			'field' 		=>	'description',
			'label' 		=>	'my_lang:description',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.product.description.required',
			),
		),
        array(
			'field' 		=>	'list_price',
			'label' 		=>	'my_lang:list_price',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.product.list_price.required',
			),
		),
        array(
			'field' 		=>	'price',
			'label' 		=>	'my_lang:price',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.product.price.required',
			),
		),
    ),
	
	'admin/news/item' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.news.name.required',
			),
		),
        array(
			'field' 		=>	'description',
			'label' 		=>	'my_lang:description',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.news.description.required',
			),
		),
        array(
			'field' 		=>	'news_time',
			'label' 		=>	'時間',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.news.news_time.required',
			),
		),
    ),
	
	'admin/user/item' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:admin_user.name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.user.name.required',
			),
		),
        array(
			'field' 		=>	'account',
			'label' 		=>	'my_lang:account',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.user.account.required',
			),
		),
    ),
	
	'admin/user_info/list' => array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:admin_user.name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.user_info.name.required',
			),
		),
    ),
	
	'admin/tag/item'		=>	array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'my_lang:admin_tag.name',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'my_lang:error.tag.name.required',
			),
		),
	),
	
	'admin/order/item'		=>	array(
        array(
			'field' 		=>	'buyer_name',
			'label' 		=>	'購買人姓名',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'必填',
			),
		),
        array(
			'field' 		=>	'buyer_address',
			'label' 		=>	'購買人地址',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'必填',
			),
		),
        array(
			'field' 		=>	'buyer_mobile',
			'label' 		=>	'購買人手機',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'必填',
			),
		),
        array(
			'field' 		=>	'recipient_name',
			'label' 		=>	'收件人姓名',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'必填',
			),
		),
        array(
			'field' 		=>	'recipient_address',
			'label' 		=>	'收件人地址',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'必填',
			),
		),
        array(
			'field' 		=>	'recipient_mobile',
			'label' 		=>	'收件人手機',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'必填',
			),
		),
	),
	
	'admin/collection/item'		=>	array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'名稱',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'必填',
			),
		),
        array(
			'field' 		=>	'url',
			'label' 		=>	'網址名稱',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'網址名稱必填',
			),
		),
        array(
			'field' 		=>	'description',
			'label' 		=>	'敘述',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'必填',
			),
		),
	),
	
	
	
	
	/*
		Member
	*/
	'member/register'	=>	array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'姓名',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'%s 必填',
			),
		),
        array(
			'field' 		=>	'account',
			'label' 		=>	'帳號',
			'rules'			=>	array(
				'required',
				'valid_email',
			),
			'errors' 		=> array(
				'required'		=>	'%s 必填',
				'valid_email'	=>	'%s 需為電子郵件 例：abc@def.com'
			),
			'remote'		=>	array(
				'url'			=>	'request/member_account/',
				'type'			=>	'POST',
				'data'			=>	array(
					'account'		=>	'#account'
				),
				'message'		=>	'已被註冊',
			),
		),
        array(
			'field' 		=>	'password',
			'label' 		=>	'密碼',
			'rules'			=>	array(
				'required',
				'min_length[6]',
			),
			'errors' 		=> array(
				'required'		=>	'%s 必填',
				'min_length'		=>	'%s 至少輸入6碼',
			),
		),
        array(
			'field' 		=>	'password_confirm',
			'label' 		=>	'再次填寫密碼',
			'rules'			=>	array(
				'required',
				'matches[password]'
			),
			'errors' 		=> array(
				'required'		=>	'%s 必填',
				'matches'		=>	'密碼不相同',
			),
		),
    ),
	'member/facebook_register'	=>	array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'姓名',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'%s 必填',
			),
		),
        array(
			'field' 		=>	'account',
			'label' 		=>	'帳號',
			'rules'			=>	array(
				'required',
				'valid_email',
			),
			'errors' 		=> array(
				'required'		=>	'%s 必填',
				'valid_email'	=>	'%s 需為電子郵件 例：abc@def.com'
			),
			'remote'		=>	array(
				'url'			=>	'request/member_account/',
				'type'			=>	'POST',
				'data'			=>	array(
					'account'		=>	'#account'
				),
				'message'		=>	'已被註冊',
			),
		),
    ),
	'member/login'	=>	array(
        array(
			'field' 		=>	'account',
			'label' 		=>	'帳號',
			'rules'			=>	array(
				'required',
				'valid_email',
			),
			'errors' 		=> array(
				'required'		=>	'%s 必填',
				'valid_email'	=>	'%s 需為電子郵件 例：abc@def.com'
			),
		),
        array(
			'field' 		=>	'password',
			'label' 		=>	'密碼',
			'rules'			=>	array(
				'required',
				'min_length[6]',
			),
			'errors' 		=> array(
				'required'		=>	'%s 必填',
				'min_length'		=>	'%s 至少輸入6碼',
			),
		),
    ),
	'member/info'	=>	array(
        array(
			'field' 		=>	'name',
			'label' 		=>	'姓名',
			'rules'			=>	array(
				'required',
			),
			'errors' 		=> array(
				'required'		=>	'%s 必填',
			),
		),
        array(
			'field' 		=>	'password',
			'label' 		=>	'密碼',
			'rules'			=>	array(
				'min_length[6]',
			),
			'errors' 		=> array(
				'min_length'		=>	'%s 至少輸入6碼',
			),
		),
    ),
	'member/forgot_password'	=>	array(
        array(
			'field' 		=>	'account',
			'label' 		=>	'帳號',
			'rules'			=>	array(
				'required',
				'valid_email',
			),
			'errors' 		=> array(
				'required'		=>	'%s 必填',
				'valid_email'	=>	'%s 需為電子郵件 例：abc@def.com'
			),
		),
    ),
	
);

?>