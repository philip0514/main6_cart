<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Theme Library Configuration
 *
 * This files holds theme settings
 *
 * @package 	CodeIgniter
 * @category 	Configuration
 * @author 	Kader Bouyakoub <bkader@mail.com>, Chuang I Hao <philip0514@gmail.com>
 * @link 	https://github.com/bkader
 * @link 	https://twitter.com/KaderBouyakoub
 */

/*
| -------------------------------------------------------------------
| Theme Settings
| -------------------------------------------------------------------
|
|  'theme.theme' 		the activated site theme
|  'theme.layout' 		theme default layout file
|  'theme.title_sep' 	string to be used as title separator
|  'theme.minify' 		whether to compress HTML output to not
|
*/

// Site default theme
$config['theme']['theme'] = 'default';

// Site default layout file
$config['theme']['layout'] = 'default';

$config['theme']['folder'] = 'ci_theme';

$config['theme']['themes_folder'] = 'themes';

$config['theme']['component_folder'] = '_component';

$config['theme']['error_folder'] = 'error';

// Site title separator
$config['theme']['title_sep'] = '-';

// Minify HTML Output
//$config['theme']['minify'] = (ENVIRONMENT === 'production');
$config['theme']['minify'] = TRUE;

// Enable CDN (to use 2nd argument or css() & js() functions)
$config['theme']['cdn_enabled'] = TRUE;

// The CDN URL if you host your files there
$config['theme']['cdn_server'] = NULL; // 'http://static.myhost.com/';

// ------------------------------------------------------------------------
// Backup plan :D for site name, desription & keywords
// ------------------------------------------------------------------------

// Default site name
$config['theme']['site_name']        = 'CI-Theme';
$config['theme']['site_description'] = 'CodeIgniter Themes Library';
$config['theme']['site_keywords']    = 'codeigniter, themes, libraries';

$config['theme']['header_config'] 	=	array(
	'theme'				=>	'default',
	'display'			=>	true,
	'stickup'			=>	false,
	'width'				=>	'within_fullwidth',
);

$config['theme']['footer_config'] 	=	array(
	'theme'				=>	'default',
	'display'			=>	true,
	'width'				=>	'fullwidth',
);

$config['theme']['ogimage']		=	array(
	'id'			=>	1,
	'type'			=>	'site_ogimage'
);
/* End of file theme.php */
/* Location: ./application/third_party/theme/config/theme.php */