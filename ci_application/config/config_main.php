<?
date_default_timezone_set("Asia/Taipei");
//session_start();

$Website['user_info'] = $_SESSION['user_info'];
$Website['index_php'] = '';
$Website['checked'] = array('', 'checked');
$Website['display_class'] = array('display_unchecked', 'display_checked');

//table structure
$Website['table'] = array(
	'about'							=>	'about',
	//'color_type'					=>	'color_type',
	//'color'							=>	'color',
	'category'						=>	'category',
	'collection'					=>	'collection',
	'coupon'						=>	'coupon',
	'spec_type'						=>	'spec_type',
	'spec'							=>	'spec',
	'inventory'						=>	'inventory',
	'mail_history'					=>	'mail_history',
	'mail_template'					=>	'mail_template',
	'media'							=>	'media',
	'media_relation'				=>	'media_relation',
	'member'						=>	'member',
	'member_coupon_relation'		=>	'member_coupon_relation',
	'feedback_fund'					=>	'feedback_fund',
	'news'							=>	'news',
	'order'							=>	'orders',
	'orders'						=>	'orders',
	'orders_payment'				=>	'orders_payment',
	'product'						=>	'product',
	//'product_color_relation'		=>	'product_color_relation',
	'product_spec_relation'			=>	'product_spec_relation',
	'product_collection_relation'	=>	'product_collection_relation',
	'product_category_relation'		=>	'product_category_relation',
	'product_tag_relation'			=>	'product_tag_relation',
	'tag'							=>	'tag',
	'theme'							=>	'theme',
	'zip_city'						=>	'zip_city',
	'zip_area'						=>	'zip_area',
	
	
	'login_attempts'				=>	'sys_login_attempts',
	'structure'						=>	'sys_structure',
	'setting'						=>	'sys_setting',
	'group_structure'				=>	'sys_groups_structure',
	'user'							=>	'sys_users',
	'group'							=>	'sys_groups',
	'user_group'					=>	'sys_users_groups',
);

//language
$Website['language']	=	array(
	array(
		'code'		=>	'886',
		'name'		=>	'繁體中文',
		'icon'		=>	'flag-icon flag-icon-tw',
		'language'	=>	'zh_tw',
		'tinymce'	=>	'zh_TW',
	),
	array(
		'code'		=>	'1',
		'name'		=>	'English',
		'icon'		=>	'flag-icon flag-icon-us',
		'language'	=>	'english',
		'tinymce'	=>	'en_GB',
	)
);

//js version
$Website['js_version']	=	array(
	'bootstrap'		=>	'3.3.4',
	'jquery'		=>	'1.11.3',
	'jquery_ui'		=>	'1.11.4',
	'datatable'		=>	'1.10.7',
);

//pagination
$Website['pagination'] = array(
	'use_page_numbers'	=> TRUE,
	'full_tag_open' 	=> '<ul class="pagination">',
	'full_tag_close' 	=> '</ul>',
	'first_link' 		=> '&laquo;',
	'first_tag_open' 	=> '<li>',
	'first_tag_close' 	=> '</li>',
	'last_link' 		=> '&raquo;',
	'last_tag_open' 	=> '<li>',
	'last_tag_close' 	=> '</li>',
	'next_link' 		=> '下一頁',
	'next_tag_open' 	=> '<li>',
	'next_tag_close' 	=> '</li>',
	'prev_link' 		=> '上一頁',
	'prev_tag_open' 	=> '<li>',
	'prev_tag_close' 	=> '</li>',
	'cur_tag_open' 		=> '<li class="active"><a href="javascript:;">',
	'cur_tag_close'		=> '</a></li>',
	'num_tag_open' 		=> '<li>',
	'num_tag_close' 	=> '</li>',
);

//image
$Website['upload_path'] = array(
	'admin_icon'		=>	'upload/icon/',
	'image'				=>	'upload/image/',
	'product'			=>	'upload/product/',
);


$Website['media'] = array(
	'upload_folder'		=>	'upload/media/',
	'thumb_folder'		=>	'thumb_640/',
	'allowed_types'		=>	'jpg|jpeg|png|gif',
	'max_size'			=>	'30000',
	'thumb_width'		=>	640,
	
	'thumb_size'		=>	array(
		640					=>	array(
			'folder'			=>	'thumb_640/',
			'width'				=>	640,
		),
		1200				=>	array(
			'folder'			=>	'thumb_1200/',
			'width'				=>	1200,
		),
	)
);

/*
小寫英文			a-z
大寫英文			A-Z
數字				 0-9
符號				 ~%.:_-
中文				 \x{4e00}-\x{9fa5}
日文				 \x{3130}-\x{318F}
韓文				 \x{AC00}-\x{D7A3}
*/
$Website['url_allow_chars'] = '/[^a-zA-Z0-9~%:_\- \x{4e00}-\x{9fa5} \x{0800}-\x{4e00} \x{3130}-\x{318F} \x{AC00}-\x{D7A3}]+/isu';

//email 代碼
$Website['mail_code'] = array(
	array(
		'code'		=>	'{=mail_content=}',
		'text'		=>	'信件內容，只能在信件框架中使用',
	),
	array(
		'code'		=>	'{=member_name=}',
		'text'		=>	'會員姓名',
	),
	array(
		'code'		=>	'{=member_account=}',
		'text'		=>	'會員帳號',
	),
	array(
		'code'		=>	'{=member_password=}',
		'text'		=>	'會員密碼',
	),
	array(
		'code'		=>	'{=current_time=}',
		'text'		=>	'目前時間，所有信件皆可使用，格式為：yyyy/mm/dd HH:ii:ss',
	),
	array(
		'code'		=>	'{=current_date=}',
		'text'		=>	'目前日期，所有信件皆可使用，格式為：yyyy/mm/dd',
	),
);





//運費 與 運費門檻
$Website['freight'] = array(
	'gate'		=>	1000,
	'price'		=>	100,
);


//金流
$Website['payment_type'] = array(
	0	=>	array(
		'id'		=>	0,
		'name'		=>	'ATM付款',
		'display'	=>	true,
		'setting'	=>	array(),
	),
	1	=>	array(
		'id'		=>	1,
		'name'		=>	'信用卡 ／ 不分期',
		'display'	=>	true,
		'setting'	=>	array(
			'main'		=>	array(
				'url'				=>	'/cart/payment_fisc/',
				'MerchantID'		=>	'',
				'TerminalID'		=>	'',
				'merID'				=>	'',
				'AuthResURL'		=>	'',
				'view_page'			=>	'cart/payment_fisc',
			),
			'test'		=>	array(
				'url'				=>	'/cart/payment_fisc/',
				'MerchantID'		=>	'',
				'TerminalID'		=>	'',
				'merID'				=>	'',
				'AuthResURL'		=>	'',
				'view_page'			=>	'cart/payment_fisc',
			),
		),
	),
	2	=>	array(
		'id'		=>	2,
		'name'		=>	'信用卡 ／ 三期',
		'display'	=>	true,
		'setting'	=>	array(
			'main'		=>	array(
				'url'				=>	'/cart/payment_fisc/',
				'MerchantID'		=>	'',
				'TerminalID'		=>	'',
				'AuthResURL'		=>	'',
				'view_page'			=>	'cart/payment_fisc',
			),
			'test'		=>	array(
				'url'				=>	'/cart/payment_fisc/',
				'MerchantID'		=>	'',
				'TerminalID'		=>	'',
				'AuthResURL'		=>	'',
				'view_page'			=>	'cart/payment_fisc',
			),
		),
	),
	3	=>	array(
		'id'		=>	3,
		'name'		=>	'信用卡 ／ 六期',
		'display'	=>	true,
		'setting'	=>	array(
			'main'		=>	array(
				'url'				=>	'/cart/payment_fisc/',
				'MerchantID'		=>	'',
				'TerminalID'		=>	'',
				'AuthResURL'		=>	'',
				'view_page'			=>	'cart/payment_fisc',
			),
			'test'		=>	array(
				'url'				=>	'/cart/payment_fisc/',
				'MerchantID'		=>	'',
				'TerminalID'		=>	'',
				'AuthResURL'		=>	'',
				'view_page'			=>	'cart/payment_fisc',
			),
		),
	),
);

$Website['payment_status'] = array(
	0	=>	array(
		'id'		=>	0,
		'name'		=>	'等待付款',
	),
	1	=>	array(
		'id'		=>	1,
		'name'		=>	'完成付款',
	),
	2	=>	array(
		'id'		=>	2,
		'name'		=>	'付款失敗',
	),
	3	=>	array(
		'id'		=>	3,
		'name'		=>	'取消付款',
	),
	4	=>	array(
		'id'		=>	4,
		'name'		=>	'退款完成',
	),
	5	=>	array(
		'id'		=>	5,
		'name'		=>	'內部作廢',
	),
	6	=>	array(
		'id'		=>	6,
		'name'		=>	'申請取消訂單',
	),
	7	=>	array(
		'id'		=>	7,
		'name'		=>	'取消訂單處理中',
	),
	8	=>	array(
		'id'		=>	8,
		'name'		=>	'取消訂單完成',
	),
	9	=>	array(
		'id'		=>	9,
		'name'		=>	'申請退貨',
	),
	10	=>	array(
		'id'		=>	10,
		'name'		=>	'退貨處理中',
	),
	11	=>	array(
		'id'		=>	11,
		'name'		=>	'退貨完成',
	),
	12	=>	array(
		'id'		=>	12,
		'name'		=>	'訂單逾期',
	),
);


//配送
$Website['shipping_type'] = array(
	0	=>	array(
		'id'		=>	0,
		'name'		=>	'宅配',
		'display'	=>	1,
		'price'		=>	0,
		'box_size'	=>	0,
		'weight'	=>	0,
	),
);

$Website['shipping_day'] = array(
	0		=>	array(
		'id'		=>	0,
		'name'		=>	'不指定',
		'display'	=>	true,
	),
	1		=>	array(
		'id'		=>	1,
		'name'		=>	'平日',
		'display'	=>	true,
	),
	2		=>	array(
		'id'		=>	2,
		'name'		=>	'假日',
		'display'	=>	true,
	),
);
$Website['shipping_time'] = array(
	0		=>	array(
		'id'		=>	0,
		'name'		=>	'不指定',
		'display'	=>	true,
	),
	1		=>	array(
		'id'		=>	1,
		'name'		=>	'早上',
		'display'	=>	true,
	),
	2		=>	array(
		'id'		=>	2,
		'name'		=>	'下午',
		'display'	=>	true,
	),
	3		=>	array(
		'id'		=>	3,
		'name'		=>	'19:00前',
		'display'	=>	false,
	),
);

$Website['shipping_status'] = array(
	0	=>	array(
		'name'		=>	'未處理',
	),
	1	=>	array(
		'name'		=>	'理貨中',
	),
	2	=>	array(
		'name'		=>	'缺貨中',
	),
	3	=>	array(
		'name'		=>	'出貨中',
	),
	4	=>	array(
		'name'		=>	'已送達',
	),
);

//發票
$Website['invoice'] = array(
	0	=>	array(
		'id'		=>	0,
		'name'		=>	'捐贈發票',
		'display'	=>	true,
	),
	1	=>	array(
		'id'		=>	1,
		'name'		=>	'二聯式',
		'display'	=>	true,
	),
	2	=>	array(
		'id'		=>	2,
		'name'		=>	'三聯式',
		'display'	=>	true,
	),
);

$Website['invoice_donate'] = array(
	array(
		'id'		=>	8585 ,
		'name'		=>	'家扶基金會',
		'display'	=>	true,
	),
	array(
		'id'		=>	25885 ,
		'name'		=>	'伊甸基金會',
		'display'	=>	true,
	),
	array(
		'id'		=>	9527 ,
		'name'		=>	'罕病基金會',
		'display'	=>	true,
	),
);

$Website['invoice_duplicate'] = array(
	0	=>	array(
		'id'		=>	0,
		'name'		=>	'實體發票',
		'display'	=>	true,
	),
	1	=>	array(
		'id'		=>	1,
		'name'		=>	'會員載具：如有中獎，網站將會主動通知您。',
		'display'	=>	true,
	),
	2	=>	array(
		'id'		=>	2,
		'name'		=>	'手機條碼載具',
		'display'	=>	true,
	),
	3	=>	array(
		'id'		=>	3,
		'name'		=>	'自然人憑證條碼載具',
		'display'	=>	true,
	),
);

//折扣
$Website['discount_type'] = array(
	0	=>	array(
		'id'			=>	0,
		'name'			=>	'不折扣',
		'display'		=>	true,
	),
	1	=>	array(
		'id'			=>	1,
		'name'			=>	'購物金',
		'display'		=>	false,
	),
	2	=>	array(
		'id'			=>	2,
		'name'			=>	'選擇優惠卷',
		'display'		=>	true,
	),
	3	=>	array(
		'id'			=>	3,
		'name'			=>	'手動輸入優惠卷',
		'display'		=>	true,
	),
);


$config['Website'] = $Website;
?>