<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Assets Controller Configuration
 *
 * @package 	CodeIgniter
 * @category 	Configuration
 * @author 	Kader Bouyakoub <bkader@mail.com>
 * @link 	http://www.bkader.com/
 */

// Whether to minify CSS & JS Files or not
$config['minify_assets'] = FALSE;

// Set this to n minutes to caches files
$config['cache_assets'] = 0;

/* End of file assets.php */
/* Location: ./application/config/assets.php */