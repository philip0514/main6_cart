<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('my_lang'))
{
	/**
	 * Lang
	 *
	 * Fetches a language variable and optionally outputs a form label
	 *
	 * @param	string	$line		The language line but if want to use array, the line will look like 「admin.all」
	 * @param	string	$for		The "for" value (id of the form element)
	 * @param	array	$attributes	Any additional HTML attributes
	 * @return	string
	 * //echo my_lang('admin.a.c.e');exit;
	 */
	function my_lang($line, $for = '', $attributes = array()){//echo $line.'<br>';//exit;
		$line_array = explode('.', $line);
		if(sizeof($line_array)>1){
			$key = $line_array[0];
		}else{
			$key = 'global';
			$line_array[1] = $line_array[0];
		}
		//echo $key.'<br>';//exit;
		$row = get_instance()->lang->line($key);
		
		for($i=1; $i<sizeof($line_array); $i++){
			if(is_array($row[$line_array[$i]])){
				$row = $row[$line_array[$i]];
			}else{
				$line = $row[$line_array[$i]];
			}
		}
		
		if(!$line && $key!='global'){
			$line = my_lang('global.'.$line_array[$i]);
		}
		
		if ($for !== ''){
			$line = '<label for="'.$for.'"'._stringify_attributes($attributes).'>'.$line.'</label>';
		}
		
		return $line;
	}
}

/* End of file MY_language_helper.php */
/* Location: ./application/helpers/MY_language_helper.php */