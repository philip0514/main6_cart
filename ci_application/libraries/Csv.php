<?
class Csv{
	
	/**
	 The MIT License
	
	 Copyright (c) 2011 <Tsung-Hao>
	
	 Permission is hereby granted, free of charge, to any person obtaining a copy
	 of this software and associated documentation files (the "Software"), to deal
	 in the Software without restriction, including without limitation the rights
	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	 copies of the Software, and to permit persons to whom the Software is
	 furnished to do so, subject to the following conditions:
	
	 The above copyright notice and this permission notice shall be included in
	 all copies or substantial portions of the Software.
	
	 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	 THE SOFTWARE.
	 *
	 * @author: Tsung <tsunghao@gmail.com>
	 */
	/**
	 * @brief 將 Array 轉換成 CSV 的字串輸出
	 * array_to_csv(string $file_name, array $fields [, string $delimiter = ',' [, string $enclosure = '"' ]])
	 *
	 * @ref http://php.net/manual/en/function.fputcsv.php
	 * $param string $file_name 檔案名稱
	 * @param array $fields 要轉換成 CSV 的資料陣列
	 * @param string $delimiter = ',' 每個欄位間, 要用什麼符號間隔
	 * @param string $enclosure = '"' 每個欄位要用什麼符號包住
	 * @return string (csv data string)
	 */
	public function array_to_csv($file_name, $fields, $delimiter = ',', $enclosure = '"'){
		$csv = '';
	
		foreach ($fields as $field) {
			$first_element = true;
	
			foreach ($field as $element) {
				// 除了第一個欄位外, 於 每個欄位 前面都需加上 欄位分隔符號
				if (!$first_element)
				   $csv .= $delimiter;
				$element = ($element);
				$first_element = false;
				
				// CSV 遇到 $enclosure, 需要重複一次, ex: " => ""
				$element = str_replace($enclosure, $enclosure . $enclosure, $element);
				$csv .= $enclosure . $element . $enclosure;
			}
	
			$csv .= "\n";
		}
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$file_name.'"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');				// Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');		// always modified
		header ('Cache-Control: cache, must-revalidate');				// HTTP/1.1
		header ('Pragma: public');										// HTTP/1.0
		echo  mb_convert_encoding($csv, 'big5', 'UTF-8');
		
	}
}
?>