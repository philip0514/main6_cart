<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Theme Library
 * @package 	CodeIgniter\Designith
 * @category 	Libraries
 * @author 	Kader Bouyakoub <bkade@mail.com>, Chuang I Hao <philip0514@gmail.com>
 * @link 	http://www.bkader.com/
 */
class Theme
{
	/**
	 * Instance of CI object
	 * @var 	object
	 */
	protected $CI;

	/**
	 * Configuration area
	 * @var array
	 */
	protected $config = array(
		'theme'				=> 'default',
		'layout'			=> 'default',
		'title_sep'			=> '-',
		'minify'			=> FALSE,
		'cdn_enabled'		=> FALSE,
		'cdn_server'		=> NULL,
		'site_name'			=> 'CodeIgniter',
		'site_description'	=> 'CodeIgniter Themes Library',
		'site_keywords'		=> 'codeigniter, themes, libraries',
		'header_config' 	=>	array(
			'theme'				=>	'default',
			'display'			=>	true,
			'stickup'			=>	true,
			'width'				=>	'fullwidth',
		),
		'footer_config' 	=>	array(
			'theme'				=>	'default',
			'display'			=>	true,
			'width'				=>	'fullwidth',
		),
	);
	
	/**
	 * Current module's details
	 * @var 	string 	$module 		module's name if any
	 * @var 	string 	$controller 	controller's name
	 * @var 	string 	$method 		method's name
	 */
	protected $module     = NULL;
	protected $controller = NULL;
	protected $method     = NULL;

	/**
	 * Additional partial views
	 * @var 	array
	 */
	protected $partials = array();

	/**
	 * Page title, description & keywords
	 */
	protected $title       = '';
	protected $description = '';
	protected $keywords    = '';

	/**
	 * Page's additional CSS, JS & meta tags
	 */
	protected $css_files = array();
	protected $js_files  = array();
	protected $metatags  = array();
	protected $icontags  = array();

	/**
	 * Array of variables to pass to view
	 * @var 	array
	 */
	protected $data = array();

	/**
	 * Constructor
	 */
	public function __construct($config = array())
	{

		// Prepare instance of CI object
		$this->CI =& get_instance();

		// We load the configuration file before this library's config
		if ($_config = $this->CI->config->item('theme')) 
		{
			$config = $_config;
			unset($_config);
		}

		// If the config does not exist, we load library's default config
		elseif (is_array($config) && isset($config['theme']))
		{
			$config = $config['theme'];
		}
		// Otherwise, we use our default config set above ($this->config)
		else
		{
			$config = $this->config;
		}
		
		// We loop through all settings and replaces our default config
		// (only if config is different from default one)
		isset($config['theme']) && is_array($config['theme']) && $config = $config['theme'];
		if ($config != $this->config)
		{
			$this->config = array_replace_recursive($this->config, $config);
		}


		// Turn every config item into a class property
		foreach ($this->config as $key => $val)
		{
			$this->{$key} = $val;
		}
		unset($key, $val);

		// Prepare title separator
		$this->title_sep = ' '.trim($this->title_sep).' ';

		// Make sure URL helper is load then we load our helper
		function_exists('base_url') OR $this->CI->load->helper('url');
		$this->CI->load->helper('theme');


		// Prepare current module's details
		if (method_exists($this->CI->router, 'fetch_module')) {
			$this->module = $this->CI->router->fetch_module();
		}
		$this->controller = $this->CI->router->fetch_class();
		$this->method     = $this->CI->router->fetch_method();

		// Set some useful variables
		function_exists('site_url') OR $this->CI->load->helper('url');
		$this->set(array(
			'site_name'			=> $this->site_name,//$this->CI->config->item('site_name', 'theme'),
			'site_url'			=> site_url(),
			'base_url'			=> base_url(),
			'current_url'		=> current_url(),
			'assets_url'		=> assets_url(),
			'uri_string'		=> $this->CI->uri->uri_string(),
		), NULL, TRUE);
	}

	// ------------------------------------------------------------------------

	/**
	 * Magic __set
	 * @access 	public
	 * @param 	string 	$var 	property's name
	 * @param 	mixed 	$val 	property's value
	 * @return 	void
	 */
	public function __set($var, $val = NULL)
	{
		$this->$var = $val;
	}

	/**
	 * Magic __get
	 * @access 	public
	 * @param 	string 	$var 	property's name
	 * @return 	mixed 	property's value
	 */
	public function __get($var)
	{
		return $this->$var;
	}

	// ------------------------------------------------------------------------

	/**
	 * Sets class properties
	 * @access 	public
	 * @param 	mixed 		$var 		property's name or associative array
	 * @param 	mixed 		$val 		property's value or NULL if $var is array
	 * @param 	boolean 	$global 	make property global or not
	 * @return 	instance of class
	 */
	public function set($var, $val = NULL, $global = FALSE)
	{
		if (is_array($val))
		{
			foreach($val as $key => $value)
			{
				$this->set($key, $value, $global);
			}
		}

		if ($global === TRUE)
		{
			$this->CI->load->vars($var, $val);
		}
		else
		{
			$this->data[$var] = $val;
		}

		return $this;
	}

	// ------------------------------------------------------------------------
	// General Setters
	// ------------------------------------------------------------------------
	
	
	public function config($data){
		
		// 1. get metadata from database
		if(!$data['ogimage']['id']){
			$data['ogimage']['id'] = 1;
		}
		if(!$data['ogimage']['type']){
			$data['ogimage']['type'] = 'site_ogimage';
		}
		$meta = $this->CI->theme_query->meta($data['ogimage']);
		
		if($meta['site_name']){
			$this->site_name($meta['site_name']);
		}
		
		if($meta['description']){
			$this->description($meta['description']);
		}
		
		if($meta['keywords']){
			$this->keywords($meta['keywords']);
		}
		
		if($meta['facebook_appid']){
			$this->facebook_appid = $meta['facebook_appid'];
		}
		
		if(!sizeof($meta['ogimage'])){
			$data['ogimage']['id'] = 1;
			$data['ogimage']['type'] = 'site_ogimage';
			$site_meta = $this->CI->theme_query->meta($data['ogimage']);
			$meta['ogimage'] = $site_meta['ogimage'];
		}
		
		if($meta['icon16']){
			$this->add_icon('shortcut icon', $meta['icon16']);
			$this->add_icon('bookmark', $meta['icon16']);
		}
		
		if($meta['icon57']){
			$this->add_icon('apple-touch-icon', $meta['icon57']);
		}
		
		if($meta['icon114']){
			$this->add_icon('apple-touch-icon', $meta['icon114'], '114x114');
		}
		
		if($meta['icon72']){
			$this->add_icon('apple-touch-icon', $meta['icon72'], '72x72');
		}
		
		if($meta['icon144']){
			$this->add_icon('apple-touch-icon', $meta['icon144'], '144x144');
		}
		
		for($i=0; $i<sizeof($meta['ogimage']); $i++){
			$this->add_meta('og:image', $meta['ogimage'][$i]);
		}
		
		unset($data['ogimage']);
		
		// 2. update metadata from setting
		if($data['site_name']){
			$this->site_name($data['site_name']);
		}
		
		foreach($data as $key => $value){
			switch($key){
				case 'title':
					$this->title($value);
					break;
				case 'description':
					if($value){
						$this->description($value);
					}
					break;
				case 'header_config':
					foreach($value as $j => $j_value){
						$this->header_config[$j] = $j_value;
					}
					break;
				case 'footer_config':
					foreach($value as $j => $j_value){
						$this->footer_config[$j] = $j_value;
					}
					break;
				default:
					$this->$key = $value;
					break;
			}
		}
		
		//若無設定theme 則以後台設定的為主
		if(!$data['theme']){
			$this->theme($this->CI->theme_query->get_active());
		}
		
		//echo $this->title;exit;
		/*
		if($data['site_name']){
			$this->site_name($data['site_name']);
		}
		
		if($data['title']){
			$this->title($data['title']);
		}
		
		if($data['description']){
			$this->description($data['description']);
		}
		
		if($data['keywords']){
			$this->keywords($data['keywords']);
		}
		
		if($data['theme']){
			$this->theme($data['theme']);
		}
		/*
		if(isset($data['has_header'])){
			$this->has_header = $data['has_header'];
		}
		
		if(isset($data['header'])){
			$this->header = $data['header'];
		}
		
		if(isset($data['footer'])){
			$this->footer = $data['footer'];
		}
		*/
	}
	
	/**
	 * Sets page title
	 * @access 	public
	 * @param 	string 	$title 	the title to user
	 * @return  instance of the class
	 */
	public function title($title = '')
	{
		$this->title = $title;
		empty($this->title) OR $this->title .= $this->title_sep.$this->site_name;
		return $this;
	}
	
	public function site_name($site_name = '')
	{
		$this->site_name = $site_name;
		return $this;
	}

	/**
	 * Sets page description
	 * @access 	public
	 * @param 	string 	$description 	the description to user
	 * @return  instance of the class
	 */
	public function description($description = '')
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * Sets page keywords
	 * @access 	public
	 * @param 	string 	$keywords 	the keywords to user
	 * @return  instance of the class
	 */
	public function keywords($keywords = '')
	{
		$this->keywords = $keywords;
		return $this;
	}

	// ------------------------------------------------------------------------
	// !META, CSS & JS
	// ------------------------------------------------------------------------

	/**
	 * Appends meta tags
	 * @access 	public
	 * @param 	mixed 	$name 	meta tag's name
	 * @param 	mixed 	$content
	 * @return 	object
	 */
    public function add_meta($name, $content = NULL)
    {
    	// In case of multiple elements
    	if (is_array($name)) {
    		foreach ($name as $key => $val) {
    			$this->add_meta($key, $val);
    		}

    		return $this;
    	}
		
		if($name=='og:image'){
			$this->metatags[$name][] = $content;
		}else{
    		$this->metatags[$name] = $content;
		}
		//print_r($this->metatags);
		
		//$this->metatags[$name] = $content;
    	return $this;
    }
	
	public function add_icon($rel, $url, $size=NULL){
		if($size){
			$size_text = 'sizes="'.$size.'"';
		}
		
		$this->icontags[]  = '<link rel="'.$rel.'" href="'.$url.'" '.$size_text.' />';
	}

    /**
     * Display a HTML meta tag
     *
     * @access 	public
     *
     * @param   mixed   $name   string or associative array
     * @param   string  $value  value or NULL if $name is array
     * 
     * @return  string
     */
    public function meta($name, $content = NULL)
    {
        // Loop through multiple meta tags
        if (is_array($name)) {
            $meta = array();
            foreach ($name as $key => $val) {
				if(is_array($val)){
					for($i=0; $i<sizeof($val); $i++){
						$meta[] = $this->meta($key, $val[$i]);
					}
				}else{
                	$meta[] = $this->meta($key, $val);
				}
            }
            return implode("\t", $meta);
        }
		
        // Prepare name & content first
		$name    = htmlspecialchars($name, ENT_QUOTES, 'UTF-8');
		$content = htmlspecialchars($content, ENT_QUOTES, 'UTF-8');
		
        return ((strpos($name, 'og:') !== FALSE) || strpos($name, 'fb:') !== FALSE)
                ? '<meta property="'.$name.'" content="'.$content.'">'."\n"
                : '<meta name="'.$name.'" content="'.$content.'">'."\n";
				
    }

    // ------------------------------------------------------------------------

    /**
     * Returns the full url to css file
     * @param   string  $file   filename with or without .css extension
     * @return  string
     */
    public function css_url($file = NULL, $folder = NULL)
    {
    	// If a valid URL is passed, we simply return it
        if (filter_var($file, FILTER_VALIDATE_URL) !== FALSE) {

        	return $file;
        }

        if (strpos($file, '?') !== FALSE) {
            $args = explode('?', $file);
            $file = $args[0];
            $ver  = $args[1];
            unset($args);
        }
        isset($ver) OR $ver = '';

        $file = preg_replace('/.css$/', '', $file).'.css';

        return assets_url('css/'.$file.$ver, $folder);
    }

    /**
     * Returns the full css <link> tag
     * 
     * @param   string  $file   filename to load
     * @param   string  $cdn    to use in case of CDN user
     * @param   mixed   $attr   attributes to append to string
     * 
     * @return  string
     */
    public function css($file, $cdn = NULL, $attrs = '')
    {
    	// Only if a $file a requested
        if ($file) {

        	// Use the 2nd parameter if it's set & the CDN use is enabled.
            $this->cdn_enabled && $cdn !== NULL && $file = $cdn;

            // Return the full link tag
            return '<link rel="stylesheet" type="text/css" href="'.css_url($file).'"'._stringify_attributes($attrs).'>'."\n";
        }

        return NULL;
    }

	/**
	 * Appends CSS files to theme
	 * @access 	public
	 * @param 	none
	 * @return 	object
	 */
	public function add_css()
	{
		$css = func_get_args();
		if (count($css) === 1 && is_array($css[0])) {
			$css = $css[0];
		}

		$this->css_files = array_merge($this->css_files, $css);
		return $this;
	}

	public function get_css()
	{
		return $this->css_files;
	}

	// ------------------------------------------------------------------------

    /**
     * Returns the full url to js file
     * @param   string  $file   filename with or without .js extension
     * 
     * @return  string
     */
    public function js_url($file = NULL, $folder = NULL)
    {
    	// If a valid URL is passed, we simply return it
        if (filter_var($file, FILTER_VALIDATE_URL) !== FALSE) {

        	return $file;
        }

        if (strpos($file, '?') !== FALSE) {
            $args = explode('?', $file);
            $file = $args[0];
            $ver  = $args[1];
            unset($args);
        }
        isset($ver) OR $ver = '';

        $file = preg_replace('/.js$/', '', $file).'.js';

        return assets_url('js/'.$file.$ver, $folder);
    }

    /**
     * Returns the full js <link> tag
     * 
     * @param   string  $file   filename to load
     * @param   string  $cdn    to use in case of CDN user
     * @param   mixed   $attr   attributes to append to string
     * 
     * @return  string
     */
    public function js($file, $cdn = NULL, $attrs = '')
    {
    	// Only if a $file a requested
        if ($file) {
        	// Use the 2nd parameter if it's set & the CDN use is enabled.
            $this->cdn_enabled && $cdn !== NULL && $file = $cdn;
            return '<script type="text/javascript" src="'.js_url($file).'"'._stringify_attributes($attrs).'></script>'."\n";
        }
        return NULL;
    }

	/**
	 * Appends JS files to theme
	 * @access 	public
	 * @param 	none
	 * 
	 * @return 	object
	 */
	public function add_js($js)
	{
		$js = func_get_args();
		if (count($js) === 1 && is_array($js[0])) {
			$js = $js[0];
		}
		$this->js_files = array_merge($this->js_files, $js);
		return $this;
	}

	/**
	 * Collect all additional CSS files and prepare them for output
	 * @access 	protected
	 * @param 	none
	 * @return 	string
	 */
	protected function _output_css()
	{
		$css = array();
		
		foreach ($this->css_files as $file) {
			if (is_array($file)) {
				$css[] = css($file[0], $file[1]);
			}
			else {
				$css[] = css($file);
			}
		}
		
		return implode("\t", $css);
	}

	/**
	 * Collect all additional JS files and prepare them for output
	 * @access 	protected
	 * @param 	none
	 * @return 	string
	 */
	protected function _output_js()
	{
		$js = array();
		
		foreach ($this->js_files as $file) {
			if (is_array($file)) {
				$js[] = js($file[0], $file[1]);
			}
			else {
				$js[] = js($file);
			}
		}
		
		return implode("\t", $js);
	}

	/**
	 * Collectes all additional metatags and prepare them for output
	 * 
	 * @access 	protected
	 * @param 	none
	 * 
	 * @return 	string
	 */
	protected function _output_meta()
	{
		return $this->meta($this->metatags);
	}
	
	protected function _output_icon()
	{
		return implode("\t\n", $this->icontags);
	}

	// ------------------------------------------------------------------------

	/**
	 * Sets page theme
	 * @access 	public
	 * @param 	string 	$theme 	theme's name
	 * @return 	object
	 */
	public function theme($theme = 'default')
	{
		$this->theme = $theme;
		return $this;
	}

	/**
	 * Sets page layout
	 * @access 	public
	 * @param 	string 	$layout 	layout's name
	 * @return 	object
	 */
	public function layout($layout = 'default')
	{
		$this->layout = $layout;
		return $this;
	}

	/**
	 * Adds partial view
	 * @access 	public
	 * @param 	string 	$view 	view file to load
	 * @param 	array 	$data 	array of data to pass
	 * @param 	string 	$name 	name of the variable to use
	 */
	public function add_partial($view, $data = array(), $name = FALSE)
	{
		$name || $name = $view;
		$this->partials[$name] = $this->_load_file('partial', $view, $data, TRUE);
		return $this;
	}

	/**
	 * Displays a partial view
	 * @access 	public
	 * @param 	string 	$view 	the partial view name
	 * @param 	array 	$data 	array of data to pass
	 * @param 	bool 	$return whether to return or output
	 * @return 	mixed
	 */
	public function partial($view, $data = array(), $return = FALSE)
	{
		return $this->_load_file('partial', $view, $data, $return);
	}

	/**
	 * Displays a single view
	 * @access 	public
	 * @param 	string 	$view 	the view name
	 * @param 	array 	$data 	array of data to pass
	 * @param 	bool 	$return whether to return or output
	 * @return 	mixed
	 */
	public function view($view, $data = array(), $return = FALSE)
	{
		return $this->_load_file('view', $view, $data, $return);
	}

	// ------------------------------------------------------------------------

	/**
	 * Loads view file
	 * @access 	public
	 * @param 	string 	$view 		view to load
	 * @param 	array 	$data 		array of data to pass to view
	 * @param 	bool 	$return 	whether to output view or not
	 * @param 	string 	$master 	in case you use a distinct master view
	 * @return  void
	 */
	public function views($view, $data = array(), $return = FALSE, $master = 'template')
	{
		// Start beckmark
		$this->CI->benchmark->mark('theme_start');
		//echo '<pre>';print_r(($data));exit;
		
		// Build the whole outout
		$output = $this->_build_theme_output($view, $data, $master);

		// Stop benchmark
		$this->CI->benchmark->mark('theme_end');

		if ( ! $return) {
            $this->CI->output->set_output($output);
            return;
		}

		return $output;
	}
	public function error_views($view, $data = array(), $return = FALSE, $master = 'template')
	{
		// Start beckmark
		//$this->CI->benchmark->mark('theme_start');
		
		// Build the whole outout
		$output = $this->_build_theme_output($this->config['component_folder'].'/'.$this->config['error_folder'].'/'.$view, $data, $master);

		// Stop benchmark
		//$this->CI->benchmark->mark('theme_end');

		if ( ! $return) {
            $this->CI->output->set_output($output);
            return;
		}

		return $output;
	}

	/**
	 * This methods build everything and returns the final output
	 * 
	 * @access 	protected
	 * 
	 * @param 	string 	$view 	the view to load
	 * @param 	array 	$data 	array of data to pass to view
	 * @param 	string 	$master in case you want to use a distinct master view
	 *
	 * @return 	string
	 */
	protected function _build_theme_output($view, $data = array(), $master = 'template')
	{
		$Website = $this->CI->Website;
		// Make sure title, description & keywords are never empty
		empty($this->title) && $this->title = $this->site_name;
		empty($this->description) && $this->description = $this->site_description;
		empty($this->keywords) && $this->keywords = $this->site_keywords;
		
		if($this->facebook_appid){
			$this->add_meta('fb:app_id', $this->facebook_appid);
		}
		if($this->site_name){
			$this->add_meta('og:site_name', $this->site_name);
		}
		if($this->title){
			$this->add_meta('og:title', $this->title);
		}
		if($this->description){
			$this->add_meta('og:description', $this->description);
		}
		$this->add_meta('og:url', current_url());
		$this->add_meta('og:type', 'website');

		// Put all together.
		$this->set(array(
			'site_name'   => $this->site_name,
			'title'       => $this->title,
			'description' => $this->description,
			'keywords'    => $this->keywords,
			'meta'        => $this->_output_meta(),
			'icon'        => $this->_output_icon(),
			'css_files'   => $this->_output_css(),
			'js_files'    => $this->_output_js(),
		), NULL, TRUE);

		// Set page layout and put content in it
		$content = array();

		// Add partial views only if requested
		if ( ! empty($this->partials)) {
			foreach ($this->partials as $key => $value) {
				$content[$key] = $value;
			}
			unset($key, $value);
		}
		
		//echo $this->has_header;exit;
		if($this->header_config['display']){
			$header_data = array(
				'config'	=>	$this->header_config,
				'category'	=>	$this->CI->theme_query->select('tree'),
				'Website'	=>	$Website,
			);
			$header = $this->_load_file('component', 'header/'.$this->header_config['theme'], $header_data, TRUE);
		}
		
		if($this->footer_config['display']){
			$footer_data = array(
				'config'	=>	$this->footer_config,
				'Website'	=>	$Website,
			);
			$footer = $this->_load_file('component', 'footer/'.$this->footer_config['theme'], array(), TRUE);
		}
		
		if($this->layout && $this->layout!='default'){
			// Prepare view content
			$content['content'] = $this->_load_file('view', $view, $data, TRUE);
			$layout = $this->_load_file('component', 'layout/'.$this->layout, $content, TRUE);
		}else{
			//without default layout
			$layout = $this->_load_file('view', $view, $data, TRUE);
		}
		
		// Prepare layout content
		$this->set(array(
			'header'	=> $header,
			'layout'	=> $layout,
			'footer'	=> $footer,
		), NULL, TRUE);
		unset($content);
		
		
		// Prepare the output
		$output = $this->_load_file('template', $master, $this->data, TRUE);

		// Minify HTML output if set to TRUE
		if (isset($this->config['minify']) && (bool) $this->config['minify'] === TRUE) {
			$output = $this->_minify_output($output);
		}

		return $output;
	}

	// ------------------------------------------------------------------------
	// !PROTECTED METHODS
	// ------------------------------------------------------------------------

	/**
	 * Load view files with locations depending on files types
	 * @access 	protected
	 * @param 	string 	$type 	type of view
	 * @param 	string 	$view 	the view file to load
	 * @param 	array 	$data 	array of data to pass to view file
	 * @param 	bool 	$return whether to output or simply return
	 * @return 	mixed
	 */
	protected function _load_file($type = 'view', $view = '', $data = array(), $return = FALSE)
	{
		switch ($type) {

			// In case of a view file
			case 'view':
			case 'views':

				// prepare all path
				$paths = array(
					build_path(FCPATH, $this->config['folder'], $this->config['themes_folder'], $this->theme, 'modules', $this->module),
					build_path(FCPATH, $this->config['folder'], $this->config['themes_folder'], $this->theme, 'modules', $this->module, 'views'),
					build_path(FCPATH, $this->config['folder'], $this->config['themes_folder'], $this->theme, 'views'),
					build_path(FCPATH, 'modules', $this->module, 'views'),
					build_path(APPPATH, 'modules', $this->module, 'views'),
					build_path(APPPATH, 'views'),
					build_path(VIEWPATH),
				);


				// remove uneccessary paths if $this->module is NULL
				if (empty($this->module))
				{
					unset($paths[0], $paths[1], $paths[3], $paths[4]);
				}

				// Remove unecessary paths if $this->theme is not set
				if ( ! isset($this->theme) OR empty($this->theme))
				{
					unset($paths[0], $paths[1], $paths[2]);
				}

				if ( ! empty($paths))
				{
					$found = FALSE;
					foreach (array_unique($paths) as $path)
					{
						if (file_exists($path.$view.'.php'))
						{
							$found = TRUE;//print_r($data);
							$this->CI->load->vars($data);
							return $this->CI->load->file($path.$view.'.php', $return);
						}
					}

					if ($found !== TRUE) {
						show_error('The requested view file was not found in any of the following directories: <ul><li>'.implode('</li><li>', array_unique($paths)).'</li></ul>');
						die('fuck');
					}

					return NULL;
				}

				return NULL;
				break;

			// In case of a partial view
			case 'partial':
			case 'partials':
				
				// prepare all path
				$paths = array(
					build_path(FCPATH, $this->config['folder'], $this->config['themes_folder'], $this->theme, 'modules', $this->module, 'views', $this->config['component_folder']),
					build_path(FCPATH, $this->config['folder'], $this->config['themes_folder'], $this->theme, 'views', $this->config['component_folder']),
					build_path(APPPATH, 'modules', $this->module, 'views', $this->config['component_folder']),
					build_path(APPPATH, 'views', $this->config['component_folder']),
					build_path(VIEWPATH, $this->config['component_folder']),
				);


				// remove uneccessary paths if $this->module is NULL
				if (empty($this->module)) 
				{
					unset($paths[0], $paths[2], $paths[3]);
				}

				// Remove unecessary paths if $this->theme is not set
				if ( ! isset($this->theme) OR empty($this->theme))
				{
					unset($paths[0], $paths[1]);
				}

				if ( ! empty($paths))
				{
					foreach (array_unique($paths) as $path)
					{
						if (file_exists($path.$view.'.php'))
						{
							$this->CI->load->vars($data);
							return $this->CI->load->file($path.$view.'.php', $return);
						}
					}

					if ($found !== TRUE) {
						show_error('The requested partial file was not found in any of the following directories: <ul><li>'.implode('</li><li>', array_unique($paths)).'</li></ul>');
						die();
					}

					return NULL;
				}

				return NULL;
				break;
				
			// In case of a layout view
			case 'layout':
			case 'layouts':
				/*
				// prepare all path
				$paths = array(
					build_path(FCPATH, $this->config['folder'], $this->config['themes_folder'], $this->theme, 'modules', $this->module, 'views', $this->config['component_folder']),
					build_path(FCPATH, $this->config['folder'], $this->config['themes_folder'], $this->theme, 'views', $this->config['component_folder']),
					build_path(FCPATH, 'modules', $this->module, 'views', $this->config['component_folder']),
					build_path(APPPATH, 'modules', $this->module, 'views', $this->config['component_folder']),
					build_path(APPPATH, 'views', $this->config['component_folder']),
					build_path(VIEWPATH, $this->config['component_folder']),
				);


				// remove uneccessary paths if $this->module is NULL
				if (empty($this->module))
				{
					unset($paths[0], $paths[2], $paths[3]);
				}

				// Remove unecessary paths if $this->theme is not set
				if ( ! isset($this->theme) OR empty($this->theme))
				{
					unset($paths[0], $paths[1]);
				}

				if ( ! empty($paths))
				{
					$found = FALSE;
					foreach (array_unique($paths) as $path)
					{
						if (file_exists($path.$view.'.php'))
						{
							$found = TRUE;
							$this->CI->load->vars($data);
							return $this->CI->load->file($path.$view.'.php', $return);
						}
					}

					if ($found !== TRUE) {
						show_error('The requested layout file was not found in any of the following directories: <ul><li>'.implode('</li><li>', array_unique($paths)).'</li></ul>');
						die();
					}

					return NULL;
				}

				return NULL;
				break;
				*/
			// Load main theme file
			case 'component':
			case 'main':
			case 'theme':
			case 'master':
			case 'template':
			default:

				// prepare all path
				$paths = array(
					build_path(FCPATH, $this->config['folder'], $this->config['themes_folder'], $this->theme, 'modules', $this->module, 'views', $this->config['component_folder']),
					build_path(FCPATH, $this->config['folder'], $this->config['themes_folder'], $this->theme, 'views', $this->config['component_folder']),
					build_path(FCPATH, 'modules', $this->module, 'views', $this->config['component_folder']),
					build_path(APPPATH, 'modules', $this->module, 'views', $this->config['component_folder']),
					build_path(APPPATH, 'views', $this->config['component_folder']),
					build_path(VIEWPATH),
				);
				
				// remove uneccessary paths if $this->module is NULL
				if (empty($this->module))
				{
					unset($paths[0], $paths[2], $paths[3]);
				}

				// Remove unecessary paths if $this->theme is not set
				if ( ! isset($this->theme) OR empty($this->theme))
				{
					unset($paths[0], $paths[1]);
				}
				
				if ( ! empty($paths))
				{
					$found = FALSE;
					foreach (array_unique($paths) as $path)
					{
						if (file_exists($path.$view.'.php'))
						{
							$found = TRUE;
							$this->CI->load->vars($data);//echo $path.$view.'<br>';
							return $this->CI->load->file($path.$view.'.php', $return);
						}
					}

					if ($found !== TRUE) {
						show_error('The requested master view was not found in any of the following directories: <ul><li>'.implode('</li><li>', array_unique($paths)).'</li></ul>');
						die();
					}

					return NULL;
				}

				return NULL;
				break;
		}
	}

	/**
	 * Compresses the HTML output
	 * @access 	protected
	 * @param 	string 	$output 	the html output to minify
	 * @return 	string 	the minified version of $output
	 */
	protected function _minify_output($output)
	{
		// Make sure $output is always a string
		is_string($output) OR $output = (string) $output;

		// In orders, we are searching for
		// 1. White-spaces after tags, except space.
		// 2. White-spaces before tags, except space.
		// 3. Multiple white-spaces sequences.
		// 4. HTML comments
		// 5. CDATA

		// We return the minified $output
		return preg_replace(array(
			'/\>[^\S ]+/s',
			'/[^\S ]+\</s',
			'/(\s)+/s',
			'/<!--(?!<!)[^\[>].*?-->/s',
			'#(?://)?<!\[CDATA\[(.*?)(?://)?\]\]>#s',
		), array(
			'>',
			'<',
			'\\1',
			'',
			"//&lt;![CDATA[\n".'\1'."\n//]]>"
		), $output);
	}
}

/* End of file Theme.php */
/* Location: ./cibase/packages/theme/libraries/Theme.php */