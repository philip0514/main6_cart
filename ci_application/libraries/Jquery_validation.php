<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Jquery_validation {
	// json rules for jQuery validator
    private $rules;
	
	// custom error messages for jQuery validator
    private $messages;
	
	// CI rule names to jQuery validator rules
    private $js_rules = array(
		'required'		=> 'required',
		'matches'		=> 'equalTo',
		'min_length'	=> 'minlength',
		'max_length'	=> 'maxlength',
		'greater_than'	=> 'min',
		'less_than'		=> 'max',
		'numeric'		=> 'number', 
		'integer'		=> 'digits',
		'valid_email'	=> 'email'
	 );
	
	
	 private $default_config = 
	'
	onblur: true,
	onkeyup: false,
	onsubmit: true,
	highlight: function(element, errorClass, validClass){
		form_highlight(element, errorClass, validClass);
	},
	unhighlight: function(element, errorClass, validClass){
		form_unhighlight(element, errorClass, validClass);
	},
	errorElement: "span",
	errorPlacement: function($error, $element){
		form_error_text($error, $element);
	}
	';
	
    /**
    * Build and return jQuery validation code
    *
    * @access public
    * @param  string
    * @return string
    */
    public function run($form_name, $config=null){
        return $this->build_script($form_name, $config);
    }
    
    /**
    * Build jQuery validationcode
    *
    * @access private
    * @param  string
    * @return string
    */
    private function build_script($form_name, $config=null){
        $script = 
'$("'.$form_name.'").validate({
	rules: %s,
	messages: %s,%s
	%s
});';
		
		if($config['submit_handler']){
			$submit_handler = ', submitHandler: function(form){'.$config['submit_handler'].'(form);}';
		}
		
		$rules = str_replace("\/", "/", $this->rules);
		$rules = str_replace('"function', "function", $rules);
		$rules = str_replace(';}"', ";}", $rules);
		$message = str_replace("\/", "/", ($this->messages ? $this->messages : '{}'));
		
        return sprintf($script, 
					   $rules, 
					   $message,
					   $this->default_config,
					   $submit_handler
					  );
    }
    /**
    * Set validation rules
    * 
    * @access public
    * @param  array
    * @return string json formatted string
    */
    public function set_rules($rules){
        foreach ($rules as $k => $v){
			if(is_array($v['rules'])){
				$expl_rules = $v['rules'];
			}else{
				// CI uses "|" delimiter to apply different rules. Let's split it ... 
				$expl_rules = explode('|', $v['rules']);
			}
            foreach ($expl_rules as $index => $rule){
				
				if(is_array($rule)){
					continue;
				}
				//echo $rule.'<br>';
                // check and parse rule if it has parameter. eg. min_length[2]
                if (preg_match("/(.*?)\[(.*)\]/", $rule, $match)){
                    // Check if we have similar rule in jQuery plugin
                    if($this->is_js_rule($match[1])){
                        // If so, let's use jQuery rule name instead of CI's one
						if($match[1]=='matches'){
							$rule_json[$v['field']][$this->get_js_rule($match[1])] = '#'.$match[2];	
						}else{
                        	$rule_json[$v['field']][$this->get_js_rule($match[1])] = $match[2];	
						}
                    }
                }
                // jQuery plugin doesn't support callback like CI, so we'll ignore it and convert everything else 
                elseif (!preg_match("/callback\_/",$rule)){
                    if($this->is_js_rule($rule)){
                    	$rule_json[$v['field']][$this->get_js_rule($rule)] = TRUE;	
                    }else{
						$rule_json[$v['field']][$rule] = TRUE;	
					}
                }
            }
			
			$expl_messages = $v['errors'];
			foreach($expl_messages as $index => $message){
				if ($this->is_js_rule($index)){
                    // Remove CI rule name ...
                    unset($messages[$k][$index]);
                    // and insert jQuery's one 
                    $messages[$k][$this->get_js_rule($index)] = sprintf($message, $v['label']);
                }
				//$message_json[$v['field']][$this->get_js_rule($index)] = sprintf($message, $v['label']);
				$message_json[$v['field']][$this->get_js_rule($index)] = $this->translate(sprintf($message, $v['label']));
			}
			
			if(is_array($v['remote'])){
				$remote_data = $v['remote']['data'];
				$remote_message = $v['remote']['message'];
				unset($v['remote']['data']);
				unset($v['remote']['message']);
				foreach($remote_data as $index => $data){
					$v['remote']['data'][$index] = sprintf("function(){return $('%s').val();}", $data);
				}
				$rule_json[$v['field']]['remote'] = $v['remote'];
				$message_json[$v['field']]['remote'] = $remote_message;
			}
        }
        $this->rules = json_encode($rule_json);
        $this->messages = json_encode($message_json);
        return $this->rules;
    }
    
    /**
    * check if we have alternative rule of CI in jQuery
    *
    * @access private
    * @param  string
    * @return bool
    */
    private function is_js_rule($filter) 
    {
        if (in_array($filter,array_keys($this->js_rules)))
        {
            return TRUE;
        } 
        else 
        {
            return FALSE;
        }
    }
    
    /**
    * Get rule name
    *
    * get jQuery rule name by CI rule name
    *
    * @access private
    * @param  string
    * @return string
    */
    private function get_js_rule($filter)
    {
        return $this->js_rules[$filter];
    }
	
	private function translate($message){
		
		if(sscanf($message, 'my_lang:%s', $line) === 1){
			$label = str_replace('my_lang:', '', $message);
			return my_lang($label);
		}elseif (sscanf($message, 'lang:%s', $line) === 1 && FALSE === ($message = $this->CI->lang->line($line, FALSE))){
			return $line;
		}
		
		return $message;
	}
    
}
// END Jquery_validation class

/* End of file Jquery_validation.php */
/* Location: ./application/libraries/Jquery_validation.php */
