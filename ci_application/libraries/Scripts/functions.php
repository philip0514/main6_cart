<?
function sql_string($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = ""){
	$theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;
	$theValue = trim($theValue);
	switch ($theType) {
		case "text":
			$theValue = ($theValue != "") ? "'" . htmlspecialchars($theValue, ENT_QUOTES) . "'" : "NULL";
			//$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
			//$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
			break;
		case "long":
		case "int":
			$theValue = ($theValue != "") ? intval($theValue) : "0";
			break;
		case "double":
			$theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
			break;
		case "date":
			$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
			break;
		case "defined":
			$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
			break;
	}
	return $theValue;
}


function ci_sql_string($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = ""){
	//$theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;
	$theValue = trim($theValue);
	switch ($theType) {
		case "text":
			$theValue = ($theValue != "") ? htmlspecialchars($theValue, ENT_QUOTES) : "";
			break;
		case "long":
		case "int":
			$theValue = ($theValue != "") ? intval($theValue) : "0";
			break;
		case "double":
			$theValue = ($theValue != "") ? doubleval($theValue) : "";
			break;
		case "date":
			$theValue = ($theValue != "") ? $theValue : "";
			break;
		case "defined":
			$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
			break;
	}
	return $theValue;
}

function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}

function json_format_readable($json) {
    $result      = '';
    $pos         = 0;
    $strLen      = strlen($json);
    $indentStr   = '  ';
    $newLine     = "\n";
    $prevChar    = '';
    $outOfQuotes = true;

    for ($i=0; $i<=$strLen; $i++) {

        // Grab the next character in the string.
        $char = substr($json, $i, 1);

        // Are we inside a quoted string?
        if ($char == '"' && $prevChar != '\\') {
            $outOfQuotes = !$outOfQuotes;
        
        // If this character is the end of an element, 
        // output a new line and indent the next line.
        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
            $result .= $newLine;
            $pos --;
            for ($j=0; $j<$pos; $j++) {
                $result .= $indentStr;
            }
        }
        
        // Add the character to the result string.
        $result .= $char;

        // If the last character was the beginning of an element, 
        // output a new line and indent the next line.
        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
            $result .= $newLine;
            if ($char == '{' || $char == '[') {
                $pos ++;
            }
            
            for ($j = 0; $j < $pos; $j++) {
                $result .= $indentStr;
            }
        }
        
        $prevChar = $char;
    }

    return $result;
}

function array_to_output($data) {
	global $output;
	if ($output == 'xml') {
		return array_to_xml($data);
	} else {
		return json_format_readable(json_encode($data));
	}
}


function array_to_xml($array, $level=1) {
	$xml = '';
	if ($level==1) {
        $xml .= '<?xml version="1.0" encoding="UTF-8" ?>'.
                "\n<result>\n";
    }
    foreach ($array as $key=>$value) {
        $key = strtolower($key);
        if (is_array($value)) {
            $multi_tags = false;
            foreach($value as $key2=>$value2) {
                if (is_array($value2)) {
                    $xml .= str_repeat("\t",$level)."<$key>\n";
                    $xml .= array_to_xml($value2, $level+1);
                    $xml .= str_repeat("\t",$level)."</$key>\n";
                    $multi_tags = true;
                } else {
                    if (trim($value2)!='') {
                        if (htmlspecialchars($value2)!=$value2) {
                            $xml .= str_repeat("\t",$level).
                                    "<$key><![CDATA[$value2]]>".
                                    "</$key>\n";
                        } else {
                            $xml .= str_repeat("\t",$level).
                                    "<$key>$value2</$key>\n";
                        }
                    } else {
						$xml .= str_repeat("\t",$level).
                                "<$key />\n";
					}
                    $multi_tags = true;
                }
            }
            if (!$multi_tags and count($value)>0) {
                $xml .= str_repeat("\t",$level)."<$key>\n";
                $xml .= array_to_xml($value, $level+1);
                $xml .= str_repeat("\t",$level)."</$key>\n";
            }
        } else {
            if (trim($value)!='') {
                if (htmlspecialchars($value)!=$value) {
                    $xml .= str_repeat("\t",$level)."<$key>".
                            "<![CDATA[$value]]></$key>\n";
                } else {
                    $xml .= str_repeat("\t",$level).
                            "<$key>$value</$key>\n";
                }
            } else {
				$xml .= str_repeat("\t",$level).
                        "<$key />\n";
			}
        }
    }
    if ($level==1) {
        $xml .= "</result>\n";
    }
    return $xml;
}


function xml2nest_array($obj, $level=0) {
    $items = array();
     
    if(!is_object($obj)) return $items;
         
    $child = (array)$obj;
	
    if(sizeof($child)>1) {
         foreach($child as $aa=>$bb) {
             if(is_array($bb)) {
                 foreach($bb as $ee=>$ff) {
                     if(!is_object($ff)) {
                         $items[$aa][$ee] = $ff;
                     } else
                     if(get_class($ff)=='SimpleXMLElement') {
                         $items[$aa][$ee] = xml2nest_array($ff,$level+1);
                     }
                 }
             } else
             if(!is_object($bb)) {
                 $items[$aa] = $bb;
             } else
             if(get_class($bb)=='SimpleXMLElement') {
                 $items[$aa] = xml2nest_array($bb,$level+1);
             }
         }
     } else
     if(sizeof($child)>0) {
         foreach($child as $aa=>$bb) {
             if(!is_array($bb)&&!is_object($bb)) {
                 $items[$aa] = $bb;
             } else
             if(is_object($bb)) {
                 $items[$aa] = xml2nest_array($bb,$level+1);
             } else {
                 foreach($bb as $cc=>$dd) {
                     if(!is_object($dd)) {
                         $items[$obj->getName()][$cc] = $dd;
                     } else
                     if(get_class($dd)=='SimpleXMLElement') {
                         $items[$obj->getName()][$cc] = xml2nest_array($dd,$level+1);
                     }
                 }
             }
         }
     }
 
    return $items;
 }
function xml2flat_array($items, $fid=NULL, $layer=1){
	global $rows1;
	//print_r($items);
	if(is_array($items['@attributes'])){
		$items[0] = array(
					'@attributes'	=>	$items['@attributes'],
					'content'		=>	$items['content'],
					'item'			=>	$items['item'],
					
			);
			unset($items['@attributes']);
			unset($items['content']);
			unset($items['item']);
	}
	for($i=0; $i<sizeof($items); $i++){
		if($items[$i]['@attributes']['id']){
			
			$display = '';
			
			if($items[$i]['@attributes']['checked_state']=='undetermined'){
				$display = 2;
			}
			if($items[$i]['@attributes']['checked_state']=='checked'){
				$display = 1;
			}
			if($items[$i]['@attributes']['checked_state']=='unchecked'){
				$display = 0;
			}
			
			
			$rows1[] = array(
				'id'		=>	$items[$i]['@attributes']['id'],
				'name'		=>	$items[$i]['content']['name'],
				'edit_type'	=>	$items[$i]['@attributes']['edit_type'],
				'fid'		=>	$fid,
				'layer'		=>	$layer,
				'display'	=>	$display,
				'link'		=>	$items[$i]['@attributes']['link'],
			);
		}
		if(is_array($items[$i]['item']['@attributes'])){
			$items[$i]['item'][0] = array(
					'@attributes'	=>	$items[$i]['item']['@attributes'],
					'content'		=>	$items[$i]['item']['content'],
					'item'			=>	$items[$i]['item']['item'],
					
			);
			unset($items[$i]['item']['@attributes']);
			unset($items[$i]['item']['content']);
			unset($items[$i]['item']['item']);
		}
		
		if(is_array($items[$i]['item'][0])){
			xml2flat_array($items[$i]['item'], $items[$i]['@attributes']['id'], $layer+1);
		}
		
		
	}
}

function FileUpload($files, $file_o, $UploadPath, $extend, $UploadSize, $img_resize=array(array(10000,10000,100)), $copy=0, $rights=0777){
	$size = sizeof($files['name']);
	for($i=0; $i<$size; $i++){
		
		$time = time();
		
		$FileName_Extend = explode('\.',$files['name'][$i]);
		$FileName_Extend = strtolower(array_pop($FileName_Extend));
		if($FileName_Extend=='jpeg'){
			$FileName_Extend = 'jpg';
		}
		
		if($files['name'][$i] && in_array($FileName_Extend,$extend) && ($files['size'][$i]<=$UploadSize || !$UploadSize)){
			$FileName = $time.rand(1000,9999).".".$FileName_Extend;
			$file_tmp_name = $files["tmp_name"][$i];
			
			for($temp=0; $temp<=$copy; $temp++){
				$FilePath = $UploadPath[$temp].$FileName;
				$FilePath_o = $UploadPath[$temp].$file_o[$i];
				
				if(file_exists($FilePath))
					unlink($FilePath);
					
				if(file_exists($FilePath_o) && $file_o[$i])
					unlink($FilePath_o);
				
				if($temp==$copy)
					move_uploaded_file($file_tmp_name, $FilePath);
				else
					copy($file_tmp_name, $FilePath);
				
				if(in_array($FileName_Extend,array('jpg','jpeg', 'gif', 'png'))){
					$image_size = getimagesize($FilePath);
					//如果寬大於高
					if($image_size[0] > $image_size[1]){
						//如果目前的寬大於應該縮小的寬 則縮小
						/*
						if($image_size[0] > $img_resize[$temp][0]){
							$thumb 	= 	new Imagick($FilePath);
							$thumb	->	thumbnailImage($img_resize[$temp][0], 0);
							$thumb	->	writeImage($FilePath);
						}
						
						$thumb 	= 	new Imagick($FilePath);
						$thumb  ->  cropImage($img_resize[$temp][0], $img_resize[$temp][1], 0, 0);
						$thumb	->	writeImage($FilePath);
						/*
						//再次確認 如果目前的高大於應縮小的高
						$image_size = getimagesize($FilePath);
						if($image_size[1] > $img_resize[$temp][1]){
							$thumb 	= 	new Imagick($FilePath);
							$thumb	->	thumbnailImage(0, $img_resize[$temp][1]);
							$thumb	->	writeImage($FilePath);
						}
						*/
						
					}else{
						/*
						//如果高大於寬
						//如果目前的高大於應該縮小的高 則縮小
						if($image_size[1] > $img_resize[$temp][1]){
							$thumb 	= 	new Imagick($FilePath);
							$thumb	->	thumbnailImage(0, $img_resize[$temp][1]);
							$thumb	->	writeImage($FilePath);
						}
						/*
						//如果目前的寬大於應該縮小的寬 則縮小
						$image_size = getimagesize($FilePath);
						if($image_size[0] > $img_resize[$temp][0]){
							$thumb 	= 	new Imagick($FilePath);
							$thumb	->	thumbnailImage($img_resize[$temp][0], 0);
							$thumb	->	writeImage($FilePath);
						}
						*/
					}
				}
				
				chmod($FilePath, $rights);
			}
			
			$UploadName[] = $FileName;
		}else{
			$UploadName[] = $file_o[$i];
		}
	}
	return $UploadName;
}


function last_page(){
	//echo $_GET['modal'];
	if($_GET['modal']){
		$_SERVER['REQUEST_URI'] = str_replace('?modal=1', '', $_SERVER['REQUEST_URI']);
	}
	//echo $_SERVER['REQUEST_URI'];exit;
	$_SESSION['last_page'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}

function getbrowser(){
	global $_SERVER;
	$agent= $_SERVER['HTTP_USER_AGENT'];
	$browser= '';
	$browser_ver= '';
	//print_r($agent);exit;
	if(preg_match('/iPhone([^\s]+)/i', $agent, $regs)){
		$mobile = 1;
	}
	if(preg_match('/Android([^\s]+)/i', $agent, $regs)){
		$mobile = 1;
	}
	
	if(preg_match('/OmniWeb\/(v*)([^\s|;]+)/i', $agent, $regs)){
		$browser='OmniWeb';
		$browser_ver= $regs[2];
	}
	
	if(preg_match('/Netscape([\d]*)\/([^\s]+)/i', $agent, $regs)){
		$browser='Netscape';
		$browser_ver= $regs[2];
	}
	
	if(preg_match('/safari\/([^\s]+)/i', $agent, $regs)){
		$browser='Safari';
		$browser_ver=$regs[1];
	}
	
	if(preg_match("/Chrome\/([^\s]+)/i", $agent, $regs)) {
		$browser='Chrome';
		$browser_ver=$regs[1];
		if($browser_ver<10){
			//$block = 1;
		}
	}
	
	if(preg_match("/CriOS\/([^\s]+)/i", $agent, $regs)) {
		$browser='Chrome Mobile';
		$browser_ver=$regs[1];
	}
	
	if(preg_match('/MSIE\s([^\s|;]+)/i', $agent, $regs)){
		$browser='Internet Explorer';
		$browser_ver= $regs[1];
		
		if($browser_ver<9){
			$block = 1;
		}
	}
	
	if(preg_match('/Opera[\s|\/]([^\s]+)/i', $agent, $regs)){
		$browser='Opera';
		$browser_ver=$regs[1];
	}
	
	if(preg_match('/NetCaptor\s([^\s|;]+)/i', $agent, $regs)){
		$browser='(Internet Explorer ' .$browser_ver. ') NetCaptor';
		$browser_ver= $regs[1];
	}
	
	if(preg_match('/Maxthon/i', $agent, $regs)){
		$browser='(Internet Explorer ' .$browser_ver. ') Maxthon';
		$browser_ver='';
	}
	
	if(preg_match('/FireFox\/([^\s]+)/i', $agent, $regs)){
		$browser='FireFox';
		$browser_ver=$regs[1];
		if($browser_ver<11){
			//$block = 1;
		}
	}
	
	if(preg_match('/Lynx\/([^\s]+)/i', $agent, $regs)){
		$browser='Lynx';
		$browser_ver=$regs[1];
	}
	
	
	
	if($browser != ''){
		//return $browser.' '.$browser_ver;
		//echo $block;
		return array(
			'browser'	=>	$browser,
			'version'	=>	$browser_ver,
			'block'		=>	$block,
			'mobile'	=>	$mobile,
			);
	}else{
		//exit;
		return array(
			'browser'	=>	'Unknow browser',
			'version'	=>	'',
			'block'		=>	'',
			'mobile'	=>	''
			);
		//return 'Unknow browser';
	}
}
function getMicrotime(){
    list($usec, $sec) = explode(' ', microtime());
    return ((double)$usec + (double)$sec);
}
/*
//$browser = getbrowser();
//print_r($browser);exit;
if($browser['block'] && !$skip_browser_check){
//if($browser['block']){
	$Website['host_http'] = 'http://'.$_SERVER['SERVER_NAME'];
	//print_r($_SERVER);
	//echo $Website['host_http'].'/browserblock/';exit;
	//header('Location: '.$Website['host_http'].'/browserblock/');exit;
}
*/
?>
