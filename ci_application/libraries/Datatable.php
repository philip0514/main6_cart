<?
class Datatable{
	
	/*
	 *	Function query
	 */
	public function query($rows1, $_VAR){
		/* 
		 * Paging
		 */
		$sLimit = "";
		if(isset($_VAR['iDisplayStart']) && $_VAR['iDisplayLength']!='-1'){
			$sLimit = " ".( (int)$_VAR['iDisplayStart'] ).", ".( (int)$_VAR['iDisplayLength'] );
		}
		
		/*
		 * Ordering
		 */
		$order_array = array();
		for($i=0; $i<intval($_VAR['iSortingCols']); $i++){
			if($_VAR['bSortable_'.intval($_VAR['iSortCol_'.$i])]=="true"){
				$order_text = $rows1[0][$_VAR['iSortCol_'.$i]].' '.$_VAR['sSortDir_'.$i];
				if(in_array($order_text, $order_array)){
					continue;
				}
				$order_array[] = $order_text;
			}
			
			$sOrder = implode(', ', $order_array);
		}
		
		if($rows1[2]['icon']['sort'] && !$sOrder){
			$sOrder = $rows1[2]['order_columns'][1].' asc';
		}
		
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		 
		$sWhere = "";
		$where_array = array();
		if(isset($_VAR['sSearch']) && $_VAR['sSearch']!=""){
			$sWhere = "(";
			
			for($i=0; $i<count($rows1[1]); $i++){
				$where_array[] = $rows1[1][$i]." LIKE '%".$_VAR['sSearch']."%'";
			}
			$sWhere .= implode(' OR ', $where_array);
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for($i=0; $i<count($rows1[1]); $i++){
			if(isset($_VAR['bSearchable_'.$i]) && $_VAR['bSearchable_'.$i] == "true" && $_VAR['sSearch_'.$i] != ''){
				if($sWhere==""){
					$sWhere = "";
				}else{
					$sWhere .= " AND ";
				}
				$sWhere .= "".$rows1[1][$i]." LIKE '%".($_VAR['sSearch_'.$i])."%' ";
			}
		}
		
		if($sWhere){
			$sWhere = 'WHERE '.$sWhere;
		}
		if($sOrder){
			$sOrder = 'ORDER BY '.$sOrder;
		}
		if($sLimit){
			$sLimit = 'LIMIT '.$sLimit;
		}
		
		return array(
			'sLimit'	=>	$sLimit,
			'sOrder'	=>	$sOrder,
			'sWhere'	=>	$sWhere,
		);
	}
	
	/*
	 *	Function actions
	 */
	public function actions($rows1){
		$input_id = array(
			'element'	=>	'input',
			'class'		=>	'hidden',
			'id'		=>	'id[]',
			'name'		=>	'id[]',
			'value'		=>	$rows1['id'],
		);
		$input_id = $this->set_icon($input_id);
		
		$sort = array(
			'element'	=>	'a',
			'href'		=>	'javascript:;',
			'class'		=>	'box_tip pull-left sort_icon',
			'title'		=>	my_lang('sort'),
			'value'		=>	$rows1['id'],
			'inside'	=>	array(
				array(
					'element'	=>	'i',
					'class'		=>	'fa fa-sort',
				),
			),
		);
		$sort = $this->set_icon($sort);
		
		$edit = array(
			'element'	=>	'a',
			'href'		=>	$rows1['edit'],
			'class'		=>	'box_tip pull-left edit_link',
			'title'		=>	my_lang('edit'),
			'inside'	=>	array(
				array(
					'element'	=>	'i',
					'class'		=>	'fa fa-pencil',
				),
			),
		);
		$edit = $this->set_icon($edit);
		
		$display = array(
			'element'	=>	'a',
			'href'		=>	'javascript:;',
			'class'		=>	'box_tip pull-left checkbox_display '.$rows1['display_class'],
			'title'		=>	my_lang('display'),
			'inside'	=>	array(
				array(
					'element'	=>	'input',
					'type'		=>	'checkbox',
					'id'		=>	'display[]',
					'name'		=>	'display[]',
					'value'		=>	$rows1['id'],
					'class'		=>	'hidden',
					'checked'	=>	$rows1['checked']
				),
				array(
					'element'	=>	'i',
					'class'		=>	'fa fa-check',
				),
			),
		);
		$display = $this->set_icon($display);
		
		$delete = array(
			'element'	=>	'a',
			'href'		=>	'javascript:;',
			'class'		=>	'box_tip pull-left checkbox_delete delete_unchecked',
			'title'		=>	my_lang('delete'),
			'inside'	=>	array(
				array(
					'element'	=>	'input',
					'type'		=>	'checkbox',
					'id'		=>	'delete[]',
					'name'		=>	'delete[]',
					'value'		=>	$rows1['id'],
					'class'		=>	'hidden'
				),
				array(
					'element'	=>	'i',
					'class'		=>	'fa fa-trash',
				),
			),
		);
		$delete = $this->set_icon($delete);
		
		
		$return = '';
		$return .= $input_id;
		
		if($rows1['icon']['sort']){
			$return .= $sort;
		}
		
		if($rows1['icon']['edit']){
			$return .= $edit;
		}
		
		if($rows1['icon']['display']){
			$return .= $display;
		}
		
		if($rows1['icon']['delete']){
			$return .= $delete;
		}
		
		return '<div class="action_group">'.$return.'</div>';
	}
	
	private function set_icon($rows1){
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement($rows1['element']);
		$node = $dom->appendChild($node);
		
		foreach($rows1	as $key	=>	$value){
			if($key=='element'){
				continue;
			}
			if($key=='inside'){
				for($i=0; $i<sizeof($rows1['inside']); $i++){
					$inside_node = $dom->createElement($rows1['inside'][$i]['element']);
					
					foreach($rows1['inside'][$i]	as $key_i	=>	$value_i){
						if($key_i=='element'){
							continue;
						}
						if($key_i=='checked' && !$value_i){
						}else{
							$inside_node->setAttribute($key_i, $value_i);
						}
					}
					$inside_node = $node->appendChild($inside_node);
				}
				continue;
			}
			$node->setAttribute($key, $value);
		}
		
		return $dom->saveHTML();
	}
	
	/*
	 *	Function button
	 */
	public function button($rows1){
		
		
		//新增
		$insert_txt = my_lang('insert');
		if($rows1['btn_txt']['insert']){
			//自訂按鈕名稱
			$insert_txt = $rows1['btn_txt']['insert'];
		}
		$insert = array(
			'element'	=>	'a',
			'text'		=>	$insert_txt,
			'title'		=>	$insert_txt,
			'href'		=>	$rows1['path']['item'],
			'class'		=>	'btn btn-info btn-cons datatable_button',
			'icon'		=>	'fa fa-plus',
		);
		$insert = $this->set_button($insert);
		
		
		//排序
		$sort_txt = my_lang('sort');
		if($rows1['btn_txt']['sort']){
			//自訂按鈕名稱
			$sort_txt = $rows1['btn_txt']['sort'];
		}
		$sort = array(
			'element'	=>	'button',
			'type'		=>	'submit',
			'id'		=>	'sort',
			'name'		=>	'sort',
			'title'		=>	$sort_txt,
			'text'		=>	$sort_txt,
			'class'		=>	'btn btn-primary btn-cons datatable_button',
			'onclick'	=>	'ssort();',
			'icon'		=>	'fa fa-sort',
		);
		$sort = $this->set_button($sort);
		
		
		//上下架與刪除
		$display_txt = my_lang('display');
		if($rows1['btn_txt']['display']){
			//自訂按鈕名稱
			$display_txt = $rows1['btn_txt']['display'];
		}
		if($rows1['btn']['display']){
			$dd_txt[] = $display_txt;
		}
		$delete_txt = my_lang('delete');
		if($rows1['btn_txt']['delete']){
			//自訂按鈕名稱
			$delete_txt = $rows1['btn_txt']['delete'];
		}
		if($rows1['btn']['delete']){
			$dd_txt[] = $delete_txt;
		}
		$update = array(
			'element'	=>	'button',
			'type'		=>	'submit',
			'id'		=>	's',
			'name'		=>	's',
			'title'		=>	implode(' | ', $dd_txt),
			'text'		=>	implode(' | ', $dd_txt),
			'class'		=>	'btn btn-primary btn-cons datatable_button',
			'icon'		=>	'fa fa-upload',
		);
		$update = $this->set_button($update);
		
		//可自行增加項目
		
		
		
		//------------------------------------------------//
		//組合
		if($rows1['btn']['insert']){
			$return .= $insert;
		}
		if($rows1['btn']['sort']){
			$return .= $sort;
		}
		if($rows1['btn']['display'] || $rows1['btn']['delete']){
			$return .= $update;
		}
		$html = '<div class="btn-group">'.$return.'</div>';
		
		return $html;
	}
	
	private function set_button($rows1){
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		
		if($rows1['icon']){
			$icon = $dom->createElement('i');
			$icon->setAttribute('class', $rows1['icon']);
		}
		
		$text = $dom->createElement('span', ' '.$rows1['text']);
		
		if($rows1['text']){
			$node = $dom->createElement($rows1['element']);
			$node = $dom->appendChild($node);
			
			if($rows1['icon']){
				$node->appendChild($icon);
			}
			
			$node->appendChild($text);
		}else{
			$node = $dom->createElement($rows1['element']);
			$node = $dom->appendChild($node);
		}
		
		//$node = $dom->appendChild($icon);
		
		foreach($rows1	as $key	=>	$value){
			if($key=='element' || $key=='text'){
				continue;
			}
			$node->setAttribute($key, $value);
		}
		return $dom->saveHTML();
	}
}
?>