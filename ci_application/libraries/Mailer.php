<?
require_once APPPATH."/third_party/phpmailer/PHPMailerAutoload.php"; 

class Mailer{
	
	/*
	 *	Function to
	 */
	
	public function to($data){
		$CI =& get_instance();
		$Website = $CI->config->item('Website');
		
		$rows1 = $data['rows1'];
		$mailto = $data['mailto'];
		$member_name = $data['member_name'];
		$member_id = $data['member_id'];
		
		$sql = sprintf('SELECT A.*
						FROM %s A
						WHERE A.id=1
						',
						$Website['table']['setting']
						);
		$rows0 = $CI->query->select($sql, 1);
		
		$debug_message = 0;
		switch($data['type']){
			case 'register':
				$mail_id = 2;
				break;
			case 'facebook_register':
				$mail_id = 3;
				break;
			case 'forget_password':
				$mail_id = 4;
				break;
			default:
			case 'test':
				$mailto = 'philip0514@gmail.com';
				$mail_id = 0;
				$debug_message = 2;
				break;
		}
		
		if($mail_id){
			$sql = sprintf('SELECT A.*
							FROM %s A
							WHERE A.id=1 or A.id=%s
							ORDER BY A.id asc
							',
							$Website['table']['mail_template'],
							sql_string($mail_id, 'int')
							);
			$rows2 = $CI->query->select($sql);
			
			$mailto = $data['mailto'];
			$title = $this->string_replace($rows2[1]['name'], $rows1);
			$content = $this->string_replace($rows2[1]['content'], $rows1);
			
			//樣板
			$content = $this->string_replace(htmlspecialchars_decode($rows2[0]['content']), array('mail_content'	=>	$content));
			
		}else{
			$mailto = $data['mailto'];
			$title = '測試信件';
			$content = '<p>您於 '.date('Y/m/d H:i:s', time()).' 發送此測試信</p>';
		}
		
		$sql = sprintf("INSERT INTO %s 
						(
							name, content, send_to, send_from, send_time, 
							send_type, member_id, member_name, display
						) 
						VALUES 
						(
							%s, %s, %s, %s, %s,
							%s, %s, %s, 1
						)",
						$Website['table']['mail_history'],
						sql_string($title, "text"),
						sql_string($content, "text"),
						sql_string($mailto, "text"),
						sql_string($rows0['mail_from'], "text"),
						time(),
						
						sql_string($data['type'], "text"),
						sql_string($member_id, "int"),
						sql_string($member_name, "text")
						);//echo $sql;exit;
		$id = $CI->query->sql($sql);
		
		//Mail
		$mail = new PHPMailer();
		$mail->IsSMTP();									// telling the class to use SMTP
		$mail->SMTPDebug  		= $debug_message;         	// enables SMTP debug information (for testing)
															// 1 = errors and messages
															// 2 = messages only
		$mail->SMTPAuth   		= true;      				// enable SMTP authentication
		$mail->SMTPSecure 		= $rows0['mail_secure']?'ssl':'tls';
		$mail->Host    			= $rows0['mail_host']; 		// SMTP server
		$mail->Port    			= $rows0['mail_port'];     	// set the SMTP port for the GMAIL server
		$mail->Username			= $rows0['mail_username'];	// SMTP account username
		$mail->Password			= base64_decode($rows0['mail_password']);	// SMTP account password
		$mail->CharSet			= $rows0['mail_charset'];
		$mail->Encoding 		= $rows0['mail_encoding'];
		$mail->AuthType 		= "PLAIN";
		
		$mail->From 			= $rows0['mail_from']; 		//設定寄件者信箱        
		$mail->FromName 		= $rows0['mail_fromname']; 	//設定寄件者姓名       
		$mail->Subject 			= $title; 
		
		$mail->AddAddress($mailto);
		$mail->MsgHTML($content);
		
		//echo '<pre>';print_r($mail);exit;
		
		if(!$mail->Send()){
			echo "發信錯誤: ".$to . $mail->ErrorInfo;exit;
			$sql = sprintf("UPDATE %s SET status=0, status_info=%s WHERE id=%s",
							$Website['table']['mail_history'],
							sql_string($mail->ErrorInfo, "text"),
							sql_string($id, "int")
							);//echo $sql;
			$CI->query->sql($sql);
			if(!$mail_id){
				echo '
'.$mailto.' 發信失敗 ';
			}
			return $mail->ErrorInfo;
		}else{
			$sql = sprintf("UPDATE %s SET status=1 WHERE id=%s",
							$Website['table']['mail_history'],
							sql_string($id, "int")
							);//echo $sql;
			$CI->query->sql($sql);
			
			if(!$mail_id){
				echo '
'.$mailto.' 發信成功 ';
			}
			return 'success';
		}
	}
	
	public function resend($rows1){
		$CI =& get_instance();
		$Website = $CI->config->item('Website');
		
		$title = $rows1['title'];
		$mailto = $rows1['mailto'];
		$content = $rows1['content'];
		$id = $rows1['id'];
		
		$sql = sprintf('SELECT A.*
						FROM %s A
						WHERE A.id=1
						',
						$Website['table']['setting']
						);
		$rows0 = $CI->query->select($sql, 1);
		
		$mail = new PHPMailer();
		$mail->IsSMTP();									// telling the class to use SMTP
		$mail->SMTPDebug  		= $debug_message;         	// enables SMTP debug information (for testing)
															// 1 = errors and messages
															// 2 = messages only
		$mail->SMTPAuth   		= true;      				// enable SMTP authentication
		$mail->SMTPSecure 		= $rows0['mail_secure']?'ssl':'tls';
		$mail->Host    			= $rows0['mail_host']; 		// SMTP server
		$mail->Port    			= $rows0['mail_port'];     	// set the SMTP port for the GMAIL server
		$mail->Username			= $rows0['mail_username'];	// SMTP account username
		$mail->Password			= base64_decode($rows0['mail_password']);	// SMTP account password
		$mail->CharSet			= $rows0['mail_charset'];
		$mail->Encoding 		= $rows0['mail_encoding'];
		$mail->AuthType 		= "PLAIN";
		$mail->From 			= $rows0['mail_from']; 		//設定寄件者信箱        
		$mail->FromName 		= $rows0['mail_fromname']; 	//設定寄件者姓名       
		$mail->Subject 			= $title;
		$mail->SetFrom($rows0['mail_from'], $rows0['mail_fromname']);
		$mailto = explode(';', $mailto);
		
		for($i=0; $i<sizeof($mailto); $i++){
			$mail-> AddAddress(trim($mailto[$i]));
		}
		
		$mail->MsgHTML($content);
		
		if(!$mail->Send()){
			$sql = sprintf("UPDATE %s SET status=0, status_info=%s WHERE id=%s",
							$Website['table']['mail_history'],
							sql_string($mail->ErrorInfo, "text"),
							sql_string($id, "int")
							);
			$CI->query->sql($sql);
			
			return "發信錯誤";
		}else{
			$sql = sprintf("UPDATE %s SET status=1, status_info=null WHERE id=%s",
							$Website['table']['mail_history'],
							sql_string($id, "int")
							);
			$CI->query->sql($sql);
			return '發信成功';
		}
	}
	
	
	private function string_replace($content, $rows1){
		$content = htmlspecialchars_decode($content);
		
		$string_start = 0;
		$continue = true;
		do{
			$string_start = strpos($content, '{=', $string_start);
			$string_end = strpos($content, '=}', $string_start);
			$string_text = substr($content, $string_start+2, $string_end-$string_start-2);
			//echo $string_text.'<br><br>';
			if(!$string_start){
				$continue = false;
			}else{
				if($string_text=='current_time'){
					$rows1['current_time'] = date('Y/m/d H:i:s', time());
				}
				if($string_text=='current_date'){
					$rows1['current_date'] = date('Y/m/d', time());
				}
				
				$content = str_replace('{='.$string_text.'=}', $rows1[$string_text], $content);
			}
			$string_start = $string_end;
			
		}while($continue);
		
		return $content;
	}
}
?>