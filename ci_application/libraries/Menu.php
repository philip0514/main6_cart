<?
class Menu{
	
	public function backend_auth(){
		$CI =& get_instance();
		$Website = $CI->Website;
		
		$uri = $CI->uri->segment_array();
		$action = array_pop($uri);
		
		if(substr($action, 0, 4)=='ajax' || substr($action, 0, 5)=='ajax_'){
			return false;
		}
		
		switch($action){
			case 'login':
			case 'logout':
			case 'error':
				return false;
				break;
			default:
				if($_SERVER['QUERY_STRING']){
					$query_string = '?'.$_SERVER['QUERY_STRING'];
				}
				
				$_SESSION['current_location']['backend'] = base_url().substr($_SERVER['REQUEST_URI'], 1);
				break;
		}
		
		if(!$_SESSION['user_info']['id']){
			$list_url = base_url().'admin/login/';
			redirect($list_url, 'refresh');
		}
		
		//get user info
		$rows1 = $CI->user_query->info();
		
		if(!$rows1['id']){
			$list_url = base_url().'admin/login/';
			redirect($list_url, 'refresh');
		}
		
		//get rights
		$rows2 = $CI->user_query->rights();
		
		for($i=0; $i<sizeof($rows2); $i++){
			if(!in_array($rows2[$i]['structure_id'], $rights)){
				$rights[] = $rows2[$i]['structure_id'];
				$list_link = explode('/',$rows2[$i]['list_link']);
				$rights_edit[$list_link[0]] = explode(',', $rows2[$i]['editable']);
			}else{
				//當有重複的權限，則merge在一起
				$e = explode(',', $rows2[$i]['editable']);
				$list_link = explode('/',$rows2[$i]['list_link']);
				$rights_edit[$list_link[0]] = array_merge((array)$rights_edit[$list_link[0]], (array)$e);
			}
		}
		$_SESSION['user_info']['rights_edit'] = $rights_edit;
		
		//非管理員導回登入頁
		if(!in_array(1, $rights)){
			$list_url = base_url().'admin/login/';
			redirect($list_url, 'refresh');
			exit;
		}
		
		//取出menu
		$structure = $CI->menu_query->backend_structure($rights);
		$html = $this->backend_nav($structure);
		
		return $html;
	}
	
	private function backend_nav($rows1){
		//query
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->Website;
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement("ul");
		$node->setAttribute("class", 'menu-items m-t-30');
		$parnode = $dom->appendChild($node);
		
		$pathinfo_array = $CI->uri->segment_array();
		
		$start_count = 0;
		for($i=0; $i<sizeof($rows1); $i++){
			$is_here = 0;
			if(!$access_success){
				//判斷是否有權限可以進入
				if(substr($rows1[$i]['list_link'], -1)=='/' && substr($rows1[$i]['list_link'], 0, -1)==$pathinfo_array[2]){
					$is_here = 1;
					$access_success = 1;
					$structure_id = $rows1[$i]['id'];
					$structure_fid = $rows1[$i]['fid'];
					if($structure_fid){
						$nodes[$structure_fid]->setAttribute("class", 'open active');
					}
				}
			}
			
			$id = $rows1[$i]['id'];
			$fid = $rows1[$i]['fid'];
			$parent = $parnode;
			
			if($fid){
				if(!$node_ol[$fid]){
					$node_ol[$fid] = $dom->createElement("ul");								//start ul
					$node_ol[$fid]->setAttribute("class", 'sub-menu');
				}
				$parent = $node_ol[$fid];
			}
			
			//li
			unset($class_name);
			$nodes[$id] = $dom->createElement("li");										//start li
			if($start_count==0){
				$class_name[] = 'm-t-30';
			}
			$start_count++;
			if($is_here){
				$class_name[] = 'active';
			}
			$nodes[$id]->setAttribute("class", implode(' ',$class_name));
			
			//li > a
			$link = $dom->createElement('a');												//start a
			
			if($rows1[$i]['list_link']!='/'){
				//不為首頁
				$link->setAttribute("href", 'admin/'.$rows1[$i]['list_link']);
			}else{
				$link->setAttribute("href", 'admin/');
			}
			
			if($rows1[$i]['list_link']=='javascript:;'){
				$link->setAttribute("href", $rows1[$i]['list_link']);
			}
			
			//li > a > span
			$span = $dom->createElement('span', my_lang('admin_menu.'.$rows1[$i]['name']));						//start span
			$span->setAttribute("class", 'title');
			$link->appendChild($span);
			
			//若有子項目
			if($rows1[$i+1]['fid']==$rows1[$i]['id']){
				$span = $dom->createElement('span');										//start span
				$span->setAttribute("class", 'arrow');
				$link->appendChild($span);
			}
			
			//li > span
			if(substr($rows1[$i]['icon'], 0, 3)=='fa-'){
				$span = $dom->createElement('span');										//start span
				$span->setAttribute("class", 'icon-thumbnail');
					$span_i = $dom->createElement('i');											//start span
					$span_i->setAttribute("class", 'fa '.$rows1[$i]['icon']);
				$span->appendChild($span_i);
			}else{
				$span = $dom->createElement('span', $rows1[$i]['icon']);						//start span
				$span->setAttribute("class", 'icon-thumbnail');
			}
			
			$nodes[$id]->appendChild($link);												
			$nodes[$id]->appendChild($span);												//end a
			
			$parent->appendChild($nodes[$id]);												//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);											//end ul
			}
		}
		
		if(!$access_success){
			//如果沒權限 則導回首頁
			header('Location: '.base_url().'admin/');exit;
		}
		
		$data = array(
			'menu'			=>	$dom->saveHTML(),
			//'breadcrumbs'	=>	$this->breadcrumbs_list($structure_id, $Website['table']['structure']),
		);
		return $data;
	}
	
	/*
		後台類別 樹狀拖曳排序
	*/
	public function backend_category_sortable($rows1){
		$CI =& get_instance();
		$Website = $CI->Website;
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement("ol");
		$node->setAttribute("class", 'sortable');
		$parnode = $dom->appendChild($node);
		
		for($i=0; $i<sizeof($rows1); $i++){
			
			$id = $rows1[$i]['id'];
			$fid = $rows1[$i]['fid'];
			$parent = $parnode;
			
			if($fid){
				if(!$node_ol[$fid]){
					$node_ol[$fid] = $dom->createElement("ol");					//start ol
				}
				$parent = $node_ol[$fid];
			}
			$display_class = $rows1[$i]['display'] ? '':'alert alert-danger';
			
			$nodes[$id] = $dom->createElement("li");	//start li
			$nodes[$id]->setAttribute("id", 'list_'.$rows1[$i]['id']);
			$nodes[$id]->setAttribute("class", $display_class);
				
				$div = $dom->createElement('div');	//start div
					
						$span1 = $dom->createElement('span');	//start span
					$span2 = $dom->createElement('span');	//start span
					$span2->setAttribute("class", 'disclose');
					$span2->appendChild($span1);
					
					$span3 = $dom->createElement('span', $rows1[$i]['name']);	//start span
					$span3->setAttribute("class", 'text');
					
					
						$edit = $dom->createElement('i');
						$edit->setAttribute("class", 'fa fa-pencil');
					$edit_link = $dom->createElement('a');	//start span
					$edit_link->setAttribute("href", 'javascript:;');
					$edit_link->setAttribute("class", 'pull-right sortable_icon edit_link');
					$edit_link->setAttribute("title", my_lang('edit'));
					$edit_link->appendChild($edit);
					
						$drag = $dom->createElement('i');
						$drag->setAttribute("class", 'fa fa-sort');
					$drag_link = $dom->createElement('a');	//start span
					$drag_link->setAttribute("href", 'javascript:;');
					$drag_link->setAttribute("class", 'pull-left sortable_icon handler');
					$drag_link->setAttribute("title", my_lang('sort'));
					$drag_link->appendChild($drag);
					
						
						$delete = $dom->createElement('i');
						$delete->setAttribute("class", 'fa fa-trash');
					$delete_link = $dom->createElement('a');	//start span
					$delete_link->setAttribute("href", 'javascript:;');
					$delete_link->setAttribute("class", 'pull-right sortable_icon delete');
					$delete_link->setAttribute("title", my_lang('delete'));
					$delete_link->appendChild($delete);
					
				$div->appendChild($drag_link);
				$div->appendChild($span2);
				$div->appendChild($span3);
				$div->appendChild($delete_link);
				$div->appendChild($edit_link);
				
			$nodes[$id]->appendChild($div);
			
			$parent->appendChild($nodes[$id]);	//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);					//end ol
			}
		}
		
		$menu = $dom->saveHTML();
		return $menu;
	}
	
	/*
		後台商品menu
	*/
	public function backend_category_nav(){
		$CI =& get_instance();
		$Website = $CI->Website;
		
		$rows1 = $CI->category_query->select_all();
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement("ul");
		$parnode = $dom->appendChild($node);
		
		for($i=0; $i<sizeof($rows1); $i++){
			
			$id = $rows1[$i]['id'];
			$fid = $rows1[$i]['fid'];
			$layer = $rows1[$i]['layer'];
			$parent = $parnode;
			
			if($fid){
				if(!$node_ul[$fid]){
					$node_ul[$fid] = $dom->createElement("ul");									//start ul
					$node_ul[$fid]->setAttribute('class', 'classic');
				}
				$parent = $node_ul[$fid];
			}
			
			$nodes[$id] = $dom->createElement("li");											//start li
			if($layer==1){
				$nodes[$id]->setAttribute('class', 'classic');
			}
			
			$link = $dom->createElement("a", $rows1[$i]['name']);
			$link->setAttribute('href', 'admin/product/'.$rows1[$i]['id'].'/');
			
			if($rows1[$i]['id']==$rows1[$i+1]['fid']){
				$span = $dom->createElement('span');											//start span
				$span->setAttribute("class", 'arrow');
				$link->appendChild($span);		
				$link->setAttribute('href', 'javascript:;');									//end span
			}
			$nodes[$id]->appendChild($link);
			
			$parent->appendChild($nodes[$id]);													//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);												//end ul
			}
		}
		
		return $dom->saveHTML();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
		舊的
	*/
	/*
		後台Menu 與 判斷
	*/
	protected function _admin_srtucture(){
		$CI =& get_instance();
		$Website = $CI->config->item('Website');
		
		$uri = explode('/', $_SERVER['REQUEST_URI']);
		$action = array_pop($uri);
		if(!$action || substr($action, 0, 1)=='?'){
			$action = array_pop($uri);
		}
		
		if(substr($action, 0, 4)=='ajax' || substr($action, 0, 5)=='ajax_'){
			return false;
		}
		
		switch($action){
			default:
				if($_SERVER['QUERY_STRING']){
					$query_string = '?'.$_SERVER['QUERY_STRING'];
				}
				$_SESSION['current_location']['backend'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].$query_string;
				break;
			case 'login':
			case 'logout':
			case 'error':
			case 'language':
				return false;
				break;
		}
		
		if(!$_SESSION['user_info']['id']){
			$list_url = base_url().'admin/login/';
			redirect($list_url, 'refresh');
		}
		
		//get user info
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.active=1 and A.id=%s
						",
						$Website['table']['user'],
						$_SESSION['user_info']['id']
						);
		$rows1 = $CI->db->query($sql)->result_array();
		
		//get rights
		$sql = sprintf("SELECT B.structure_id, B.editable, C.list_link
						FROM %s A
						LEFT JOIN %s B ON A.group_id=B.group_id
						LEFT JOIN %s C ON B.structure_id=C.id
						WHERE A.user_id=%s and B.structure_id is not null
						",
						$Website['table']['user_group'],
						$Website['table']['group_structure'],
						$Website['table']['structure'],
						$_SESSION['user_info']['id']
						);
		$rows2 = $CI->db->query($sql)->result_array();
		
		for($i=0; $i<sizeof($rows2); $i++){
			if(!in_array($rows2[$i]['structure_id'], $rights)){
				$rights[] = $rows2[$i]['structure_id'];
				$list_link = explode('/',$rows2[$i]['list_link']);
				$rights_edit[$list_link[0]] = explode(',', $rows2[$i]['editable']);
			}else{
				//當有重複的權限，則merge在一起
				$e = explode(',', $rows2[$i]['editable']);
				$list_link = explode('/',$rows2[$i]['list_link']);
				$rights_edit[$list_link[0]] = array_merge((array)$rights_edit[$list_link[0]], (array)$e);
			}
		}
		$_SESSION['user_info']['rights_edit'] = $rights_edit;
		
		//非管理員導回登入頁
		if(!in_array(1, $rights)){
			$list_url = base_url().'admin/login/';
			header('Location: '.$list_url);
			exit;
		}
		
		//取出menu
		for($i=0; $i<sizeof($rights); $i++){
			$rights_array[] = 'A.id='.$rights[$i];
		}
		$rights_query = implode(' or ', $rights_array);
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.display=1 and (%s)
						ORDER BY A.sort asc
						",
						$Website['table']['structure'],
						$rights_query
						);
		$rows3 = $CI->db->query($sql)->result_array();
		$rows3 = $this->admin_srtucture_html($rows3);
		return $rows3;
	}
	
	/*
		後台左側Menu
	*/
	protected function _admin_srtucture_html($structure){
		//取得menu結構
		$menu = $this->admin_menu($structure);
		
		return $menu;
		
	}
	
	protected function _admin_menu($rows1){
		//query
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->config->item('Website');
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement("ul");
		$node->setAttribute("class", 'menu-items m-t-30');
		$parnode = $dom->appendChild($node);
		
		//echo '<pre>';print_r($rows1);exit;
		//echo '<pre>';print_r($_SERVER);exit;
		
		$full_url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$base_url = base_url();
		$path_url = substr($full_url, strlen($base_url));
		
		if(substr($path_url, 0, 1)!='/'){
			$path_url = '/'.$path_url;
		}
		if(substr($path_url, -1)!='/'){
			$path_url = $path_url.'/';
		}
		$pathinfo_array = explode('/', $path_url);
		//echo '<pre>';print_r($pathinfo_array);exit;
		
		$start_count = 0;
		for($i=0; $i<sizeof($rows1); $i++){
			
			$is_here = 0;
			if(!$access_success){
				//判斷是否有權限可以進入
				if(substr($rows1[$i]['list_link'], -1)=='/' && substr($rows1[$i]['list_link'], 0, -1)==$pathinfo_array[2]){
					$is_here = 1;
					$access_success = 1;
					$structure_id = $rows1[$i]['id'];
					$structure_fid = $rows1[$i]['fid'];
					if($structure_fid){
						$nodes[$structure_fid]->setAttribute("class", 'open active');
					}
				}
			}
			
			$id = $rows1[$i]['id'];
			$fid = $rows1[$i]['fid'];
			$parent = $parnode;
			
			if($fid){
				if(!$node_ol[$fid]){
					$node_ol[$fid] = $dom->createElement("ul");								//start ul
					$node_ol[$fid]->setAttribute("class", 'sub-menu');
				}
				$parent = $node_ol[$fid];
			}
			
			//li
			unset($class_name);
			$nodes[$id] = $dom->createElement("li");										//start li
			if($start_count==0){
				$class_name[] = 'm-t-30';
			}
			$start_count++;
			if($is_here){
				$class_name[] = 'active';
			}
			$nodes[$id]->setAttribute("class", implode(' ',$class_name));
			
			//li > a
			$link = $dom->createElement('a');												//start a
			
			if($rows1[$i]['list_link']!='/'){
				//不為首頁
				$link->setAttribute("href", 'admin/'.$rows1[$i]['list_link']);
			}else{
				$link->setAttribute("href", 'admin/');
			}
			
			if($rows1[$i]['list_link']=='javascript:;'){
				$link->setAttribute("href", $rows1[$i]['list_link']);
			}
			
			//li > a > span
			$span = $dom->createElement('span', my_lang('admin_menu.'.$rows1[$i]['name']));						//start span
			$span->setAttribute("class", 'title');
			$link->appendChild($span);
			
			//若有子項目
			if($rows1[$i+1]['fid']==$rows1[$i]['id']){
				$span = $dom->createElement('span');										//start span
				$span->setAttribute("class", 'arrow');
				$link->appendChild($span);
			}
			
			//li > span
			if(substr($rows1[$i]['icon'], 0, 3)=='fa-'){
				$span = $dom->createElement('span');										//start span
				$span->setAttribute("class", 'icon-thumbnail');
					$span_i = $dom->createElement('i');											//start span
					$span_i->setAttribute("class", 'fa '.$rows1[$i]['icon']);
				$span->appendChild($span_i);
			}else{
				$span = $dom->createElement('span', $rows1[$i]['icon']);						//start span
				$span->setAttribute("class", 'icon-thumbnail');
			}
			
			$nodes[$id]->appendChild($link);												
			$nodes[$id]->appendChild($span);												//end a
			
			$parent->appendChild($nodes[$id]);												//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);											//end ul
			}
			
		}
		
		if(!$access_success){
			//如果沒權限 則導回首頁
			header('Location: '.base_url().'admin/');exit;
		}
		
		$menu = $dom->saveHTML();
		
		//echo $menu;exit;
		$srtucture = array(
			'menu'			=>	$menu,
			'breadcrumbs'	=>	$this->breadcrumbs_list($structure_id, $Website['table']['structure']),
		);
		return $srtucture;
	}
	
	/*
		後台商品類別 樹狀圖拖曳排序
	*/
	protected function _nested_sortable($rows1){
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->Website;
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement("ol");
		$node->setAttribute("class", 'sortable');
		$parnode = $dom->appendChild($node);
		
		for($i=0; $i<sizeof($rows1); $i++){
			
			$id = $rows1[$i]['id'];
			$fid = $rows1[$i]['fid'];
			$parent = $parnode;
			
			if($fid){
				if(!$node_ol[$fid]){
					$node_ol[$fid] = $dom->createElement("ol");					//start ol
				}
				$parent = $node_ol[$fid];
			}
			$display_class = $rows1[$i]['display'] ? '':'alert alert-danger';
			
			$nodes[$id] = $dom->createElement("li");	//start li
			$nodes[$id]->setAttribute("id", 'list_'.$rows1[$i]['id']);
			$nodes[$id]->setAttribute("class", $display_class);
				
				$div = $dom->createElement('div');	//start div
					
						$span1 = $dom->createElement('span');	//start span
					$span2 = $dom->createElement('span');	//start span
					$span2->setAttribute("class", 'disclose');
					$span2->appendChild($span1);
					
					$span3 = $dom->createElement('span', $rows1[$i]['name']);	//start span
					$span3->setAttribute("class", 'text');
					
					
						$edit = $dom->createElement('i');
						$edit->setAttribute("class", 'fa fa-pencil');
					$edit_link = $dom->createElement('a');	//start span
					$edit_link->setAttribute("href", 'javascript:;');
					$edit_link->setAttribute("class", 'pull-right sortable_icon edit_link');
					$edit_link->setAttribute("title", my_lang('edit'));
					$edit_link->appendChild($edit);
					
						$drag = $dom->createElement('i');
						$drag->setAttribute("class", 'fa fa-sort');
					$drag_link = $dom->createElement('a');	//start span
					$drag_link->setAttribute("href", 'javascript:;');
					$drag_link->setAttribute("class", 'pull-left sortable_icon handler');
					$drag_link->setAttribute("title", my_lang('sort'));
					$drag_link->appendChild($drag);
					
						
						$delete = $dom->createElement('i');
						$delete->setAttribute("class", 'fa fa-trash');
					$delete_link = $dom->createElement('a');	//start span
					$delete_link->setAttribute("href", 'javascript:;');
					$delete_link->setAttribute("class", 'pull-right sortable_icon delete');
					$delete_link->setAttribute("title", my_lang('delete'));
					$delete_link->appendChild($delete);
					
				$div->appendChild($drag_link);
				$div->appendChild($span2);
				$div->appendChild($span3);
				$div->appendChild($delete_link);
				$div->appendChild($edit_link);
				
			$nodes[$id]->appendChild($div);
			
			$parent->appendChild($nodes[$id]);	//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);					//end ol
			}
		}
		
		$menu = $dom->saveHTML();
		return $menu;
	}
	
	/*
		後台商品管理，列表與內容 上方的Menu
	*/
	public function admin_product_menu_structure(){
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->config->item('Website');
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						ORDER BY A.sort asc
						",
						$Website['table']['category']
						);
		$rows1 = $CI->query->select($sql);
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement("ul");
		$node->setAttribute('class', 'nav navbar-nav');
		$parnode = $dom->appendChild($node);
		
		for($i=0; $i<sizeof($rows1); $i++){
			
			$id = $rows1[$i]['id'];
			$fid = $rows1[$i]['fid'];
			$layer = $rows1[$i]['layer'];
			$parent = $parnode;
			
			if($fid){
				if(!$node_ul[$fid]){
					$node_ul[$fid] = $dom->createElement("ul");									//start ol
					if($layer==2){
						$node_ul[$fid]->setAttribute('class', 'dropdown-menu multi-level');
					}else{
						$node_ul[$fid]->setAttribute('class', 'dropdown-menu');
					}
				}
				$parent = $node_ul[$fid];
			}
			
			$nodes[$id] = $dom->createElement("li");												//start li
			if($layer==1){
				$nodes[$id]->setAttribute('class', 'dropdown');
			}
				$link = $dom->createElement("a", $rows1[$i]['name']);
				$link->setAttribute('href', 'admin/product/index/'.$rows1[$i]['id'].'/');
				if($layer==1){
					$link->setAttribute('class', 'dropdown-toggle');
					$link->setAttribute('dropdown-toggle', 'dropdown');
				}
				
			if($rows1[$i]['id']==$rows1[$i+1]['fid']){
				
					$span = $dom->createElement('span');											//start span
					$span->setAttribute("class", 'arrow');
					$link->appendChild($span);													//end span
				if($layer==2){
					$nodes[$id]->setAttribute('class', 'dropdown-submenu');
				}elseif($layer>2){
					//$nodes[$id]->setAttribute('class', 'dropdown-submenu');
				}
			}
				
			
			$nodes[$id]->appendChild($link);
			
			$parent->appendChild($nodes[$id]);													//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);												//end ol
			}
		}
		$menu = $dom->saveHTML();
		return $menu;
	}
	
	/*
		後台商品管理 內容頁 關聯勾選
	*/
	public function product_type_checkbox($rows1, $root_checkbox=false){
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->Website;
		error_reporting(-1);
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement("ol");
		$node->setAttribute("class", '');
		$parnode = $dom->appendChild($node);
		//echo '<pre>';print_r($rows1);exit;
		for($i=0; $i<sizeof($rows1); $i++){
			
			$id = $rows1[$i]['id'];
			$fid = $rows1[$i]['fid'];
			$parent = $parnode;
			
			if($fid){
				if(!$node_ol[$fid]){
					$node_ol[$fid] = $dom->createElement("ol");						//start ol
				}
				$parent = $node_ol[$fid];
			}
			$display_class = $rows1[$i]['display'] ? '':'alert alert-danger';
			
			$nodes[$id] = $dom->createElement("li");									//start li
			$nodes[$id]->setAttribute("class", $display_class);
				
				$div = $dom->createElement('div');									//start div
					
					if($rows1[$i+1]['fid']!=$rows1[$i]['id'] or $root_checkbox){
						$div->setAttribute("class", 'checkbox check-primary');
						$checkbox = $dom->createElement('input');
						$checkbox->setAttribute("id", 'type_'.$rows1[$i]['id']);
						$checkbox->setAttribute("name", 'type[]');
						$checkbox->setAttribute("root", $root_checkbox);
						$checkbox->setAttribute("type", 'checkbox');
						$checkbox->setAttribute("value", $rows1[$i]['id']);
						if($rows1[$i]['product_id']){
							//如果有商品id 則要加入checked
							$checkbox->setAttribute("checked", 'checked');
						}
						$div->appendChild($checkbox);
					}
			
					$label = $dom->createElement('label', ' '.$rows1[$i]['name']);							//start label
					$label->setAttribute("for", 'type_'.$rows1[$i]['id']);
				
				$div->appendChild($label);
				
			$nodes[$id]->appendChild($div);
			
			$parent->appendChild($nodes[$id]);										//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);									//end ol
			}
			
		}
		
		$menu = $dom->saveHTML();
		return $menu;
	}
	
	/*
		後台商品管理 列表頁
	*/
	public function nested_child($id, $table, $path){
		//query
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->config->item('Website');
		
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						ORDER BY A.sort asc
						",
						$Website['table'][$table]
						);
		$rows1 = $CI->query->select($sql);
		$start = 0;
		
		for($i=0; $i<sizeof($rows1); $i++){
			if($rows1[$i]['id']==$id){
				$start=1;
				$fid = $rows1[$i]['fid'];
			}
			if($rows1[$i]['id']!=$id && $rows1[$i]['fid']==$fid){
				$start = 0;
			}
			if($start){
				$rows2[] = $rows1[$i]['id'];
			}
		}
		
		return $rows2;
		
	}
	
	/*
		後台Menu 確認是否可以進入與權限管理
	*/
	public function check_editable($settings){
		
		//確認是否有編輯權限
		$path_info = explode('/', $_SERVER['PATH_INFO']);
		for($i=sizeof($path_info); $i>0; $i--){
			if($path_info[$i] && 
				$path_info[$i]!='ajax' && 
				$path_info[$i]!='index' && 
				$path_info[$i]!='item' && 
				!is_numeric($path_info[$i])){
					
					$uri_text = $path_info[$i];
					break;
			}
			
			if($path_info[$i]=='item'){
				//在內頁時，確認是否可以編輯
				$in_item = 1;
			}
		}
		
		if(!in_array(2, $_SESSION['user_info']['rights_edit'][$uri_text])){
			//新增
			$settings['btn']['insert'] = 0;
		}
		if(!in_array(3, $_SESSION['user_info']['rights_edit'][$uri_text])){
			//修改
			$settings['btn']['display'] = 0;
			$settings['btn']['sort'] = 0;
			$settings['icon']['edit'] = 0;
			$settings['icon']['sort'] = 0;
			$settings['icon']['display'] = 0;
			
			if($in_item){
				redirect($settings['path']['list'], 'refresh');exit;
			}
		}
		if(!in_array(4, $_SESSION['user_info']['rights_edit'][$uri_text])){
			//刪除
			$settings['btn']['delete'] = 0;
			$settings['icon']['delete'] = 0;
			
		}
		
		$settings['datatable'] = array(
			'first'				=>	my_lang('datatable_first'),
			'last'				=>	my_lang('datatable_last'),
			'info'				=>	my_lang('datatable_info'),
			'info_filtered'		=>	my_lang('datatable_info_filtered'),
			'zero'				=>	my_lang('datatable_zero'),
			'search_placeholder'=>	my_lang('datatable_search_placeholder'),
		);
		
		return $settings;
	}
	
	/*
		取得並顯示麵包屑的路徑
	*/
	public function breadcrumbs_list($id, $table, $path){
		//query
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->config->item('Website');
		
		do{
			$sql = sprintf("SELECT A.*
							FROM %s A
							WHERE A.id=%s
							",
							$table,
							$id
							);
			$rows1 = $CI->query->select($sql, 1);
			
			$rows2[] = $rows1;
			$id = $rows1['fid'];
			
			
		}while($rows1['fid']);
		
		for($i=sizeof($rows2)-1; $i>=0; $i--){
			if($rows2[$i]['list_link']=='javascript:;'){
				$html .= '<li>'.$rows2[$i]['name'].'</li>';
			}else{
				$html .= '<li><a href="admin/'.$rows2[$i]['list_link'].'">'.$rows2[$i]['name'].'</a></li>';
			}
			
		}
		
		return $html;
	}
	

	
	
	//以下不確定會用到 甚至需要刪除
	/*
	public function product_structure($data, $template){
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->config->item('Website');
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.display=1
						ORDER BY A.sort asc
						",
						$Website['table']['product_type']
						);
		$rows1 = $CI->query->select($sql);
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement("ul");
		$node->setAttribute('class', 'nav navbar-nav');
		$parnode = $dom->appendChild($node);
		
		for($i=0; $i<sizeof($rows1); $i++){
			
			$id = $rows1[$i]['id'];
			$fid = $rows1[$i]['fid'];
			$parent = $parnode;
			
			if($fid){
				if(!$node_ul[$fid]){
					$node_ul[$fid] = $dom->createElement("ul");					//start ol
					$node_ul[$fid]->setAttribute('class', 'dropdown-menu multi-level');
				}
				$parent = $node_ul[$fid];
			}
			
			$nodes[$id] = $dom->createElement("li");	//start li
				$link = $dom->createElement("a", $rows1[$i]['name']);
				$link->setAttribute('href', 'product/index/'.$rows1[$i]['id'].'/');
				
			if($rows1[$i]['id']==$rows1[$i+1]['fid']){
				$nodes[$id]->setAttribute('class', 'dropdown-submenu');
			}
				
			$nodes[$id]->appendChild($link);
			
			$parent->appendChild($nodes[$id]);	//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);					//end ol
			}
		}
		$menu = $dom->saveHTML();
		
		return $menu;
	}
	//*/
	
	/*
	private function nested_menu($table, $path, $dom){
		
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->config->item('Website');
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						ORDER BY A.sort asc
						",
						$table
						);
		$rows1 = $CI->db->query($sql)->result_array();
		
		$node = $dom->createElement("ul");
		$node->setAttribute('class', 'dropdown-menu multi-level');
		$parnode = $dom->appendChild($node);
		
		for($i=0; $i<sizeof($rows1); $i++){
			
			$id = $rows1[$i]['id'];
			$fid = $rows1[$i]['fid'];
			$parent = $parnode;
			
			if($fid){
				if(!$node_ul[$fid]){
					$node_ul[$fid] = $dom->createElement("ul");					//start ol
					$node_ul[$fid]->setAttribute("class", 'dropdown-menu');
				}
				$parent = $node_ul[$fid];
			}
			
			$nodes[$id] = $dom->createElement("li");	//start li
				$link = $dom->createElement("a", $rows1[$i]['name']);
				$link->setAttribute('href', 'admin/'.$path.$rows1[$i]['id'].'/');
				
			$nodes[$id]->appendChild($link);
			if($rows1[$i+1]['fid']==$rows1[$i]['id']){
				//有子選單
				
				$nodes[$id]->setAttribute('class', 'dropdown-submenu');
				$link->setAttribute('class', 'dropdown-toggle');	//設定a的class
				$link->setAttribute('data-toggle', 'dropdown');		//設定a的toggle
			}
			
			$parent->appendChild($nodes[$id]);	//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);					//end ol
			}
		}
		
		return $node;
	}
	//*/
	
	/*
	public function nested_select_menu($table, $item_type){
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->Website;
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						ORDER BY A.sort asc
						",
						$Website['table'][$table[1]]
						);
		$rows1 = $CI->query->select($sql);
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement("ul");
		$parnode = $dom->appendChild($node);
		
		for($i=0; $i<sizeof($rows1); $i++){
			
			$id = $rows1[$i]['id'];
			$fid = $rows1[$i]['fid'];
			$parent = $parnode;
			
			if($fid){
				if(!$node_ul[$fid]){
					$node_ul[$fid] = $dom->createElement("ul");					//start ol
				}
				$parent = $node_ul[$fid];
			}
			
			$nodes[$id] = $dom->createElement("li");	//start li
				$link = $dom->createElement("a", $rows1[$i]['name']);
				$link->setAttribute('href', 'javascript:;');
				$link->setAttribute('value', $rows1[$i]['id']);
				
				$class = '';
				if($rows1[$i]['id']==$item_type){
					$class .= ' fg-menu-success';
				}
				if(!$rows1[$i]['display']){
					$class .= ' fg-menu-delete';
				}
				if($class){
					$link->setAttribute('class', $class);
				}
				
				
			$nodes[$id]->appendChild($link);
			
			
			$parent->appendChild($nodes[$id]);	//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);					//end ol
			}
		}
		$menu = $dom->saveHTML();
		return $menu;
	}
	//*/
	
	/*
	public function nested_select_menu_bootstrap($table, $item_type){
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->Website;
		
		$sql = sprintf("SELECT A.*
						FROM %s A
						ORDER BY A.sort asc
						",
						$Website['table'][$table[1]]
						);
		$rows1 = $CI->query->select($sql);
		
		$dom = new DOMDocument("1.0");
		$dom->formatOutput = true;
		$node = $dom->createElement("ul");
		$node->setAttribute('class', 'nav navbar-nav nav-selectable');
		
		
		$root = $dom->appendChild($node);
		$root_li = $dom->createElement("li");			//start li
		$root_li->setAttribute('class', 'dropdown');
			
		
			$root_link = $dom->createElement("a", '請選擇分類');
			$root_link->setAttribute('class', 'dropdown-toggle dropdown-main-text');
			$root_link->setAttribute('dropdown-toggle', 'dropdown-toggle');
			$root_link->setAttribute('href', 'javascript:;');
			$root_link->setAttribute('value', 0);
			
		$root_li->appendChild($root_link);		//end a
					
		$root->appendChild($root_li);		//end li
		
		$root_li_ul = $dom->createElement("ul");
		$root_li_ul->setAttribute('class', 'dropdown-menu multi-level');
		$root_li->appendChild($root_li_ul);		//end a
		
		
		for($i=0; $i<sizeof($rows1); $i++){
			$id = $rows1[$i]['id'];
			$fid = $rows1[$i]['fid'];
			$parent = $root_li_ul;
			
			
			if($fid){
				if(!$node_ul[$fid]){
					$node_ul[$fid] = $dom->createElement("ul");					//start ol
					$node_ul[$fid]->setAttribute('class', 'dropdown-menu multi-level');
				}
				$parent = $node_ul[$fid];
			}
			
			$nodes[$id] = $dom->createElement("li");	//start li
				$link = $dom->createElement("a", $rows1[$i]['name']);
				$link->setAttribute('href', 'javascript:;');
				
			if($rows1[$i]['id']==$rows1[$i+1]['fid']){
				//有子分類時
				$link->setAttribute('class', 'dropdown-toggle');
				$link->setAttribute('dropdown-toggle', 'dropdown-toggle');
				$nodes[$id]->setAttribute('class', 'dropdown-submenu');
			}else{
				//為leaf時
				
				$link->setAttribute('value', $rows1[$i]['id']);
				
				$class = ' item_selectable ';
				if($rows1[$i]['id']==$item_type){
					$class .= ' bg-success ';
					$root_link->nodeValue = $rows1[$i]['name'];
				}
				if(!$rows1[$i]['display']){
					$class .= ' bg-delete ';
				}
				if($class){
					$link->setAttribute('class', $class);
				}
				
				
				//$link->setAttribute('class', 'item_selectable');
			}
			
			
				
			
				
			
			$nodes[$id]->appendChild($link);
			
			$parent->appendChild($nodes[$id]);	//end li
			
			if($fid){
				$nodes[$fid]->appendChild($parent);					//end ol
			}
		}
		$menu = $dom->saveHTML();
		return $menu;
	}
	//*/
	
	/*
	public function breadcrumbs_item($rows1, $settings){
		$txt = $rows1['id'].'. '.$rows1['name'];
		if(!$rows1['name']){
			$txt = '新增';
		}
		return $settings['breadcrumbs_item'].'<li>'.$txt.'</li>';
	}
	//*/
	
}
?>