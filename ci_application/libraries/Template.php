<?
class Template{
	
	public function view($template, $data){
		$CI =& get_instance();
		$content = $CI->load->view($template, $data, true);
		return $content;
	}
	
	public function views($template, $data){
		$CI =& get_instance();
		/*
		$csrf =	array(
			'name' => $CI->security->get_csrf_token_name(),
			'hash' => $CI->security->get_csrf_hash()
		);
		$data['csrf'] = $csrf;
		*/
		$CI->load->view('main_template/main_header', $data);
		$CI->load->view($template, $data);
		$CI->load->view('main_template/main_footer', $data);
	}
	
	public function views_admin_login($template, $data){
		$CI =& get_instance();
		/*
		$csrf =	array(
			'name' => $CI->security->get_csrf_token_name(),
			'hash' => $CI->security->get_csrf_hash()
		);
		$data['csrf'] = $csrf;
		*/
		$CI->load->view('admin/'.$template, $data);
	}
	
	public function views_admin($template, $data, $echo=false){
		$CI =& get_instance();
		/*
		$csrf =	array(
			'name' => $CI->security->get_csrf_token_name(),
			'hash' => $CI->security->get_csrf_hash()
		);
		$data['csrf'] = $csrf;
		*/
		$data['content'] = $CI->load->view('admin/'.$template, $data, true);
		if(!$echo){
			$CI->load->view('admin/template', $data);	
		}else{
			echo $CI->load->view('admin/template', $data, true);
		}
	}
}
?>