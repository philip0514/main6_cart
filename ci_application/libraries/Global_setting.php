<?
class Global_setting{
	public function main(){
		$CI =& get_instance();
		$CI->load->model('query');
		$CI->load->helper('url');
		$Website = $CI->config->item('Website');
		
		//get user info
		$sql = sprintf("SELECT A.*
						FROM %s A
						WHERE A.id=1
						limit 1
						",
						$Website['table']['setting']
						);
		$rows1 = $CI->db->query($sql)->result_array();
		
		$rows2 = objectToArray(json_decode($rows1[0]['json']));
		
		$rows1[0]['logo'] = $rows2['logo'];
		$rows1[0]['logo2x'] = $rows2['logo2x'];
		$rows1[0]['icon16'] = $rows2['icon16'];
		$rows1[0]['icon57'] = $rows2['icon57'];
		$rows1[0]['icon72'] = $rows2['icon72'];
		$rows1[0]['icon114'] = $rows2['icon114'];
		$rows1[0]['icon144'] = $rows2['icon144'];
		
		return $rows1[0];
	}
}
?>