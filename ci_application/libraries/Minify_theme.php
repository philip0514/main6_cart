<?php
/**
 * Minify Library Class
 *
 * PHP Version 5.3
 *
 * @category  PHP
 * @package   Library
 * @author    Slawomir Jasinski <slav123@gmail.com>
 * @copyright 2015 All Rights Reserved SpiderSoft
 * @license   Copyright 2015 All Rights Reserved SpiderSoft
 * @link      Location: http://github.com/slav123/CodeIgniter-Minify
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * the Minify LibraryClass
 *
 * @category  PHP
 * @package   Controller
 * @author    Slawomir Jasinski <slav123@gmail.com>
 * @copyright 2016 All Rights Reserved SpiderSoft
 * @license   Copyright 2015 All Rights Reserved SpiderSoft
 * @link      http://www.spidersoft.com.au
 */
class Minify_theme
{
	/**
	 * CodeIgniter global.
	 *
	 * @var object
	 */
	protected $ci;

	/**
	 * Css files array.
	 *
	 * @var array
	 */
	protected $css_array = array();

	/**
	 * Js files array.
	 *
	 * @var array
	 */
	protected $js_array = array();

	/**
	 * Assets dir.
	 *
	 * @var string
	 */
	public $assets_dir = 'assets';

	/**
	 * Assets dir for css (optional).
	 *
	 * @var string
	 */
	public $assets_dir_css = '';

	/**
	 * Assets dir for js (optional).
	 *
	 * @var string
	 */
	public $assets_dir_js = '';

	/**
	 * Css dir.
	 *
	 * @var string
	 */
	public $css_dir = 'assets/css';

	/**
	 * Js dir.
	 *
	 * @var string
	 */
	public $js_dir = 'assets/js';

	/**
	 * Output css file name.
	 *
	 * @var string
	 */
	public $css_file = 'styles.css';

	/**
	 * Output js file name.
	 *
	 * @var string
	 */
	public $js_file = 'scripts.js';

	/**
	 * Automatic file names.
	 *
	 * @var bool
	 */
	public $auto_names = FALSE;

	/**
	 * Compress files or not.
	 *
	 * @var bool
	 */
	public $compress = TRUE;

	/**
	 * Compression engines.
	 *
	 * @var array
	 */
	public $compression_engine = array('css' => 'minify', 'js' => 'closurecompiler');

	/**
	 * Closurecompiler settings.
	 *
	 * @var array
	 */
	public $closurecompiler = array('compilation_level' => 'SIMPLE_OPTIMIZATIONS');

	/**
	 * Css file name with path.
	 *
	 * @var string
	 */
	private $_css_file = '';

	/**
	 * Js file name with path.
	 *
	 * @var string
	 */
	private $_js_file = '';

	/**
	 * Last modification.
	 *
	 * @var array
	 */
	private $_lmod = array('css' => 0, 'js' => 0);
	
	
	
	
	
	
	
	/**
	 * library settings
	 */
	public $library_dir = 'library';
	public $library_css_file = 'library.styles.css';
	public $library_js_file = 'library.scripts.js';
	
	public $default_dir = 'theme/default/';
	public $default_css_file = 'default.styles.css';
	public $default_js_file = 'default.scripts.js';
	
	
	

	/**
	 * Constructor
	 *
	 * @param array $config Config array
	 */
	public function __construct($config = array())
	{
		$this->ci = get_instance();
		$this->ci->load->config('minify_theme', TRUE, TRUE);
		$Website = $this->ci->Website;

		// user specified settings from config file
		$this->theme_main_dir     = $this->ci->config->item('theme_main_dir', 'minify_theme') ?: $this->theme_main_dir;
		$this->theme_dir		  = $this->theme_main_dir.'/'.$Website['global_setting']['theme'];
		$this->assets_dir         = $this->ci->config->item('assets_dir', 'minify_theme') ?: $this->assets_dir;
		$this->assets_dir_css     = $this->ci->config->item('assets_dir_css', 'minify_theme') ?: $this->assets_dir_css;
		$this->assets_dir_js      = $this->ci->config->item('assets_dir_js', 'minify_theme') ?: $this->assets_dir_js;
		$this->css_dir            = $this->ci->config->item('css_dir', 'minify_theme') ?: $this->css_dir;
		$this->js_dir             = $this->ci->config->item('js_dir', 'minify_theme') ?: $this->js_dir;
		$this->css_file           = $this->ci->config->item('css_file', 'minify_theme') ?: $this->css_file;
		$this->asset_css_file	  = $this->css_dir.'/'.($this->ci->config->item('css_file', 'minify_theme') ?: $this->css_file);
		$this->js_file            = $this->ci->config->item('js_file', 'minify_theme') ?: $this->js_file;
		$this->asset_js_file	  = $this->js_dir.'/'.($this->ci->config->item('js_file', 'minify_theme') ?: $this->js_file);
		$this->auto_names         = $this->ci->config->item('auto_names', 'minify_theme') ?: $this->auto_names;
		$this->compress           = $this->ci->config->item('compress', 'minify_theme') ?: $this->compress;
		$this->compression_engine = $this->ci->config->item('compression_engine', 'minify_theme') ?: $this->compression_engine;
		$this->closurecompiler    = $this->ci->config->item('closurecompiler', 'minify_theme') ?: $this->closurecompiler;
		
		// save default names for later use/reset
		$this->css_file_default   = $this->css_file;
		$this->js_file_default    = $this->js_file;
		
		if (count($config) > 0)
		{
			// custom config array
			foreach ($config as $key => $val)
			{
				if (isset($this->$key))
				{
					$this->$key = $val;
				}
			}
		}
		
		// perform checks
		$this->_config_checks();
		
		log_message('debug', "Minify Class Initialized");
	}

	//--------------------------------------------------------------------

	/**
	 * Declare css files list
	 *
	 * @param mixed $css   File or files names
	 * @param bool  $group Set group for files
	 *
	 * @return void
	 */
	public function css($css, $group = 'theme')
	{
		if (is_array($css))
		{
			$this->css_array[$group] = $css;
		}
		else 
		{
			$this->css_array[$group] = array_map('trim', explode(',', $css));
		}

		return $this;
	}

	/**
	 * Declare js files list
	 *
	 * @param mixed $js    File or files names
	 * @param bool  $group Set group for files
	 *
	 * @return void
	 */
	public function js($js, $group = 'theme')
	{
		if (is_array($js))
		{
			$this->js_array[$group] = $js;
		}
		else 
		{
			$this->js_array[$group] = array_map('trim', explode(',', $js));
		}

		return $this;
	}

	//--------------------------------------------------------------------

	/**
	 * Declare css files list
	 *
	 * @param mixed $css   File or files names
	 * @param bool  $group Set group for files
	 *
	 * @return void
	 */
	public function add_css($css, $group = 'theme')
	{
		if ( ! isset($this->css_array[$group]))
		{
			$this->css_array[$group] = array();
		}

		if (is_array($css))
		{
			$this->css_array[$group] = array_unique(array_merge($this->css_array[$group], $css));
		}
		else 
		{
			$this->css_array[$group] = array_unique(array_merge($this->css_array[$group], array_map('trim', explode(',', $css))));
		}

		return $this;
	}

	//--------------------------------------------------------------------

	/**
	 * Declare js files list
	 *
	 * @param mixed $js    File or files names
	 * @param bool  $group Set group for files
	 *
	 * @return void
	 */
	public function add_js($js, $group = 'theme')
	{
		if ( ! isset($this->js_array[$group]))
		{
			$this->js_array[$group] = array();
		}

		if (is_array($js))
		{
			$this->js_array[$group] = array_unique(array_merge($this->js_array[$group], $js));
		}
		else 
		{
			$this->js_array[$group] = array_unique(array_merge($this->js_array[$group], array_map('trim', explode(',', $js))));
		}

		return $this;
	}

	//--------------------------------------------------------------------

	/**
	 * Deploy and minify CSS
	 *
	 * @param bool $force     Force to rewrite file
	 * @param null $file_name File name to create
	 * @param null $group     Group name
	 *
	 * @return string
	 */
	public function deploy_css($force = TRUE, $file_name = NULL, $group = NULL)
	{
		$return = '';

		if (is_null($file_name))
		{
			$file_name = $this->css_file_default;
		}

		if (is_null($group))
		{
			foreach ($this->css_array as $group_name => $group_array)
			{
				$return .= $this->_deploy_css($force, $file_name, $group_name) . PHP_EOL;
			}
		}
		else
		{
			$return .= $this->_deploy_css($force, $file_name, $group);
		}

		return $return;
	}

	//--------------------------------------------------------------------

	/**
	 * Deploy and minify js
	 *
	 * @param bool $force     Force rewriting js file
	 * @param null $file_name File name
	 * @param null $group     Group name
	 *
	 * @return string
	 */
	public function deploy_js($force = FALSE, $file_name = NULL, $group = NULL)
	{
		$return = '';

		if (is_null($file_name))
		{
			$file_name = $this->js_file_default;
		}

		if (is_null($group))
		{
			foreach ($this->js_array as $group_name => $group_array)
			{
				$return .= $this->_deploy_js($force, $file_name, $group_name) . PHP_EOL;
			}
		}
		else
		{
			$return .= $this->_deploy_js($force, $file_name, $group);
		}

		return $return;
	}

	//--------------------------------------------------------------------

	/**
	 * Build and minify CSS
	 *
	 * @param bool $force     Force to rewrite file
	 * @param null $file_name File name to create
	 * @param null $group     Group name
	 *
	 * @return string
	 */
	private function _deploy_css($force = TRUE, $file_name = NULL, $group = NULL)
	{
		if ($this->auto_names OR $file_name === 'auto')
		{
			$file_name = md5(serialize($this->css_array[$group])) . '.css';
		}
		else
		{
			$file_name = ($group === 'theme') ? $file_name : $group . '_' . $file_name;
		}

		$this->_set('css_file', $file_name);
		
		$this->_scan_files('css', $force, $group);

		return '<link href="' . base_url($this->asset_css_file) . '" rel="stylesheet" type="text/css" />';
	}

	//--------------------------------------------------------------------

	/**
	 * Build and minify js
	 *
	 * @param bool $force     Force rewriting js file
	 * @param null $file_name File name
	 * @param null $group     Group name
	 *
	 * @return string
	 */
	private function _deploy_js($force = FALSE, $file_name = NULL, $group = NULL)
	{
		if ($this->auto_names OR $file_name === 'auto')
		{
			$file_name = md5(serialize($this->js_array[$group])) . '.js';
		}
		else
		{
			$file_name = ($group === 'theme') ? $file_name : $group . '_' . $file_name;
		}

		$this->_set('js_file', $file_name);
		
		$this->_scan_files('js', $force, $group);
		
		switch($group){
			case 'default':
				$js_file = base_url($this->js_dir.'/'.$this->default_js_file);
				break;
			case 'library':
				$js_file = base_url($this->js_dir.'/'.$this->library_js_file);
				break;
			default:
			case 'theme':
				$js_file = base_url($this->asset_js_file);
				break;
		}
		
		
		return '<script type="text/javascript" src="' . $js_file . '"></script>';
	}

	//--------------------------------------------------------------------

	/**
	 * construct js_file and css_file
	 *
	 * @param string $name  File type
	 * @param string $value File name
	 *
	 * @return void
	 */
	private function _set($name, $value)
	{
		switch ($name)
		{
			case 'js_file':

				if ($this->compress)
				{
					if ( ! preg_match("/\.min\.js$/", $value)) 
					{
						$value = str_replace('.js', '.min.js', $value);
					}

					$this->js_file = $value;
				}

				// determine if we have special dir for js specified
				$assets_dir = empty($this->assets_dir_js) ? $this->assets_dir : $this->assets_dir_js;
				$this->_js_file = $this->theme_dir.'/'.$assets_dir . '/' . $value;

				if ( ! file_exists($this->_js_file) && ! touch($this->_js_file))
				{
					throw new Exception('Can not create file ' . $this->_js_file);
				}
				else
				{
					chmod($this->_js_file, 0777);
					$this->_lmod['js'] = filemtime($this->_js_file);
				}

				break;
			case 'css_file':

				if ($this->compress)
				{
					if ( ! preg_match("/\.min\.css$/", $value)) 
					{
						$value = str_replace('.css', '.min.css', $value);
					}

					$this->css_file = $value;
				}

				// determine if we have special dir for css specified
				$assets_dir = empty($this->assets_dir_css) ? $this->assets_dir : $this->assets_dir_css;
				$this->_css_file = $this->theme_dir.'/'.$assets_dir . '/' . $value;
				
				if ( ! file_exists($this->_css_file) && ! touch($this->_css_file))
				{
					throw new Exception('Can not create file ' . $this->_css_file);
				}
				else
				{
					chmod($this->_css_file, 0777);
					$this->_lmod['css'] = filemtime($this->_css_file);
				}

				break;
		}
	}


	/**
	 * scan CSS directory and look for changes
	 *
	 * @param string $type  Type (css | js)
	 * @param bool   $force Rewrite no mather what
	 * @param string $group Group name
	 */
	private function _scan_files($type, $force, $group)
	{
		switch ($type)
		{
			case 'css':
				$files_array = $this->css_array[$group];
				$directory   = $this->theme_dir.'/'.$this->css_dir;
				$out_file    = $this->theme_dir.'/'.$this->css_dir.'/'.$this->css_file;
				break;
			case 'js':
				$files_array = $this->js_array[$group];
				
				switch($group){
					case 'default':
						$directory   = $this->default_dir.'/';
						$out_file    = $this->theme_dir.'/'.$this->js_dir.'/'.$this->default_js_file;
						break;
					case 'library':
						$directory   = $this->library_dir.'/';
						$out_file    = $this->theme_dir.'/'.$this->js_dir.'/'.$this->library_js_file;
						break;
					default:
					case 'theme':
						$directory   = $this->theme_dir.'/'.$this->js_dir;
						$out_file    = $this->theme_dir.'/'.$this->js_dir.'/'.$this->js_file;
						//echo $directory;
						break;
				}
				break;
		}
		
		// if multiple files
		if (is_array($files_array))
		{
			$compile = FALSE;
			foreach ($files_array as $file)
			{
				$filename = $directory . '/' . $file;
				
				if (file_exists($filename))
				{
					if (filemtime($filename) > $this->_lmod[$type])
					{
						$compile = TRUE;
					}
				}
				else
				{
					throw new Exception('File ' . $filename . ' is missing');
				}
			}

			// check if this is init build
			if ((file_exists($out_file) && filesize($out_file) === 0) || !file_exists($out_file))
			{
				$force = TRUE;
			}
			
			if ($compile OR $force)
			{
				$this->_concat_files($files_array, $directory, $out_file);
			}
		}
	}

	//--------------------------------------------------------------------

	/**
	 * add merge files
	 *
	 * @param string $file_array Input file array
	 * @param string $directory  Directory
	 * @param string $out_file   Output file
	 *
	 * @return void
	 */
	private function _concat_files($file_array, $directory, $out_file)
	{
		
		//print_r($file_array);echo '<br>';
		//print_r($directory);echo '<br>';
		//print_r($out_file);echo '<br>';
		if ($fh = fopen($out_file, 'w'))
		{
			foreach ($file_array as $file_name)
			{
				$file_name = $directory . '/' . $file_name;
				$handle    = fopen($file_name, 'r');
				$contents  = fread($handle, filesize($file_name));
				fclose($handle);

				// if this is javascript file, check if we have ; at the end
				if (preg_match("/.js$/i", $out_file)) {
					if (substr(rtrim($contents), -1) !== ';') {
						$contents .= ';';
					}
				}
				fwrite($fh, $contents);
			}
			fclose($fh);
		}
		else
		{
			throw new Exception('Can\'t write to ' . $out_file);
		}

		if ($this->compress)
		{
			// read output file contest (already concated)
			$handle   = fopen($out_file, 'r');
			$contents = fread($handle, filesize($out_file));
			fclose($handle);

			// recreate file
			$handle = fopen($out_file, 'w');

			if (preg_match("/.css$/i", $out_file))
			{
				$engine = '_' . $this->compression_engine['css'];
			}

			if (preg_match("/.js$/i", $out_file))
			{
				$engine = '_' . $this->compression_engine['js'];
			}
			
			// call function name to compress file
			fwrite($handle, call_user_func(array($this, $engine), $contents));
			fclose($handle);
		}
	}

	//--------------------------------------------------------------------

	/**
	 * Compress javascript using closure compiler service
	 *
	 * @param string $data Source to compress
	 *
	 * @return mixed
	 */
	private function _closurecompiler($data)
	{
		$config = $this->closurecompiler;

		$ch = curl_init('https://closure-compiler.appspot.com/compile');

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'output_info=compiled_code&output_format=text&compilation_level=' . $config['compilation_level'] . '&js_code=' . urlencode($data));
		$output = curl_exec($ch);
		curl_close($ch);

		return $output;
	}

	//--------------------------------------------------------------------

	/**
	 * Implements jsmin as alternative to closure compiler
	 *
	 * @param string $data Source to compress
	 *
	 * @return string
	 */
	private function _jsmin($data)
	{
		require_once(APPPATH . 'libraries/minify/JSMin.php');

		return JSMin::minify($data);
	}

	//--------------------------------------------------------------------

	/**
	 * Implements jsminplus as alternative to closure compiler
	 *
	 * @param string $data Source to compress
	 *
	 * @return string
	 */
	private function _jsminplus($data)
	{
		require_once(APPPATH . 'libraries/minify/JSMinPlus.php');

		return JSMinPlus::minify($data);
	}

	//--------------------------------------------------------------------

	/**
	 * Implements cssmin compression engine
	 *
	 * @param string $data Source to compress
	 *
	 * @return string
	 */
	private function _cssmin($data)
	{
		require_once(APPPATH . 'libraries/minify/cssmin-v3.0.1.php');

		return CssMin::minify($data);
	}

	//--------------------------------------------------------------------

	/**
	 * Implements cssminify compression engine
	 *
	 * @param string $data Source to compress
	 *
	 * @return string
	 */
	private function _minify($data) 
	{
		require_once(APPPATH . 'libraries/minify/cssminify.php');
		$cssminify = new cssminify();
		
		return $cssminify->compress($data);
	}

	//--------------------------------------------------------------------

	/**
	 * Perform config checks
	 *
	 * @return void
	 */
	private function _config_checks()
	{	
		
		
		chmod($this->theme_dir.'/'.$this->assets_dir, 0755);
		chmod($this->theme_dir.'/'.$this->assets_dir_js, 0755);
		chmod($this->theme_dir.'/'.$this->assets_dir_css, 0755);
		chmod($this->theme_dir.'/'.$this->css_dir, 0755);
		chmod($this->theme_dir.'/'.$this->js_dir, 0755);
		
		if ((empty($this->assets_dir_css) OR empty($this->assets_dir_js)) && ! is_writable($this->theme_dir.'/'.$this->assets_dir))
		{
			throw new Exception('Assets directory ' . $this->theme_dir.'/'.$this->assets_dir . ' is not writable');
		}
		
		if ( ! empty($this->assets_dir_css) && ! is_writable($this->theme_dir.'/'.$this->assets_dir_css))
		{
			//echo $this->theme_dir.'/'.$this->assets_dir_css;
		//echo 52;exit;
			throw new Exception('Assets directory for css ' . $this->theme_dir.'/'.$this->assets_dir_css . ' is not writable');
		}

		if ( ! empty($this->assets_dir_js) && ! is_writable($this->theme_dir.'/'.$this->assets_dir_js))
		{
			throw new Exception('Assets directory for js ' . $this->theme_dir.'/'.$this->assets_dir_js . ' is not writable');
		}
		
		if (empty($this->css_dir) && ! is_writable($this->theme_dir.'/'.$this->css_dir))
		{
			throw new Exception('Assets directory ' . $this->theme_dir.'/'.$this->css_dir . ' is not writable');
		}
		
		if (empty($this->js_dir) && ! is_writable($this->theme_dir.'/'.$this->js_dir))
		{
			throw new Exception('Assets directory ' . $this->theme_dir.'/'.$this->js_dir . ' is not writable');
		}

		if (empty($this->css_dir))
		{
			throw new Exception('CSS directory must be set');
		}

		if (empty($this->js_dir))
		{
			throw new Exception('JS directory must be set');
		}

		if ( ! $this->auto_names)
		{
			if (empty($this->css_file))
			{
				throw new Exception('CSS file name can\'t be empty');
			}

			if (empty($this->js_file))
			{
				throw new Exception('JS file name can\'t be empty');
			}
		}
		
		if ($this->compress)
		{
			if ( ! isset($this->compression_engine['css']) OR empty($this->compression_engine['css']))
			{
				throw new Exception('Compression engine for CSS is required');
			}

			if ( ! isset($this->compression_engine['js']) OR empty($this->compression_engine['js']))
			{
				throw new Exception('Compression engine for JS is required');
			}

			if ($this->compression_engine['js'] === 'closurecompiler' && ( ! isset($this->closurecompiler['compilation_level']) OR empty($this->closurecompiler['compilation_level'])))
			{
				throw new Exception('Compilation level for closurecompiler is needed');
			}
		}
		
	}
}
/* End of file Minify.php */
/* Location: ./libraries/Minify.php */