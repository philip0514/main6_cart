<?
include('Scripts/functions.php');

class Preload{
	public function admin(){
		$CI =& get_instance();
		$CI->load->model(array(	
			'category_query',
			'coupon_query',
			'collection_query',
			'global_query',
			'inventory_query',
			'media_query',
			'member_query',
			'menu_query',
			'product_query',
			'order_query',
			'spec_query',
			'user_query',
			'user_group_query',
		));
		
		$CI->load->library(array(
			'csv',
			'datatable', 
			'form_validation', 
			'global_setting',
			'ion_auth', 
			'jquery_validation',
			'mailer',
			//'minify',
			'menu',
			'multiple_upload',
			'template', 
		));
		
		$CI->load->helper(array(
			'language',
			'url', 
		));
		
		$CI->Website = $CI->config->item('Website');
		
		$language = $_SESSION['user_info']['site_lang'] ? $_SESSION['user_info']['site_lang'] : $CI->Website['language'][0]['language'];
		$CI->lang->load('global_lang', $language);
		$CI->lang->load('admin_lang', $language);
		$CI->lang->load('auth', $language);
		$CI->lang->load('admin', $language);
		
		$CI->form_validation->set_error_delimiters(
			$CI->config->item('error_start_delimiter', 'ion_auth'), 
			$CI->config->item('error_end_delimiter', 'ion_auth')
		);
		
		$structure_html = $CI->menu->backend_auth();
		
		$CI->Website['admin_menu'] 			= $structure_html['menu'];
		$CI->Website['breadcrumbs'] 		= $structure_html['breadcrumbs'];
		$CI->Website['global_setting'] 		= $CI->global_setting->main();
		$CI->Website['admin_title'] 		= $CI->Website['global_setting']['title'].my_lang('backend_title');
	}
	
	public function main(){
		$CI =& get_instance();
		
		$CI->load->model(array(	
			'coupon_query',
			'category_query',
			'global_query', 
			'media_query',
			'member_query',
			'product_query',
			'theme_query',
		));
		
		$CI->load->helper(array(
			'language',
			'url', 
		));
		
		$CI->load->library(array(
			'global_setting',
		));
		
		$CI->Website = $CI->config->item('Website');
		$CI->Website['global_setting'] 			= $CI->global_setting->main();
		$CI->Website['global_setting']['theme']	= $CI->theme_query->get_active();
		
		$CI->load->library(array(
			'cart',
			'datatable',
			'form_validation',  
			'ion_auth', 
			'jquery_validation',
			'mailer',
			'menu',
			'minify_theme',
			'multiple_upload',
			'pagination',
			'template', 
			'theme',
		));
	}
	
	public function check_domain(){
		$reffer_array = explode('/', $_SERVER['HTTP_REFERER']);
		if($reffer_array[2]!=$_SERVER['HTTP_HOST']){
			echo 'not the same domain error';exit;
		}
	}
	
	public function current_location(){
		//紀錄目前頁面
		if($_SERVER['QUERY_STRING']){
			$query_string = '?'.$_SERVER['QUERY_STRING'];
		}
		$_SESSION['current_location']['frontend'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].$query_string;
	}
	
	public function cookie_login(){
		$CI =& get_instance();
		$CI->member_query->cookie_login();
	}
	
	public function browser_update(){
		$CI =& get_instance();
		
		
		$data = array(
			'Website'	=>	$Website,
			'component'	=>	'page',
		);
		$CI->template->views($data, 'welcome/browser_update');
		
        echo $CI->output->get_output();
        exit;
		
	}
	
	public function memory($exit=0){
		
		if($_GET['memory']){
			echo $this->convert_size(memory_get_usage(true));
			
			if(!$exit){
				echo '<br>';
			}else{
				exit;
			}
		}
	}
	
	private function convert_size($size){
		$unit=array('b','kb','mb','gb','tb','pb');
    	return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
	}
	
	public function ip(){
		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		
		return $ip;
	}
	
}
?>