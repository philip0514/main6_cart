<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation{

	protected $CI;
	
    function __construct($rules = array()){
        parent::__construct($rules);
    }
	
	public function url_name($str){
		$CI = $this->CI =& get_instance(); // Get your CodeIgniter instance
		/*
			
			if (($str = strtotime($str)) === FALSE) { // Basic timestamp check
				// Set error message by calling the method through the CI instance.
				// Obviously must be done BEFORE returning any value
				$this->CI->form_validation->set_message('valid_date', '{field} must be a valid date.');
				return FALSE;
			} else {
				return TRUE;
			}
		
		*/
		return true;
	}
	
	/**
	 * Get the error message for the rule
	 *
	 * @param 	string $rule 	The rule name
	 * @param 	string $field	The field name
	 * @return 	string
	 *//*
	protected function _get_error_message($rule, $field)
	{$this->gem($rule, $field);exit;
		// check if a custom message is defined through validation config row.
		if (isset($this->_field_data[$field]['errors'][$rule]))
		{
			return $this->_field_data[$field]['errors'][$rule];
		}
		// check if a custom message has been set using the set_message() function
		elseif (isset($this->_error_messages[$rule]))
		{
			return $this->_error_messages[$rule];
		}
		elseif (FALSE !== ($line = $this->CI->lang->line('form_validation_'.$rule)))
		{
			return $line;
		}
		// DEPRECATED support for non-prefixed keys, lang file again
		elseif (FALSE !== ($line = $this->CI->lang->line($rule, FALSE)))
		{
			return $line;
		}

		return $this->CI->lang->line('form_validation_error_message_not_set').'('.$rule.')';
	}
	*/
	protected function _get_error_message($rule, $field)
	{//$this->gem($rule, $field);exit;
		// check if a custom message is defined through validation config row.
		if (isset($this->_field_data[$field]['errors'][$rule]))
		{
			$error_msg = $this->_field_data[$field]['errors'][$rule];
			
			if(sscanf($error_msg, 'my_lang:%s', $line) === 1){
				//$label = str_replace('my_lang:', '', $error_msg);
				$label = sscanf($error_msg, 'my_lang:%s');
				return my_lang($label[0]);
			}elseif (sscanf($error_msg, 'lang:%s', $line) === 1 && FALSE === ($error_msg = $this->CI->lang->line($line, FALSE))){
				return $line;
			}
			
			return $error_msg;
		}
		// check if a custom message has been set using the set_message() function
		elseif (isset($this->_error_messages[$rule]))
		{
			return $this->_error_messages[$rule];
		}
		elseif (FALSE !== ($line = $this->CI->lang->line('form_validation_'.$rule)))
		{
			return $line;
		}
		// DEPRECATED support for non-prefixed keys, lang file again
		elseif (FALSE !== ($line = $this->CI->lang->line($rule, FALSE)))
		{
			return $line;
		}

		return $this->CI->lang->line('form_validation_error_message_not_set').'('.$rule.')';
	}
	
	private function gem($rule, $field){
		if (isset($this->_field_data[$field]['errors'][$rule]))
		{
			$error_msg = $this->_field_data[$field]['errors'][$rule];
			
			if(sscanf($error_msg, 'my_lang:%s', $line) === 1){
				//$label = str_replace('my_lang:', '', $error_msg);
				echo $error_msg;
				$label = sscanf($error_msg, 'my_lang:%s');echo $label[0];
				echo  my_lang($label[0]);exit;
			}elseif (sscanf($error_msg, 'lang:%s', $line) === 1 && FALSE === ($error_msg = $this->CI->lang->line($line, FALSE))){
				return $line;
			}
			
			return $error_msg;
		}
		// check if a custom message has been set using the set_message() function
		elseif (isset($this->_error_messages[$rule]))
		{echo 2;
			return $this->_error_messages[$rule];
		}
		elseif (FALSE !== ($line = $this->CI->lang->line('form_validation_'.$rule)))
		{echo 3;
			return $line;
		}
		// DEPRECATED support for non-prefixed keys, lang file again
		elseif (FALSE !== ($line = $this->CI->lang->line($rule, FALSE)))
		{echo 24;
			return $line;
		}
		echo 5;exit;
		//return $this->CI->lang->line('form_validation_error_message_not_set').'('.$rule.')';
	}
	
	/**
	 * Translate a field name
	 *
	 * @param	string	the field name
	 * @return	string
	 */
	protected function _translate_fieldname($fieldname)
	{//$this->tf($fieldname);exit;
		if(sscanf($fieldname, 'my_lang:%s', $line) === 1){
			$label = str_replace('my_lang:', '', $fieldname);
			return my_lang($label);
		}elseif (sscanf($fieldname, 'lang:%s', $line) === 1 && FALSE === ($fieldname = $this->CI->lang->line($line, FALSE))){
			return $line;
		}
		
		return $fieldname;
	}
	
	private function tf($fieldname){
		if(sscanf($fieldname, 'my_lang:%s', $line) === 1){
			$label = str_replace('my_lang:', '', $fieldname);
			return my_lang($label);
		}elseif (sscanf($fieldname, 'lang:%s', $line) === 1 && FALSE === ($fieldname = $this->CI->lang->line($line, FALSE))){
			return $line;
		}
		
		return $fieldname;
	}
}