<div class="row media_selectable">
	<? 
	for($i=0; $i<sizeof($rows1); $i++){
		$selected = '';
		if(in_array($rows1[$i]['id'], $value)){
			$selected = 'ui-selected';
		}
	?>
	<div class="col-xl-1 col-lg-2 col-md-3 col-sm-6 col-xs-6 col-padding file_single <?=$selected?>">
		<a href="javascript:;" class="file_link" title="<?=$rows1[$i]['orig_name']?>" 
			data-isimage="<?=(int)$rows1[$i]['is_image']?>" 
			data-link="<?=$Website['media']['upload_folder'].(!$file_original?$Website['media']['thumb_folder']:'').$rows1[$i]['name']?>"
			data-value="<?=$rows1[$i]['id']?>"
			data-title="<?=$rows1[$i]['title']?>"
			>
		<? if($rows1[$i]['is_image']){ ?>
			<div class="media_preview">
				<img src="<?=$Website['media']['upload_folder'].$Website['media']['thumb_folder'].$rows1[$i]['name']?>" style="width:100%;" class="img-rounded" />
			</div>
		<? }else { ?>
			<div class="is_file">
				<div class="file_icon">
					<span class="fa fa-file" aria-hidden="true"></span>
				</div>
			</div>
		<? }?>
		</a>
		<div class="file_name"><?=$rows1[$i]['orig_name']?></div>
	</div>
	<? }?>
	<a id="next" class="btn btn-block btn-primary p-l-0 p-r-0" href="javascript:scroll_inf(<?=$next?>);" data-value="<?=$next?>" style="display:none;">Next</a>
</div>