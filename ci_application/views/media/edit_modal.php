<link href="library/jQuery/cropper/css/cropper.min.css" rel="stylesheet">
<link href="library/jQuery/cropper/css/main.css" rel="stylesheet">
<div class="modal fade modal_fullscreen" id="edit_modal">
	<div class="modal-dialog">
		<div class="modal-content" id="crop-avatar">
          <form class="avatar-form" action="media/crop/<?=$id?>" enctype="multipart/form-data" method="post" onSubmit="return false;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					媒體修改
				</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row avatar-btns">
						<div class="col-md-8">
							
	
						</div>	
						
						<div class="col-md-4">
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-8">
							<div class="img-container">
								<img src="<?=$rows1['file_path'].$rows1['name']?>">
							</div>
						</div>
						<div class="col-md-4">
						
							
							<div class="form-group">
								<label class="control-label" for="title">* 標題</label>
								<div>
									<input type="text" class="form-control required" id="title" name="title" placeholder="標題 必填" value="<?=$rows1['title']?>">
									<span class="help-inline help-block"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label" for="content">說明</label>
								<div>
									<input type="text" class="form-control" id="content" name="content" placeholder="說明" value="<?=$rows1['content']?>">
									<span class="help-inline help-block"></span>
								</div>
							</div>
							
							<input class="avatar-src form-control" name="avatar_src" type="hidden" value="<?=$rows1['file_path'].$rows1['name']?>">
							<input class="avatar-name form-control" name="avatar_name" type="hidden" value="<?=$rows1['name']?>">
							<input class="avatar-path form-control" name="avatar_path" type="hidden" value="<?=$rows1['file_path']?>">
							<input class="avatar-data form-control" name="avatar_data" type="hidden">
							<input class="avatar-type form-control" name="avatar_type" type="hidden" value="<?=$type?>">
						
							<div class="docs-preview clearfix">
								<div class="img-preview preview-lg"></div>
								<div class="img-preview preview-md"></div>
								<div class="img-preview preview-sm"></div>
								<div class="img-preview preview-xs"></div>
							</div>
						</div>
					</div>
					
					
				</div>
  
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">關閉</button>
				<button type="submit" class="btn btn-success avatar-save">裁切並儲存</button>
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>
<script src="library/jQuery/cropper/js/cropper.min.js"></script>
<script src="library/jQuery/cropper/js/class_crop_ajax.js"></script>
<script>
$image = $(".img-container > img");
$(function(){
	$('#edit_modal').on('shown.bs.modal', function (e) {
		var height = $("#edit_modal .modal-body").height();
		$('.img-container').css('max-height', height-80);
		var crop_box_data = <?=$rows2['box_data']?>;
		var canvas_data = <?=$rows2['canvas_data']?>;
		
		$image.cropper({
   			autoCropArea: 1,
			preview: ".img-preview",
			responsive: true,
			//aspectRatio: 1 / 1,
			aspectRatio: <?=$rows2['aspectRatio']?>,
			minCropBoxWidth: <?=$rows2['min_box_width']?>,
			minCropBoxHeight: <?=$rows2['min_box_height']?>,
			crop: function(data) {
				var json = [
                  '{"x":' + data.x,
                  '"y":' + data.y,
                  '"height":' + data.height,
                  '"width":' + data.width,
                  '"rotate":' + data.rotate + '}'
                ].join();
				$('.avatar-data').val(json);
			},
			built: function () {
				<? if($rows2['crop_data']){?>
				$image.cropper('setData', <?=htmlspecialchars_decode($rows2['crop_data'])?>);
				<? }?>
			}
		});
	});
	
})
</script>
