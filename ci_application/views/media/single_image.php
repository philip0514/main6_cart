<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-xs-6 media_single padding_v_10" data-value="<?=$rows1['id']?>">
	<div class="media_preview">
		<img src="<?=$rows1['file_url']?>" alt="<?=$rows1['name']?>" class="img-rounded" />
	</div>
	<div class="media_btn_group btn-group" role="group">
		<button type="button" class="btn btn-danger media_delete">
			<i class="fa fa-remove"></i>
			<?=my_lang('delete')?>
		</button>
	</div>
</div>