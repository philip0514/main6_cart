<div class="modal fade modal_fullscreen stick-up" id="media_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="padding: 17px;">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">媒體 
				<? if($selectable_limit){?>
					<span style="font-size:14px; color:#666666;"> 最多可選 <span style="font-size:18px; color:#000; font-weight:bold;"><?=$selectable_limit?></span> 個項目</span>
				<? }?>
				</h4>
			</div>
			<div class="modal-body" style="padding: 0; overflow: hidden; background: white;">
				<div class="media_list_container">
					
					<form id="fileupload" class="fileupload p-l-10 p-r-10" action="media/do_upload" method="POST" enctype="multipart/form-data">
						<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
						<div class="row fileupload-buttonbar">
							<!-- The global progress state -->
							
							<div class="col-lg-12 p-l-0">
								<!-- The global file processing state -->
								<span class="fileupload-process"></span>
								<!-- The fileinput-button span is used to style the file input field as button -->
								<a class="btn btn-primary btn-lg btn-block fileinput-button" href="javascript:;">
									<i class="fa fa-plus"></i>
									<span>新增檔案</span>
									<input type="file" id="userfile" name="userfile" style="height:50px;" multiple>
								</a>
							</div>
							
							<div class="col-lg-12 p-l-0 fileupload-progress fade">
								<!-- The global progress bar -->
								<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
									<div class="progress-bar progress-bar-success" style="width:0%;"></div>
								</div>

								<!-- The extended global progress state -->
								<div class="progress-extended alert-info">&nbsp;</div>
							</div>
						</div>
					</form>

					<ul class="nav nav-tabs p-l-10 p-r-10" role="tablist">
						<li role="presentation" class="active">
							<a href="#media_mine" role="tab" id="media_mine_tab" data-toggle="tab" aria-controls="media_mine" aria-expanded="false">我上傳的圖片</a>
						</li>
						<li role="presentation" class="">
							<a href="#media" role="tab" id="media_tab" data-toggle="tab" aria-controls="media" aria-expanded="true">媒體庫</a>
						</li>
						<li role="presentation" class="">
							<a href="#media_search" role="tab" id="media_search_tab" data-toggle="tab" aria-controls="media_search" aria-expanded="false">搜尋</a>
						</li>
						<li role="presentation" class="">
							<a href="#media_selected" role="tab" id="media_selected_tab" data-toggle="tab" aria-controls="media_selected" aria-expanded="false">已選擇</a>
						</li>
					</ul>
					
					<div class="tab-content p-l-0 p-r-0" style="position: inherit; height: 100vh;">
						<!--
							我上傳的圖片
						-->
						<div role="tabpanel" class="tab-pane fade active in" id="media_mine" aria-labelledby="media_mine_tab">
							<!-- The file upload form used as target for the file upload widget -->
							<div class="container-fluid file_area p-l-10 p-r-10">
								<div class="row media_selectable m-l-0 m-r-0">
									<? for($i=0; $i<sizeof($rows3); $i++){
										$selected = '';
										if(in_array($rows3[$i]['id'], $input_field)){
											$selected = 'ui-selected';
										}
									?>
									<div class="col-xl-1 col-lg-2 col-md-3 col-sm-6 col-xs-6 col-padding file_single <?=$selected?>">
										<a href="javascript:;" class="file_link" title="<?=$rows3[$i]['orig_name']?>" 
											data-isimage="<?=(int)$rows3[$i]['is_image']?>" 
											data-link="<?=$Website['media']['upload_folder'].(!$file_original?$Website['media']['thumb_folder']:'').$rows3[$i]['name']?>"
											data-value="<?=$rows3[$i]['id']?>"
											data-title="<?=$rows3[$i]['title']?>"
											>
											<? if($rows3[$i]['is_image']){ ?>
												<div class="media_preview">
													<img src="<?=$Website['media']['upload_folder'].$Website['media']['thumb_folder'].$rows3[$i]['name']?>" class="img-rounded" />
												</div>
											<? }else { ?>
											<div class="is_file">
												<div class="file_icon">
													<span class="fa fa-file" aria-hidden="true"></span>
												</div>
											</div>
											<? }?>
										</a>
										<div class="file_name"><?=$rows3[$i]['orig_name']?></div>
									</div>
									<? }?>
								
									<div class="col-sm-12 page_next p-l-0 p-r-0" style="padding-top:10px;">
										<a id="next" href="javascript:;" class="btn btn-primary btn-block">Next</a>
									</div>
								</div>
							</div>
						</div>
						<!--
							媒體庫 全部的圖片
						-->
						<div role="tabpanel" class="tab-pane fade" id="media" aria-labelledby="media_tab">
							<div class="container-fluid file_area p-l-10 p-r-10">
								<div class="row media_selectable m-l-0 m-r-0">
									<? for($i=0; $i<sizeof($rows1); $i++){
										$selected = '';
										if(in_array($rows1[$i]['id'], $input_field)){
											$selected = 'ui-selected';
										}
									?>
									<div class="col-xl-1 col-lg-2 col-md-3 col-sm-6 col-xs-6 col-padding file_single <?=$selected?>">
										<a href="javascript:;" class="file_link" title="<?=$rows1[$i]['orig_name']?>" 
											data-isimage="<?=(int)$rows1[$i]['is_image']?>" 
											data-link="<?=$Website['media']['upload_folder'].(!$file_original?$Website['media']['thumb_folder']:'').$rows1[$i]['name']?>"
											data-value="<?=$rows1[$i]['id']?>"
											data-title="<?=$rows1[$i]['title']?>"
											>
											<? if($rows1[$i]['is_image']){ ?>
												<div class="media_preview">
													<img src="<?=$Website['media']['upload_folder'].$Website['media']['thumb_folder'].$rows1[$i]['name']?>" style="width:100%;" class="img-rounded" />
												</div>
											<? }else { ?>
											<div class="is_file">
												<div class="file_icon">
													<span class="fa fa-file" aria-hidden="true"></span>
												</div>
											</div>
											<? }?>
										</a>
										<div class="file_name"><?=$rows1[$i]['orig_name']?></div>
									</div>
									<? }?>
								
									<div class="col-sm-12 page_next p-l-0 p-r-0" style="padding-top:10px;">
										<a id="next" href="javascript:;" class="btn btn-primary btn-block">Next</a>
									</div>
								</div>
							</div>
						</div>
						<!--
							搜尋圖片
						-->
						<div role="tabpanel" class="tab-pane fade" id="media_search" aria-labelledby="media_search_tab">
							<div class="container-fluid file_area p-l-10 p-r-10">
								<div class="row m-l-0 m-r-0">
									<div class="input-group">
										<input type="text" class="form-control" id="input_search" placeholder="輸入圖片名稱">
										<span class="input-group-btn">
											<button class="btn btn-primary" type="button" id="btn-search">送出</button>
										</span>
									</div>
								</div>
								
								<div class="row media_search m-l-0 m-r-0">
									
								</div>
							</div>
						</div>
						<!--
							已選擇
						-->
						<div role="tabpanel" class="tab-pane fade" id="media_selected" aria-labelledby="media_selected_tab">
							<div class="container-fluid file_area p-l-10 p-r-10">
								<div class="row media_selectable m-l-0 m-r-0">
									<? for($i=0; $i<sizeof($rows2); $i++){
										$selected = 'ui-selected';
									?>
									<div class="col-xl-1 col-lg-2 col-md-3 col-sm-6 col-xs-6 col-padding file_single <?=$selected?>">
										<a href="javascript:;" class="file_link" title="<?=$rows2[$i]['orig_name']?>" 
											data-isimage="<?=(int)$rows2[$i]['is_image']?>" 
											data-link="<?=$Website['media']['upload_folder'].(!$file_original?$Website['media']['thumb_folder']:'').$rows2[$i]['name']?>"
											data-value="<?=$rows2[$i]['id']?>"
											data-title="<?=$rows2[$i]['title']?>"
										>
											<? if($rows2[$i]['is_image']){ ?>
												<div class="media_preview">
													<img src="<?=$Website['media']['upload_folder'].$Website['media']['thumb_folder'].$rows2[$i]['name']?>" style="width:100%;" class="img-rounded" />
												</div>
											<? }else { ?>
												<div class="is_file">
													<div class="file_icon">
														<span class="fa fa-file" aria-hidden="true"></span>
													</div>
												</div>
											<? }?>
										</a>
										<div class="file_name"><?=$rows2[$i]['orig_name']?></div>
									</div>
									<? }?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="btn-group">
					<button type="button" class="btn btn-cons btn-large btn-default" data-dismiss="modal"><i class="fa fa-times"></i> 取消</button>
					<button type="button" class="btn btn-cons btn-large btn-primary btn-save"><i class="fa fa-upload"></i> 送出</button>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>

<!-- The template to display files available for upload -->
<script id="template_upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	{% if(!file.error){ %}
	<div class="col-xl-1 col-lg-2 col-md-3 col-sm-6 col-xs-6 col-padding template-upload fade">
		<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
			<div class="progress-bar progress-bar-success" style="width:0%;"></div>
		</div>
	</div>
	{% } %}
{% } %}
</script>

<!-- The template to display files available for download -->
<script id="template_download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	{% if(!file.error){ %}
	<div class="col-xl-1 col-lg-2 col-md-3 col-sm-6 col-xs-6 col-padding file_single">
		<a href="javascript:;" class="file_link" id="file_{%=file.id%}" title="{%=file.orig_name%}"
			data-isimage="{%=file.is_image%}"
			data-link="upload/media/<?=(!$file_original?$Website['media']['thumb_folder']:'')?>{%=file.name%}"
			data-value="{%=file.id%}"
			>	
			{% if(file.is_image){ %}
				<div class=" media_preview">
					<img src="{%=file.thumbnailUrl%}" style="width:100%;" class="img-rounded">
				</div>
			{% }else { %}
				<div class="file_icon">
					<span class="fa fa-file" aria-hidden="true"></span>
				</div>
			{% }%}
		</a>
		<div class="file_name">{%=file.orig_name%}</div>
	</div>
	{% }%}
{% } %}
</script>
<!---->