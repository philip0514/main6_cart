<script>
$(function(){
	validate_form('setting');
	media_ogimage(3);
	
	$('.logo_manager').media_multiple({
		area: 					'.logo_area',
		input_field: 			'.logo_input',
		selectable_limit:		1,
		selectable_multiple:	0,
		full_column:			true
	});
	
	$('.logo2x_manager').media_multiple({
		area: 					'.logo2x_area',
		input_field: 			'.logo2x_input',
		selectable_limit:		1,
		selectable_multiple:	0,
		full_column:			true
	});
	
	$('.icon16_manager').media_multiple({
		area: 					'.icon16_area',
		input_field: 			'.icon16_input',
		selectable_limit:		1,
		selectable_multiple:	0,
		full_column:			true
	});
	
	$('.icon57_manager').media_multiple({
		area: 					'.icon57_area',
		input_field: 			'.icon57_input',
		selectable_limit:		1,
		selectable_multiple:	0,
		full_column:			true
	});
	
	$('.icon72_manager').media_multiple({
		area: 					'.icon72_area',
		input_field: 			'.icon72_input',
		selectable_limit:		1,
		selectable_multiple:	0,
		full_column:			true
	});
	
	$('.icon114_manager').media_multiple({
		area: 					'.icon114_area',
		input_field: 			'.icon114_input',
		selectable_limit:		1,
		selectable_multiple:	0,
		full_column:			true
	});
	
	$('.icon144_manager').media_multiple({
		area: 					'.icon144_area',
		input_field: 			'.icon144_input',
		selectable_limit:		1,
		selectable_multiple:	0,
		full_column:			true
	});
});
</script>
<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('setting')?></div>
							</div>
							<div class="panel-body">
								<div class="form-group form-input required">
									<label class="form-label" for="title"><?=my_lang('admin_setting.title')?></label>
									<span class="help help-inline"><?=my_lang('admin_setting.title_help')?></span>
									<div>
										<input type="text" class="form-control required" id="title" name="title" placeholder="<?=my_lang('admin_setting.title_placeholder')?>" value="<?=$rows1['title']?>">
									</div>
								</div>
								
								<div class="form-group form-input required">
									<label class="form-label" for="description"><?=my_lang('admin_setting.description')?></label>
									<span class="help help-inline"><?=my_lang('admin_setting.description_help')?></span>
									<div>
										<input type="text" class="form-control required" id="description" name="description" placeholder="<?=my_lang('admin_setting.description_placeholder')?>" value="<?=$rows1['description']?>">
									</div>
								</div>

								<div class="form-group form-input required">
									<label class="form-label" for="keyword"><?=my_lang('admin_setting.keyword')?></label>
									<span class="help help-inline"><?=my_lang('admin_setting.keyword_help')?></span>
									<div>
										<input type="text" class="form-control required" id="keyword" name="keyword" placeholder="<?=my_lang('admin_setting.keyword_placeholder')?>" value="<?=$rows1['keyword']?>">
									</div>
								</div>
								
								<div class="form-group">
									<div>
										<a href="javascript:;" class="btn btn-primary ogimage_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> <?=my_lang('ogimage')?></a>
										<div class="alert alert-info"><?=my_lang('ogimage_help')?></div>
										<div class="row ogimage_area"><?=$rows1['ogimage_area']?></div>
										<input id="ogimage_input" name="ogimage_input" class="ogimage_input" type="hidden" value="<?=$rows1['ogimage_input']?>" />
									</div>
								</div>
								
								
								
								
								
								
								
								
								
								
								<div class="row">
									<div class="col-md-4">
										<div>
											<a href="javascript:;" class="btn btn-primary btn-block logo_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> Logo</a>
											<div class="row logo_area"><?=$rows1['logo_area']?></div>
											<input id="logo_input" name="logo_input" class="logo_input" type="hidden" value="<?=$rows1['logo_input']?>" />
										</div>
									</div>
									<div class="col-md-4">
										<div>
											<a href="javascript:;" class="btn btn-primary btn-block logo2x_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> Logo 2X</a>
											<div class="row logo2x_area"><?=$rows1['logo2x_area']?></div>
											<input id="logo2x_input" name="logo2x_input" class="logo2x_input" type="hidden" value="<?=$rows1['logo2x_input']?>" />
										</div>
									</div>
									<div class="col-md-4">
										<div>
											<a href="javascript:;" class="btn btn-primary btn-block icon16_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> Favicon (16x16)</a>
											<div class="row icon16_area"><?=$rows1['icon16_area']?></div>
											<input id="icon16_input" name="icon16_input" class="icon16_input" type="hidden" value="<?=$rows1['icon16_input']?>" />
										</div>
									</div>
								</div>
									
								<div class="row">
									<div class="col-md-3">
										<div>
											<a href="javascript:;" class="btn btn-primary btn-block icon57_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> Apple iPhone Icon (57x57)</a>
											<div class="row icon57_area"><?=$rows1['icon57_area']?></div>
											<input id="icon57_input" name="icon57_input" class="icon57_input" type="hidden" value="<?=$rows1['icon57_input']?>" />
										</div>
									</div>
									<div class="col-md-3">
										<div>
											<a href="javascript:;" class="btn btn-primary btn-block icon114_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> Apple iPhone Retina Icon (114x114)</a>
											<div class="row icon114_area"><?=$rows1['icon114_area']?></div>
											<input id="icon114_input" name="icon114_input" class="icon114_input" type="hidden" value="<?=$rows1['icon114_input']?>" />
										</div>
									</div>
									<div class="col-md-3">
										<div>
											<a href="javascript:;" class="btn btn-primary btn-block icon72_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> Apple iPad Icon (72x72)</a>
											<div class="row icon72_area"><?=$rows1['icon72_area']?></div>
											<input id="icon72_input" name="icon72_input" class="icon72_input" type="hidden" value="<?=$rows1['icon72_input']?>" />
										</div>
									</div>
									<div class="col-md-3">
										<div>
											<a href="javascript:;" class="btn btn-primary btn-block icon144_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> Apple iPad Retina Icon (144x144)</a>
											<div class="row icon144_area"><?=$rows1['icon144_area']?></div>
											<input id="icon144_input" name="icon144_input" class="icon144_input" type="hidden" value="<?=$rows1['icon144_input']?>" />
										</div>
									</div>
								</div>
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('admin_setting.facebook')?></div>
							</div>
							<div class="panel-body">
								<div class="form-group form-input">
									<label class="form-label" for="facebook_login"><?=my_lang('admin_setting.facebook_login')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="checkbox" id="facebook_login" name="facebook_login" value="1" data-init-plugin="switchery" data-size="small" data-color="primary" <?=$Website['checked'][$rows1['facebook_login']]?>/>
									</div>
								</div>
								
								<div class="form-group form-input">
									<label class="form-label" for="facebook_scope"><?=my_lang('admin_setting.facebook_scope')?></label>
									<span class="help help-inline"><?=my_lang('admin_setting.facebook_scope_help')?></span>
									<div>
										<input type="text" class="form-control" id="facebook_scope" name="facebook_scope" placeholder="<?=my_lang('admin_setting.facebook_scope_placeholder')?>" value="<?=$rows1['facebook_scope']?>">
									</div>
								</div>

								<div class="form-group form-input">
									<label class="form-label" for="facebook_appid"><?=my_lang('admin_setting.facebook_appid')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="facebook_appid" name="facebook_appid" placeholder="<?=my_lang('admin_setting.facebook_appid_placeholder')?>" value="<?=$rows1['facebook_appid']?>">
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('admin_setting.google')?></div>
							</div>
							<div class="panel-body">
								<div class="form-group form-input">
									<label class="form-label" for="ga"><?=my_lang('admin_setting.ga_code')?></label>
									<span class="help help-inline"></span>
									<div>
										<textarea class="form-control" id="ga" name="ga" placeholder="<?=my_lang('admin_setting.ga_code_placeholder')?>" style="height: 200px;"><?=$rows1['ga']?></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('admin_setting.page_js')?></div>
							</div>
							<div class="panel-body">
								<div class="form-group form-input">
									<label class="form-label" for="page_js"><?=my_lang('admin_setting.page_js')?></label>
									<span class="help help-inline"></span>
									<div>
										<textarea class="form-control" id="page_js" name="page_js" placeholder="<?=my_lang('admin_setting.page_js_placeholder')?>" style="height: 200px;"><?=$rows1['page_js']?></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('admin_setting.mail_setting')?></div>
							</div>
							<div class="panel-body">
								
								<div class="form-group form-input">
									<label class="form-label"><?=my_lang('admin_setting.mail_function')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="checkbox" id="mail_on" name="mail_on" value="1" data-init-plugin="switchery" data-size="small" data-color="primary" <?=$Website['checked'][$rows1['mail_on']]?>/>
									</div>
								</div>
								
								<div class="form-group form-input">
									<label class="form-label" for="mail_type"><?=my_lang('admin_setting.mail_type')?></label>
									<span class="help help-inline"></span>
									<div>
										<div class="radio radio-primary">
											<input type="radio" class="mail_type" name="mail_type" id="mail_type0" value="0" <?=(!$rows1['mail_type'])?'checked':''?> />
											<label for="mail_type0"><?=my_lang('admin_setting.mail_type0')?></label>
											<input type="radio" class="mail_type" name="mail_type" id="mail_type1" value="1" <?=($rows1['mail_type']==1)?'checked':''?> />
											<label for="mail_type1"><?=my_lang('admin_setting.mail_type1')?></label>
										</div>
									</div>
								</div>
								
								<div class="form-group form-input">
									<label class="form-label" for="mail_host"><?=my_lang('admin_setting.mail_host')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="mail_host" name="mail_host" placeholder="<?=my_lang('admin_setting.mail_host_placeholder')?>" value="<?=$rows1['mail_host']?>">
									</div>
								</div>
								
								<div class="form-group form-input">
									<label class="form-label" for="mail_port"><?=my_lang('admin_setting.mail_port')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="mail_port" name="mail_port" placeholder="<?=my_lang('admin_setting.mail_port_placeholder')?>" value="<?=$rows1['mail_port']?>">
									</div>
								</div>
								
								<div class="form-group form-input">
									<label class="form-label" for="mail_username"><?=my_lang('admin_setting.mail_username')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="mail_username" name="mail_username" placeholder="<?=my_lang('admin_setting.mail_username_placeholder')?>" value="<?=$rows1['mail_username']?>">
									</div>
								</div>
								
								<div class="form-group form-input">
									<label class="form-label" for="mail_password"><?=my_lang('admin_setting.mail_password')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="password" class="form-control" id="mail_password" name="mail_password" placeholder="<?=my_lang('admin_setting.mail_password_placeholder')?>" value="">
									</div>
								</div>

								<div class="form-group form-input">
									<label class="form-label" for="mail_secure"><?=my_lang('admin_setting.mail_secure')?></label>
									<span class="help help-inline"></span>
									<div>
										<div class="radio radio-primary">
											<input type="radio" class="mail_secure" name="mail_secure" id="mail_secure0" value="0" <?=(!$rows1['mail_secure'])?'checked':''?> />
											<label for="mail_secure0">TLS</label>
											<input type="radio" class="mail_secure" name="mail_secure" id="mail_secure1" value="1" <?=($rows1['mail_secure']==1)?'checked':''?> />
											<label for="mail_secure1">SSL</label>
										</div>
									</div>
								</div>

								<div class="form-group form-input">
									<label class="form-label" for="mail_charset"><?=my_lang('admin_setting.mail_charset')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="mail_charset" name="mail_charset" placeholder="<?=my_lang('admin_setting.mail_charset_placeholder')?>" value="<?=$rows1['mail_charset']?>">
									</div>
								</div>

								<div class="form-group form-input">
									<label class="form-label" for="mail_encoding"><?=my_lang('admin_setting.mail_encoding')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="mail_encoding" name="mail_encoding" placeholder="<?=my_lang('admin_setting.mail_encoding_placeholder')?>" value="<?=$rows1['mail_encoding']?>">
									</div>
								</div>

								<div class="form-group form-input">
									<label class="form-label" for="mail_from"><?=my_lang('admin_setting.mail_from')?></label>
									<span class="help help-inline">必填</span>
									<div>
										<input type="text" class="form-control" id="mail_from" name="mail_from" placeholder="<?=my_lang('admin_setting.mail_from_placeholder')?>" value="<?=$rows1['mail_from']?>">
									</div>
								</div>

								<div class="form-group form-input">
									<label class="form-label" for="mail_fromname"><?=my_lang('admin_setting.mail_fromname')?></label>
									<span class="help help-inline">必填</span>
									<div>
										<input type="text" class="form-control" id="mail_fromname" name="mail_fromname" placeholder="<?=my_lang('admin_setting.mail_fromname_placeholder')?>" value="<?=$rows1['mail_fromname']?>">
									</div>
								</div>
								
								<div>
									<a href="javascript:;" class="btn btn-block btn-primary btn-mailtest"><?=my_lang('admin_setting.mail_test')?></a>	
								</div>
								
								<div class="mailtest_result"></div>
								
							</div>
						</div>
					</div>
					
				</div>
			</div>
			<div id="toolbar"></div>
			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-ok"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
									<!--
									<input type="button" class="btn btn-info btn-lg save_publish" value="儲存並發佈">
									<input type="button" class="btn btn-success btn-lg save_publish_back" value="儲存發佈回列表">
									-->
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>