<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron lg vertical center bg-warning column-w4 vertical-bottom no-margin" data-pages="parallax">
			<div class="market-hero">
				<div class="container-fluid container container-fixed-lg sm-p-l-20 sm-p-r-20 full-height">
					<div class="inner full-height" style="transform: translateY(50.83px); opacity: 1;">
						<div class="container-xs-height full-height">
							<div class="col-xs-height col-middle  ">
								<p class="font-montserrat bold hint-text"><?=$Website['admin_title']?></p>
								<div class="m-b-50">
									<h2>Hi, <span class="bold">&nbsp;<?=$_SESSION['user_info']['username']?></span> </h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>