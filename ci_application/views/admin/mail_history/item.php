<script>
$(function(){
	$('.btn-resend').click(function(){
		var id = $(this).data('value');
		$.ajax({
			url: 'admin/mail_history/resend/'+id+'/',	//檔案位置
			type: 'POST',	//or POST
			data: {},
			error: function(xhr) {
				alert('Ajax request 發生錯誤');
			},
			success: function(response) {
				alert(response);
			}
		});

		return false;
	})
})
</script>
<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('setting')?></div>
							</div>
							<div class="panel-body">
						
								<div class="form-group">
									<div>
										<a class="btn btn-primary btn-resend" href="javascript:;" data-value="<?=$rows1['id']?>">重新寄信</a>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="name"><?=my_lang('admin_mail_history.name')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=$rows1['name']?>
									</div>
								</div>
								
								
						
								<div class="form-group">
									<label class="form-label" for=""><?=my_lang('admin_mail_history.send_to')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=$rows1['send_to'].' - '.$rows1['member_name']?>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for=""><?=my_lang('admin_mail_history.send_time')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=date('Y/m/d H:i:s',$rows1['send_time'])?>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for=""><?=my_lang('admin_mail_history.status')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=$rows1['status']?my_lang('status_success'):my_lang('status_failed')?>
									</div>
								</div>

								<? if(!$rows1['status']){?>
								<div class="form-group">
									<label class="form-label" for=""><?=my_lang('admin_mail_history.error')?></label>
									<span class="help help-inline"></span>
									<div>
										<div class="alert alert-danger">
										<?=$rows1['status_info']?>
										</div>
									</div>
								</div>
								<? }?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="form-group">
									<label class="form-label" for="content"><?=my_lang('content')?></label>
									<span class="help help-inline"></span>
									<?=htmlspecialchars_decode($rows1['content'])?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>