<script>
$(function(){
	validate_form('admin_about');
	tinymce.init(tinymce_config('#content'));
});
</script>

<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('setting')?></div>
							</div>
							<div class="panel-body">
							
								<div class="form-group">
									<label class="form-label"><?=my_lang('display')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="checkbox" id="display" name="display" value="1" data-init-plugin="switchery" data-size="small" data-color="primary" <?=$Website['checked'][$rows1['display']]?>/>
									</div>
								</div>

								<div class="form-group required">
									<label class="form-label" for="name"><?=my_lang('name')?></label>
									<span class="help help-inline"><?=my_lang('name_help')?></span>
									<input type="text" class="form-control required" id="name" name="name" placeholder="<?=my_lang('name_placeholder')?>" value="<?=$rows1['name']?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="form-group">
									<label class="form-label" for="content"><?=my_lang('content')?></label>
									<span class="help help-inline"></span>
									<textarea class="form-control" id="content" name="content"><?=$rows1['content']?></textarea>
								</div>

								<div id="toolbar"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-check"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="panel-title">代碼說明</div>
						</div>
						<div class="panel-body">
							<div class="alert alert-info">
								如需新增代碼，或是使用代碼上有任何疑問，請洽詢工程師，因為不是每一封信件，都可以使用全部的代碼。
							</div>
							<table class="table table-hover table-striped">
								<tr>
									<th width="300">代碼</th>
									<th>說明</th>
								</tr>
								<? for($i=0; $i<sizeof($Website['mail_code']); $i++){?>
								<tr>
									<td><?=$Website['mail_code'][$i]['code']?></td>
									<td><?=$Website['mail_code'][$i]['text']?></td>
								</tr>
								<? }?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>