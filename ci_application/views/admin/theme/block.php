<div class="col-md-4 theme-single <?=$rows1['active']?'is_active':''?>">
	<div class="theme-screenshot">
		<img src="ci_theme/themes/<?=$rows1['folder']?>/screenshot.png" alt="">
	</div>
	<h2 class="theme-name">
		<?=$rows1['name']?> <i class="fa fa-check-circle primary icon-active" aria-hidden="true"></i>
	</h2>
	<div class="theme-actions">
		<a href="javascript:;" class="btn btn-default btn-active" data-id="<?=$rows1['id']?>">啟動</a>
		<!--
		<a class="btn btn-primary btn-custom" href="javascript:;" data-id="<?=$rows1['id']?>">自訂</a>
		-->
	</div>
</div>