<style>
	.theme-screenshot img{
		width: 100%;
	}
	.theme-actions{
	    transition: opacity .1s ease-in-out;
		position: absolute;
		bottom: 0;
		right: 0;
		height: 57px;
		padding: 10px 10px 0;
	}
	.fa.primary{
		color: #6f5499;
	}
	.icon-active{
		display: none;
	}
	.is_active .icon-active{
		display: inline-block;
	}
	.is_active .btn-active{
		display: none;
	}
</style>
<script>
// Variable to store your files
var files;
	
$(function(){
	$('.btn-upload').click(function(){
		$('#upload').trigger('click');
	})
	
	// Add events
	$('input[type=file]').on('change', prepareUpload);
	
	$('#form1').on('submit', uploadFiles);
	
	theme_active();
})

function theme_active(){
	$('.btn-active').unbind('click');
	$('.btn-active').click(function(){
		$('.theme-single').removeClass('is_active');
		
		var $this = $(this);
		var id = $this.data('id');
		
		$.ajax({
			url: 'admin/theme/ajax_active/',	//檔案位置
			type: 'POST',	//or POST
			data: {id: id},
			error : function(xhr, textStatus, errorThrown){
				alert(xhr.status+' '+errorThrown);
			},
			success: function(response) {
				$this.parents('.theme-single').addClass('is_active');
			}
		});
		
	})
}

// Grab the files and set them to our variable
function prepareUpload(event){
	files = event.target.files;
	//console.log(files);
	$('#form1').submit();
}

// Catch the form submit and upload the files
function uploadFiles(event){
	event.stopPropagation(); // Stop stuff happening
    event.preventDefault(); // Totally stop stuff happening

    // START A LOADING SPINNER HERE

    // Create a formdata object and add the files
    var data = new FormData();
    $.each(files, function(key, value){
        data.append(key, value);
    });

    $.ajax({
        url: 'admin/theme/upload/',
        type: 'POST',
        data: data,
        cache: false,
        //dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR){
			var res = $.parseJSON(data);
			
			$('.theme-block').prepend(res.content);
			theme_active();
			
        },
        error: function(jqXHR, textStatus, errorThrown){
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
        }
    });
}
</script>
<div class="page-content-wrapper">
	<div class="content bg-white">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="panel panel-transparent">
				<div class="panel-body">
					<div class="row p-t-10 p-b-10">
						<div class="col-md-12">
							<a href="javascript:;" class="btn btn-primary btn-upload"><i class="fa fa-upload"></i> <?=my_lang('admin_theme.upload')?></a>
						</div>
						<form class="form" id="form1" name="form1" method="post" enctype="multipart/form-data">
							<input type="file" id="upload" style="display: none;">
						</form>
					</div>
					<div class="row theme-block">
						<?=$content?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>