<link rel="stylesheet" type="text/css" href="theme/admin/css/datatable.css" />
<link rel="stylesheet" type="text/css" href="library/tagsinput/bootstrap-tagsinput.css">
<script type="text/javascript" src="library/tagsinput/bootstrap-tagsinput.js"></script>
<script type="text/javascript" src="library/typeahead.js"></script>

<link rel="stylesheet" type="text/css" href="theme/admin/css/admin/product.css" />
<script type="text/javascript" src="theme/admin/js/product.js"></script>

<script>
$(function(){
	<?=$validation?>
	tinymce.init(tinymce_config('#content'));
	
	//時間
	bs_datetimepicker('#display_start_time');
	bs_datetimepicker('#display_end_time');
	bs_datetimepicker_relate('#display_start_time', '#display_end_time');
	
	bs_datetimepicker('#preorder_start');
	bs_datetimepicker('#preorder_end');
	bs_datetimepicker_relate('#preorder_start', '#preorder_end');
	
	bs_datetimepicker('#special_start');
	bs_datetimepicker('#special_end');
	bs_datetimepicker_relate('#special_start', '#special_end');
	bs_datetimepicker('#preorder_shippingtime');
	
	//tag
	$('#tag').tagsinput({
		tagClass: 'label label-info',
		freeInput: true,
		typeaheadjs: {
			name: 'tags',
			displayKey: 'name',
		    valueKey: 'name',
			source: tags.ttAdapter()
		}
	});
	
	//media
	$('.media_manager').media_multiple();
	media_ogimage(3);
	
	$('.save_normal').click(function(e) {
		$('#here').val(1);
	});
	$('.save_publish').click(function(){
		$('#here').val(2);
		$('#form1').submit();
	});
	$('.save_publish_back').click(function(){
		$('#here').val(3);
		$('#form1').submit();
	});
	$('.save_preview').click(function(e) {
		$('#here').val(4);
		$('#form1').submit();
	});
	
	
	$('.btn-inventory').click(function(){
		var product_id = $('#id').val();
		if(product_id){
			$.ajax({
				url: 'admin/product/ajax_inventory/',	//檔案位置
				type: 'GET',	//or POST
				data: {
					product_id: product_id,
				},
				error: function(xhr) {
					alert('Ajax request 發生錯誤');
				},
				success: function(res){
					$('.modal_parent').html(res);
					$('#inventory_modal').modal('show');
				}
			});
		}else{
			alert("請先儲存商品，才能設定庫存。");
		}
	});
});
	
function btn_checkbox(){
	//上下架
	$('.checkbox_display').unbind('click');
	$('.checkbox_display').click(function(e) {
		if($(this).children('input').attr('checked')=='checked'){
			$(this).children('input').removeAttr('checked');
			$(this).removeClass('display_checked');
			$(this).addClass('display_unchecked');
		}else{
			$(this).children('input').attr('checked', 'checked');
			$(this).removeClass('display_unchecked');
			$(this).addClass('display_checked');
		}
	});

	//刪除
	$('.checkbox_delete').unbind('click');
	$('.checkbox_delete').click(function(e) {
		if($(this).children('input').attr('checked')=='checked'){
			$(this).children('input').removeAttr('checked');
			$(this).removeClass('delete_checked');
			$(this).addClass('delete_unchecked');
		}else{
			$(this).children('input').attr('checked', 'checked');
			$(this).removeClass('delete_unchecked');
			$(this).addClass('delete_checked');
		}
	});
}
</script>

<div class="page-content-wrapper">
	<div class="content horizontal-menu">
	
		<div class="bar">
			<div class="pull-right">
				<a href="#" class="text-black padding-10 visible-xs-inline visible-sm-inline pull-right m-t-10 m-b-10 m-r-10 on" data-pages="horizontal-menu-toggle">
					<i class=" pg-close_line"></i>
				</a>
			</div>
			<div class="bar-inner">
				<?=$nav?>
			</div>
		</div>
		
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-8">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
					<? if($rows1['id']){?>
					<div class="col-md-4">
						<h3 class="text-right"><span style="font-size:14px;">點擊數：</span><?=$rows1['hits']?></h3>
					</div>
					<? }?>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="admin/product/item/">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-6 col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('setting')?></div>
							</div>
							<div class="panel-body">
								
								<div class="form-group">
									<label class="form-label"><?=my_lang('display')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="checkbox" id="display" name="display" value="1" data-init-plugin="switchery" data-size="small" data-color="primary" <?=$Website['checked'][$rows1['display']]?>/>
									</div>
								</div>

								<div class="form-group required">
									<label class="form-label" for="name"><?=my_lang('name')?></label>
									<span class="help help-inline"><?=my_lang('name_help')?></span>
									<input type="text" class="form-control required" id="name" name="name" placeholder="<?=my_lang('name_placeholder')?>" value="<?=$rows1['name']?>">
								</div>
								
								<div class="form-group">
									<label class="form-label" for="description"><?=my_lang('description')?></label>
									<span class="help help-inline"><?=my_lang('description_help')?></span>
									<input type="text" class="form-control required" id="description" name="description" placeholder="<?=my_lang('description_placeholder')?>" value="<?=$rows1['description']?>">
								</div>
								
								<div class="form-group">
									<label class="form-label"><?=my_lang('admin_product.type')?></label>
									<div>
										<div class="radio radio-primary">
											<input type="radio" name="kind" id="kind0" value="0" <?=!$rows1['kind'] ? 'checked':''?> />
											<label for="kind0">
												 <?=my_lang('admin_product.type0')?>
											</label>
											<input type="radio" name="kind" id="kind1" value="1" <?=$rows1['kind']==1 ? 'checked':''?> />
											<label for="kind1">
												 <?=my_lang('admin_product.type1')?>
											</label>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<label class="form-label"><?=my_lang('admin_product.display_time')?></label>
									<span class="help help-inline"></span>
									<div>
										<div class="input-group">
											<input type="text" class="form-control start_time" placeholder="<?=my_lang('admin_product.display_start_time')?>" id="display_start_time" name="display_start_time" value="<?=$rows1['display_start_time']?date('Y/m/d H:i:s', $rows1['display_start_time']):''?>">
											<span class="input-group-addon" id="basic-addon1">～</span>
											<input type="text" class="form-control end_time" placeholder="<?=my_lang('admin_product.display_end_time')?>" id="display_end_time" name="display_end_time" value="<?=$rows1['display_end_time']?date('Y/m/d H:i:s', $rows1['display_end_time']):''?>">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="weight"><?=my_lang('admin_product.weight')?></label>
									<span class="help help-inline"></span>
									<div>
										<div class="input-group">
											<input type="text" class="form-control" id="weight" name="weight" placeholder="<?=my_lang('admin_product.weight')?>" value="<?=$rows1['weight']?>">
											<span class="input-group-addon" id="basic-addon1"><?=my_lang('admin_product.weight_unit')?></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="box_size"><?=my_lang('admin_product.box_size')?></label>
									<span class="help help-inline"></span>
									<div>
										<div class="input-group">
											<input type="text" class="form-control" id="box_size" name="box_size" placeholder="<?=my_lang('admin_product.box_size')?>" value="<?=$rows1['box_size']?>">
											<span class="input-group-addon" id="basic-addon1"><?=my_lang('admin_product.box_size_unit')?></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="tag"><?=my_lang('admin_product.tag')?></label>
									<span class="help help-inline"><?=my_lang('admin_product.tag_help')?></span>
									<div>
										<input type="text" class="form-control" id="tag" name="tag" placeholder="<?=my_lang('admin_product.tag_placeholder')?>" value="<?=$rows1['tag_text']?>">
									</div>
								</div>

								<? if($rows1['insert_time']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('insert_time')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=date('Y/m/d H:i:s', $rows1['insert_time'])?>
									</div>
								</div>
								<? }?>
								<? if($rows1['modify_time']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('modify_time')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=date('Y/m/d H:i:s', $rows1['modify_time'])?>
									</div>
								</div>
								<? }?>
								
								
								
							</div>
						</div>
							
					</div>
					
					<div class="col-xl-6 col-lg-12 col-md-12">
						<div class="row">
							<div class="col-xl-12 col-lg-6 col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="panel-title"><?=my_lang('admin_product.category')?></div>
									</div>
									<div class="panel-body select_menu">
										<?=$rows1['menu']?>
									</div>
								</div>
							</div>
							
							
							<div class="col-xl-12 col-lg-6 col-md-12 p-l-0 p-r-5">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="panel-title"><?=my_lang('admin_product.price_setting')?></div>
									</div>
									<div class="panel-body">
										<div class="form-group required">
											<label class="form-label" for="list_price"><?=my_lang('list_price')?></label>
											<span class="help-inline help"><?=my_lang('admin_product.list_price_help')?></span>
											<div>
												<input type="text" class="form-control" id="list_price" name="list_price" placeholder="<?=my_lang('admin_product.list_price_placeholder')?>" value="<?=$rows1['list_price']?>">
											</div>
										</div>

										<div class="form-group required">
											<label class="form-label" for="price"><?=my_lang('price')?></label>
											<span class="help-inline help"><?=my_lang('admin_product.price_help')?></span>
											<div>
												<input type="text" class="form-control required" id="price" name="price" placeholder="<?=my_lang('admin_product.price_placeholder')?>" value="<?=$rows1['price']?>">
											</div>
										</div>
									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="panel-title"><?=my_lang('admin_product.preorder_setting')?></div>
									</div>
									<div class="panel-body">
										<div class="form-group">
											<div>
												<div class="checkbox check-primary">
													<input type="checkbox" name="preorder" id="preorder" value="1" <?=$Website['checked'][$rows1['preorder']]?> />
													<label for="preorder"><?=my_lang('admin_product.preorder')?></label>
												</div>
												<div class="input-group">
													<div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
													<input type="text" class="form-control start_time" id="preorder_start" name="preorder_start" placeholder="<?=my_lang('admin_product.preorder_start')?>" 
														value="<?=$rows1['preorder_start']?date('Y/m/d H:i', $rows1['preorder_start']):''?>">
													<div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
													<input type="text" class="form-control end_time" id="preorder_end" name="preorder_end" placeholder="<?=my_lang('admin_product.preorder_end')?>" 
														value="<?=$rows1['preorder_end']?date('Y/m/d H:i', $rows1['preorder_end']):''?>">
												</div>
												<div class="input-group p-t-10">
													<div class="input-group-addon"><i class="fa fa-usd"></i></div>
													<input type="text" class="form-control" id="preorder_price" name="preorder_price" placeholder="<?=my_lang('admin_product.preorder_price')?>" 
														value="<?=$rows1['preorder_price']?>">
													<div class="input-group-addon"><i class="fa fa-clock-o"></i> <?=my_lang('admin_product.preorder_shipping')?></div>
													<input type="text" class="form-control start_time" id="preorder_shippingtime" name="preorder_shippingtime" placeholder="<?=my_lang('admin_product.preorder_shipping_time')?>" 
														value="<?=$rows1['preorder_shippingtime']?date('Y/m/d H:i', $rows1['preorder_shippingtime']):''?>">
												</div>
												<span class="help-inline help-block"></span>
											</div>
										</div>
									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="panel-title"><?=my_lang('admin_product.special_offer')?></div>
									</div>
									<div class="panel-body">
										<div class="form-group">
											<div>
												<div class="checkbox check-primary">
													<input type="checkbox" name="special" id="special" value="1" <?=$Website['checked'][$rows1['special']]?> />
													<label for="special"><?=my_lang('admin_product.special')?></label>
												</div>
												<div class="input-group">
													<div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
													<input type="text" class="form-control start_time" id="special_start" name="special_start" placeholder="<?=my_lang('admin_product.special_start')?>"
														value="<?=$rows1['special_start']?date('Y/m/d H:i', $rows1['special_start']):''?>">
													<div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
													<input type="text" class="form-control end_time" id="special_end" name="special_end" placeholder="<?=my_lang('admin_product.special_end')?>"
														value="<?=$rows1['special_end']?date('Y/m/d H:i', $rows1['special_end']):''?>">
												</div>
												<div class="input-group" style="padding-top:10px;">
													<div class="input-group-addon"><i class="fa fa-usd"></i></div>
													<input type="text" class="form-control" id="special_price" name="special_price" placeholder="<?=my_lang('admin_product.special_price')?>" 
														value="<?=$rows1['special_price']?>">
													<div class="input-group-addon"><?=my_lang('admin_product.special_max')?></div>
													<input type="text" class="form-control" id="special_select_max" name="special_select_max" placeholder="<?=my_lang('admin_product.special_max_placeholder')?>" 
														value="<?=$rows1['special_select_max']?>">
												</div>

												<span class="help-inline help-block"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				
				</div>
				
				
				<div class="row">
					<div class="col-xl-6 col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('admin_product.image_setting')?></div>
							</div>
							<div class="panel-body">
								<div class="form-group">
									<div>
										<a href="javascript:;" class="btn btn-primary media_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> <?=my_lang('admin_product.image')?></a>
										<div class="alert alert-info"><?=my_lang('admin_product.image_help')?></div>
										<div class="row media_area">
											<?=$rows1['media_area']?>
										</div>
										<input id="media_input" name="media_input" class="media_input" type="hidden" value="<?=$rows1['media_input']?>" />
										<span class="help-inline help-block"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-6 col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('meta')?></div>
							</div>
							<div class="panel-body">
								<div class="form-group">
									<div>
										<a href="javascript:;" class="btn btn-primary ogimage_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> <?=my_lang('ogimage')?></a>
										<div class="alert alert-info"><?=my_lang('ogimage_help')?></div>
										<div class="row ogimage_area"><?=$rows1['ogimage_area']?></div>
										<input id="ogimage_input" name="ogimage_input" class="ogimage_input" type="hidden" value="<?=$rows1['ogimage_input']?>" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<a href="javascript:;" class="btn btn-primary btn-block btn-inventory">庫存管理</a>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="form-group">
									<label class="form-label" for="content"><?=my_lang('content')?></label>
									<span class="help help-inline"></span>
									<textarea class="form-control" id="content" name="content"><?=$rows1['content']?></textarea>
								</div>

								<div id="toolbar"></div>
							</div>
						</div>
					</div>
				</div>
				
			</div>

			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-ok"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
									<button type="button" class="btn btn-info btn-lg save_publish"><i class="fa fa-upload"></i> <?=my_lang('publish')?></button>
									<button type="button" class="btn btn-success btn-lg save_publish_back"><i class="fa fa-upload"></i> <?=my_lang('publish_and_return')?></button>
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
								<input type="hidden" id="type_original" name="type_original" value="<?=implode(',', $rows1['type_original'])?>" />
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>