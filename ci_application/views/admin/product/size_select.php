<div class="btn-group" data-toggle="buttons">
	<? for($i=0; $i<sizeof($rows1); $i++){
		$active = '';
		$checked = '';
		if($rows1[$i]['product_id']){
			$active = 'active';
			$checked = 'checked';
		}
	?>
	<label class="btn btn-default <?=$active?>" data-value="<?=$rows1[$i]['id']?>">
		<input type="checkbox" id="size[]" name="size[]" value="<?=$rows1[$i]['id']?>" autocomplete="off" <?=$checked?>> 
		<?=$rows1[$i]['name']?>
	</label>
	<? }?>
</div>