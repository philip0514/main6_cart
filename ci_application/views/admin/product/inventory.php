<div class="modal fade modal_fullscreen" id="inventory_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form2" name="form2" action="admin/product/ajax_inventory/" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">庫存管理</h4>
				</div>
				<div class="modal-body">
					<table class="table table-striped table-hover inventory_table">
						<thead>
							<tr>
								<th>動作</th>
								<!--
								<th>顏色</th>
								-->
								<th>規格</th>
								<th>成本</th>
								<th>庫存</th>
							</tr>
						</thead>
						<tbody>
							<? for($i=0; $i<sizeof($rows1); $i++){?>
							<tr>
								<td>
									<a href="javascript:;" class="box_tip pull-left checkbox_display <?=$rows1[$i]['display']?'display_checked':'display_unchecked'?>" title="上下架">
										<input type="checkbox" id="display[]" name="display[]" value="<?=$rows1[$i]['id']?>" class="hidden" <?=$rows1[$i]['display']?'checked':''?>>
										<i class="fa fa-check"></i>
									</a>
									<!-- 刪除 -->
									<a href="javascript:;" class="box_tip pull-left checkbox_delete delete_unchecked" title="刪除">
										<input type="checkbox" id="delete[]" name="delete[]" value="<?=$rows1[$i]['id']?>" class="hidden">
										<i class="fa fa-trash"></i>
									</a>
								</td>
								<!--
								<td>
									<select class="form-control selectpicker" id="color_id[]" name="color_id[]">
										<? 
											foreach($rows2 as $key => $value){
										?>
										<optgroup label="<?=$value['name']?>">
											<? for($j=0; $j<sizeof($value['data']); $j++){?>
											<option value="<?=$value['data'][$j]['id']?>" <?=$value['data'][$j]['id']==$rows1[$i]['color_id']?'selected':''?>><?=$value['data'][$j]['name']?></option>
											<? }?>
										</optgroup>
										<? }?>
									</select>
								</td>
								-->
								<td>
									<select class="form-control selectpicker" id="spec_id[]" name="spec_id[]">
										<? 
											foreach($rows3 as $key => $value){
										?>
										<optgroup label="<?=$value['name']?>">
											<? for($j=0; $j<sizeof($value['data']); $j++){?>
											<option value="<?=$value['data'][$j]['id']?>" <?=$value['data'][$j]['id']==$rows1[$i]['spec_id']?'selected':''?>><?=$value['data'][$j]['name']?></option>
											<? }?>
										</optgroup>
										<? }?>
									</select>
								</td>
								<td>
									<input type="text" class="form-control cost_price" id="cost_price[]" name="cost_price[]" placeholder="成本" value="<?=$rows1[$i]['cost_price']?>" >
								</td>
								<td>
									<input type="text" class="form-control" id="quantity[]" name="quantity[]" placeholder="數量" value="<?=$rows1[$i]['quantity']?>" >
									<input type="hidden" id="inventory_id[]" name="inventory_id[]" value="<?=$rows1[$i]['id']?>" >
								</td>
							</tr>
							<? }?>
						</tbody>
					</table>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info btn-inventory-insert">新增庫存</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
					<button type="submit" class="btn btn-primary">儲存</button>
					
					<input type="hidden" id="product_id" name="product_id" value="<?=$product_id?>" />
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
$(function(){
	btn_checkbox();
	
	$('.selectpicker').selectpicker();
	
	$('.btn-inventory-insert').click(function(){
		$.ajax({
			url: 'admin/product/ajax_inventory_item/',
			type: 'POST',
			data: {},
			error: function(xhr){},
			success: function(response){
				$('.inventory_table tbody').append(response);
				$('.selectpicker').selectpicker();
				btn_checkbox();
			}
		});
	});
	
	$('#form2').validate({
		onblur: true,
		onkeyup: false,
		onsubmit: true,
		rules: {
		},
		messages: {
		},
		highlight: function(element, errorClass, validClass) {
			form_highlight(element, errorClass, validClass);
		},
		unhighlight: function(element, errorClass, validClass) {
			form_unhighlight(element, errorClass, validClass);
		},
		errorElement: 'span',
		errorPlacement: function($error, $element) {
			var text = $error[0].innerText;
			$element.parents('.form-group').find('.help-inline').html(text);
		},
		submitHandler: function(form){
			
			var num = $('input:checkbox[name="delete[]"][checked="checked"]').length;
			var confirmed = 1;
			if(num>0){
				confirmed = confirm('您確定要刪除這 '+num+' 個項目？');
			}
			
			if(confirmed){
				$('#form2').ajaxSubmit(function(response){//console.log(response);return 0;
					$('#inventory_modal').modal('hide');
				});
			}
		}
	});
	
})
</script>