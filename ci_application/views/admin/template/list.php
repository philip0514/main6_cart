<link rel="stylesheet" type="text/css" href="theme/admin/css/datatable.css" />

<div class="page-content-wrapper">
	<div class="content bg-white">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="panel panel-transparent">
				<div class="panel-body">
					<form action="" method="post" id="form1" name="form1">
						<table class="datatable table table-hover table-responsive-block">
							<thead>
								<tr>
									<th><?=implode('</th><th>', $settings['table_columns'])?></th>
								</tr>
							</thead>
						</table>
						<input type="hidden" name="here" 			id="here" 			value="1" />
						<input type="hidden" name="orderby" 		id="orderby" 		value="" />
						<input type="hidden" name="limit_start" 	id="limit_start" 	value="" />
						<input type="hidden" name="epp" 			id="epp" 			value="" />
						<input type="hidden" name="page_no" 		id="page_no" 		value="" />
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="library/DataTables/datatables.js"></script> 
<script type="text/javascript" src="library/DataTables/jquery.dataTables.rowReordering.js"></script>
<script type="text/javascript" src="theme/admin/js/datatable.js"></script>
<script>
$(function(){
	dtable = $(".datatable").dataTable();
});
</script>