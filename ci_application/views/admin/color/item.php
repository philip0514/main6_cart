<link rel="stylesheet" type="text/css" href="library/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" />
<script type="text/javascript" src="library/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>

<script>
$(function(){
	<?=$validation?>
	//media_ogimage(1);
	$('#color_area').colorpicker();
	$('.media_manager').media_multiple({
		area: 					'.media_area',
		input_field: 			'.media_input',
		selectable_limit:		1,
		selectable_multiple:	0,
	});
});
</script>
<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('setting')?></div>
							</div>
							<div class="panel-body">
							
								<div class="form-group">
									<label class="form-label"><?=my_lang('display')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="checkbox" id="display" name="display" value="1" data-init-plugin="switchery" data-size="small" data-color="primary" <?=$Website['checked'][$rows1['display']]?>/>
									</div>
								</div>
								
								<div class="form-group required">
									<label class="form-label" for="name"><?=my_lang('name')?></label>
									<span class="help help-inline"><?=my_lang('name_help')?></span>
									<input type="text" class="form-control required" id="name" name="name" placeholder="<?=my_lang('name_placeholder')?>" value="<?=$rows1['name']?>">
								</div>
								
								<div class="form-group">
									<label class="form-label">分類</label>
									<select class="form-control selectpicker" id="type" name="type">
										<option value="0" <?=(!$rows1['type']?'selected':'')?>>-- 無 --</option>
										<? for($i=0; $i<sizeof($rows2); $i++){?>
										<option value="<?=$rows2[$i]['id']?>" <?=($rows2[$i]['id']==$rows1['type']?'selected':'')?>><?=$rows2[$i]['name']?></option>
										<? }?>
									</select>
								</div>

								<? if($rows1['insert_time']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('insert_time')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=date('Y/m/d H:i:s', $rows1['insert_time'])?>
									</div>
								</div>
								<? }?>
								<? if($rows1['modify_time']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('modify_time')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=date('Y/m/d H:i:s', $rows1['modify_time'])?>
									</div>
								</div>
								<? }?>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">色票 / 圖片</div>
							</div>
							<div class="panel-body">
								<div class="form-group">
									<label class="form-label">標示使用</label>
									<span class="help help-inline"></span>
									<div>
										<div class="radio radio-primary">
											<input id="used0" name="used" type="radio" value="0" <?=(!$rows1['used'])?'checked':''?> />
											<label for="used0">色票</label>
											<input id="used1" name="used" type="radio" value="1" <?=($rows1['used']==1)?'checked':''?> />
											<label for="used1">圖片</label>
										</div>
										
									</div>
								</div>
							
								<div class="form-group">
									<label class="form-label" for="color">色票</label>
									<span class="help help-inline">格式：#fff012</span>
									<div>
										<div id="color_area" class="input-group colorpicker-component">
											<input type="text" class="form-control" id="color" name="color" placeholder="色票" value="<?=$rows1['color']?>">
											<span class="input-group-addon"><i></i></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="image">圖片</label>
									<div>
										<a href="javascript:;" class="btn btn-primary media_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> 選擇圖片</a>
										<div class="row media_area">
											<?=$rows1['media_area']?>
										</div>
										<input id="media_input" name="media_input" class="media_input" type="hidden" value="<?=$rows1['media_input']?>" />
										<span class="help-inline help-block"></span>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
			</div>

			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-check"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>
