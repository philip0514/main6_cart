<link rel="stylesheet" type="text/css" href="theme/admin/css/datatable.css" />
<div class="page-content-wrapper">
	<div class="content bg-white">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="panel panel-transparent">
				<div class="panel-body">
					<form id="fileupload" action="fileupload/do_upload" method="POST" enctype="multipart/form-data">
						<div class="row fileupload-buttonbar">
							<div class="col-md-8 fileupload-progress fade">
								<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
									<div class="progress-bar progress-bar-success" style="width:0%;"></div>
								</div>
								<div class="progress-extended">&nbsp;</div>
							</div>
							
							<div class="col-md-4 text-right">
								<a class="btn btn-primary btn-cons fileinput-button" href="javascript:;" style="margin-right: 7px">
									<i class="fa fa-plus"></i>
									<span> <?=my_lang('admin_media.insert')?></span>
									<input type="file" id="userfile" name="userfile" style="height:50px;" multiple>
								</a>
							</div>
						</div>
					</form>
				
					<form action="" method="post" id="form1" name="form1">
						<table class="datatable table table-hover table-responsive-block">
							<thead>
								<tr>
									<th width="150">
										<label style="display: inline-block;"><input type="checkbox" id="delete_all" name="delete_all" value="1" /> <span><?=my_lang('delete_all')?></span></label>
									</th>
									<th width="50"><?=my_lang('id')?></th>
									<th width="50"><?=my_lang('image')?></th>
									<th width="300"><?=my_lang('name')?></th>
									<th><?=my_lang('content')?></th>
								</tr>
							</thead>
						</table>
						<input type="hidden" name="here" 			id="here" 			value="1" />
						<input type="hidden" name="orderby" 		id="orderby" 		value="" />
						<input type="hidden" name="limit_start" 	id="limit_start" 	value="" />
						<input type="hidden" name="epp" 			id="epp" 			value="" />
						<input type="hidden" name="page_no" 		id="page_no" 		value="" />
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="library/DataTables/datatables.js"></script> 
<script type="text/javascript" src="library/DataTables/jquery.dataTables.rowReordering.js"></script>
<script type="text/javascript" src="theme/admin/js/datatable.js"></script>

<script>
$(function(){
	dtable = $(".datatable").dataTable();
	validate_form('list');
	
	$('#delete_all').click(function(e) {
		var checked = $(this).is(':checked');
		if(checked){
			$('.checkbox_delete').each(function(index, element) {
				$(this).children('input').attr('checked', 'checked');
				$(this).removeClass('delete_unchecked');
				$(this).addClass('delete_checked');
			});
		}else{
			$('.checkbox_delete').each(function(index, element) {
				$(this).children('input').removeAttr('checked');
				$(this).removeClass('delete_checked');
				$(this).addClass('delete_unchecked');
			});
		}
	});
});
	
	
$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
		url: 'media/do_upload/',
		filesContainer: '#form1 tbody'
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
	
	// Load existing files:
	$('#fileupload').addClass('fileupload-processing');
	$.ajax({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		url: $('#fileupload').fileupload('option', 'url'),
		dataType: 'json',
		context: $('#fileupload')[0]
	}).always(function () {
		$(this).removeClass('fileupload-processing');
	}).done(function (result) {
		$(this).fileupload('option', 'done')
			.call(this, $.Event('done'), {result: result});
	});

});
</script>

<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	{% if(!file.error){ %}
	<tr role="row" class="odd">
		<td colspan="5">
			<div class="template-upload fade" style="width:100%;">
				<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					<div class="progress-bar progress-bar-success" style="width:0%;"></div>
				</div>
			</div>
		</td>
	</tr>
	
	{% } %}
{% } %}
</script>

<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	{% if(!file.error){ %}
	
	<tr role="row" class="odd">
		<td>
			<div class="action_group">
				<input class="hidden" id="id[]" name="id[]" value="{%=file.id%}">
				<a href="javascript:;" class="box_tip pull-left checkbox_delete delete_unchecked" title="刪除">
					<input type="checkbox" id="delete[]" name="delete[]" value="{%=file.id%}" class="hidden"><i class="fa fa-trash"></i>
				</a>
			</div>
		</td>
		<td class="sorting_1">{%=file.id%}</td>
		<td><img src="{%=file.thumbnailUrl%}" width="50" class="img-rounded"></td>
		<td>{%=file.orig_name%}</td>
		<td></td>
	</tr>
	{% }%}
{% } %}

</script>