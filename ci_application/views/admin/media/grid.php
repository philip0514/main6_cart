
<script type="text/javascript" src="library/jQuery/infinite-scroll/jquery.infinitescroll.min.js"></script> 

<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="library/jQuery/fileupload/css/jquery.fileupload.css">
<link rel="stylesheet" href="library/jQuery/fileupload/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="library/jQuery/fileupload/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="library/jQuery/fileupload/css/jquery.fileupload-ui-noscript.css"></noscript>

<style>
.preview{
	overflow: hidden;
	max-height: 100px;
	max-width: 100px;
}
.col-padding{
	padding:15px;
	height:195px;
}
.file_icon{
	text-align:center; font-size:100px;
	color:#444;
}
.file_name{
	padding-top: 30px;
	text-align: center;
	color:#444;
}
</style>
<script>
function file_edit(id){
	$.ajax({
		type: 'POST',
		url: 'request/file_edit_modal/',
		data:{
			id: id
		},
		error: function(xhr) {
			alert('Ajax request 發生錯誤');
		},
		success: function(data, textStatus, jqXHR){
			$('.modal_parent').html(data);
			$('#content_modal').modal();
		}
	});
}
</script>


<!-- The file upload form used as target for the file upload widget -->
<form id="fileupload" action="fileupload/do_upload" method="POST" enctype="multipart/form-data">
    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
    <div class="row fileupload-buttonbar">
        <div class="col-lg-7">
            <!-- The fileinput-button span is used to style the file input field as button -->
            <a class="btn btn-success btn-lg fileinput-button" href="javascript:;">
                <i class="fa fa-plus"></i>
                <span>新增檔案...</span>
                <input type="file" name="userfile" style="height:50px;" multiple>
            </a>
            <!-- The global file processing state -->
            <span class="fileupload-process"></span>
        </div>
        <!-- The global progress state -->
        <div class="col-lg-5 fileupload-progress fade">
            <!-- The global progress bar -->
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
            <!-- The extended global progress state -->
            <div class="progress-extended">&nbsp;</div>
        </div>
    </div>
    <!-- The table listing the files available for upload/download -->
	<div class="row files">
		<? for($i=0; $i<sizeof($rows1); $i++){?>
		<div class="col-sm-2 col-padding">
			<a href="javascript:file_edit(<?=$rows1[$i]['id']?>);" class="file_edit" id="file_<?=$rows1[$i]['id']?>" title="<?=$rows1[$i]['orig_name']?>">
				<? if($rows1[$i]['is_image']){?>
					<img src="<?=$rows1[$i]['thumbnailUrl']?>" style="width:100%;" class="img-rounded" title="<?=$rows1[$i]['orig_name']?>">
				<? }else{?>
					<div class="file_icon">
						<span class="fa fa-file" aria-hidden="true"></span>
					</div>
					<div class="file_name"><?=$rows1[$i]['orig_name']?></div>
				<? }?>
			</a>
		</div>
		<? }?>
	</div>
	
	
	<a id="next" href="javascript:;" class="btn btn-default">Next</a>
	
</form>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	{% if(!file.error){ %}
	<div class="col-sm-2 col-padding template-upload fade">
		<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
			<div class="progress-bar progress-bar-success" style="width:0%;"></div>
		</div>
	</div>
	{% } %}
{% } %}
</script>

<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	{% if(!file.error){ %}
	<div class="col-sm-2 col-padding">
		<a href="javascript:file_edit({%=file.id%});" class="file_edit" id="file_{%=file.id%}" title="{%=file.orig_name%}">
			{% if(file.is_image){ %}
				<img src="{%=file.thumbnailUrl%}" style="width:100%;" class="img-rounded">
			{% }else { %}
				<div class="file_icon">
					<span class="fa fa-file" aria-hidden="true"></span>
				</div>
				<div class="file_name">{%=file.orig_name%}</div>
			{% }%}
		</a>
	</div>
	{% }%}
{% } %}
</script>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="library/jQuery/fileupload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- blueimp Gallery script -->
<script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="library/jQuery/fileupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="library/jQuery/fileupload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="library/jQuery/fileupload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="library/jQuery/fileupload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="library/jQuery/fileupload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="library/jQuery/fileupload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="library/jQuery/fileupload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="library/jQuery/fileupload/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script>
var grid_start = 0;
var grid_limit = 20;
</script>
<script src="library/jQuery/fileupload/main.js"></script>
<script>
//
$(function(){
	
	$('.files').infinitescroll({
		navSelector  	: "#next:last",
		nextSelector 	: "a#next:last",
		itemSelector 	: ".files .col-sm-2",
		debug		 	: true,
		dataType	 	: 'html',
//      maxPage         : 3,
//		prefill			: true,
		path: function(index){
			var url = "fileupload/get_file_list/"+index;
			return url;
		},
		//behavior		: 'twitter',
		//appendCallback	: false, // USE FOR PREPENDING
		//pathParse     	: function( pathStr, nextPage ){ return pathStr.replace('2', nextPage ); }
    }, function(newElements, data, url){
    	//USE FOR PREPENDING
		//console.log(newElements);
    	// $(newElements).css('background-color','#ffef00');
    	// $(this).prepend(newElements);
    	//
    	//END OF PREPENDING

//    	window.console && console.log('context: ',this);
//    	window.console && console.log('returned: ', newElements);
    	
    });
})
</script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="library/jQuery/fileupload/js/cors/jquery.xdr-transport.js"></script>
<![endif]-->