<div id="crop-avatar">
	<form class="avatar-form" id="form1" name="form1" action="" method="post" enctype="multipart/form-data">
	
		<div class="content"> 
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h3><?=$Website['page_header']?></h3>
			</div>
			<!-- END PAGE TITLE --> 
			<!-- BEGIN PlACE PAGE CONTENT HERE --> 
			<div class="row">
				<div class="col-md-4">
					<div class="grid simple">
						<div class="grid-title no-border">
							<h4>一般設定</h4>
							<div class="tools">
								<a href="javascript:;" class="collapse"></a>
							</div>
						</div>
						<div class="grid-body no-border">
							<!--
							<div class="form-group">
								<label class="form-label">上/下架</label>
								<div>
									<p class="form-control-static">
									<label for="display">
										<input type="checkbox" name="display" id="display" value="1" <?=$Website['checked'][$rows1['display']]?> /> 上架
									</label>
									</p>
								</div>
							</div>
							-->
							<div class="form-group">
								<label class="form-label" for="title">* 標題</label>
								<div>
									<input type="text" class="form-control required" id="title" name="title" placeholder="標題 必填" value="<?=$rows1['title']?>">
									<span class="help-inline help-block"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="form-label" for="content">說明</label>
								<div>
									<input type="text" class="form-control" id="content" name="content" placeholder="說明" value="<?=$rows1['content']?>">
									<span class="help-inline help-block"></span>
								</div>
							</div>
							<!--
							<div class="form-group">
								<label class="form-label" for="excerpt">代替文字</label>
								<div>
									<input type="text" class="form-control" id="excerpt" name="excerpt" placeholder="代替文字" value="<?=$rows1['excerpt']?>">
									<span class="help-inline help-block"></span>
								</div>
							</div>
							-->
						</div>
					</div>
				</div>
				
				<div class="col-md-8">
					<div class="grid simple">
						<div class="grid-title no-border">
							<h4>裁切</h4>
							<div class="tools">
								<a href="javascript:;" class="collapse"></a>
							</div>
						</div>
						<div class="grid-body no-border">
							
							<div class="form-group">
								<label class="form-label" for="content">裁切</label>
								<div class="col-sm-8">
									<div class="img-container">
										<img src="<?=$rows1['file_path'].$rows1['name']?>">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-btn">
											<button class="btn btn-primary turn_left" data-method="rotate" data-direction="-" type="button">
												<span class="fa fa-mail-reply"></span>
											</button>
											</span>
											<input class="form-control" id="deg" name="deg" value="15">
											<span class="input-group-btn">
											<button class="btn btn-primary turn_right" data-method="rotate" data-direction="+" type="button">
												<span class="fa fa-mail-forward"></span>
											</button>
											</span>
										</div><!-- /input-group -->
									</div>
									<div class="docs-preview clearfix">
										<div class="img-preview preview-lg"></div>
										<div class="img-preview preview-md"></div>
										<div class="img-preview preview-sm"></div>
										<div class="img-preview preview-xs"></div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-actions">
						<div class="pull-right">
							<input type="submit" class="btn btn-primary btn-lg btn-cons avatar-save" value="裁切並儲存" />
							
							<input type="hidden" id="here" name="here" value="1" />
							
							<input class="avatar-src form-control" name="avatar_src" type="hidden" value="<?=$rows1['file_path'].$rows1['name']?>">
							<input class="avatar-name form-control" name="avatar_name" type="hidden" value="<?=$rows1['name']?>">
							<input class="avatar-path form-control" name="avatar_path" type="hidden" value="<?=$rows1['file_path']?>">
							<input class="avatar-data form-control" name="avatar_data" type="hidden">
							
							
						</div>
					</div>
				</div>
			</div>
			
		</div>
	
		
		
		
		
		
		<div class="form-group">
			<div class="col-sm-12 text-center">
			</div>
		</div>
	</form>
</div>

<link href="library/jQuery/cropper/css/cropper.min.css" rel="stylesheet">
<link href="library/jQuery/cropper/css/main.css" rel="stylesheet">
<style>
.img-preview {
  float: left;
  margin-right: 10px;
  margin-bottom: 10px;
}
.img-container, .img-preview {
  background-color: #f7f7f7;
  overflow: hidden;
  width: 100%;
  text-align: center;
}
.img-container{
	max-height:500px;
}


.preview-sm {
  width: 69px;
  height: 39px;
}
.preview-xs {
  width: 35px;
  height: 20px;
  margin-right: 0;
}
</style>


<link rel="stylesheet" type="text/css" href="library/jQuery/tagsinput/bootstrap-tagsinput.css?t=<?=$Website['js_timestamp']?>">
<script type="text/javascript" src="library/jQuery/tagsinput/bootstrap-tagsinput.js?t=<?=$Website['js_timestamp']?>"></script>
<script type="text/javascript" src="library/jQuery/typeahead.js?t=<?=$Website['js_timestamp']?>"></script>

<script type="text/javascript" src="library/jQuery/cropper/js/cropper.min.js"></script>
<script type="text/javascript" src="library/jQuery/cropper/js/class_crop.js"></script>

<script>
var tags;
$image = $(".img-container > img");

$(function(){
	
	validate_form('admin_brief');
	
	//標籤
	//tag_initialize('request/tag_search/?q=%QUERY');
	//tag('#tag');
	
	//裁切
	var height = $('.avatar-form').height();
	$('.img-container').css('max-height', height-80);
	var crop_box_data = {"top":0,"width":500,"height":500};
	var canvas_data = {"top":0,"width":500,"height":500};
	
	$image.cropper({
		autoCropArea: 1,
		preview: ".img-preview",
		responsive: true,
		aspectRatio: 1 / 1,
		minCropBoxWidth: <?=(int)$Website['media']['thumb_width']?>,
		minCropBoxHeight: <?=(int)$Website['media']['thumb_height']?>,
		crop: function(data) {
			var json = [
				'{"x":' + data.x,
				'"y":' + data.y,
				'"height":' + data.height,
				'"width":' + data.width,
				'"rotate":' + data.rotate + '}'
			].join();
			//console.log(json);
			$('.avatar-data').val(json);
		},
		built: function () {
			<? if($rows1['crop_data']){?>
			$image.cropper('setData', <?=htmlspecialchars_decode($rows1['crop_data'])?>);
			<? }?>
		}
	});
	$('.turn_left').click(function(e) {
		$image.cropper('rotate', 0-parseInt($('#deg').val()));
	});
	$('.turn_right').click(function(e) {
		$image.cropper('rotate', parseInt($('#deg').val()));
	});
})
</script>

