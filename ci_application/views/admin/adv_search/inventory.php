<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="col-sm-2 control-label" for="search"><?=my_lang('search')?></label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="search" name="search" placeholder="商品名稱或商品ID" value="<?=$rows1['search']?>">
				<span class="help-inline help-block"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?=my_lang('column')?></label>
			<div class="col-sm-10">
				<select class="form-control selectpicker" id="column_visible" name="column_visible" multiple>
					<? for($i=1; $i<sizeof($setting['table_columns']); $i++){?>
					<option value="<?=$i?>" 
						<?=$setting['column_visible'][$i]?'selected':''?>
						>
						<?=$setting['table_columns'][$i]?>
					</option>
					<? }?>
				</select>

			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-2"></div>
			<div class="col-sm-10 p-t-10">
				<div class="btn-group btn-group-fullwidth" role="group">
					<button type="submit" id="s" class="btn btn-primary btn-cons">
						<i class="fa fa-search"></i> <?=my_lang('search_btn')?>
					</button>
					<button type="button" id="cancel" class="btn btn-default btn-cons">
						<i class="fa fa-refresh"></i> <?=my_lang('search_reset_btn')?>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>