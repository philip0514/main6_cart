<script>
$(function(){
	date_datetimepicker('#start_insert_date');
	date_datetimepicker('#end_insert_date');
	bs_datetimepicker_relate('#start_insert_date', '#end_insert_date');
	
	date_datetimepicker('#start_payment_date');
	date_datetimepicker('#end_payment_date');
	bs_datetimepicker_relate('#start_payment_date', '#end_payment_date');
});
</script>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="col-sm-2 control-label" for="search"><?=my_lang('search')?></label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="search" name="search" placeholder="編號、購買人資訊、收件人資訊" value="<?=$rows1['search']?>">
				<span class="help-inline help-block"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="payment_type">付款方式</label>
			<div class="col-sm-10">
				<? 
					$payment_type = explode(',', $rows1['payment_type']);
				?>
				<select class="form-control selectpicker" id="payment_type" name="payment_type" multiple>
					<option value="0" <?=(in_array(0, $payment_type) || !sizeof($payment_type) || !$payment_type)?'selected':''?>>全部</option>
					<? for($i=0; $i<sizeof($Website['payment_type']); $i++){?>
					<option value="<?=$Website['payment_type'][$i]['id']+1?>" <?=in_array($Website['payment_type'][$i]['id']+1, $payment_type)?'selected':''?>><?=$Website['payment_type'][$i]['name']?></option>
					<? }?>
				</select>
				<span class="help-inline help-block"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="payment_status">訂單狀態</label>
			<div class="col-sm-10">
				<? 
					$payment_status = explode(',', $rows1['payment_status']);
				?>
				<select class="form-control selectpicker" id="payment_status" name="payment_status" multiple>
					<option value="0" <?=(in_array(0, $payment_status) || !sizeof($payment_status) || !$payment_status)?'selected':''?>>全部</option>
					<? for($i=0; $i<sizeof($Website['payment_status']); $i++){?>
					<option value="<?=$Website['payment_status'][$i]['id']+1?>" <?=in_array($Website['payment_status'][$i]['id']+1, $payment_status)?'selected':''?>><?=$Website['payment_status'][$i]['name']?></option>
					<? }?>
				</select>
				<span class="help-inline help-block"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">訂單時間</label>
			<div class="col-sm-10">
				<div class="input-group">
					<input id="start_insert_date" name="start_insert_date" class="form-control" type="text" placeholder="開始時間" 
						value="<?=$rows1['start_insert_date']?>">
					<div class="input-group-addon">~</div>
					<input id="end_insert_date" name="end_insert_date" class="form-control" type="text" placeholder="結束時間" 
						value="<?=$rows1['end_insert_date']?>">
				</div>
				<span class="help-inline help-block"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">付款時間</label>
			<div class="col-sm-10">
				<div class="input-group">
					<input id="start_payment_date" name="start_payment_date" class="form-control" type="text" placeholder="開始時間" 
						value="<?=$rows1['start_payment_date']?>">
					<div class="input-group-addon">~</div>
					<input id="end_payment_date" name="end_payment_date" class="form-control" type="text" placeholder="結束時間" 
						value="<?=$rows1['end_payment_date']?>">
				</div>
				<span class="help-inline help-block"></span>
			</div>
		</div>
		
		
		
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label"><?=my_lang('column')?></label>
			<div class="col-sm-10">
				<select class="form-control selectpicker" id="column_visible" name="column_visible" multiple>
					<? for($i=1; $i<sizeof($setting['table_columns']); $i++){?>
					<option value="<?=$i?>" 
						<?=$setting['column_visible'][$i]?'selected':''?>
						>
						<?=$setting['table_columns'][$i]?>
					</option>
					<? }?>
				</select>

			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-2"></div>
			<div class="col-sm-10 p-t-10">
				<div class="btn-group btn-group-fullwidth" role="group">
					<button type="submit" id="s" class="btn btn-primary btn-cons">
						<i class="fa fa-search"></i> <?=my_lang('search_btn')?>
					</button>
					<button type="button" id="cancel" class="btn btn-default btn-cons">
						<i class="fa fa-refresh"></i> <?=my_lang('search_reset_btn')?>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>