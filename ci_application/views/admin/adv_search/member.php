<script>
$(function(){
	date_datetimepicker('#start_date');
	date_datetimepicker('#end_date');
	bs_datetimepicker_relate('#start_date', '#end_date');
	zip_code('#city_id', '#area_id', 1);
	
	$('#export').click(function(){
		document.location.href = 'admin/member/ajax_export/';
	});
});
</script>

<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<label class="col-sm-2 control-label" for="search"><?=my_lang('search')?></label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="search" name="search" placeholder="<?=my_lang('admin_member.search_placeholder')?>" value="<?=$rows1['search']?>">
				<span class="help-inline help-block"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gender"><?=my_lang('admin_member.gender')?></label>
			<div class="col-sm-10">
				<select class="form-control selectpicker" id="gender" name="gender">
					<option value="0" <?=!$rows1['gender']?'selected':''?>><?=my_lang('admin_member.gender0')?></option>
					<option value="1" <?=$rows1['gender']==1?'selected':''?>><?=my_lang('admin_member.gender1')?></option>
					<option value="2" <?=$rows1['gender']==2?'selected':''?>><?=my_lang('admin_member.gender2')?></option>
				</select>
				<span class="help-inline help-block"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="city_id"><?=my_lang('admin_member.city')?></label>
			<div class="col-sm-10">
				<select class="form-control selectpicker" id="city_id" name="city_id">
					<option value="0"><?=my_lang('admin_member.city0')?></option>
					<? for($i=0; $i<sizeof($rows2); $i++){?>
					<option value="<?=$rows2[$i]['id']?>" 
						<?=$rows2[$i]['id']==$rows1['city_id']?'selected':''?>
						>
						<?=$rows2[$i]['name']?>
					</option>
					<? }?>
				</select>
				<span class="help-inline help-block"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="area_id"><?=my_lang('admin_member.area')?></label>
			<div class="col-sm-10">
				<select class="form-control selectpicker" id="area_id" name="area_id">
					<option value="0"><?=my_lang('admin_member.area0')?></option>
					<? for($i=0; $i<sizeof($rows3); $i++){?>
					<option value="<?=$rows3[$i]['id']?>" 
						<?=$rows3[$i]['id']==$rows1['area_id']?'selected':''?>
						>
						<?=$rows3[$i]['name']?>
					</option>
					<? }?>
				</select>
				<span class="help-inline help-block"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label"><?=my_lang('admin_member.birthday')?></label>
			<div class="col-sm-10">
				<div class="input-group">
					<input id="start_date" name="start_date" class="form-control start_date" type="text" placeholder="<?=my_lang('admin_member.birthday_start')?>" 
						value="<?=$rows1['start_date']?>">
					<div class="input-group-addon">~</div>
					<input id="end_date" name="end_date" class="form-control end_date" type="text" placeholder="<?=my_lang('admin_member.birthday_end')?>" 
						value="<?=$rows1['end_date']?>">
				</div>
				<span class="help-inline help-block"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="display"><?=my_lang('display')?></label>
			<div class="col-sm-10">
				<select id="display" name="display" class="form-control selectpicker">
					<option value="3" <?=$rows1['display']==3?'selected':''?>><?=my_lang('all')?></option>
					<option value="1" <?=$rows1['display']==1?'selected':''?>><?=my_lang('display_on')?></option>
					<option value="2" <?=$rows1['display']==2?'selected':''?>><?=my_lang('display_off')?></option>
				</select>
				<span class="help-inline help-block"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label"><?=my_lang('column')?></label>
			<div class="col-sm-10">
				<select class="form-control selectpicker" id="column_visible" name="column_visible" multiple>
					<? for($i=1; $i<sizeof($setting['table_columns']); $i++){?>
					<option value="<?=$i?>" 
						<?=$setting['column_visible'][$i]?'selected':''?>
						>
						<?=$setting['table_columns'][$i]?>
					</option>
					<? }?>
				</select>

			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-2"></div>
			<div class="col-sm-10 p-t-10">
				<div class="btn-group btn-group-fullwidth" role="group">
					<button type="submit" id="s" class="btn btn-primary btn-cons">
						<i class="fa fa-search"></i> <?=my_lang('search_btn')?>
					</button>
					<button type="button" id="cancel" class="btn btn-default btn-cons">
						<i class="fa fa-refresh"></i> <?=my_lang('search_reset_btn')?>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>