<script type="text/javascript" src="theme/admin/js/coupon.js"></script>
<script>
$(function(){
	<?=$validation?>
	bs_datetimepicker('#start_date');
	bs_datetimepicker('#end_date');
	bs_datetimepicker_relate('#start_date', '#end_date');
	
	Coupon.insert_relationship();
});
</script>
<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('setting')?></div>
							</div>
							<div class="panel-body">
							
								<div class="form-group">
									<label class="form-label"><?=my_lang('display')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="checkbox" id="display" name="display" value="1" data-init-plugin="switchery" data-size="small" data-color="primary" <?=$Website['checked'][$rows1['display']]?>/>
									</div>
								</div>
						
								<div class="form-group required">
									<label class="form-label" for="name">序號</label>
									<span class="help help-inline">必填</span>
									<input type="text" class="form-control required" id="name" name="name" placeholder="序號 必填" value="<?=$rows1['name']?>">
								</div>

								<div class="form-group">
									<label class="form-label" for="limit_count">使用次數限制</label>
									<span class="help help-inline">預設為0，不限使用次數，1為使用一次</span>
									<div>
										<?=(int)$rows1['limit_count']?>
									</div>
								</div>

								<div class="form-group required">
									<label class="form-label" for="price">折扣卷金額/折數</label>
									<span class="help help-inline">必填，例：「打九折，請輸入90，並選擇%」，「折兩百元，請輸入200，並選擇元」</span>
									<div class="row">
										<div class="col-sm-6">
											<input type="text" class="form-control required" id="discount" name="discount" placeholder="折扣卷金額/折數 必填" value="<?=$rows1['discount']?>">
										</div>
										<div class="col-sm-6">
											<select id="discount_type" name="discount_type" class="form-control selectpicker">
												<option value="0" <?=!$rows1['discount_type']?'selected':''?> >元</option>
												<option value="1" <?=$rows1['discount_type']==1?'selected':''?> >%</option>
											</select>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="discount_gate">消費門檻</label>
									<span class="help help-inline">限定使用門檻，消費金額大於等於Ｘ元才可使用，預設為0，代表不限消費金額</span>
									<input type="text" class="form-control" id="discount_gate" name="discount_gate" placeholder="消費門檻" value="<?=(int)$rows1['discount_gate']?>">
								</div>

								<div class="form-group">
									<label class="form-label" for="length">起迄時間</label>
									<span class="help help-inline"></span>
									<div class="input-group">
										<input id="start_date" name="start_date" class="form-control start_time" type="text" placeholder="起始時間" value="<?=$rows1['start_time'] ? date('Y/m/d H:i', $rows1['start_time']) : ''?>">
										<div class="input-group-addon">~</div>
										<input id="end_date" name="end_date" class="form-control end_time" type="text" placeholder="結束時間" value="<?=$rows1['end_time'] ? date('Y/m/d H:i', $rows1['end_time']) : ''?>">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="note">簡述</label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="note" name="note" placeholder="簡述" value="<?=$rows1['note']?>">
									</div>
								</div>

								<? if($rows1['insert_time']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('insert_time')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=date('Y/m/d H:i:s', $rows1['insert_time'])?>
									</div>
								</div>
								<? }?>
								<? if($rows1['modify_time']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('modify_time')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=date('Y/m/d H:i:s', $rows1['modify_time'])?>
									</div>
								</div>
								<? }?>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">會員紀錄</div>
							</div>
							<div class="panel-body">
								
					
								<? if($rows1['limit_count'] && sizeof($rows2)){}else{?>
								<div class="form-group insert_relationship_block">
									<label class="form-label" for="email">新增配送</label>
									<span class="help help-inline"></span>
									<div>
										<div class="input-group">
											<input id="email" class="form-control" type="text" placeholder="請輸入會員email，並用分號「;」分隔" value="">
											<span class="input-group-btn">
												<input type="button" class="btn btn-primary" id="insert_relationship" value="新增配送">
											</span>
										</div>
									</div>
								</div>
								<? }?>

								<table class="table table-striped table-hover relation_table">
									<tr>
										<th>發送對象</th>
										<th>發送時間</th>
										<th>使用人</th>
										<th>使用時間</th>
										<th>訂單</th>
									</tr>
									<? for($i=0; $i<sizeof($rows2); $i++){?>
									<tr>
										<td><a href="admin/member/item/<?=$rows2[$i]['member_id']?>/"><?=$rows2[$i]['member_name']?></a></td>
										<td><?=$rows2[$i]['sent_time']?date('Y/m/d H:i:s', $rows2[$i]['sent_time']):''?></td>
										<td><?=$rows2[$i]['used_by']?'<a href="admin/member/item/'.$rows2[$i]['used_by'].'/">'.$rows2[$i]['used_member_name'].'</a>':'--'?></td>
										<td><?=$rows2[$i]['used_time']?date('Y/m/d H:i:s', $rows2[$i]['used_time']):'尚未使用'?></td>
										<td><?=$rows2[$i]['order_id']?'<a href="admin/order/item/'.$rows2[$i]['order_id'].'/">'.$rows2[$i]['order_id'].'</a>':'--'?></td>
									</tr>
									<? }?>
								</table>

								<? if(!sizeof($rows2)){?>
									<div class="alert alert-info rows_zero">
										無發送與使用紀錄
									</div>
								<? }?>
							
							
							</div>
						</div>
					</div>
				</div>
				<div id="toolbar"></div>
				
			</div>

			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-check"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>
