<link rel="stylesheet" type="text/css" href="library/typed/typed.css" />
<script type="text/javascript" src="library/typed/typed.min.js"></script>
<script type="text/javascript" src="theme/admin/js/coupon.js"></script>
<script>
$(function(){
	<?=$validation?>
	Coupon.init();
});
</script>
<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default hover-stroke">
							<div class="panel-body no-padding">
								<div class="container-sm-height">
									<div class="row row-sm-height">
										<div class="col-sm-12 col-sm-height padding-20 col-top">
											<p class="font-montserrat bold">預覽</p>
											<h3 class="no-margin font-arial">
												<span class="typed_prefix"></span><span class="typed_element"></span>
											</h3>
												<p class="small hint-text">此處僅為預覽參考用，請以最後實際產生的序號為主。</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-9 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('setting')?></div>
							</div>
							<div class="panel-body">
								
								<div class="form-group">
									<label class="form-label">序號類型</label>
									<div>
										<div class="row">
											<div class="col-md-6">
												<div class="radio radio-primary">
													<input class="serial_type" id="type0" name="type" type="radio" value="0" checked />
													<label for="type0">隨機亂數</label>
													<input class="serial_type" id="type1" name="type" type="radio" value="1" />
													<label for="type1">固定順序</label>
													<input class="serial_type" id="type2" name="type" type="radio" value="2" />
													<label for="type2">單一序號</label>
												</div>
											</div>
											<div class="col-md-6">
												<ul>
													<li>單一序號，「可被使用次數」預設為「0」，代表不限使用次數，<br>如為「1」，代表只能給1人使用，如為「2」，代表可給2人使用，以此類推</li>
													<li>如行銷需要廣發序號，請選擇「單一序號」，並將「可被使用次數」預設為「0」</li>
												</ul>

											</div>
										</div>

									</div>
								</div>

								<div class="form-group serial_type1">
									<label class="form-label" for="prefix">序號前置詞</label>
									<span class="help help-inline"></span>
									<div>
									<input type="text" class="form-control" id="prefix" name="prefix" placeholder="序號前置詞" value="">
									</div>
								</div>

								<div class="form-group serial_type1 required">
									<label class="form-label" for="length">序號長度</label>
									<span class="help help-inline">必填</span>
									<input type="text" class="form-control" id="length" name="length" placeholder="序號長度 必填" value="10">
								</div>
								
								<div class="form-group serial_type1 required">
									<label class="form-label" for="amount">序號組數</label>
									<span class="help help-inline">必填</span>
									<input type="text" class="form-control" id="amount" name="amount" placeholder="序號組數 必填" value="10">
								</div>
								
								<div class="form-group serial_type2">
									<label class="form-label" for="number_start">序號開始數字</label>
									<span class="help help-inline"></span>
									<input type="text" class="form-control" id="number_start" name="number_start" placeholder="序號開始數字" value="1">
									
									<div class="serial_check alert"></div>
								</div>

								<div class="form-group serial_type3 required">
									<label class="form-label" for="serial">序號名稱</label>
									<span class="help help-inline">必填</span>
									<input type="text" class="form-control" id="serial" name="serial" placeholder="自行設定序號 必填" value="">
								</div>

								<div class="form-group serial_type3 required">
									<label class="form-label" for="limit_count">使用次數限制</label>
									<span class="help help-inline">必填，預設為0，不限使用次數</span>
									<input type="text" class="form-control" id="limit_count" name="limit_count" placeholder="使用次數限制 必填" value="0">
								</div>


								<div class="form-group required">
									<label class="form-label" for="discount">折扣卷金額/折數</label>
									<span class="help help-inline">必填，例：「折兩百元，請輸入200，並選擇元」，「打九折，請輸入90，並選擇%」</span>
									<div class="row">
										<div class="col-sm-6">
											<input type="text" class="form-control required" id="discount" name="discount" placeholder="折扣卷金額/折數 必填" value="">
										</div>
										<div class="col-sm-6">
											<select id="discount_type" name="discount_type" class="form-control selectpicker">
												<option value="0">元</option>
												<option value="1">%</option>
											</select>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="discount_gate">消費門檻</label>
									<span class="help help-inline">限定使用門檻，消費金額大於等於Ｘ元才可使用，預設為0，代表不限消費金額</span>
									<input type="text" class="form-control" id="discount_gate" name="discount_gate" placeholder="消費門檻" value="">
								</div>

								<div class="form-group">
									<label class="form-label">起迄時間</label>
									<span class="help help-inline"></span>
									<div>
										<div class="input-group">
											<input id="start_date" name="start_date" class="form-control start_time" type="text" placeholder="起始時間" value="">
											<div class="input-group-addon">~</div>
											<input id="end_date" name="end_date" class="form-control end_time" type="text" placeholder="結束時間" value="">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="note">簡述</label>
									<span class="help help-inline"></span>
									<input type="text" class="form-control" id="note" name="note" placeholder="簡述" value="">
								</div>
								
							</div>
						</div>
					</div>
					
				</div>
				<div id="toolbar"></div>
			</div>

			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-check"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>
