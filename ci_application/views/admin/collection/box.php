<div class="col-xs-6 col-sm-3 col-md-2 col-xl-1 product_item">
	<span class="close product_delete" data-product_id="<?=$rows1['product_id']?>">
		<i class="fa fa-trash"></i>
	</span>
	
	<div class="product_image">
		<img src="<?=$rows1['file_url']?>">
	</div>
	
	<div class="text-center">
		<?=$rows1['product_id']?>
	</div>
	
	<div class="product_name">
		<?=$rows1['name']?>
	</div>
	
	<div style="p-t-10 p-b-10">
		<input class="form-control input-sortable" type="text" id="sort[]" name="sort[]" value="<?=(int)$rows1['sort']?>" />
		<input type="hidden" id="product_id[]" name="product_id[]" value="<?=(int)$rows1['product_id']?>" />
	</div>
</div>