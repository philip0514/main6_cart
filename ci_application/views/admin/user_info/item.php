<script>
$(function(){
	<?=$validation?>
});
</script>

<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('setting')?></div>
							</div>
							<div class="panel-body">
								
								<div class="form-group">
									<label class="form-label" for="account"><?=my_lang('account')?></label>
									<span class="help help-inline"></span>
									<h3>
										<?=$rows1['email']?>
									</h3><br>
								</div>
						
								<div class="form-group required">
									<label class="form-label" for="name"><?=my_lang('name')?></label>
									<span class="help help-inline"><?=my_lang('name_help')?></span>
									<div>
										<input type="text" class="form-control required" id="name" name="name" placeholder="<?=my_lang('name_placeholder')?>" value="<?=$rows1['username']?>">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="password"><?=my_lang('password')?></label>
									<span class="help help-inline"><?=my_lang('password_help')?></span>
									<div>
										<input type="password" class="form-control" id="password" name="password" placeholder="<?=my_lang('password_placeholder')?>" value="">
									</div>
								</div>
								
								<div class="form-group">
									<label class="form-label" for="company"><?=my_lang('admin_user.company')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="company" name="company" placeholder="<?=my_lang('admin_user.company_placeholder')?>" value="<?=$rows1['company']?>">
									</div>
								</div>
								
								<div class="form-group">
									<label class="form-label" for="phone"><?=my_lang('admin_user.phone')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="phone" name="phone" placeholder="<?=my_lang('admin_user.phone_placeholder')?>" value="<?=$rows1['phone']?>">
									</div>
								</div>
								
								
								
							</div>
						</div>
					</div>
					
					
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('admin_user.record')?></div>
							</div>
							<div class="panel-body">
								<? if(sizeof($rows4)){?>
								<table class="table table-hover table-striped">
									<tr>
										<th><?=my_lang('admin_user.record_time')?></th>
										<th><?=my_lang('admin_user.record_ip')?></th>
										<th><?=my_lang('admin_user.record_status')?></th>
									</tr>
									<? for($i=0; $i<sizeof($rows4); $i++){?>
									<tr>
										<td><?=date('Y/m/d H:i:s', $rows4[$i]['time'])?></td>
										<td><?=$rows4[$i]['ip_address']?></td>
										<td><?=$rows4[$i]['status']?my_lang('status_success'):my_lang('status_failed')?></td>
									</tr>
									<? }?>
								</table>
								<? }else{?>
								<div class="alert alert-info">
									<?=my_lang('admin_user.record_zero')?>
								</div>
								<? }?>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
			<div id="toolbar"></div>
								
			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-ok"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>