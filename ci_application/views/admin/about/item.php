<script>
$(function(){
	<?=$validation?>
	tinymce.init(tinymce_config('#content', '<?=$_SESSION['user_info']['site_lang']?>'));
	media_ogimage(3);
});
</script>
<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('setting')?></div>
							</div>
							<div class="panel-body">
							
								<div class="form-group">
									<label class="form-label"><?=my_lang('display')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="checkbox" id="display" name="display" value="1" data-init-plugin="switchery" data-size="small" data-color="primary" <?=$Website['checked'][$rows1['display']]?>/>
									</div>
								</div>
								
								<div class="form-group required">
									<label class="form-label" for="name"><?=my_lang('name')?></label>
									<span class="help help-inline"><?=my_lang('name_help')?></span>
									<input type="text" class="form-control required" id="name" name="name" placeholder="<?=my_lang('name_placeholder')?>" value="<?=$rows1['name']?>">
								</div>

								<? if($rows1['insert_time']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('insert_time')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=date('Y/m/d H:i:s', $rows1['insert_time'])?>
									</div>
								</div>
								<? }?>
								<? if($rows1['modify_time']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('modify_time')?></label>
									<span class="help help-inline"></span>
									<div>
										<?=date('Y/m/d H:i:s', $rows1['modify_time'])?>
									</div>
								</div>
								<? }?>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('meta')?></div>
							</div>
							<div class="panel-body">
								<div class="form-group required">
									<label class="form-label" for="description"><?=my_lang('description')?></label>
									<span class="help help-inline"><?=my_lang('description_help')?></span>
									<input type="text" class="form-control required" id="description" name="description" placeholder="<?=my_lang('description_placeholder')?>" value="<?=$rows1['description']?>">
								</div>
								
								<div class="form-group">
									<div>
										<a href="javascript:;" class="btn btn-primary ogimage_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> <?=my_lang('ogimage')?></a>
										<div class="alert alert-info"><?=my_lang('ogimage_help')?></div>
										<div class="row ogimage_area"><?=$rows1['ogimage_area']?></div>
										<input id="ogimage_input" name="ogimage_input" class="ogimage_input" type="hidden" value="<?=$rows1['ogimage_input']?>" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="form-group">
									<label class="form-label" for="content"><?=my_lang('content')?></label>
									<span class="help help-inline"></span>
									<textarea class="form-control" id="content" name="content"><?=$rows1['content']?></textarea>
								</div>

								<div id="toolbar"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-check"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
									<!--
									<input type="button" class="btn btn-info btn-lg save_publish" value="儲存並發佈">
									<input type="button" class="btn btn-success btn-lg save_publish_back" value="儲存發佈回列表">
									-->
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>
