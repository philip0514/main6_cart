<div class="modal fade stick-up" id="category_edit" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<form id="form2" name="form2" action="admin/category/ajax_item/" method="post" enctype="multipart/form-data" onsubmit="javascript:; return false;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h3 class="modal-title"><?=my_lang('admin_category.modify')?></h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="panel simple">
								<div class="panel-body no-border">
									<div class="form-group">
										<div>
											<div class="checkbox check-primary">
												<input type="checkbox" name="display" id="display" value="1" <?=$Website['checked'][$rows1['display']]?> />
												<label for="display"><?=my_lang('display')?></label>
											</div>
										</div>
									</div>
									
									<div class="form-group required">
										<label class="form-label" for="name"><?=my_lang('name')?></label>
										<span class="help-inline help"><?=my_lang('name_help')?></span>
										<div>
											<input type="text" class="form-control required" id="name" name="name" placeholder="<?=my_lang('name_placeholder')?>" value="<?=$rows1['name']?>">
										</div>
									</div>
									
									<div class="form-group">
										<label class="form-label" for="url_name"><?=my_lang('admin_category.url_name')?></label>
										<span class="help-inline help"><?=my_lang('admin_category.url_name_help')?></span>
										<div>
											<input type="text" class="form-control" id="url_name" name="url_name" placeholder="<?=my_lang('admin_category.url_name_placeholder')?>" value="<?=$rows1['url_name']?>">
										</div>
									</div>
								
									<div class="form-group">
										<div>
											<a href="javascript:;" class="btn btn-primary ogimage_manager m-t-10 m-b-10"><i class="fa fa-plus"></i> <?=my_lang('ogimage')?></a>
											<div class="alert alert-info"><?=my_lang('ogimage_help')?></div>
											<div class="row ogimage_area"><?=$rows1['ogimage_area']?></div>
											<input id="ogimage_input" name="ogimage_input" class="ogimage_input" type="hidden" value="<?=$rows1['ogimage_input']?>" />
										</div>
									</div>

									<? if($rows1['insert_time']){?>
									<div class="form-group">
										<label class="form-label"><?=my_lang('insert_time')?></label>
										<span class="help help-inline"></span>
										<div>
											<?=date('Y/m/d H:i:s', $rows1['insert_time'])?>
										</div>
									</div>
									<? }?>
									<? if($rows1['modify_time']){?>
									<div class="form-group">
										<label class="form-label"><?=my_lang('modify_time')?></label>
										<span class="help help-inline"></span>
										<div>
											<?=date('Y/m/d H:i:s', $rows1['modify_time'])?>
										</div>
									</div>
									<? }?>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="modal-footer">
					<input id="id" name="id" type="hidden" value="<?=$rows1['id']?>" />
					<input id="new_id" name="new_id" type="hidden" value="<?=$rows1['new_id']?>" />
					<input id="here" name="here" type="hidden" value="1" />
					<a class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> <?=my_lang('cancel')?></a>
					<button class="btn btn-primary" id="category_save" type="submit"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
					
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal --> 


<script>
$(function(){
	<?=$validation?>
	
	media_ogimage(3);
});
</script>