<link type="text/css" rel="stylesheet" href="library/nested_sortable/nestedSortable.css" />

<script type="text/javascript" src="library/touch-punch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="library/nested_sortable/jquery.mjs.nestedSortable.js"></script>
<script type="text/javascript" src="theme/admin/js/nested_sortable.js"></script>
<script>
$(function(){
	nested_sortable.init();
})
</script>
<style type="text/css">
.modal:nth-of-type(even) {
    z-index: 1040 !important;
}
.modal-backdrop.in:nth-of-type(even) {
    z-index: 1039 !important;
}
    
</style>

<div class="page-content-wrapper bg-white">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="admin/category/">
			<div class="container">
				<div class="row">
					
					
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-body">
								
								<div class="btn-group">
									<button type="button" name="new_item" id="new_item" class="btn btn-primary"><i class="fa fa-plus"></i> <?=my_lang('admin_category.insert')?></button>
									<button type="button" name="expanded" id="expanded" class="btn btn-success"><i class="fa fa-plus-square-o"></i> <?=my_lang('admin_category.expanded')?></button>
									<button type="button" name="collapsed" id="collapsed" class="btn btn-success"><i class="fa fa-minus-square-o"></i> <?=my_lang('admin_category.collapsed')?></button>
								</div>
								<div class="alert alert-info m-t-20 p-t-20">
									<div class="row sortable_info">
										<div class="col-xl-2 col-md-3"><i class="fa fa-sort"></i> <?=my_lang('sort')?></div>
										<div class="col-xl-2 col-md-3"><i class="fa fa-pencil"></i> <?=my_lang('edit')?></div>
										<div class="col-xl-2 col-md-3"><i class="fa fa-trash"></i> <?=my_lang('delete')?></div>
									</div>
								</div>

								<hr />

								<?=$nested?>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
			<div id="toolbar"></div>

			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-check"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="ssort" name="ssort" value="">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>