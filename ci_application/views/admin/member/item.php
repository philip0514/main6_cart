<script>
$(function(){
	<?=$validation?>
	date_datetimepicker('#birth');
	use_member();
	zip_code('#city_id', '#area_id');
	
	$('.save_normal').click(function(e) {
		$('#here').val(1);
	});
	
	$('.save_publish_back').click(function(){
		$('#here').val(3);
		$('#form1').submit();
	});
});
</script>

<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="admin/member/item/">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('setting')?></div>
							</div>
							<div class="panel-body">
								
								<? if($rows1['id']){?>
								<div class="form-group">
									<a href="javascript:;" class="btn btn-primary use_member" value="<?=$rows1['id']?>"><?=my_lang('admin_member.identity')?></a>
								</div>
								<? }?>

								<div class="form-group">
									<label class="form-label"><?=my_lang('display')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="checkbox" id="display" name="display" value="1" data-init-plugin="switchery" data-size="small" data-color="primary" <?=$Website['checked'][$rows1['display']]?>/>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label"><?=my_lang('admin_member.verified')?></label>
									<div>
										<input type="checkbox" id="checked" name="checked" value="1" data-init-plugin="switchery" data-size="small" data-color="primary" <?=$Website['checked'][$rows1['checked']]?>/>
									</div>
								</div>

								<? if($rows1['facebook_id']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('admin_member.facebook_id')?></label>
									<div>
										<p class="form-control-static">
											<?=$rows1['facebook_id']?>
										</p>
										<span class="help-inline help-block"></span>
									</div>
								</div>
								<? }?>

								<div class="form-group required">
									<label class="form-label" for="name"><?=my_lang('admin_member.name')?></label>
									<span class="help help-inline"><?=my_lang('admin_member.name_help')?></span>
									<div>
										<input type="text" class="form-control required" id="name" name="name" placeholder="<?=my_lang('admin_member.name_placeholder')?>" value="<?=$rows1['name']?>">
									</div>
								</div>

								<div class="form-group required">
									<label class="form-label" for="account"><?=my_lang('account')?></label>
									<span class="help help-inline"><?=my_lang('admin_member.account_help')?></span>
									<div>
										<input type="text" class="form-control required" id="account" name="account" placeholder="<?=my_lang('admin_member.account_placeholder')?>" value="<?=$rows1['account']?>">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="password"><?=my_lang('password')?></label>
									<span class="help help-inline"><?=my_lang('admin_member.password_help')?></span>
									<div>
										<input type="password" class="form-control" id="password" name="password" placeholder="<?=my_lang('admin_member.password_placeholder')?>" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('admin_member.personal_data')?></div>
							</div>
							<div class="panel-body">
								
								<div class="form-group">
									<label class="form-label" for="gender"><?=my_lang('admin_member.gender')?></label>
									<span class="help help-inline"></span>
									<div>
										<div class="radio radio-primary">
											<input type="radio" name="gender" id="gender1" value="1" <? if($rows1['gender']==1 || !$rows1['gender']) echo 'checked';?> />
											<label for="gender1"><?=my_lang('admin_member.gender1')?></label>
											<input type="radio" name="gender" id="gender2" value="2" <? if($rows1['gender']==2) echo 'checked';?> />
											<label for="gender2"><?=my_lang('admin_member.gender2')?></label>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="birth"><?=my_lang('admin_member.birthday')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="birth" name="birth" placeholder="<?=my_lang('admin_member.birthday_placeholder')?>" value="<?=$rows1['birth_text']?>">
									</div>
								</div>


								<div class="form-group">
									<label class="form-label"><?=my_lang('admin_member.city')?></label>
									<span class="help help-inline"></span>
									<div>
										<select class="form-control" id="city_id" name="city_id">
											<option value="0"><?=my_lang('admin_member.city_null')?></option>
											<? for($i=0; $i<sizeof($rows2); $i++){?>
											<option value="<?=$rows2[$i]['id']?>" 
												<?=$rows2[$i]['id']==$rows1['city_id']?'selected':''?>
												>
												<?=$rows2[$i]['name']?>
											</option>
											<? }?>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="area_id"><?=my_lang('admin_member.area')?></label>
									<span class="help help-inline"></span>
									<div>
										<select class="form-control" id="area_id" name="area_id">
											<option value="0"><?=my_lang('admin_member.area_null')?></option>
											<? for($i=0; $i<sizeof($rows3); $i++){?>
											<option value="<?=$rows3[$i]['id']?>" 
												<?=$rows3[$i]['id']==$rows1['area_id']?'selected':''?>
												>
												<?=$rows3[$i]['name'].' '.$rows3[$i]['code']?>
											</option>
											<? }?>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="address"><?=my_lang('address')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="address" name="address" placeholder="<?=my_lang('address')?>" value="<?=$rows1['address']?>">
									</div>
								</div>


								<div class="form-group">
									<label class="form-label" for="mobile"><?=my_lang('mobile')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="mobile" name="mobile" placeholder="<?=my_lang('mobile')?>" value="<?=$rows1['mobile']?>">
									</div>
								</div>
								
								<? if($rows1['insert_time']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('admin_member.insert_time')?></label>
									<div>
										<p class="form-control-static">
											<?=date('Y/m/d H:i:s', $rows1['insert_time'])?>
										</p>
										<span class="help-inline help-block"></span>
									</div>
								</div>
								<? }?>

								<? if($rows1['modify_time']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('modify_time')?></label>
									<div>
										<p class="form-control-static">
											<?=date('Y/m/d H:i:s', $rows1['modify_time'])?>
										</p>
										<span class="help-inline help-block"></span>
									</div>
								</div>
								<? }?>

								<? if($rows1['login_time']){?>
								<div class="form-group">
									<label class="form-label"><?=my_lang('admin_member.last_login_time')?></label>
									<div>
										<p class="form-control-static">
											<?=date('Y/m/d H:i:s', $rows1['login_time'])?>
										</p>
										<span class="help-inline help-block"></span>
									</div>
								</div>
								<? }?>
									
									
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="toolbar"></div>

			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-ok"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
									<button type="button" class="btn btn-success btn-lg save_publish_back"><i class="fa fa-upload"></i> <?=my_lang('submit_and_return')?></button>
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>