<!DOCTYPE html>
<html class=" js no-touch csstransforms3d csstransitions">
<head>
	<base href="<?=base_url();?>"/>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="robots" content="noindex">
	<meta name="googlebot" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
	<meta content="" name="description">
	<meta content="" name="author">
	<title>
	<?=$Website['page_header']?>
	|
	<?=$Website['admin_title']?>
	</title>

	<link rel="stylesheet" type="text/css" href="library/pace/pace-theme-flash.css">
	<link rel="stylesheet" type="text/css" href="library/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="library/font-awesome/css/font-awesome.css"/>
	<link rel="stylesheet" type="text/css" href="library/jquery-scrollbar/jquery.scrollbar.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="library/bootstrap-select/css/bootstrap-select.min.css">
	<link rel="stylesheet" type="text/css" href="library/switchery/css/switchery.min.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="theme/pages/css/pages-icons.css">
	<link rel="stylesheet" type="text/css" href="theme/pages/css/pages.css" class="main-stylesheet">
    <link rel="stylesheet" type="text/css" href="theme/flag/css/flag-icon.css">
	<link rel="stylesheet" type="text/css" href="theme/admin/css/admin.css"/>

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>


	<!--[if lte IE 9]>
		<link href="theme/pages/css/ie9.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<script type="text/javascript">
		window.onload = function () {
			// fix for windows 8
			if ( navigator.appVersion.indexOf( "Windows NT 6.2" ) != -1 )
				document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="theme/pages/css/windows.chrome.fix.css" />';
		}
	</script>
	<style>
		.btn-group.bootstrap-select{
			width: 100% !important;
		}
	</style>
</head>
<body class="fixed-header ">
	<div class="login-wrapper ">

		<div class="bg-pic">
			<img src="theme/pages/img/night.jpg" data-src="theme/pages/img/night.jpg" data-src-retina="theme/pages/img/night.jpg" alt="" class="lazy">
		</div>
		
		<div class="login-container bg-white">
			<div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
				<p class="p-t-35"><h3><?=$Website['admin_title']?></h3></p>

				<form role="form" id="form1" name="form1" action="" method="post" class="form-login p-t-15">

					<div class="form-group form-group-default">
						<label><?=my_lang('account')?></label>
						<div class="controls">
							<input class="form-control" type="text" name="account" id="account" placeholder="<?=my_lang('account')?>">
						</div>
					</div>

					<div class="form-group form-group-default">
						<label><?=my_lang('password')?></label>
						<div class="controls">
							<input class="form-control" type="password" name="password" id="password" placeholder="<?=my_lang('password')?>">
						</div>
					</div>

						<div class="controls">
							<select class="selectpicker" name="language_setting" id="language_setting">
								<? for($i=0; $i<sizeof($Website['language']); $i++){?>
								<option value="<?=$Website['language'][$i]['code']?>" <?=($_SESSION['user_info']['site_lang']==$Website['language'][$i]['language'])?'selected':''?> data-content="<span class='<?=$Website['language'][$i]['icon']?>'></span> <?=$Website['language'][$i]['name']?>"></option>
								<? }?>
							</select>
						</div>

					<button class="btn btn-primary btn-cons m-t-10" type="submit"><?=my_lang('login')?></button>
					<input type="hidden" name="here" id="here" value="1" />
					<input type="hidden" id="<?=$csrf['name'];?>" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					
					<? if($error){?>
					<div class="row p-t-10 m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
						<div class="control-group col-md-12">
							<div class="alert alert-danger">
								<?=my_lang('login_error')?>
							</div>
						</div>
					</div>
					<? }?>
				</form>

				
			</div>
		</div>
	</div>
</body>

<script type="text/javascript" src="library/pace/pace.min.js"></script>
<script type="text/javascript" src="library/modernizr.custom.js"></script>
<script type="text/javascript" src="library/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="library/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="library/bootstrap-select/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="library/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="library/jquery-validate/jquery.validate.min.js"></script> 
<script type="text/javascript" src="library/jquery-validate/jquery.form.js"></script> 

<script type="text/javascript" src="theme/pages/js/pages.js"></script>
<script type="text/javascript" src="theme/admin/js/admin.js"></script>
<script type="text/javascript" src="theme/admin/js/admin_validate.js"></script>

<script>
$(function(){
	//validate_form('admin_login');
	<?=$validation?>
	$('#account').focus();
	csrf();
})	
</script>
</html>