<script>
$(function(){
	<?=$validation?>
	
	bs_datetimepicker('#payment_date');
	bs_datetimepicker('#shipping_date');
	bs_datetimepicker('#restart_payment_end');
	
	use_member();
	
	zip_code('#buyer_city_id', '#buyer_area_id');
	zip_code('#recipient_city_id', '#recipient_area_id');
	
	$('#discount_price, #freight').change(function(e) {
		var subprice = parseInt($('.subprice').html());
		var discount_price = parseInt($('#discount_price').val());
		var freight = parseInt($('#freight').val());
		
		var total_price = subprice-discount_price+freight;
		$('.total_price').html(total_price);
		$('#total_price').val(total_price);
		
	});
});
</script>
<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-striped cart">
							<thead>
								<tr>
									<th>#</th>
									<th>商品名稱</th>
									<th>規格</th>
									<th>成本</th>
									<th width="150">數量</th>
									<th width="150">小計</th>
								</tr>
							</thead>
							<tbody>
								<? 
								for($i=0; $i<sizeof($rows2); $i++){
									$product_total_price += $rows2[$i]['price'];
									
									$label = '';
									switch($rows2[$i]['plus_type']){
										default:
										case 0:
											$label = '';
											break;
										case 1:
											$label = '<span class="label label-primary">贈品</span> ';
											break;
										case 2:
											$label = '<span class="label label-success">加價購</span> ';
											break;
									}
									
								?>
								<tr>
									<td><?=$i+1?></td>
									<td><?=$label?> <a href="admin/product/item/<?=$rows2[$i]['product_id']?>/" targasdet="_blank"><?=$rows2[$i]['name'].' - '.$rows2[$i]['product_id']?></a></td>
									<td><?=$rows2[$i]['spec_name']?></td>
									<td><?=$rows2[$i]['cost_price']?></td>
									<td><?=$rows2[$i]['quantity']?></td>
									<td>
										<div class="price_count"><?=$rows2[$i]['price']?></div>
										<input type="hidden" id="inventory_id[]" name="inventory_id[]" value="<?=$rows2[$i]['id']?>">
										
										<input type="hidden" id="product_id[]" name="product_id[]" value="<?=$rows2[$i]['product_id']?>">
										<input type="hidden" id="spec_id[]" name="spec_id[]" value="<?=$rows2[$i]['spec_id']?>">
										<input type="hidden" id="quantity[]" name="quantity[]" value="<?=$rows2[$i]['quantity']?>">
									</td>
								</tr>
								<? }?>
								
								<tr>
									<td class="text-right" colspan="5">
										商品總金額 $
									</td>
									<td>
										<div class="subprice"><?=(int)($rows1['subprice'])?></div>
									</td>
								</tr>
								
								<tr>
									<td class="text-right" colspan="4" style="vertical-align:bottom;">
										<div>
											折扣方式：<?=$Website['discount_type'][$rows1['discount_type']]['name']?>
										</div>
										<div>
											<? switch($rows1['discount_type']){
												default:
												case 0:
													break;
												case 1:
													break;
												case 2:
												case 3:
													echo '使用優惠卷：'.$rows8['name'].'<br> 金額：'.$rows8['discount'].($rows8['discount_type']?'%':'元');
													break;
											}?>
										</div>
									</td>
									<td class="text-right" style="vertical-align:bottom;">折扣 - $</td>
									<td style="vertical-align:bottom;">
										<input type="text" class="form-control" id="discount_price" name="discount_price" placeholder="折扣金額" value="<?=$rows1['discount_price']?>"/>
									</td>
								</tr>
								
								<tr>
									<td class="text-right" colspan="5" style="vertical-align:bottom;">運費 $</td>
									<td style="vertical-align:bottom;">
									<div class="freight">
										<input type="text" class="form-control" id="freight" name="freight" placeholder="運費" value="<?=$rows1['freight']?>"/>
									</div>
									</td>
								</tr>
								<tr>
									<td class="text-right" colspan="5" style="vertical-align:bottom;">總計 $</td>
									<td style="vertical-align:bottom;">
										<div class="total_price">
											<?=$rows1['total_price']?>
										</div>
										<input type="hidden" id="total_price" name="total_price"value="<?=$rows1['total_price']?>"/>
									</td>
								</tr>
								
							</tbody>
						</table>
					</div>
				</div>
				
				
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">購買人資料</div>
							</div>
							<div class="panel-body">

								<div class="form-group">
									<label class="form-label" for="buyer_name">姓名</label>
									<span class="help help-inline">必填</span>
									<div>
										<input type="text" class="form-control required" id="buyer_name" name="buyer_name" placeholder="購買人姓名 必填" value="<?=$rows1['buyer_name']?>">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label">地址</label>
									<div class="p-b-10">
										<select class="form-control selectpicker" id="buyer_city_id" name="buyer_city_id">
											<? for($i=0; $i<sizeof($rows4); $i++){?>
											<option value="<?=$rows4[$i]['id']?>" 
												<?=$rows4[$i]['id']==$rows1['buyer_city_id']?'selected':''?>
												>
												<?=$rows4[$i]['name']?>
											</option>
											<? }?>
										</select>
									</div>
									<div>
										<select class="form-control selectpicker" id="buyer_area_id" name="buyer_area_id">
											<? for($i=0; $i<sizeof($rows5); $i++){?>
											<option value="<?=$rows5[$i]['id']?>" 
												<?=$rows5[$i]['id']==$rows1['buyer_area_id']?'selected':''?>
												>
												<?=$rows5[$i]['name'].' '.$rows5[$i]['code']?>
											</option>
											<? }?>
										</select>
									</div>
								</div>

								<div class="form-group">
									<div>
										<input type="text" class="form-control required" id="buyer_address" name="buyer_address" placeholder="地址" value="<?=$rows1['buyer_address']?>">
									</div>
									<span class="help">必填</span>
								</div>

								<div class="form-group">
									<label class="form-label" for="buyer_mobile">行動電話</label>
									<span class="help help-inline">必填</span>
									<div>
										<input type="text" class="form-control required" id="buyer_mobile" name="buyer_mobile" placeholder="購買人手機 必填" value="<?=$rows1['buyer_mobile']?>">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="buyer_phone">住家電話</label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="buyer_phone" name="buyer_phone" placeholder="購買人電話" value="<?=$rows1['buyer_phone']?>">
									</div>
								</div>

							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">收件人資料</div>
							</div>
							<div class="panel-body">

								<div class="form-group">
									<label class="form-label" for="recipient_name">姓名</label>
									<span class="help help-inline">必填</span>
									<div>
										<input type="text" class="form-control required" id="recipient_name" name="recipient_name" placeholder="收件人姓名 必填" value="<?=$rows1['recipient_name']?>">
									</div>
								</div>
								
								<div class="form-group">
									<label class="form-label">地址</label>
									<div class="p-b-10">
										<select class="form-control selectpicker" id="recipient_city_id" name="recipient_city_id">
											<? for($i=0; $i<sizeof($rows6); $i++){?>
											<option value="<?=$rows6[$i]['id']?>" 
												<?=$rows6[$i]['id']==$rows1['recipient_city_id']?'selected':''?>
												>
												<?=$rows6[$i]['name']?>
											</option>
											<? }?>
										</select>
									</div>
									<div>
										<select class="form-control selectpicker" id="recipient_area_id" name="recipient_area_id">
											<? for($i=0; $i<sizeof($rows7); $i++){?>
											<option value="<?=$rows7[$i]['id']?>" 
												<?=$rows7[$i]['id']==$rows1['recipient_area_id']?'selected':''?>
												>
												<?=$rows7[$i]['name'].' '.$rows7[$i]['code']?>
											</option>
											<? }?>
										</select>
									</div>
								</div>

								<div class="form-group">
									<div>
										<input type="text" class="form-control required" id="recipient_address" name="recipient_address" placeholder="地址" value="<?=$rows1['recipient_address']?>">
									</div>
									<span class="help">必填</span>
								</div>
								
								<div class="form-group">
									<label class="form-label" for="recipient_mobile">行動電話</label>
									<span class="help help-inline">必填</span>
									<div>
										<input type="text" class="form-control required" id="recipient_mobile" name="recipient_mobile" placeholder="收件人手機 必填" value="<?=$rows1['recipient_mobile']?>">
									</div>
								</div>
								
								<div class="form-group">
									<label class="form-label" for="recipient_phone">住家電話</label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="recipient_phone" name="recipient_phone" placeholder="收件人電話" value="<?=$rows1['recipient_phone']?>">
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="recipient_note">備註</label>
									<span class="help help-inline"></span>
									<div>
										<textarea class="form-control" style="height:100px;" id="recipient_note" name="recipient_note" placeholder="備註" ><?=$rows1['recipient_note']?></textarea>
									</div>
								</div>

							</div>
						</div>
					</div>

				</div>
				
				
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">付款方式</div>
							</div>
							<div class="panel-body">
								<ul class="list-unstyled">
									<?
									for($i=0; $i<sizeof($Website['payment_type']); $i++){
										if(!$Website['payment_type'][$i]['display']){
											continue;
										}
										$checked = '';
										if($i==$rows1['payment_type']){
											$checked = 'checked';
										}
									?>
									<li>
										<label>
											<input type="radio" id="payment_type" name="payment_type" value="<?=$i?>" <?=$checked?> />
											<?=$Website['payment_type'][$i]['name']?>
										</label>
									</li>
									<? }?>
									<li class="p-t-10">
										<div class="input-group">
											<span class="input-group-addon">匯款後五碼</span>
											<input type="text" id="atm_number" name="atm_number" value="<?=$rows1['atm_number']?>" />
										</div>
									</li>
								</ul>
								
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">訂單狀態</div>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6">
										<ul class="list-unstyled">
											<?
											for($i=0; $i<6; $i++){
												$checked = '';
												if($i==$rows1['payment_status']){
													$checked = 'checked';
												}

												if($i==4){
													continue;
												}
											?>
											<li>
												<label>
													<input type="radio" id="payment_status" name="payment_status" value="<?=$i?>" <?=$checked?> />
													<?=$Website['payment_status'][$i]['name']?>
												</label>
											</li>
											<? }?>
										</ul>
									</div>
									<div class="col-md-6">
										<ul class="list-unstyled">
											<?
											for($i=6; $i<sizeof($Website['payment_status']); $i++){
												$checked = '';
												if($i==$rows1['payment_status']){
													$checked = 'checked';
												}
											?>
											<li>
												<label>
													<input type="radio" id="payment_status" name="payment_status" value="<?=$i?>" <?=$checked?> />
													<?=$Website['payment_status'][$i]['name']?>
												</label>
											</li>
											<? }?>
										</ul>
									</div>
								</div>
								
								<input type="hidden" id="payment_status_original" name="payment_status_original" value="<?=$rows1['payment_status']?>" />
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">出貨方式</div>
							</div>
							<div class="panel-body">
								<ul class="list-unstyled">
									<?
									for($i=0; $i<sizeof($Website['shipping_type']); $i++){
										$checked = '';
										if($i==$rows1['shipping_type']){
											$checked = 'checked';
										}
									?>
									<li>
										<label>
											<input type="radio" id="shipping_type" name="shipping_type" value="<?=$i?>" <?=$checked?> />
											<?=$Website['shipping_type'][$i]['name']?>
										</label>
									</li>
									<? }?>
								</ul>
							</div>
						</div>
					</div>
					
					<div class="col-lg-4 col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">出貨狀態</div>
							</div>
							<div class="panel-body">
								
								<ul class="list-unstyled">
									<?
									for($i=0; $i<sizeof($Website['shipping_status']); $i++){

										$checked = '';
										if($i==$rows1['shipping_status']){
											$checked = 'checked';
										}
									?>
									<li>
										<label>
											<input type="radio" id="shipping_status" name="shipping_status" value="<?=$i?>" <?=$checked?> />
											<?=$Website['shipping_status'][$i]['name']?>
										</label>
									</li>
									<? }?>

								</ul>
								<input type="hidden" id="shipping_status_original" name="shipping_status_original" value="<?=$rows1['shipping_status']?>" />
								
								<div class="form-group">
									<label class="form-label" for="shipping_date">出貨日期</label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="shipping_date" name="shipping_date" placeholder="出貨日期" value="<?=$rows1['shipping_date'] ? date('Y/m/d', $rows1['shipping_date']) : ''?>">
									</div>
								</div>
								
							</div>
						</div>
					</div>
					
					<div class="col-lg-4 col-md-12">
						<div class="panel panel-default">
							<div class="row">
								<div class="col-lg-12 col-xs-6">
									<div class="panel-heading">
										<div class="panel-title">收貨設定</div>
									</div>
									<div class="panel-body">
										<ul class="list-unstyled">
											<?
											for($i=0; $i<sizeof($Website['shipping_day']); $i++){

												$checked = '';
												if($i==$rows1['shipping_day']){
													$checked = 'checked';
												}
											?>
											<li>
												<label>
													<input type="radio" id="shipping_day" name="shipping_day" value="<?=$i?>" <?=$checked?> />
													<?=$Website['shipping_day'][$i]['name']?>
												</label>
											</li>
											<? }?>
										</ul>
									</div>
								</div>
								<div class="col-lg-12 col-xs-6">
									<div class="panel-heading">
										<div class="panel-title">收貨時間</div>
									</div>
									<div class="panel-body">
										<ul class="list-unstyled">
											<?
											for($i=0; $i<sizeof($Website['shipping_time']); $i++){

												$checked = '';
												if($i==$rows1['shipping_time']){
													$checked = 'checked';
												}
											?>
											<li>
												<label>
													<input type="radio" id="shipping_time" name="shipping_time" value="<?=$i?>" <?=$checked?> />
													<?=$Website['shipping_time'][$i]['name']?>
												</label>
											</li>
											<? }?>

										</ul>

									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">發票</div>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-4 col-md-6 p-b-20">
										<label><input type="radio" id="invoice" name="invoice" value="0" <?=(!$rows1['invoice'])?'checked':''?>> <?=$Website['invoice'][0]['name']?></label>
										
										<div class="p-t-20 p-l-20 p-b-20">
											<select class="form-control selectpicker" id="invoice_donate" name="invoice_donate">
												<? for($i=0; $i<sizeof($Website['invoice_donate']); $i++){?>
												<option value="<?=$Website['invoice_donate'][$i]['id']?>" <?=($Website['invoice_donate'][$i]['id']==$rows1['invoice_donate'])?'selected':''?>><?=$Website['invoice_donate'][$i]['name']?></option>
												<? }?>
											</select>
										</div>
									</div>
									<div class="col-lg-4 col-md-6">
										<label><input type="radio" id="invoice" name="invoice" value="1" <?=($rows1['invoice']==1)?'checked':''?>> <?=$Website['invoice'][1]['name']?></label>
										
										<div class="p-t-20 p-l-20 p-b-20">
											<? if($Website['invoice_duplicate'][0]['display']){?>
											<div class="p-b-10">
												<label>
													<input type="radio" name="invoice_duplicate" value="0" <?=(!$rows1['invoice_duplicate'])?'checked':''?>> 
													<?=$Website['invoice_duplicate'][0]['name']?>
												</label>
											</div>
											<? }?>

											<? if($Website['invoice_duplicate'][1]['display']){?>
											<div class="p-b-10">
												<label>
													<input type="radio" name="invoice_duplicate" value="1" <?=($rows1['invoice_duplicate']==1)?'checked':''?>> 
													<?=$Website['invoice_duplicate'][1]['name']?>
												</label>
											</div>
											<? }?>


											<? if($Website['invoice_duplicate'][2]['display']){?>
											<div class="p-b-10">
												<label>
													<input type="radio" name="invoice_duplicate" value="2" <?=($rows1['invoice_duplicate']==2)?'checked':''?>> 
													<?=$Website['invoice_duplicate'][2]['name']?>
												</label>
												<div>
													<input class="form-control" name="invoice_mobile" value="<?=$rows1['invoice_mobile']?>" placeholder="請輸入手機條碼載具(限大寫英數字)">
												</div>
											</div>
											<? }?>

											<? if($Website['invoice_duplicate'][3]['display']){?>
											<div class="p-b-10">
												<label>
													<input type="radio" name="invoice_duplicate" value="3" <?=($rows1['invoice_duplicate']==3)?'checked':''?>> 
													<?=$Website['invoice_duplicate'][3]['name']?>
												</label>
												<div class="p-b-10">
													<input class="form-control" name="invoice_emcard" value="<?=$rows1['invoice_emcard']?>" placeholder="請輸入自然人憑證載具(限英數字)">
												</div>
											</div>
											<? }?>
										</div>
									</div>
									<div class="col-lg-4 col-md-12">
										<label><input type="radio" id="invoice" name="invoice" value="2" <?=($rows1['invoice']==2)?'checked':''?>> <?=$Website['invoice'][2]['name']?></label>
										
										<div class="p-t-20 p-l-20 p-b-20">
											<div class="form-group">
												<input type="text" class="form-control" name="invoice_guid" id="invoice_guid" placeholder="統一編號" value="<?=$rows1['invoice_guid']?>" />
												<span class="help"></span>
											</div>
											<div class="form-group">
												<input type="text" class="form-control" name="invoice_title" id="invoice_title" placeholder="公司抬頭" value="<?=$rows1['invoice_title']?>" />
												<span class="help"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">訂單設定</div>
							</div>
							<div class="panel-body">
								<div class="form-group">
									<label class="form-label">上/下架</label>
									<span class="help help-inline"></span>
									<div>
										<div class="checkbox check-success">
											<input type="checkbox" name="display" id="display" value="1" <?=$Website['checked'][$rows1['display']]?> />
											<label for="display">是否顯示在使用者前台頁面中</label>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label" for="payment_date">付款時間</label>
									<div>
										<input type="text" class="form-control" id="payment_date" name="payment_date" placeholder="付款時間" value="<?=$rows1['payment_date']?date('Y/m/d H:i:s',$rows1['payment_date']):''?>">
										<span class="help-inline"></span>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label">IP</label>
									<div>
										<?=$rows1['ip']?>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label">訂單時間</label>
									<div>
										<?=$rows1['insert_time']?date('Y/m/d H:i:s', $rows1['insert_time']):''?>
									</div>
								</div>

								<div class="form-group">
									<label class="form-label">最後修改時間</label>
									<div>
										<?=$rows1['modify_time']?date('Y/m/d H:i:s', $rows1['modify_time']):''?>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">重啟付款</div>
							</div>
							<div class="panel-body">
								<div class="alert alert-warning">
									<h6>
										<strong>注意！</strong>
									</h6>
									如原本狀態為：付款失敗、取消付款、內部作廢、取消訂單完成、退貨完成、訂單逾期，<br>
									欲開啟重啟付款，則記得手動扣除庫存，否則庫存會有誤差
								</div>
								<ul class="list-unstyled">
									<li class="li_left">
										<label>
											<input type="radio" id="restart_payment" name="restart_payment" value="0" 
											<?=!$rows1['restart_payment']?'checked':''?> />
											關閉
										</label>
									</li>
									<li class="li_left">
										<label>
											<input type="radio" id="restart_payment" name="restart_payment" value="1" 
											<?=$rows1['restart_payment']?'checked':''?> />
											開啟
										</label>
									</li>
									<li class="li_left">
										<label for="restart_payment_end">最後付款日期：</label>
										<input type="text" class="form-control" id="restart_payment_end" name="restart_payment_end" value="<?=$rows1['restart_payment_end']?date('Y/m/d H:i:s',$rows1['restart_payment_end']):''?>"
										placeholder="最後付款日期：YYYY/mm/dd，如不設定，則無法開啟付款"
										/>
									</li>

								</ul>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">付款單號</div>
							</div>
							<div class="panel-body">
								<ul class="list-unstyled">
									<? for($i=0; $i<sizeof($rows3); $i++){?>
									<li>
										<?=$rows3[$i]['id']?>
									</li>
									<? }?>
								</ul>
								
							</div>
						</div>
					</div>
					
					<div class="col-lg-4 col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">付款結果</div>
							</div>
							<div class="panel-body">
								<pre><? print_r($payment_result);?></pre>
							</div>
						</div>
					</div>
					
					<div class="col-lg-4 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">內部備註</div>
							</div>
							<div class="panel-body">
								<textarea class="form-control" style="height:200px;" id="note_inside" name="note_inside" placeholder="內部備註" ><?=$rows1['note_inside']?></textarea>
							</div>
						</div>
					</div>
				</div>
				
				
				
				
				<!--
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"></div>
							</div>
							<div class="panel-body">
							
								
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"></div>
							</div>
							<div class="panel-body">
								
							</div>
						</div>
					</div>
				</div>
				-->
				<div id="toolbar" class="toolbar"></div>
			</div>

			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-check"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
									<!--
									<input type="button" class="btn btn-info btn-lg save_publish" value="儲存並發佈">
									<input type="button" class="btn btn-success btn-lg save_publish_back" value="儲存發佈回列表">
									-->
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>
