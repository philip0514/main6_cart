<!DOCTYPE html>
<html class=" js no-touch csstransforms3d csstransitions">
<head>
	<base href="<?=base_url();?>"/>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="robots" content="noindex">
	<meta name="googlebot" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
	<meta content="" name="description">
	<meta content="" name="author">
	<title>
	<?=implode(' - ', array($Website['page_header'], $Website['admin_title']))?>
	</title>

	<link rel="stylesheet" type="text/css" href="library/pace/pace-theme-flash.css">
	<link rel="stylesheet" type="text/css" href="library/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="library/font-awesome/css/font-awesome.css"/>
	<link rel="stylesheet" type="text/css" href="library/jquery-scrollbar/jquery.scrollbar.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="library/bootstrap-select/css/bootstrap-select.min.css">
	<link rel="stylesheet" type="text/css" href="library/bootstrap-timepicker/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" type="text/css" href="library/switchery/css/switchery.min.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="theme/pages/css/pages-icons.css">
	<link rel="stylesheet" type="text/css" href="theme/pages/css/pages.css"/>
	<link rel="stylesheet" type="text/css" href="theme/pages/css/calendar.css"/>
    <link rel="stylesheet" type="text/css" href="theme/flag/css/flag-icon.css">
	<link rel="stylesheet" type="text/css" href="theme/admin/css/admin.css"/>

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="library/bootstrap/js/bootstrap.min.js"></script>


	<!--[if lte IE 9]>
		<link href="theme/pages/css/ie9.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<script type="text/javascript">
		window.onload = function () {
			// fix for windows 8
			if ( navigator.appVersion.indexOf( "Windows NT 6.2" ) != -1 )
				document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="theme/pages/css/windows.chrome.fix.css" />';
		}
	</script>
	<script>
	var settings = <?=json_encode($settings)?>;
	$(function(){//sidebar-visible menu-pin
		$('.pin-sidebar').click(function(e) {
			var status = $('body').hasClass('menu-pin');
			$.ajax({
				url: 'request/layout_toggle/',	//檔案位置
				type: 'POST',	//or POST
				data: {
					status: status
				},
				error: function(xhr) {
					alert('Ajax request 發生錯誤');
				},
				success: function(response) {
					if(response==0){
						$('body').removeClass('menu-pin');
					}else{
						$('body').addClass('menu-pin');
					}
				}
			});
		});
		if($('#toolbar').length){
			//底部存檔bar
			toolbar_top = $('#toolbar').offset().top;
			toolbar_width = $('#toolbar').parent().width();
			toolbar_position();

			$(window).bind('scroll',function(){
				toolbar_position();
			});
		}
		
		$('#language_setting').change(function(){
			var nation = $(this).val();
			$.ajax({
				url: 'language/',	//檔案位置
				type: 'POST',		//or POST
				data: {
					nation: nation,
					admin: 1
				},
				success: function(response) {
					location.reload();
				}
			});
		})
	})
	</script>
</head>
<? //unset($_SESSION['layout_toggle'])?>
<body class="fixed-header <?=$_SESSION['layout_toggle']?'sidebar-visible menu-pin':''?>">
	
	<nav class="page-sidebar" data-pages="sidebar">
	
		<div class="sidebar-header">
			<img src="theme/admin/img/logo_white.png" alt="logo" class="brand" data-src="theme/admin/img/logo_white.png" data-src-retina="theme/admin/img/logo_white_2x.png" width="75">
			<div class="sidebar-header-controls">
				<button type="button" class="btn btn-link visible-lg-inline pin-sidebar" data-toggle-pin="sidebar">
					<i class="fa fs-12"></i>
				</button>
			</div>
		</div>
		<div class="sidebar-menu">
			
            <?=$Website['admin_menu']?>
            
			<div class="clearfix"></div>
		</div>
	</nav>
	<div class="page-container">
		<div class="header ">
			<div class="container-fluid relative">
				<div class="pull-left full-height visible-sm visible-xs">
					<div class="header-inner">
						<a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
							<span class="icon-set menu-hambuger"></span>
						</a>
					</div>
				</div>
				<div class="pull-center hidden-md hidden-lg">
					<div class="header-inner">
						<div class="brand inline">
							<img src="theme/admin/img/logo.png" alt="logo" data-src="theme/admin/img/logo.png" data-src-retina="theme/admin/img/logo_2x.png" width="75">
						</div>
					</div>
				</div>
				<div class="pull-right full-height visible-sm visible-xs">
					<div class="header-inner"></div>
				</div>
			</div>
			<div class=" pull-left sm-table hidden-xs hidden-sm">
				<div class="header-inner">
					<div class="brand inline">
						<img src="theme/admin/img/logo_2x.png" alt="logo" data-src="theme/admin/img/logo.png" data-src-retina="theme/admin/img/logo_2x.png" width="75">
					</div>
				</div>
			</div>
			<div class="pull-right">
				<div class="visible-lg visible-md m-t-10">
					<div class="pull-left p-r-10 p-t-5 fs-16 font-heading">
						<select class="selectpicker" name="language_setting" id="language_setting">
							<? for($i=0; $i<sizeof($Website['language']); $i++){?>
							<option value="<?=$Website['language'][$i]['code']?>" <?=($_SESSION['user_info']['site_lang']==$Website['language'][$i]['language'])?'selected':''?> data-content="<span class='<?=$Website['language'][$i]['icon']?>'></span> <?=$Website['language'][$i]['name']?>"></option>
							<? }?>
						</select>
					</div>
				</div>
			</div>
			<div class="pull-right">
				<div class="visible-lg visible-md m-t-10">
					<div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
						Hi, 
						<span class="semi-bold"><?=$_SESSION['user_info']['username']?></span>
					</div>
					<div class="dropdown pull-right"></div>
				</div>
			</div>
		</div>
		
		<?=$content?>

	</div>
	
	<!-- Modal -->
	<div class="modal_parent"></div>
	<div class="modal_parent_1"></div>
	<div class="modal_parent_2"></div>
	<div class="image_edit_modal_content"></div>
	<div class="media_modal_content"></div>
	
	<? if($settings['readonly']){?>
	<script>
	$(function(){
		$('.form-actions').remove();
		$('.page-content-wrapper > form input').attr('disabled', 'disabled');
	})
	</script>
	<? }?>
</body>

<script type="text/javascript" src="library/pace/pace.min.js"></script>
<script type="text/javascript" src="library/modernizr.custom.js"></script>
<script type="text/javascript" src="library/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="library/bootstrap-select/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="library/bootstrap-timepicker/js/moment.js"></script>
<script type="text/javascript" src="library/bootstrap-timepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="library/switchery/js/switchery.min.js"></script>
<script type="text/javascript" src="library/jquery-validate/jquery.validate.min.js"></script> 
<script type="text/javascript" src="library/jquery-validate/jquery.form.js"></script> 

<script type="text/javascript" src="theme/pages/js/pages.js"></script>
<script type="text/javascript" src="theme/admin/js/admin.js"></script>
<script type="text/javascript" src="theme/admin/js/admin_validate.js"></script>

<!-- media upload -->
<link rel="stylesheet" type="text/css" href="library/blueimp/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" type="text/css" href="library/fileupload/css/jquery.fileupload.css">
<link rel="stylesheet" type="text/css" href="library/fileupload/css/jquery.fileupload-ui.css">
<noscript><link rel="stylesheet" type="text/css" href="library/fileupload/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" type="text/css" href="library/fileupload/css/jquery.fileupload-ui-noscript.css"></noscript>
<script type="text/javascript" src="library/infinite-scroll/jquery.infinitescroll.min.js"></script> 
<script type="text/javascript" src="library/fileupload/js/vendor/jquery.ui.widget.js"></script>
<script type="text/javascript" src="library/blueimp/JavaScript-Templates/js/tmpl.min.js"></script>
<script type="text/javascript" src="library/blueimp/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script type="text/javascript" src="library/blueimp/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script type="text/javascript" src="library/blueimp/JavaScript-Load-Image/js/load-image.js"></script>
<script type="text/javascript" src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image-scale.js"></script>
<script type="text/javascript" src="library/blueimp/JavaScript-Load-Image/js/load-image-orientation.js"></script>
<script type="text/javascript" src="library/blueimp/JavaScript-Load-Image/js/load-image-meta.js"></script>
<script type="text/javascript" src="library/blueimp/JavaScript-Load-Image/js/load-image-exif.js"></script>
<script type="text/javascript" src="library/blueimp/JavaScript-Load-Image/js/load-image-exif-map.js"></script>
<script type="text/javascript" src="library/fileupload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="library/fileupload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="library/fileupload/js/jquery.fileupload-process.js"></script>
<script type="text/javascript" src="library/fileupload/js/jquery.fileupload-image.js"></script>
<script type="text/javascript" src="library/fileupload/js/jquery.fileupload-audio.js"></script>
<script type="text/javascript" src="library/fileupload/js/jquery.fileupload-video.js"></script>
<script type="text/javascript" src="library/fileupload/js/jquery.fileupload-validate.js"></script>
<script type="text/javascript" src="library/fileupload/js/jquery.fileupload-ui.js"></script>
<script type="text/javascript" src="theme/admin/js/media_mce.js"></script>
<script type="text/javascript" src="theme/admin/js/media_multiple.js"></script>

<!-- tinymce -->
<script type="text/javascript" src='library/tinymce/js/tinymce/tinymce.min.js'></script>



</html>