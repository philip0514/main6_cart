<script>
$(function(){
	user_item();
});
</script>

<div class="page-content-wrapper">
	<div class="content">
		<div class="jumbotron" data-pages="parallax">
			<div class="container-fluid sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$Website['page_header']?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<form id="form1" name="form1" method="post" action="">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('setting')?></div>
							</div>
							<div class="panel-body">
							
								<div class="form-group">
									<label class="form-label"><?=my_lang('display')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="checkbox" id="display" name="display" value="1" data-init-plugin="switchery" data-size="small" data-color="primary" <?=$Website['checked'][$rows1['display']]?>/>
									</div>
								</div>

								<div class="form-group required">
									<label class="form-label" for="name"><?=my_lang('name')?></label>
									<span class="help help-inline"><?=my_lang('name_help')?></span>
									<input type="text" class="form-control required" id="name" name="name" placeholder="<?=my_lang('name_placeholder')?>" value="<?=$rows1['name']?>">
								</div>
								
								<div class="form-group">
									<label class="form-label" for="description"><?=my_lang('description')?></label>
									<span class="help help-inline"></span>
									<div>
										<input type="text" class="form-control" id="description" name="description" placeholder="<?=my_lang('description_placeholder')?>" value="<?=$rows1['description']?>">
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title"><?=my_lang('admin_user_group.authority')?></div>
							</div>
							<div class="panel-body">
								<?=$nestedHTML?>
							</div>
						</div>
					</div>
					
				</div>
			</div>

			<div id="toolbar"></div>
			
			<div class="row">
				<div class="col-md-12 bg-master-lighter">
					<div class="pull-right">
						<div class="sticky_bottom p-t-10 p-r-20 p-b-10 m-b-20">
							<div class="text-right btn-save-group">
								<span class="submit_saved label label-primary"><i class="fa fa-ok"></i> <?=my_lang('saved')?></span>
								<div class="btn-group" role="group">
									<button type="submit" class="btn btn-primary btn-lg save_normal"><i class="fa fa-upload"></i> <?=my_lang('submit')?></button>
								</div>
								<input type="hidden" id="here" name="here" value="1">
								<input type="hidden" id="id" name="id" value="<?=$rows1['id']?>" />
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>