<?php
// application/core/MY_Exceptions.php
class MY_Exceptions extends CI_Exceptions {
	
	public function show_404(){
        $CI =& get_instance();
		$Website = $CI->Website;
		$title = '404 Page Not Found';
		
		$data = array(
			'title'			=>	$title,
		);
		$CI->theme->config($data);
		
		set_status_header(400);
		$data = array(
			'Website'	=>	$Website,
		);
		$CI->theme->error_views('404', $data);
		
        echo $CI->output->get_output();
        exit;
	}
	
	public function show_error($heading, $message, $template = 'general', $status_code = 500){
        $CI =& get_instance();
		$Website = $CI->Website;
		$title = $status_code.'Page Error';
		
		$data = array(
			'title'			=>	$title,
		);
		$CI->theme->config($data);
		
		if (is_cli()){
			$message = "\t".(is_array($message) ? implode("\n\t", $message) : $message);
		}else{
			set_status_header($status_code);
			$message = '<p>'.(is_array($message) ? implode('</p><p>', $message) : $message).'</p>';
		}
		
		$data = array(
			'Website'	=>	$Website,
			'message'	=>	$message,
		);
		$CI->theme->error_views('general', $data);
		
        echo $CI->output->get_output();
        exit;
	}
}
?>