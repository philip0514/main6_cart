<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Extending CI_Config
 *
 * The reason behind this is to add a new method assets_url in order
 * to use the theme library and the assets handler controller.
 *
 * @package     CodeIgniter
 * @category    Core
 * @author  Kader Bouyakoub <bkader@mail.com>, Chuang I Hao <philip0514@gmail.com>
 * @link    https://github.com/bkader
 * @link    https://twitter.com/KaderBouyakoub
 */

class MY_Config extends CI_Config
{
    /**
     * Constructor
     * @access  public
     * @param   none
     * @return  void
     */
    public function __construct()
    {
        // Prepare an instance of config array
        $this->config =& get_config();

        $_config = array();
        $config  = array();

        /* Load Theme Configration File */

        if (file_exists(APPPATH.'config/theme.php')) {
            require APPPATH.'config/theme.php';
            $_config = array_replace_recursive($_config, $config);
            $config = array();
        }

        if (file_exists(APPPATH.'config/'.ENVIRONMENT.'/theme.php')) {
            require APPPATH.'config/'.ENVIRONMENT.'/theme.php';
            $_config = array_replace_recursive($_config, $config);
            $config = array();
        }

        $this->config = array_replace_recursive($this->config, $_config);
        unset($_config, $config);
		
        log_message('info', 'MY_Config Class Initialized');
    }
    /**
     * Returns Assets URL
     * @param   string  $uri        URI to append to URL
     * @param   string  $folder     In case of a distinct folder
     * @return  string
     */
    public function assets_url($uri = '', $folder = NULL)
    {
        // We build our assets_url. If it's a full URL, we return it as it is.
        // Otherwise, we build our URL.
        if (filter_var($uri, FILTER_VALIDATE_URL) !== FALSE)
        {
            return $uri;
        }
        elseif ( ! empty($this->config['theme']['cdn_server']) && $this->config['theme']['cdn_enabled'] === TRUE)
        {
            $assets_url = $this->config['theme']['cdn_server'];
        }
        else
        {
            $assets_url = site_url();
        }

        // If no $folder set, we use the default one (assets)
        empty($folder) && $folder = 'assets';
        return rtrim($assets_url, '/').'/'.$folder.'/'.trim($uri, '/');
    }
}

/* End of file MY_Config.php */
/* Location: ./application/core/MY_Config.php */