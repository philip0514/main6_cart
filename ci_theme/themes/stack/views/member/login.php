<div class="main-container">
	<section class="height-100 imagebg text-center" data-overlay="4">
		<div class="background-image-holder" style="background: url(assets/img/inner-6.jpg); opacity: 1;">
			<img alt="background" src="assets/img/inner-6.jpg">
		</div>
		<div class="container pos-vertical-center">
			<div class="row">
				<div class="col-sm-7 col-md-5">
					<h2>Login</h2>
					<p class="lead">
						Welcome back, sign in with your account
					</p>
					
					<form class="form-horizontal" id="form1" name="form1" method="post" action="">
						<div class="row">
							
							<? if($Website['global_setting']['facebook_login']){?>
							<div class="col-sm-12">
								<a class="btn bg--facebook btn--icon btn-facebook btn-block" href="javascript:;">
									<span class="btn__text"><i class="socicon socicon-facebook"></i>LOGIN WITH FACEBOOK </span>
								</a>
							<hr>
							</div>
							<? }?>
							<div class="col-sm-12">
								<input type="text" class="form-control required" id="account" name="account" placeholder="Account as EMAIL">
							</div>
							<div class="col-sm-12">
								<input type="password" class="form-control required" id="password" name="password" placeholder="Password">
							</div>
							<div class="col-sm-12 text-left">
								<div class="input-checkbox">
									<div class="inner"></div>
									<input type="checkbox" id="remember" name="remember" value="1" />
								</div>
								<span>Remember Me</span>
							</div>
							<div class="col-sm-12">
								<button class="btn btn--primary type--uppercase" type="submit">LOGIN</button>
								<input type="hidden" class="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
							</div>
						</div>
						<!--end of row-->
					</form>
					<span class="type--fine-print block">Dont have an account yet?
						<a href="/member/register/">Create account</a>
					</span>
					<span class="type--fine-print block">Forgot your password?
						<a href="/member/forgot_password/">Get new password</a>
					</span>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
</div>
<script>
$(function(){
	<?=$validation?>
	$('.btn-facebook').click(function(){
		facebook_login();
	});
})
</script>