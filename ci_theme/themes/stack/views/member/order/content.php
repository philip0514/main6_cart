<div class="main-container">
	<section class="p-b-0">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1><?=$page_title?></h1>
					<ol class="breadcrumbs">
						<li>
							<a href="/">Home</a>
						</li>
						<li><?=$page_title?></li>
					</ol>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
	
	<section class="p-b-20">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>商品名稱</th>
								<th>售價</th>
								<th>數量</th>
								<th>小計</th>
							</tr>
						</thead>
						<tbody>
							<? 
							for($i=0; $i<sizeof($rows2); $i++){
								$subprice += $rows2[$i]['price']*$rows2[$i]['quantity'];
							?>
							<tr>
								<td><?=$i+1?></td>
								<td><?=$label?> <?=$rows2[$i]['product_name']?></td>
								<td><span class="price_sale">$ <?=$rows2[$i]['price']?></span></td>
								<td><?=$rows2[$i]['quantity']?></td>
								<td><div class="price_count">$ <?=$rows2[$i]['price']*$rows2[$i]['quantity']?></div></td>
							</tr>
							<? }?>
							<tr>
								<td class="text-right" colspan="4">
								<div>
									商品總金額：
								</div>
								</td>
								<td width="150">
									<div class="product_total_price">$ <?=(int)($subprice)?></div>
								</td>
							</tr>
							
							<tr>
								<td class="text-right" colspan="4" style="vertical-align:bottom;"><?=$Website['discount_type'][$rows1['discount_type']]['name']?>：</td>
								<td style="vertical-align:bottom;" width="150"><div class="price_shipping">- $ <?=(int)$rows1['discount_price']?></div> </td>
							</tr>
							
							<tr>
								<td class="text-right" colspan="4" style="vertical-align:bottom;">運費：</td>
								<td style="vertical-align:bottom;" width="150"><div class="price_shipping">$ <?=(int)$rows1['freight']?></div> </td>
							</tr>
							<tr>
								<td class="text-right" colspan="4" style="vertical-align:bottom;">配送日：</td>
								<td width="150"><?=$Website['shipping_day'][$rows1['shipping_day']]['name']?></td>
							</tr>
							<tr>
								<td class="text-right" colspan="4" style="vertical-align:bottom;">配送時段：</td>
								<td width="150"><?=$Website['shipping_time'][$rows1['shipping_time']]['name']?></td>
							</tr>
							<tr>
								<td class="text-right" colspan="4" style="vertical-align:bottom;">總計：</td>
								<td style="vertical-align:bottom;" width="150"><div class="price_count_total">$ <?=$rows1['total_price']?></div> </td>
							</tr>

						</tbody>
					</table>
				
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
	
	
	<section class="p-b-20">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h3>購買人資料</h3>


					<div class="form-group">
						<label class="col-sm-2 control-label" for="buyer_name">姓名</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$rows1['buyer_name']?></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="buyer_address">地址</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$rows1['buyer_zip'].' '.$rows1['buyer_city'].' '.$rows1['buyer_area'].' '.$rows1['buyer_address']?></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="buyer_phone">住家電話</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$rows1['buyer_phone']?></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="buyer_mobile">行動電話</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$rows1['buyer_mobile']?></p>
						</div>
					</div>

				</div>


				<div class="col-md-6">
					<h3>
						收件人資料 
					</h3>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="recipient_name">姓名</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$rows1['recipient_name']?></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="recipient_address">地址</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$rows1['recipient_zip'].' '.$rows1['recipient_city'].' '.$rows1['recipient_area'].' '.$rows1['recipient_address']?></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="recipient_phone">住家電話</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$rows1['recipient_phone']?></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="recipient_mobile">行動電話</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$rows1['recipient_mobile']?></p>
						</div>
					</div>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
	
	<section class="p-b-50">
		<div class="container">
			<div class="row">
				<div class="col-md-6">

					<div class="form-group">
						<label class="col-sm-2 control-label">訂單狀態</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$Website['payment_status'][$rows1['payment_status']]['name']?></p>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">訂購日期</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=date('Y/m/d H:i', $rows1['insert_time'])?></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">出貨方式</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$Website['shipping_type'][$rows1['shipping_type']]['name']?></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">付款方式</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$Website['payment_type'][$rows1['payment_type']]['name']?></p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<h3>發票</h3>
					<? if($rows1['invoice_number']){?>
					<div class="form-group">
						<label class="col-sm-2 control-label">發票號碼</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$rows1['invoice_number']?></p>
						</div>
					</div>
					<? }?>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">發票類型</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$Website['invoice'][$rows1['invoice']]['name']?></p>
						</div>
					</div>
					
					<? if(!$rows1['invoice']){?>
					<div class="form-group">
						<label class="col-sm-2 control-label">捐贈對象</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$Website['invoice_donate'][$rows1['invoice_donate']]['name']?></p>
						</div>
					</div>
					<? }?>
					
					<? 
					if($rows1['invoice']==1){
					?>
					<div class="form-group">
						<label class="col-sm-2 control-label">發票格式</label>
						<div class="col-sm-10">
							<p class="form-control-static">
					<?
						switch($rows1['invoice_duplicate']){
							default:
							case 0:
								echo $Website['invoice_duplicate'][0]['name'];
								break;
							case 1:
								echo $Website['invoice_duplicate'][1]['name'];
								break;
							case 2:
								echo $Website['invoice_duplicate'][2]['name'].'：'.$rows1['invoice_mobile'];
								break;
							case 3:
								echo $Website['invoice_duplicate'][3]['name'].'：'.$rows1['invoice_emcard'];
								break;
						}
					?>
							</p>
						</div>
					</div>
					<?
					}
					?>
					
					<? if($rows1['invoice']==2){?>
					<div class="form-group">
						<label class="col-sm-2 control-label">發票抬頭</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$rows1['invoice_title']?></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">統一編號</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?=$rows1['invoice_guid']?></p>
						</div>
					</div>
					<? }?>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
	
</div>