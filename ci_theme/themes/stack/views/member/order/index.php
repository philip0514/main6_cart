<div class="main-container">
	<section class="p-b-0">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1><?=$page_title?></h1>
					<ol class="breadcrumbs">
						<li>
							<a href="/">Home</a>
						</li>
						<li><?=$page_title?></li>
					</ol>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
	
	<section class="p-b-50">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<table class="table">
						<thead>
							<tr>
								<th>訂單編號</th>
								<th>訂購時間</th>
								<th>付款方式</th>
								<th>訂單金額</th>
								<th>訂單狀態</th>
								<th width="100" class="text-center">查看細節</th>
							</tr>
						</thead>
						<tbody>
							<? if(!sizeof($rows1)){?>
							<tr>
								<td colspan="6">
									<h3 class="text-center" style="padding-bottom:200px;">無訂單紀錄</h3>
								</td>
							</tr>
							<? }?>
							<? for($i=0; $i<sizeof($rows1); $i++){?>
							<tr>
								<td><?=$rows1[$i]['id']?></td>
								<td><?=date('Y/m/d H:i', $rows1[$i]['insert_time'])?></td>
								<td><?=$Website['payment_type'][$rows1[$i]['payment_type']]['name']?></td>
								<td>$ <?=number_format($rows1[$i]['total_price'])?></td>
								<td><?=$Website['payment_status'][$rows1[$i]['payment_status']]['name']?></td>
								<td class="text-center" valign="middle"><a href="member/order/<?=$rows1[$i]['id']?>/" class="btn btn--primary"><span class="btn__text">查看</span></a></td>
							</tr>
							<? /*
							<tr>
								<td colspan="5">
									<div class="progress table_progress">
										<? 
										//$rows1[$i]['shipping_status'] = 4;
										for($j=0; $j<sizeof($Website['shipping_status']); $j++){
											$shipping_status='progress-bar-success';

											if((int)$rows1[$i]['shipping_status']<$j && $j>0){
												//當步驟大於0 且再步驟之前
												$shipping_status = 'progress-bar-success';
											}
											if((int)$rows1[$i]['shipping_status']==$j && $j>0){
												//當步驟大於0 且正在進行時
												$shipping_status = 'progress-bar-warning';
											}
											if((int)$rows1[$i]['shipping_status']==2 && $j==2){
												$shipping_status = 'progress-bar-danger';
											}
											if((int)$rows1[$i]['shipping_status']<$j){
												//尚未到達本步驟
												$shipping_status = 'progress-bar-none';
											}
											if((int)$rows1[$i]['shipping_status']==4){
												//成功
												$shipping_status = 'progress-bar-success';
											}



											//合併理貨與缺貨
											if($rows1[$i]['shipping_status']!=2 && $j==2){
												continue;
											}
											if($rows1[$i]['shipping_status']==2 && $j==1){
												continue;
											}

										?>
										<div class="progress-bar <?=$shipping_status?>">
											<?=$Website['shipping_status'][$j]['name']?>
										</div>
										<? }?>
									</div>
								</td>
							</tr>
							*/ ?>
							<? }?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
