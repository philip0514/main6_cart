<div class="main-container">
	<section class="bg--secondary space--sm">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="boxed boxed--lg boxed--border">
						<div class="text-block text-center">
							<span class="h5"><?=$rows1['name']?></span>
						</div>
						<hr>
						<div class="text-block">
							<ul class="menu-vertical">
								<li>
									<a href="member/info/">帳號管理</a>
								</li>
								<li>
									<a href="member/logout/">登出</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="boxed boxed--lg boxed--border"> 
						<div id="account-profile">
							<h4>Profile</h4>
							<form id="form1" name="form1" method="post" action="">
								<div class="row">
									<div class="col-sm-12 form-group required">
										<label for="name">姓名</label>
										<span class="help help-inline">必填</span>
										<input type="text" class="form-control required" id="name" name="name" placeholder="姓名" value="<?=$rows1['name']?>">
									</div>
									
									<? if($rows1['facebook_id']){?>
									<div class="col-sm-12 form-group">
										<label>Facebook ID</label>
										<div>
											<p class="form-control-static">
												<?=$rows1['facebook_id']?>
											</p>
										</div>
									</div>
									<? }?>
									
									<div class="col-sm-12 form-group">
										<label>帳號</label>
										<div>
											<p class="form-control-static">
												<?=$rows1['account']?>
											</p>
										</div>
									</div>
									
									<div class="col-sm-12 form-group">
										<label for="password">密碼</label>
										<span class="help help-inline">若不修改，請留空白</span>
										<input type="password" class="form-control" id="password" name="password" placeholder="密碼，若不修改，請留空白" autocomplete="new-password">	
									</div>
									
									<div class="col-sm-12 form-group">
										<label for="gender">性別</label>
										<span class="help help-inline"></span>
										<div>
											<div class="input-radio p-r-20 <?=($rows1['gender']==1 || !$rows1['gender'])?'checked':'';?>">
												<label>先生</label>
												<div class="inner"></div>
												<input type="radio" name="gender" id="gender1" value="1" <?=($rows1['gender']==1 || !$rows1['gender'])?'checked':'';?> />
											</div>
											<div class="input-radio <?=($rows1['gender']==2)?'checked':'';?>">
												<label>小姐</label>
												<div class="inner"></div>
												<input type="radio" name="gender" id="gender2" value="2" <?=($rows1['gender']==2)?'checked':'';?> />
											</div>
										</div>
									</div>

									<div class="col-sm-12 form-group">
										<label for="birth">生日</label>
										<span class="help help-inline"></span>
										<input type="text" class="form-control" id="birth" name="birth" placeholder="生日 例:1990/01/30" value="<?=$rows1['birth_text']?>">
									</div>


									<div class="col-sm-6 form-group">
										<label>城市</label>
										<span class="help help-inline"></span>
										<select class="form-control" id="city_id" name="city_id">
											<option value="0">--- 無 ---</option>
											<? for($i=0; $i<sizeof($rows2); $i++){?>
											<option value="<?=$rows2[$i]['id']?>" 
												<?=$rows2[$i]['id']==$rows1['city_id']?'selected':''?>
												>
												<?=$rows2[$i]['name']?>
											</option>
											<? }?>
										</select>
									</div>

									<div class="col-sm-6 form-group">
										<label for="area_id">地區</label>
										<span class="help help-inline"></span>
										<select class="form-control" id="area_id" name="area_id">
											<option value="0">--- 無 ---</option>
											<? for($i=0; $i<sizeof($rows3); $i++){?>
											<option value="<?=$rows3[$i]['id']?>" 
												<?=$rows3[$i]['id']==$rows1['area_id']?'selected':''?>
												>
												<?=$rows3[$i]['name'].' '.$rows3[$i]['code']?>
											</option>
											<? }?>
										</select>
									</div>

									<div class="col-sm-12 form-group">
										<label for="address">地址</label>
										<span class="help help-inline"></span>
										<input type="text" class="form-control" id="address" name="address" placeholder="地址" value="<?=$rows1['address']?>">
									</div>


									<div class="col-sm-12 form-group">
										<label for="mobile">手機</label>
										<span class="help help-inline"></span>
										<input type="text" class="form-control" id="mobile" name="mobile" placeholder="手機" value="<?=$rows1['mobile']?>">
									</div>

									<div class="col-sm-12 form-group">
										<input type="submit" class="btn btn-primary btn-lg" value="儲存">
										<input type="hidden" class="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
									</div>
									
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
</div>
<script>
$(function(){
	<?=$validation?>
	zip_code('#city_id', '#area_id');
})
</script>