<div class="main-container">
	<section class="height-100 imagebg text-center" data-overlay="4">
		<div class="background-image-holder" style="background: url(assets/img/inner-6.jpg); opacity: 1;">
			<img alt="background" src="assets/img/inner-6.jpg">
		</div>
		<div class="container pos-vertical-center">
			<div class="row">
				<div class="col-sm-7 col-md-5">
					<h1><?=$page_title?></h1>
					<p class="lead">
						請輸入您原有的電子郵件，以取得您的新密碼
					</p>
					
					<form id="form1" name="form1" method="post" action="">
						<div class="row">
							<div class="col-xs-12 p-r-0">
								<input type="text" class="form-control required" id="account" name="account" placeholder="帳號，為電子郵件格式">
							</div>
							<div class="col-xs-12 p-l-0">
								<button class="btn btn--primary type--uppercase" type="submit">取得新密碼</button>
								<input type="hidden" class="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
							</div>
						</div>
						<!--end of row-->
					</form>
					<span class="type--fine-print block">Dont have an account yet?
						<a href="/member/register/">Create account</a>
					</span>
					<span class="type--fine-print block">Forgot your password?
						<a href="/member/forgot_password/">Get new password</a>
					</span>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
</div>
<script>
$(function(){
	<?=$validation?>
})
</script>