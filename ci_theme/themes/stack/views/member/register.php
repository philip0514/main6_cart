<div class="main-container">
	<section class="imageblock switchable feature-large height-100">
		<div class="imageblock__content col-md-6 col-sm-4 pos-right">
			<div class="background-image-holder" style="background: url(assets/img/inner-7.jpg); opacity: 1;">
				<img alt="image" src="assets/img/inner-7.jpg">
			</div>
		</div>
		<div class="container pos-vertical-center">
			<div class="row">
				<div class="col-md-5 col-sm-7">
					<h2>Create an Account</h2>
					<p class="lead">Welcome to join us</p>
					<form class="" id="form1" name="form1" method="post" action="">
						<div class="row">
							
							<? if($Website['global_setting']['facebook_login']){?>
							<div class="col-sm-12">
								<a class="btn bg--facebook btn--icon btn-facebook btn-block" href="javascript:;">
									<span class="btn__text"><i class="socicon socicon-facebook"></i>Register WITH FACEBOOK </span>
								</a>
							<hr>
							</div>
							<? }?>
							
							<div class="col-xs-12 form-group">
								<input type="text" class="form-control required" id="name" name="name" placeholder="姓名">
								<span class="help help-inline"></span>
							</div>
							<div class="col-xs-12 form-group">
								<input type="text" class="form-control required" id="account" name="account" placeholder="帳號，為電子郵件格式">
								<span class="help help-inline"></span>
							</div>
							<div class="col-xs-12 form-group">
								<input type="password" class="form-control required" id="password" name="password" placeholder="密碼">
								<span class="help help-inline"></span>
							</div>
							<div class="col-xs-12 form-group">
								<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="再次輸入密碼">
								<span class="help help-inline"></span>
							</div>
							<div class="col-xs-12 form-group">
								<button type="submit" class="btn btn--primary type--uppercase">Create Account</button>
								<input type="hidden" class="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
							</div>
							<div class="col-xs-12">
								<span class="type--fine-print">By signing up, you agree to the
									<a href="javascript:;">Terms of Service</a>
								</span>
							</div>
						</div>
						<!--end row-->
					</form>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
</div>
<script>
$(function(){
	<?=$validation?>
	$('.btn-facebook').click(function(){
		facebook_login();
	});
});
</script>