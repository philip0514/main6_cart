<div class="main-container">
	<section class="text-center height-50">
		<div class="container pos-vertical-center">
			<div class="row">
				<div class="col-sm-8">
					<h1><?=$page_title?></h1>
					<p class="lead">
						親愛的會員 您好：您的帳號已被停用。
					</p>
					<div>
						<a class="btn btn--sm btn--primary type--uppercase" href="/">
							<span class="btn__text">
								回首頁
							</span>
						</a>
					</div>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
</div>