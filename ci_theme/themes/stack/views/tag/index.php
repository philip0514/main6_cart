<div class="main-container">
	<section class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-md-8">
					<h1><?=$page_title?></h1>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
	<section>
		<div class="container">
			<div class="row">
				<? for($i=0; $i<sizeof($product); $i++){?>
				<div class="col-sm-4">
					<div class="product">
						<a href="product/<?=$product[$i]['id']?>/">
							<img class="img-responsive" src="<?=$product[$i]['file_url']?>" alt="<?=$product[$i]['name']?>">
						</a>
						<a class="block" href="product/<?=$product[$i]['id']?>/">
							<div>
								<h5><?=$product[$i]['name']?></h5>
								<span> <?=mb_substr(strip_tags(htmlspecialchars_decode($product[$i]['description'])), 0, 100)?></span>
							</div>
							<div>
								<span class="h4 inline-block">$<?=$product[$i]['price']?></span>
							</div>
						</a>
					</div>
				</div>
				<? }?>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<?=$pagination?>
				</div>
			</div>
		</div>
	</section>
</div>