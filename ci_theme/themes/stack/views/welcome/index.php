<div class="main-container">
	<section class="cover height-90 imagebg text-center" data-overlay="2" id="home">
		<div class="background-image-holder" style="background: url('assets/img/landing-10.jpg'); opacity: 1;">
			<img alt="background" src="assets/img/landing-10.jpg">
		</div>
		<div class="container pos-vertical-center">
			<div class="row">
				<div class="col-sm-8">
					<img alt="Image" class="unmarg--bottom" src="assets/img/headline-1.png">
					<h3>
						Streamline your workflow with Stack.
					</h3>
					<a class="btn btn--primary type--uppercase" href="#">
						<span class="btn__text">
							View The Demos <br>
						</span>
					</a>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
	
							<!--
							{elapsed_time}<br>{memory_usage}
							-->
	<section class="text-center cta cta-4 space--xxs border--bottom ">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<span class="label label--inline">Hot!</span>
					<span>Over 280 interface blocks, 140 demo pages and Variant Page Builder.
						<a href="https://themeforest.net/item/stack-multipurpose-html-with-page-builder/19337626?ref=medium_rare">Purchase Stack</a> for $17 USD.</span>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
	
	
	<section class="bg--secondary">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h4>最新消息</h4>
				</div>
			</div>
			<div class="row">
				<? for($i=0; $i<sizeof($news); $i++){?>
				<div class="col-md-3">
					<div class="feature feature-1 boxed boxed--border  ">
						<h5><?=$news[$i]['name']?></h5>
						<h6><?=date('Y/m/d', $news[$i]['news_time'])?></h6>
						<p>
							Ideal for large and small organisations
						</p>
						<a class="pull-right" href="news/content/<?=$news[$i]['id']?>">
							Learn More
						</a>
					</div>
				</div>
				<? }?>
			</div>
		</div>
		
	</section>
	
	<section class=" ">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h4>最新商品</h4>
				</div>
			</div>
			<div class="row">
				<? for($i=0; $i<sizeof($product); $i++){?>
				<div class="col-sm-4">
					<div class="product">
						<a href="product/<?=$product[$i]['id']?>/">
							<img class="img-responsive" src="media/image/640/<?=$product[$i]['file_name']?>" alt="<?=$product[$i]['name']?>">
						</a>
						<a class="block" href="product/<?=$product[$i]['id']?>/">
							<div>
								<h5><?=$product[$i]['name']?></h5>
								<span> <?=mb_substr(strip_tags(htmlspecialchars_decode($product[$i]['description'])), 0, 100)?></span>
							</div>
							<div>
								<span class="h4 inline-block">$<?=$product[$i]['price']?></span>
							</div>
						</a>
					</div>
				</div>
				<? }?>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
</div>