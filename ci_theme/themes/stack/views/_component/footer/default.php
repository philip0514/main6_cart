<footer class="space--sm footer-2 bg--dark">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-3 col-xs-6">
				<h6 class="type--uppercase">會員服務</h6>
				<ul class="list--hover">
					<li><a href="member/login/">會員登入</a></li>
					<li><a href="member/register/">會員註冊</a></li>
					<li><a href="member/forgot_password/">忘記密碼</a></li>
					<li><a href="member/info/">帳號管理</a></li>
				</ul>
			</div>
			<div class="col-sm-6 col-md-3 col-xs-6">
				<h6 class="type--uppercase">關於我們</h6>
				<ul class="list--hover">
					<li>
					<li><a href="about/">關於我們</a></li>
					<li><a href="news/">最新消息</a></li>
				</ul>
			</div>
			<div class="col-sm-6 col-md-offset-3 col-md-3 col-xs-6">
				<h6 class="type--uppercase">聯絡我們</h6>
				<ul class="list--hover">
					<li>某某股份有限公司</li>
					<li>philip0514@gmail.com</li>
				</ul>
			</div>
		</div>
		<!--end of row-->
		<div class="row">
			<div class="col-sm-6">
				<span class="type--fine-print">Copyright ©
					<span class="update-year"><?=date('Y', time())?></span> 某某股份有限公司 All rights reserved.</span>
			</div>
		</div>
		<!--end of row-->
	</div>
	<!--end of container-->
</footer>