<!DOCTYPE html>
<html lang="en">
<head>
	<base href="<?=base_url();?>"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="content-language" content="zh-TW" />

	<title><?php echo @$title; ?></title>
	<meta name="description" content="<?php echo @$description; ?>">
	<meta name="keywords" content="<?php echo @$keywords; ?>">
	
	<?php echo $icon."\n"; ?>
	<?php echo $meta."\n"; ?>
	<? 
		$css = array(
			'bootstrap.css',
			'stack-interface.css',
			'socicon.css',
			'lightbox.min.css',
			'flickity.css',
			'iconsmind.css',
			'font-awesome.min.css',
			'jquery.steps.css',
			'theme.css',
			'custom.css',
			'bootstrap_extend.css',
		);
		$this->minify_theme->css($css);
		echo $this->minify_theme->deploy_css(false)."\n";
	?>
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons">
	<?php echo $css_files."\n"; ?>
	
	<!-- JavaScripts -->
	<?php echo js('jquery.min', 'https://code.jquery.com/jquery.min.js'); ?>
	<?php echo js('bootstrap.min', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'); ?>

	<!--[if lt IE 9]>
		<?php echo js('html5shiv.min', 'https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js'); ?>
		<?php echo js('respond.min', 'https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'); ?>
	<![endif]-->
	
	
</head>
<body>

	<?php echo implode("\n\t", array(@$header, @$layout, @$footer)); ?>
	
<!-- JavaScripts -->
<? 
$js = array(
	'flickity.min.js',
	'parallax.js',
	'scripts.js',
);
$this->minify_theme->js($js);
echo $this->minify_theme->deploy_js()."\n";

$js = array(
	'jquery-validate/jquery.validate.min.js',
	'jquery-validate/jquery.form.js',
	'bootstrap-timepicker/js/moment.js',
	'bootstrap-timepicker/js/bootstrap-datetimepicker.min.js',
	'jquery.steps/jquery.steps.js',
);
$this->minify_theme->js($js, 'library');
echo $this->minify_theme->deploy_js(false, NULL, 'library')."\n";

$js = array(
	'js/default.js',
	'js/validate.js',
	'js/cart.js'
);
$this->minify_theme->js($js, 'default');
echo $this->minify_theme->deploy_js(false, NULL, 'default')."\n";
?>
<!--
<script type="text/javascript" src="library/jquery-validate/jquery.validate.min.js"></script> 
<script type="text/javascript" src="library/jquery-validate/jquery.form.js"></script> 
<script type="text/javascript" src="library/bootstrap-timepicker/js/moment.js"></script>
<script type="text/javascript" src="library/bootstrap-timepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="library/jquery.steps/jquery.steps.js"></script>

<script type="text/javascript" src="theme/default/js/default.js"></script>
<script type="text/javascript" src="theme/default/js/validate.js"></script>
<script type="text/javascript" src="theme/default/js/cart.js"></script>

-->
<script>
var facebook_appid = '<?=$Website['global_setting']['facebook_appid']?>';
var facebook_scope = '<?=$Website['global_setting']['facebook_scope']?>';

window.fbAsyncInit = function() {
	FB.init({
		appId      : facebook_appid,
		cookie     : true,
		xfbml      : true,
		version    : 'v2.8'
	});
};

(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/zh_TW/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<?=htmlspecialchars_decode($Website['global_setting']['ga'], ENT_QUOTES)?>
</body>
</html>