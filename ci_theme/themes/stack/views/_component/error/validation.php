<div class="main-container">
	<section class="height-50">
		<div class="container pos-vertical-center">
			<div class="row">
				<div class="col-sm-8">
					<h1><?=$page_title?> 輸入錯誤</h1>
						<ol class="bold error p-l-20">
							<?=$error_message?>
						</ol>
					<div>
						<a class="btn btn--sm btn--primary type--uppercase" href="javascript:history.back();">
							<span class="btn__text">
								回上一頁
							</span>
						</a>
					</div>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
</div>
