<div class="main-container">
	<section class="height-100 bg--dark text-center">
		<div class="container pos-vertical-center">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="h1--large">500</h1>
					<p class="lead">
						An unexpected error has occured preventing the page from loading.
					</p>
					<p>
						<?=$message?>
					</p>
					<a href="/">Go back to home page</a>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
</div>