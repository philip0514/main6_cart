<div class="bar bar--sm visible-xs">
	<div class="container">
		<div class="row">
			<div class="col-xs-3 col-sm-2">
				<a href="/">
					<img class="logo logo-dark" alt="logo" src="<?=$Website['global_setting']['logo']?>" />
					<img class="logo logo-light" alt="logo" src="assets/img/logo-light.png" />
				</a>
			</div>
			<div class="col-xs-9 col-sm-10 text-right">
				<a href="#" class="hamburger-toggle" data-toggle-class="#menu-bar;hidden-xs">
					<i class="icon icon--sm stack-interface stack-menu"></i>
				</a>
			</div>
		</div>
	</div>
</div>
<nav id="menu-bar" class="bar bar-1 bar--sm hidden-xs hiddem-sm" data-scroll-class="60px:pos-fixed">
	<div class="container">
		<div class="row">
			<div class="col-md-1 col-sm-2 hidden-xs">
				<div class="bar__module">
					<a href="/">
						<img class="logo logo-dark" alt="logo" src="<?=$Website['global_setting']['logo']?>" />
						<img class="logo logo-light" alt="logo" src="assets/img/logo-light.png" />
					</a>
				</div>
			</div>
			<div class="col-md-8">
				<div class="bar__module">
					<ul class="menu-horizontal text-left">
						<? 
						for($i=0; $i<sizeof($category); $i++){
							if(!sizeof($category[$i]['children'])){
						?>
							<li><a href="category/<?=$category[$i]['url_name']?$category[$i]['url_name']:$category[$i]['id']?>/"><?=$category[$i]['name']?></a></li>
						<? }else{ ?>
							<li class="dropdown">
								<span class="dropdown__trigger"><?=$category[$i]['name']?></span>
								<div class="dropdown__container">
									<div class="container">
										<div class="row">
											<div class="dropdown__content col-md-2">
												<ul class="menu-vertical">
													<? for($j=0; $j<sizeof($category[$i]['children']); $j++){?>
													<li>
														<a href="category/<?=$category[$i]['children'][$j]['url_name']?$category[$i]['children'][$j]['url_name']:$category[$i]['children'][$j]['id']?>/"><?=$category[$i]['children'][$j]['name']?></a>
													</li>
													<? }?>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						<?	
							}
						}
						?>
						<li><a href="about/">關於我們</a></li>
						<li><a href="news/">最新消息</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 text-right text-left-xs">
				<div class="bar__module">
					<ul class="menu-horizontal">
						<li><a href="cart/"><i class="fa fa-shopping-cart"></i> 購物車</a></li>
						<li class="dropdown">
							<span class="dropdown__trigger">
								<i class="fa fa-user"></i> <?=(!$_SESSION['member_info']['id'])?'會員':'Hi, '.$_SESSION['member_info']['name']?>
							</span>
							<div class="dropdown__container">
								<div class="container">
									<div class="row text-left">
										<div class="dropdown__content col-md-2">
											<ul class="menu-vertical">
												<? if(!$_SESSION['member_info']['id']){?>
												<li>
													<a href="member/register/">會員註冊</a>
												</li>
												<li>
													<a href="member/login/">會員登入</a>
												</li>
												<li>
													<a href="member/forgot_password/">忘記密碼</a>
												</li>
												<? }else{?>
												<li>
													<a href="member/info/">帳號管理</a>
												</li>
												<li>
													<a href="member/order/">訂單管理</a>
												</li>
												<li>
													<a href="member/logout/">登出</a>
												</li>
												<? }?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li>
							<a href="javascript:;" data-notification-link="search-box">
								<i class="fa fa-search"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
			
		</div>
	</div>
</nav>
<div class="notification pos-top pos-right search-box bg--white border--bottom notification--reveal notification--dismissed" data-animation="from-top" data-notification-link="search-box">
	<form class="" id="form_search" name="form_search" method="get" action="/search/">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				<input type="search" name="q" placeholder="搜尋網站">
			</div>
		</div>
	</form>
	<div class="notification-close-cross notification-close"></div>
</div>