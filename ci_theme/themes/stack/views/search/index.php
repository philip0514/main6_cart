<div class="main-container">
	<section class="text-center">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
					<h2>Showing results for “<?=$text?>”</h2>
					<p class="lead">
						<span><?=$item_count?></span> results found
					</p>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
	<section>
		<div class="container">
		
		
			<div class="row">
				<div class="col-md-12">
					<div class="tabs-container" data-content-align="left">
						<ul class="tabs">
							
							<li class="active">
								<div class="tab__title">
									<span class="h5">商品</span>
								</div>
								<div class="tab__content">
									
									<div class="row">
										<? for($i=0; $i<sizeof($product); $i++){?>
										<div class="col-sm-4">
											<div class="product">
												<a href="product/<?=$product[$i]['id']?>/">
													<img class="img-responsive" src="<?=$product[$i]['file_url']?>" alt="<?=$product[$i]['name']?>">
												</a>
												<a class="block" href="product/<?=$product[$i]['id']?>/">
													<div>
														<h5><?=$product[$i]['name']?></h5>
														<span> <?=mb_substr(strip_tags(htmlspecialchars_decode($product[$i]['description'])), 0, 100)?></span>
													</div>
													<div>
														<span class="h4 inline-block">$<?=$product[$i]['price']?></span>
													</div>
												</a>
											</div>
										</div>
										<? }?>
										<? if(!sizeof($product)){?>
										<div class="alert alert-info text-center">無資料</div>
										<? }?>
									</div>
									
								</div>
							</li>
							
							<li>
								<div class="tab__title">
									<span class="h5">標籤</span>
								</div>
								<div class="tab__content">
									
									<div class="row">
										<div class="col-sm-12">
											<? for($i=0; $i<sizeof($tag); $i++){?>
											<a class="btn btn--primary" href="tag/<?=$tag[$i]['name']?>/" style="margin: 0 10px 10px 0px !important;">
												<span class="btn__text"><?=$tag[$i]['name']?></span>
											</a>
											<? }?>
										</div>
										<? if(!sizeof($tag)){?>
										<div class="alert alert-info text-center">無資料</div>
										<? }?>
									</div>
									
								</div>
							</li>
							
							<li>
								<div class="tab__title">
									<span class="h5">最新消息</span>
								</div>
								<div class="tab__content">
								
									<div class="row">
										<? for($i=0; $i<sizeof($news); $i++){?>
										<div class="col-md-3">
											<div class="feature feature-1 boxed boxed--border  ">
												<h5><?=$news[$i]['name']?></h5>
												<p><?=mb_substr(strip_tags(htmlspecialchars_decode($news[$i]['content'])), 0, 100)?></p>
												<a class="pull-right" href="news/content/<?=$news[$i]['id']?>/">
													View details »
												</a>
											</div>
										</div>
										<? }?>
										<? if(!sizeof($news)){?>
										<div class="alert alert-info text-center">無資料</div>
										<? }?>
									</div>
									
								</div>
							</li>
						</ul>
					</div><!--end of tabs container-->
				</div>
			</div>
		
		
		
			
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
</div>