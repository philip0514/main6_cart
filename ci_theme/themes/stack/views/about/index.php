<div class="main-container">
	<section class="cover height-90 imagebg text-center" data-overlay="2" id="home">
		<div class="background-image-holder" style="background: url('assets/img/landing-21.jpg'); opacity: 1;">
			<img alt="background" src="assets/img/landing-21.jpg">
		</div>
		<div class="container pos-vertical-center">
			<div class="row">
				<div class="col-sm-8">
					<img alt="Image" class="unmarg--bottom" src="assets/img/headline-1.png">
					<h1><?=$page_title?></h1>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
	
	<section class="bg--secondary">
		<div class="container">
			<div class="row" style="display: flex; flex-wrap: wrap;">
				<? for($i=0; $i<sizeof($rows1); $i++){?>
				<div class="col-md-3">
					<div class="feature feature-1 boxed boxed--border  ">
						<h5><?=$rows1[$i]['name']?></h5>
						<p><?=mb_substr(strip_tags(htmlspecialchars_decode($rows1[$i]['content'])), 0, 100)?></p>
						<a class="pull-right" href="about/content/<?=$rows1[$i]['id']?>/">
							View details »
						</a>
					</div>
				</div>
				<? }?>
			</div>
		</div>
	</section>
</div>