<div class="main-container">
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">
					<article>
						<div class="article__title text-center">
							<h1 class="h2"><?=$rows1['name']?></h1>
						</div>
						<!--end article title-->
						<div class="article__body">
							<?=htmlspecialchars_decode($rows1['content'])?>
						</div>
					</article>
					<!--end item-->
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
</div>