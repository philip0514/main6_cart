<div class="main-container">
	<section class="space--sm">
		<div class="container">
			<div class="row">
				<div class="col-sm-7 col-md-6">
					<div class="slider border--round boxed--border" data-paging="true" data-arrows="true">
						<ul class="slides">
							<? for($i=0; $i<sizeof($media); $i++){?>
							<li><img class="img-responsive" src="<?=$media[$i]['file_url']?>" /></li>
							<? }?>
						</ul>
					</div>
				</div>
				<div class="col-sm-5 col-md-5 col-md-offset-1">
					<h1><?=$page_title?></h1>
					<div class="text-block">
						<span class="h4 type--strikethrough inline-block">$<?=$product['list_price']?></span>
						<span class="h4 inline-block">$<?=$product['price']?></span>
					</div>
					<p>
						<?=$product['description']?>
					</p>
					
					<ul class="accordion accordion-2 accordion--oneopen">
						<li class="active">
							<div class="accordion__title">
								<span class="h5">Tags</span>
							</div>
							<div class="accordion__content">
								<div>
									<? for($i=0; $i<sizeof($tag); $i++){?>
									<a class="btn btn--primary" href="tag/<?=$tag[$i]['name']?>/" style="margin: 0 10px 10px 0px !important;">
										<span class="btn__text"><?=$tag[$i]['name']?></span>
									</a>
									<? }?>
								</div>
							</div>
						</li>
					</ul>
					
					<form id="form1" name="form1" method="post" action="cart/insert/">
						<div class="col-sm-12">
							<select id="spec" name="spec">
								<? for($i=0; $i<sizeof($spec); $i++){?>
								<option value="<?=$spec[$i]['spec_id']?>"><?=$spec[$i]['spec_name']?></option>
								<? }?>
							</select>
						</div>
						
						<div class="col-sm-6 col-md-4">
							<input type="number" id="quantity" name="quantity" placeholder="數量" value="1" min="0">
							<input type="hidden" id="product_id" name="product_id" value="<?=$product['id']?>">
						</div>
						
						<div class="col-sm-6 col-md-8">
							<a class="btn btn--primary btn-cart-insert" href="javascript:;" style="width: 100%; margin-left: 0;">
								<span class="btn__text">Add To Cart</span>
							</a>
						</div>
					</form>
				</div>
			</div>
			<!--end of row-->
			<div class="row">
				<div class="col-sm-12">
					<article>
						<div class="article__body p-t-50">
							<?=htmlspecialchars_decode($product['content'])?>
						</div>
					</article>
				</div>
			</div>
		</div>
		
		<!--end of container-->
	</section>
</div>
<script>
$(function(){
	Cart.insert();
})
</script>