<?
	/*
		NCCC 聯合信用卡中心
		https://www.nccc.com.tw/wps/wcm/connect/zh/home
	*/
?>
<form id="form2" name="form2" method="post" action="<?=$payment['url']?>" target="hpp-frame">
	<input type="hidden" name="MerchantID" 	value="<?=$payment['MerchantID']?>">	<!--測試特約商店代號-->
	<input type="hidden" name="TerminalID" 	value="<?=$payment['TerminalID']?>">	<!--測試端末機-->
	<input type="hidden" name="OrderID" 	value="<?=$payment_id?>">				<!--交易序號-->
	<input type="hidden" name="TransMode" 	value="1">								<!--一般交易=0 分期交易=1 紅利折抵交易=2-->
	<input type="hidden" name="Install" 	value="<?=$installment?>">				<!--分期期數-->
	<input type="hidden" name="BankNo" 		value="<?=$bank?>">						<!--特約商店回應網址-->
	<input type="hidden" name="TransAmt" 	value="<?=(int)$total_price?>">			<!--授權金額-->
	<input type="hidden" name="NotifyURL" 	value="<?=$payment['AuthResURL']?>">	<!--特約商店回應網址-->
	<input type="hidden" name="TEMPLATE" 	value="<?=(!$Website['browser']['is_mobile']?'CHINESE_SIMPLE':'CHINESE')?>"><!--簡易付款頁樣版模式 -->
	<input type="hidden" name="CSS_URL" 	value=""><!--特約商店回應網址-->
	
	
	<? /*
	<input type="hidden" name="RedeemType" value="2"><!--代表紅利交易折抵方式，RedeemType ='1' 時，代表交易做全額抵扣，RedeemType ='2' 時，代表交易做部份交易金額抵扣。-->
	<input type="hidden" name="RedeemUsed" value=""><!--代表該交易抵扣的紅利點數。-->
	<input type="hidden" name="CreditAmt" value="100"><!--代表持卡人此次交易的自付額。-->
	*/ ?>
	
	<input type="submit" id="s" style="display:none;" />
</form>