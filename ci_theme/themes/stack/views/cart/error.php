<section class="imagebg image--light cover cover-blocks bg--secondary">
	<div class="background-image-holder hidden-xs" style="background: url('assets/img/promo-1.jpg'); opacity: 1;">
		<img alt="background" src="assets/img/promo-1.jpg">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-7 ">
				<div>
					<h1><?=$title?></h1>
					<p class="lead">
						抱歉，您於付款時發生錯誤，但訂單已經紀錄，管理者將會盡快與您聯絡，謝謝。
					</p>
					<p>
						<a href="/member/order/">前往訂單 →</a>
					</p>
				</div>
			</div>
		</div>
		<!--end of row-->
	</div>
	<!--end of container-->
</section>