<style>
	.wizard>.content{
		background: none;
		border: 0;
	}
	.wizard > .content > .body{
		min-height:600px;
	}
	input[type='checkbox']{
		-webkit-appearance: checkbox;
		width: 30px;
	}
	input[type='radio']{
		-webkit-appearance: radio;
		width: 30px;
	}
	label div{
		padding: 7px;
    	float: right;
	}
	.wizard > .content > .body input[type='radio'] {
    	display: inline-block;
	}
	
	
	.wizard > .content > .body ul {
		list-style: none !important;
	}
	.wizard > .content > .body ul>li {
		/*display: inherit !important;*/
	}
	.wizard ul.tabs > li{
		padding: 15px;
	}
	.wizard ul.tabs-content > li{
		padding: 0;
	}
	.recipient{
		display: none;
	}
</style>


<div class="main-container">
	<section class="p-b-0">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1>Cart</h1>
					<ol class="breadcrumbs">
						<li>
							<a href="/">Home</a>
						</li>
						<li>Cart</li>
					</ol>
				</div>
			</div>
			<!--end of row-->
		</div>
		<!--end of container-->
	</section>
	
	<? if(!sizeof($rows1)){?>
	<section class="text-center bg--primary m-b-100">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-md-12 text-center">
					<h2>空的！</h2>
					<p class="lead">
						您的購物袋中沒有任何商品，現在就去逛逛吧～ >> <a href="/">回首頁</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<? }else{?>
	<section>
		<div class="container">
			<form class="cart-form wizard" id="form1" name="form1" method="post" action="cart/submit/">
				<h5>購物袋</h5>
				<section>
					<div class="row">
					
						<? for($i=0; $i<sizeof($rows1); $i++){?>
						<div class="col-sm-4 product_item">
							<div class="product-1">
								<div class="product__controls">
									<div class="col-xs-3">
										<label>Quantity:</label>
									</div>
									<div class="col-xs-6">
										<input type="number" class="quantity" name="quantity[]" value="<?=$rows1[$i]['qty']?>" placeholder="Quantity" />
										<input type="hidden" class="row_id" name="row_id[]" value="<?=$rows1[$i]['rowid']?>">
									</div>
									<div class="col-xs-3 text-right">
										<button type="button" class="checkmark checkmark--cross bg--error btn-delete"></button>
									</div>
								</div>
								<img alt="<?=$rows1[$i]['name']?>" src="<?=$rows1[$i]['info']['media']?>" />
								<div>
									<a href="product/<?=$rows1[$i]['id']?>">
										<h5 class="m-b-0"><?=$rows1[$i]['name']?> <span> - <?=$rows1[$i]['info']['spec']?></span></h5>
									</a>
									<span><?=$rows1[$i]['info']['description']?></span>
								</div>
								<div>
									<span class="h4 inline-block">$<?=number_format($rows1[$i]['price'])?></span>
								</div>
							</div>
						</div>
						<!--end item-->
						<? }?>

					</div>
					<!--end of row-->


					<div class="row mt--2">
						<div class="col-sm-12">
							<div class="boxed boxed--border cart-total">
								<div>
									<div class="col-xs-6">
										<span class="h5">Subtotal:</span>
									</div>
									<div class="col-xs-6 text-right">
										<span>$ <span class="subtotal_price"><?=$subtotal_price?></span></span>
									</div>
								</div>
								<hr />
								<div>
									<div class="col-xs-6">
										<span class="h5">Total:</span>
									</div>
									<div class="col-xs-6 text-right">
										<span class="h5">$ <span class="total_price"><?=$total_price?></span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--end of row-->
					
				</section>
				
				<h5>運送</h5>
				<section>
					<div class="row">
						<div class="col-md-4">
							<h6>運送方式</h6>
							<div>
								<ul style="list-style: none !important;">
									<? 
									$h=0;
									for($i=0; $i<sizeof($Website['shipping_type']); $i++){
										if(!$Website['shipping_type'][$i]['display']){
											continue;
										}
										$h++;
									?>
									<li>
										<label>
											<input type="radio" name="shipping_type" value="<?=$Website['shipping_type'][$i]['id']?>" <?=$h==1?'checked':''?>> 
											<div><?=$Website['shipping_type'][$i]['name']?></div>
										</label>
									</li>
									<? }?>
								</ul>
							</div>
						</div>
						<div class="col-md-4">
							<h6>運送日</h6>
							<div>
								<ul style="list-style: none !important;">
									<? 
									$h=0;
									for($i=0; $i<sizeof($Website['shipping_day']); $i++){
										if(!$Website['shipping_day'][$i]['display']){
											continue;
										}
										$h++;
									?>
									<li>
										<label>
											<input type="radio" name="shipping_day" value="<?=$Website['shipping_day'][$i]['id']?>" <?=$h==1?'checked':''?>> 
											<div><?=$Website['shipping_day'][$i]['name']?></div>
										</label>
									</li>
									<? }?>
								</ul>
							</div>
						</div>
						<div class="col-md-4">
							<h6>運送時段</h6>
							<div>
								<ul style="list-style: none !important;">
									<? 
									$h=0;
									for($i=0; $i<sizeof($Website['shipping_time']); $i++){
										if(!$Website['shipping_time'][$i]['display']){
											continue;
										}
										$h++;
									?>
									<li>
										<label>
											<input type="radio" name="shipping_time" value="<?=$Website['shipping_time'][$i]['id']?>" <?=$h==1?'checked':''?>> 
											<div><?=$Website['shipping_time'][$i]['name']?></div>
										</label>
									</li>
									<? }?>
									
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h6>發票資訊</h6>
							<input type="hidden" id="invoice" name="invoice" value="0">
							<div class="tabs-container tabs--vertical">
								<ul class="tabs">
									<? 
									$h=0;
									for($i=0; $i<sizeof($Website['invoice']); $i++){
										if(!$Website['invoice'][$i]['display']){
											continue;
										}
										$h++;
										
									?>
									<li class="<?=$h==1?'active':''?>" data-id="<?=$i?>">
										<div class="tab__title">
											<span class="h5"><?=$Website['invoice'][$i]['name']?></span>
										</div>
										<div class="tab__content">
											<?
											switch($i){
												case 0:
											?>
											<div class="p-b-10">捐贈單位：</div>
											<select class="form-control" id="invoice_donate" name="invoice_donate">
												<? for($j=0; $j<sizeof($Website['invoice_donate']); $j++){?>
												<option value="<?=$Website['invoice_donate'][$j]['id']?>"><?=$Website['invoice_donate'][$j]['name']?></option>
												<? }?>
											</select>
											<?
													break;
												case 1:
											?>
											
											
											<? if($Website['invoice_duplicate'][0]['display']){?>
											<div>
												<label>
													<input type="radio" name="invoice_duplicate" value="0" checked> 
													<div><?=$Website['invoice_duplicate'][0]['name']?></div>
												</label>
											</div>
											<? }?>
											
											<? if($Website['invoice_duplicate'][1]['display']){?>
											<div>
												<label>
													<input type="radio" name="invoice_duplicate" value="1"> 
													<div><?=$Website['invoice_duplicate'][1]['name']?></div>
												</label>
											</div>
											<? }?>
											
											
											<? if($Website['invoice_duplicate'][2]['display']){?>
											<div>
												<label>
													<input type="radio" name="invoice_duplicate" value="2"> 
													<div><?=$Website['invoice_duplicate'][2]['name']?></div>
												</label>
												<div>
													<input class="form-control" name="invoice_mobile" value="" placeholder="請輸入手機條碼載具(限大寫英數字)">
												</div>
											</div>
											<? }?>
											
											<? if($Website['invoice_duplicate'][3]['display']){?>
											<div>
												<label>
													<input type="radio" name="invoice_duplicate" value="3"> 
													<div><?=$Website['invoice_duplicate'][3]['name']?></div>
												</label>
												<div class="p-b-10">
													<input class="form-control" name="invoice_emcard" value="" placeholder="請輸入自然人憑證載具(限英數字)">
												</div>
												<div class="p-b-10">
													<input class="form-control" name="invoice_emcard_confirm" value="" placeholder="請再次輸入自然人憑證載具">
												</div>
											</div>
											<? }?>
											
											<?		
													break;
												case 2:
											?>
											<div class="form-group">
												<input type="text" name="invoice_guid" id="invoice_guid" placeholder="統一編號" />
												<span class="help"></span>
											</div>
											<div class="form-group">
												<input type="text" name="invoice_title" id="invoice_title" placeholder="公司抬頭" />
												<span class="help"></span>
											</div>
											<?
													break;
											}
											?>
										</div>
									</li>
									<? }?>
								</ul>
							</div><!--end of tabs container-->
							
						</div>
					</div>
					<div class="row shipping_block p-t-50">
						
					</div>
				</section>
				
				<h5>付款</h5>
				<section>
					<div class="row">
						<div class="col-md-6">
							<h6>折扣</h6>
							<div class="discount_block"></div>
							
							<h6 class="p-t-50">付款方式</h6>
							<ul>
								<? 
								for($i=0; $i<sizeof($Website['payment_type']); $i++){
									if(!$Website['payment_type'][$i]['display']){
										continue;
									}
								?>
								<li>
									<label>
										<input type="radio" class="payment_type" name="payment_type" value="<?=$Website['payment_type'][$i]['id']?>" <?=!$Website['payment_type'][$i]['id']?'checked':''?> >
										<div><?=$Website['payment_type'][$i]['name']?></div>
									</label>
								</li>
								<? }?>
							</ul>
						</div>
						<div class="col-md-6">
							<h6>金額計算</h6>
							
							
							<div class="row">
								<div class="col-xs-6">
									<span class="h5">Subtotal:</span>
								</div>
								<div class="col-xs-6 text-right">
									<span>$ <span class="subtotal_price">0</span></span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<span class="h5">Discount:</span>
								</div>
								<div class="col-xs-6 text-right">
									<span>$ -<span class="discount_price">0</span></span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<span class="h5">Freight:</span>
								</div>
								<div class="col-xs-6 text-right">
									<span>$ <span class="freight">0</span></span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<span class="h5">Total:</span>
								</div>
								<div class="col-xs-6 text-right">
									<span class="h5">$ <span class="total_price">0</span></span>
								</div>
							</div>
							
						</div>
					</div>
					
					<!--
					商品
					運送方式
					發票
					購買人資訊
					收件人資訊
					-->
					<!--
					<div class="row p-t-50">
						<div class="col-md-12">
							<h6>訂單資訊</h6>
						</div>
					</div>
					<div class="row p-t-50">
						<div class="col-md-4">
							<h6>購買人資訊</h6>
						</div>
						<div class="col-md-4">
							<h6>收件人資訊</h6>
						</div>
						<div class="col-md-4">
							<h6>運送/發票</h6>
						</div>
					</div>
					-->
					
				</section>
				
			</form>
			<!--end cart form-->
		</div>
		<!--end of container-->
	</section>
	<? }?>
	
	<div class="payment_gateway"></div>
	
</div>

<div id="login-modal" class="modal-container">
	<div class="modal-content">
		<section class="feature-large bg--white border--round p-t-0">
			<div style="overflow: hidden;max-height: 250px;">
				<img alt="Image" src="assets/img/inner-5.jpg">
			</div>
			<div class="container p-t-50">
				<div class="row">
					<div class="col-md-5 col-md-offset-1 col-sm-6">
						<h2>Login</h2>
						<a class="btn block btn--icon bg--facebook type--uppercase" href="javascript:;">
							<span class="btn__text">
								<i class="socicon-facebook"></i>
								Login with Facebook
							</span>
						</a>
						
						<hr data-title="OR">
						
						<form id="form-modal-login" name="form-modal-login" method="post" action="member/ajax_modal_login/">
							<div class="row">
								<div class="col-xs-12 error hidden">
									<div class="alert bg--error">
										<div class="alert__body">
											<span class="error_message"></span>
										</div>
									</div>
								</div>
								<div class="col-xs-12 form-group">
									<input type="text" id="modal_login_account" name="modal_login_account" placeholder="Email Address" />
									<span class="help"></span>
								</div>
								<div class="col-xs-12 form-group">
									<input type="password" id="modal_login_password" name="modal_login_password" placeholder="Password" />
									<span class="help"></span>
								</div>
								
								<div class="col-xs-12">
									<label>
										<input type="checkbox" id="modal_login_remember" name="modal_login_remember" value="1"> 
										<div>Remember Me</div>
									</label>
								</div>
								<div class="col-xs-12">
									<button type="submit" class="btn btn--primary type--uppercase">Login</button>
								</div>
							</div>
							<!--end of row-->
						</form>
					</div>
					<div class="col-md-5 col-md-offset-1 col-sm-6">
						<h2>Create an account</h2>
						
						<form id="form-modal-register" name="form-modal-register" method="post" action="member/ajax_modal_register/">
							<div class="row">
								<div class="col-xs-12 form-group">
									<input type="text" name="modal_register_name" id="modal_register_name" placeholder="Name" />
									<span class="help"></span>
								</div>
								<div class="col-xs-12 form-group">
									<input type="text" name="modal_register_account" id="modal_register_account" placeholder="Email Address" />
									<span class="help"></span>
								</div>
								<div class="col-xs-12 form-group">
									<input type="password" name="modal_register_password" id="modal_register_password" placeholder="Password" />
									<span class="help"></span>
								</div>
								<div class="col-xs-12 form-group">
									<input type="password" name="modal_register_password_confirm" id="modal_register_password_confirm" placeholder="Password Confirm" />
									<span class="help"></span>
								</div>
								<div class="col-xs-12">
									<button type="submit" class="btn btn--primary type--uppercase">Create Account</button>
								</div>
								<div class="col-xs-12">
									<span class="type--fine-print">By signing up, you agree to the
										<a href="#">Terms of Service</a>
									</span>
								</div>
							</div>
							<!--end row-->
						</form>
					</div>
				</div>
				<!--end of row-->
			</div>
			<!--end of container-->
		</section>
	</div>
</div>



<script src="library/historyjs/jquery.history.js"></script>
<script>
$(function(){
	Cart.init();
})
</script>