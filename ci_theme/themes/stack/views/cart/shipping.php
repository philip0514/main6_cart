<div class="col-md-6">
	<h6>購買人資訊</h6>
	<div class="row">
		<div class="buyer">
			<div class="col-sm-12 form-group">
				<label>姓名</label>
				<input type="text" class="required" id="buyer_name" name="buyer_name" placeholder="姓名" value="<?=$rows1['name']?>">
				<div class="help"></div>
			</div>
			<div class="col-sm-12 form-group">
				<label>住址</label>
				<div class="input-select">
					<select id="buyer_city" name="buyer_city">
						<? for($i=0; $i<sizeof($rows2); $i++){?>
						<option value="<?=$rows2[$i]['id']?>" <?=$rows2[$i]['id']==$rows1['city_id']?'selected':''?>><?=$rows2[$i]['name']?></option>
						<? }?>
					</select>
				</div>
				<div class="input-select">
					<select id="buyer_area" name="buyer_area">
						<? for($i=0; $i<sizeof($rows3); $i++){?>
						<option value="<?=$rows3[$i]['id']?>" <?=$rows3[$i]['id']==$rows1['area_id']?'selected':''?>><?=$rows3[$i]['name']?></option>
						<? }?>
					</select>
				</div>
			</div>
			<div class="col-sm-12 form-group">
				<input type="text" class="required" id="buyer_address" name="buyer_address" placeholder="住址" value="<?=$rows1['address']?>">
				<div class="help"></div>
			</div>
			<div class="col-sm-12 form-group">
				<label>行動電話</label>
				<input type="text" class="required" id="buyer_mobile" name="buyer_mobile" placeholder="行動電話" value="<?=$rows1['mobile']?>">
				<div class="help"></div>
			</div>
			<div class="col-sm-12 form-group">
				<label>室內電話</label>
				<input type="text" id="buyer_phone" name="buyer_phone" placeholder="室內電話" value="<?=$rows1['phone']?>">
				<div class="help">非必填</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-6">
	<h6>收件人資訊</h6>
	<div class="row">
		<div class="recipient">
			<div class="col-sm-12 form-group">
				<label>姓名</label>
				<input type="text" class="required" id="recipient_name" name="recipient_name" placeholder="姓名" value="">
				<div class="help"></div>
			</div>
			<div class="col-sm-12 form-group">
				<label>住址</label>
				<div class="input-select">
					<select id="recipient_city" name="recipient_city">
						<? for($i=0; $i<sizeof($rows2); $i++){?>
						<option value="<?=$rows2[$i]['id']?>" <?=$rows2[$i]['id']==$rows1['city_id']?'selected':''?>><?=$rows2[$i]['name']?></option>
						<? }?>
					</select>
				</div>
				<div class="input-select">
					<select id="recipient_area" name="recipient_area">
						<? for($i=0; $i<sizeof($rows3); $i++){?>
						<option value="<?=$rows3[$i]['id']?>" <?=$rows3[$i]['id']==$rows1['area_id']?'selected':''?>><?=$rows3[$i]['name']?></option>
						<? }?>
					</select>
				</div>
			</div>
			<div class="col-sm-12 form-group">
				<input type="text" class="required" id="recipient_address" name="recipient_address" placeholder="住址" value="">
				<div class="help"></div>
			</div>
			<div class="col-sm-12 form-group">
				<label>行動電話</label>
				<input type="text" class="required" id="recipient_mobile" name="recipient_mobile" placeholder="行動電話" value="">
				<div class="help"></div>
			</div>
			<div class="col-sm-12 form-group">
				<label>室內電話</label>
				<input type="text" id="recipient_phone" name="recipient_phone" placeholder="室內電話" value="">
				<div class="help">非必填</div>
			</div>
			
		</div>
		<div class="col-sm-12 form-group">
			<label>
				<input type="checkbox" id="same" name="same" value="1" checked> 
				<div>同購買人資訊</div>
			</label>
		</div>
		<div class="col-sm-12 form-group">
			<label>備註</label>
			<textarea class="form-control" id="recipient_note" name="recipient_note" placeholder="備註"></textarea>
		</div>
		
	</div>
</div>