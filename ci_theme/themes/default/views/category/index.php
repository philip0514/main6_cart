<div class="bs-docs-header" id="content" tabindex="-1">
	<div class="container">
		<h1><?=$page_title?></h1>
	</div>
</div>
<div class="container">
	<div class="row">
		<? for($i=0; $i<sizeof($product); $i++){?>
		<div class="col-md-4 p-t-20 p-b-20">
			<a href="product/<?=$product[$i]['id']?>/">
				<img class="img-responsive" src="<?=$product[$i]['file_url']?>" alt="">
			</a>
			<h3 class="p-t-20 p-b-10">
				<a href="product/<?=$product[$i]['id']?>/"><?=$product[$i]['name']?></a>
			</h3>
			<p class="list-text"><?=mb_substr(strip_tags(htmlspecialchars_decode($product[$i]['description'])), 0, 100)?></p>
		</div>
		<? }?>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<?=$pagination?>
		</div>
	</div>
</div>