<div class="jumbotron bs-docs-header">
	<div class="container">
		<h1>Hello, world!</h1>
		<p style="color: white">This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
		<p class="p-t-20"><a class="btn btn-primary btn-lg" href="javascript:;" role="button">Learn more »</a></p>
	</div>
</div>

<div class="container p-t-20">
	<div class="row">
		<div class="col-md-4">
			<h3>關於我們</h3>
			<div class="list-text">
				通她元同以一否之越！果態興食光冷先電女，向大不我看地精媽？讀體他史子一了？戰選。<br>
之持天靜，般政得公也開文領，的給縣上子用知他部我，天可基，好注了共重兒上少響樣養學手世才已完兒面類管那弟的又據態去時統種情終雖生勢切信常變史布文色年好模我去他新玩之做身持不風家。
				
			</div>
		</div>
		<div class="col-md-8">
			<h3>最新消息</h3>
			<ul>
				<? for($i=0; $i<sizeof($news); $i++){?>
				<li class="list-text"><a href="news/content/<?=$news[$i]['id']?>"><?=date('Y/m/d', $news[$i]['news_time']).' - '.$news[$i]['name']?></a></li>
				<? }?>
			</ul>
		</div>
	</div>
</div>

<div class="container p-t-20">
	<h3>最新商品</h3>
	<div class="row">
		<? for($i=0; $i<sizeof($product); $i++){?>
		<div class="col-md-4 p-t-20 p-b-20">
			<a href="product/<?=$product[$i]['id']?>/">
				<img class="img-responsive" src="<?=$product[$i]['file_url']?>" alt="">
			</a>
			<h3 class="p-t-20 p-b-10">
				<a href="product/<?=$product[$i]['id']?>/"><?=$product[$i]['name']?></a>
			</h3>
			<p class="list-text"><?=mb_substr(strip_tags(htmlspecialchars_decode($product[$i]['description'])), 0, 100)?></p>
		</div>
		<? }?>
	</div>
</div>