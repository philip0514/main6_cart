<div class="bs-docs-header">
	<div class="container">
		<h1><?=$page_title?></h1>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-8">
			<form class="form-horizontal" id="form1" name="form1" method="post" action="">
				
				<div class="form-group required">
					<label class="form-label" for="name">帳號</label>
					<span class="help help-inline">必填</span>
					<input type="text" class="form-control required" id="account" name="account" placeholder="帳號，為電子郵件格式">
				</div>
				
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="取得新密碼">
					<input type="hidden" class="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				</div>
			</form>
		</div>
	</div>
</div>
<script>
$(function(){
	<?=$validation?>
})
</script>