<div class="bs-docs-header">
	<div class="container">
		<h1><?=$page_title?></h1>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-12 text-center">
			<div class="p-t-20 p-b-20">抱歉，查無此帳號。</div>
			<div><a href="/" class="btn btn-primary">回首頁</a></div>
		</div>
	</div>
</div>