<div class="bs-docs-header">
	<div class="container">
		<h1><?=$page_title?></h1>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-8">
			<form class="form-horizontal" id="form1" name="form1" method="post" action="">
				
				<div class="form-group required">
					<label class="form-label" for="name">姓名</label>
					<span class="help help-inline">必填</span>
					<input type="text" class="form-control required" id="name" name="name" placeholder="姓名">
				</div>
				
				<div class="form-group required">
					<label class="form-label" for="account">帳號</label>
					<span class="help help-inline">必填</span>
					<input type="text" class="form-control required" id="account" name="account" placeholder="帳號，為電子郵件格式">
				</div>
				
				<div class="form-group required">
					<label class="form-label" for="password">密碼</label>
					<span class="help help-inline">必填</span>
					<input type="password" class="form-control required" id="password" name="password" placeholder="密碼">
				</div>
				
				<div class="form-group required">
					<label class="form-label" for="password_confirm">再次輸入密碼</label>
					<span class="help help-inline">必填</span>
					<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="再次輸入密碼">
				</div>
				
				<div class="form-group">
					<input type="submit" class="btn btn-primary btn-lg" value="儲存">
					<input type="hidden" class="csrf" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				</div>
			</form>
		</div>
		<div class="col-sm-4">
			<div>
				<? if($Website['global_setting']['facebook_login']){?>
				<a href="javascript:;" class="btn btn-primary btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
				<? }?>
			</div>
		</div>
	</div>
</div>
<script>


$(function(){
	<?=$validation?>
	$('.btn-facebook').click(function(){
		facebook_login();
	});
});
</script>