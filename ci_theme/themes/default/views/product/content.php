
<link rel="stylesheet" href="assets/flexslider/css/flexslider.css" type="text/css" media="screen" />
<!-- FlexSlider -->
<script type="text/javascript" src="assets/flexslider/js/jquery.flexslider.js"></script>

<div class="bs-docs-header" id="content" tabindex="-1">
	<div class="container">
		<h1><?=$page_title?></h1>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<div class="flexslider">
				<ul class="slides">
					<? for($i=0; $i<sizeof($rows2); $i++){?>
					<li><img class="img-responsive" src="<?=$rows2[$i]['file_url']?>" /></li>
					<? }?>
				</ul>
			</div>
			<div class="custom-navigation">
				<a href="javascript:;" class="flex-prev">Prev</a>
				<div class="custom-controls-container"></div>
				<a href="javascript:;" class="flex-next">Next</a>
			</div>
		</div>
		<div class="col-sm-6">
			<div style="line-height: 30px; letter-spacing: 0.7px"><?=$rows1['description']?></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="blog-post">
				<?=htmlspecialchars_decode($rows1['content'])?>
			</div>
		</div>
	</div>
</div>
<style>
.flexslider {
    margin-bottom: 10px;
}
.custom-navigation {
    display: table;
    width: 100%;
    table-layout: fixed;
}
	
.custom-navigation .flex-next {
    text-align: right;
}
.custom-navigation > a {
    width: 50px;
}
.custom-navigation > * {
    display: table-cell;
}
.flex-control-nav {
    position: relative;
    bottom: auto;
}
</style>
<script type="text/javascript">
$(window).load(function(){
	$('.flexslider').flexslider({
		animation: "slide",
		animation: "slide",
		controlsContainer: $(".custom-controls-container"),
		customDirectionNav: $(".custom-navigation a")
	});
});
</script>