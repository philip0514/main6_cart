<!DOCTYPE html>
<html lang="en">
<head>
	<base href="<?=base_url();?>"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="content-language" content="zh-TW" />

	<title><?php echo @$title; ?></title>
	<meta name="description" content="<?php echo @$description; ?>">
	<meta name="keywords" content="<?php echo @$keywords; ?>">
	<?php echo $meta."\n"; ?>

	<!-- StyleSheets -->
	<?php echo css('bootstrap.min', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'); ?>
	<?php echo css('bootstrap-theme.min', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css'); ?>
	<link rel="stylesheet" type="text/css" href="assets/css/doc.css">
	<link rel="stylesheet" type="text/css" href="theme/default/css/bootstrap_extend.css">
	<link rel="stylesheet" type="text/css" href="library/bootstrap-timepicker/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" type="text/css" href="library/font-awesome/css/font-awesome.css">
	<?php echo $css_files."\n"; ?>
	
	<!-- JavaScripts -->
	<?php echo js('jquery.min', 'http://code.jquery.com/jquery.min.js'); ?>
	<?php echo js('bootstrap.min', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'); ?>

	<!--[if lt IE 9]>
		<?php echo js('html5shiv.min', 'https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js'); ?>
		<?php echo js('respond.min', 'https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'); ?>
	<![endif]-->
	
	
<script>
var facebook_appid = '<?=$Website['global_setting']['facebook_appid']?>';
var facebook_scope = '<?=$Website['global_setting']['facebook_scope']?>';

window.fbAsyncInit = function() {
	FB.init({
		appId      : facebook_appid,
		cookie     : true,  // enable cookies to allow the server to access the session
		xfbml      : true,  // parse social plugins on this page
		version    : 'v2.8' // use graph api version 2.5
	});
};

(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/zh_TW/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
</head>
<body>
	<?php echo implode("\n\t", array(@$header, @$layout, @$footer)); ?>
	<!-- JavaScripts -->
	<?php echo $js_files."\n"; ?>
	<script type="text/javascript" src="library/jquery-validate/jquery.validate.min.js"></script> 
	<script type="text/javascript" src="library/jquery-validate/jquery.form.js"></script> 
	<script type="text/javascript" src="library/bootstrap-timepicker/js/moment.js"></script>
	<script type="text/javascript" src="library/bootstrap-timepicker/js/bootstrap-datetimepicker.min.js"></script>
	
	<script type="text/javascript" src="theme/default/js/default.js"></script>
	<script type="text/javascript" src="theme/default/js/validate.js"></script>
	<?=htmlspecialchars_decode($Website['global_setting']['ga'], ENT_QUOTES)?>
</body>
</html>