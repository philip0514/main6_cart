<div class="page-content-wrapper">
	<div class="content bg-white">
		<div class="jumbotron" data-pages="parallax">
			<div class="container sm-p-l-20 sm-p-r-20">
				<div class="row p-t-10 p-b-10">
					<div class="col-md-12">
						<div class="inner">
							<h3><?=$page_title?> 輸入錯誤</h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="panel panel-transparent">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<ol class="bold error">
								<?=$error_message?>
							</ol>
						</div>
					</div>
					
					<div class="row p-l-40">
						<div class="col-md-12">
							<div class="btn-save-group">
								<div class="btn-group" role="group">
									<a href="javascript:history.back();" class="btn btn-primary">回上一頁</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>