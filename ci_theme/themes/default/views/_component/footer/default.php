<footer class="bs-docs-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h3 class="block-title">會員服務</h3>
				<ul class="footer_link">
					<li><a href="member/login/">會員登入</a></li>
					<li><a href="member/register/">會員註冊</a></li>
					<li><a href="member/forgot_password/">忘記密碼</a></li>
					<li><a href="member/info/">帳號管理</a></li>
				</ul>
			</div>
			<div class="col-md-4">
				<h3 class="block-title">關於我們</h3>
				<ul class="footer_link">
					<li><a href="about/">關於我們</a></li>
					<li><a href="news/">最新消息</a></li>
				</ul>
			</div>
			<div class="col-md-4">
				<h3 class="block-title">聯絡我們</h3>
				<ul class="footer_link">
					<li>某某股份有限公司</li>
					<li>philip0514@gmail.com</li>
				</ul>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">Copyright <?=date('Y')?>. All Rights Reserved</div>
		</div>
	</div>
</footer>