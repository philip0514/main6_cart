<div class="bs-docs-header" id="content" tabindex="-1">
	<div class="container">
		<h1><?=$rows1['name']?></h1>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-12 blog-main">
			<div class="blog-post">
				<?=htmlspecialchars_decode($rows1['content'])?>
			</div>
		</div>
	</div>
</div>