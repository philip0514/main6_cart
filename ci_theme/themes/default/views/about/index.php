<div class="bs-docs-header" id="content" tabindex="-1">
	<div class="container">
		<h1><?=$page_title?></h1>
	</div>
</div>
<div class="container">
	<div class="row">
		<? for($i=0; $i<sizeof($rows1); $i++){?>
		<div class="col-md-4 p-t-20 p-b-20">
			<h2><?=$rows1[$i]['name']?></h2>
			<p><?=mb_substr(strip_tags(htmlspecialchars_decode($rows1[$i]['content'])), 0, 100)?></p>
			<p><a class="btn btn-default" href="about/content/<?=$rows1[$i]['id']?>/">View details »</a></p>
        </div>
		<? }?>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<?=$pagination?>
		</div>
	</div>
</div>