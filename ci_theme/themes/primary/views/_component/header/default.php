<?
if($config['width']=='boxed'){
	$config['class']['width'] = 'container';
}else{
	$config['class']['width'] = 'container-fluid';
}

if($config['width']=='boxed' && $config['stickup']){
	$config['class']['stickup'] = '';
}elseif($config['stickup']){
	$config['class']['stickup'] = 'navbar-fixed-top';
}

if($config['width']=='within_fullwidth'){
	$config['class']['inside_navbar_width'] = 'container';
}else{
	$config['class']['inside_navbar_width'] = 'container-fluid';
}
//print_r($config);
?>
<div class="<?=$config['class']['width']?>">
	<nav class="navbar navbar-default <?=$config['class']['stickup']?>">
		<div class="<?=$config['class']['inside_navbar_width']?>">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>"><?php echo @$site_name; ?></a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-center">
					<? 
					for($i=0; $i<sizeof($category); $i++){
						if(!sizeof($category[$i]['children'])){
					?>
						<li><a href="category/<?=$category[$i]['id']?>/"><?=$category[$i]['name']?></a></li>
					<? }else{?>
						<li class="dropdown">
							<a href="category/<?=$category[$i]['id']?>/" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$category[$i]['name']?> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<? for($j=0; $j<sizeof($category[$i]['children']); $j++){?>
								<li><a href="category/<?=$category[$i]['children'][$j]['id']?>/"><?=$category[$i]['children'][$j]['name']?></a></li>
								<? }?>
							</ul>
						</li>
					<?		
						}
					}
					?>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="../navbar/">Default</a></li>
					<li><a href="../navbar-static-top/">Static top</a></li>
					<li><a href="./">Fixed top <span class="sr-only">(current)</span></a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
</div>