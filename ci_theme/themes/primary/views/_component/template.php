<!DOCTYPE html>
<html lang="en">
<head>
	<base href="<?=base_url();?>"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo @$title; ?></title>
	<meta name="description" content="<?php echo @$description; ?>">
	<meta name="keywords" content="<?php echo @$keywords; ?>">
	<?php echo $meta."\n"; ?>

	<!-- StyleSheets -->
	<?php echo css('bootstrap.min', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'); ?>
	<?php echo css('bootstrap-theme.min', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css'); ?>
	<?php echo $css_files."\n"; ?>

	<!--[if lt IE 9]>
		<?php echo js('html5shiv.min', 'https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js'); ?>
		<?php echo js('respond.min', 'https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'); ?>
	<![endif]-->
</head>
<body>
	<?php echo implode("\n\t", array(@$header, @$layout, @$footer)); ?>
	<!-- JavaScripts -->
	<?php echo js('jquery.min', 'http://code.jquery.com/jquery.min.js'); ?>
	<?php echo js('bootstrap.min', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'); ?>
	<?php echo $js_files."\n"; ?>
</body>
</html>