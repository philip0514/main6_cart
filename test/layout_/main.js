jQuery.fn.disableTextSelect = function() {
	return this.each(function() {
		$(this).css({
			'MozUserSelect':'none',
			'webkitUserSelect':'none'
		}).attr('unselectable','on').bind('selectstart', function() {
			return false;
		});
	});
};
jQuery.fn.enableTextSelect = function() {
	return this.each(function() {
		$(this).css({
			'MozUserSelect':'',
			'webkitUserSelect':''
		}).attr('unselectable','off').unbind('selectstart');
	});
};

var Layout = function () {
	'use strict';
	
	var animate_speed = 200;
	var p_dragging = false;
	var padding_block_height = 0;
	var padding_block_width = 0;
	var padding_block_height_ori = 0;
	var padding_block_width_ori = 0;
	var block_type = null;
	var block_target;
	var Y_start, X_start;
	
	var classes = {
		"section": 				"layout-section",
		"dot_section": 			".layout-section",
		"row": 					"layout-row",
		"dot_row": 				".layout-row",
		"col": 					"layout-col",
		"dot_col": 				".layout-col",
		"toolbar": 				"toolbar",
		"dot_toolbar": 			".toolbar",
		"toolbar_bottom": 		"toolbar-bottom",
		"dot_toolbar_bottom": 	".toolbar-bottom",
		"outline": 				"outline",
		"dot_outline": 			".outline",
	};
	
	
	var template = {
		"outline" : 
			'<span class="outline outline-top">'+
				'<span class="padding-handle padding-handle-top"></span>'+
				'<span class="padding-block padding-block-top">'+
					'<span class="padding-block-value"></span>'+
				'</span>'+
			'</span>'+
			'<span class="outline outline-right">'+
				'<span class="padding-handle padding-handle-right"></span>'+
				'<span class="padding-block padding-block-right">'+
					'<span class="padding-block-value"></span>'+
				'</span>'+
			'</span>'+
			'<span class="outline outline-bottom">'+
				'<span class="padding-handle padding-handle-bottom"></span>'+
				'<span class="padding-block padding-block-bottom">'+
					'<span class="padding-block-value"></span>'+
				'</span>'+
			'</span>'+
			'<span class="outline outline-left">'+
				'<span class="padding-handle padding-handle-left"></span>'+
				'<span class="padding-block padding-block-left">'+
					'<span class="padding-block-value"></span>'+
				'</span>'+
			'</span>',
		"section_toolbar": 	
			'<div class="toolbar">'+
				'<div class="setting-btn-group">'+
					'<a href="javascript:;"><i class="fa fa-arrows"></i></a>'+
					'<a href="javascript:;"><i class="fa fa-cog"></i></a>'+
					'<a href="javascript:;"><i class="fa fa-copy"></i></a>'+
					'<a href="javascript:;" class="btn-delete" data-type="section"><i class="fa fa-trash"></i></a>'+
				'</div>'+
			'</div>',
		"section_toolbar_bottom": 
			'<span class="toolbar-bottom">'+
				'<button class="btn-modal btn-modal-section"><i class="fa fa-plus"></i></button>'+
			'</span>',
		"row_toolbar": 		
			'<div class="toolbar">'+
				'<div class="setting-btn-group">'+
					'<a href="javascript:;"><i class="fa fa-arrows"></i></a>'+
					'<a href="javascript:;"><i class="fa fa-cog"></i></a>'+
					'<a href="javascript:;"><i class="fa fa-columns"></i></a>'+
					'<a href="javascript:;"><i class="fa fa-copy"></i></a>'+
					'<a href="javascript:;" class="btn-delete" data-type="row"><i class="fa fa-trash"></i></a>'+
				'</div>'+
			'</div>',
		"row_toolbar_bottom": 
			'<span class="toolbar-bottom">'+
				'<button class="btn-modal btn-modal-row"><i class="fa fa-plus"></i></button>'+
			'</span>',
		"col_toolbar": 		
			'<div class="toolbar">'+
				'<div class="setting-btn-group">'+
					'<a href="javascript:;"><i class="fa fa-arrows"></i></a>'+
					'<a href="javascript:;"><i class="fa fa-cog"></i></a>'+
					'<a href="javascript:;"><i class="fa fa-copy"></i></a>'+
					'<a href="javascript:;" class="btn-delete" data-type="col"><i class="fa fa-trash"></i></a>'+
				'</div>'+
			'</div>',
		"col_toolbar_bottom": 
			'<span class="toolbar-bottom">'+
				'<button class="btn-modal btn-modal-col"><i class="fa fa-plus"></i></button>'+
			'</span>',
	};
	
	
	var element_remove = function(){
		$(this).remove();
	};
	
	
	var btn_modal = {
		section: function(target){
			$('.btn-modal-section').unbind('click');
			$('.btn-modal-section').click(function(){
				var type = 'after';
				if($(this).hasClass('new-section')){
					target = $(this).parents('.section-block');
					type = 'replace';
				}
				$('#modal_section').modal();
				modal_html.section(target, type);
			});
		},
		row: function(target){
			$('.btn-modal-row').unbind('click');
			$('.btn-modal-row').click(function(){
				var type = 'after';
				if($(this).hasClass('new-row')){
					target = $(this).parents('.container');
					type = 'replace';
				}
				$('#modal_row').modal();
				modal_html.row(target, type);
			});
		}
	};
	
	var modal_html = {
		section: function(target, type){
			$('.btn-insert-section').unbind('click');
			$('.btn-insert-section').click(function(){
				var content = 
					'<section class="layout-section">'+
						'<div class="container">'+
								'<div class="text-center">'+
									'<button class="btn-modal btn-modal-row new-row"><i class="fa fa-plus"></i></button>'+
								'</div>'+
						'</div>'+
					'</section>';

				if(type==='after'){
					target.after(content);
				}else if(type==='replace'){
					target.html(content);
				}
				btn_modal.row(target);
				init();
				$('#modal_section').modal('hide');
			});
		},
		row: function(target, type){
			$('.btn-insert-row').unbind('click');
			$('.btn-insert-row').click(function(){
				var content = '<div class="row layout-row">';
				var content_btn = '<div class="text-center"><button class="btn-modal btn-modal-col new-col"><i class="fa fa-plus"></i></button></div>';
				var grid = $.parseJSON('['+$(this).data('grid')+']');
				for(var i=0; i<grid.length; i++){
					content = content + '<div class="col-lg-'+grid[i]+'">'+content_btn+'</div>';
				}
				content += '</div>';

				if(type==='after'){
					target.after(content);
				}else if(type==='replace'){
					target.html(content);
				}
				init();
				$('#modal_row').modal('hide');
			});
		}
	};
	/*
	var modal_col = function(target){
		$('.btn-insert-col').unbind('click');
		$('.btn-insert-col').click(function(){
			var content = 
				'';
			target.after(content);
			init();
			$('#modal_col').modal('hide');
		});
	};
	*/
	var btn_delete = function(){
		$('.btn-delete').unbind('click');
		$('.btn-delete').click(function(){
			var type = $(this).data('type');
			var item_count = $(this).parents('.layout-'+type).parent().find('.layout-'+type).length;
			
			if(item_count===1){
				$(this).parents('.layout-'+type).parent().append('<div class="text-center"><button class="btn-modal btn-modal-'+type+' new-'+type+'"><i class="fa fa-plus"></i></button></div>');
				
				switch(type){
					default:
					case 'section':
						btn_modal.section();
						break;
					case 'row':
						btn_modal.row();
						break;
				}
			}
			$(this).parents('.layout-'+type).remove();
		});
	};
	
	/*
	var btn_delete = {
		section : function(){
			console.log(1);
			$('.btn-delete').unbind('click');
			$('.btn-delete').click(function(){
				console.log(2)
				//target.remove();
			});
		},
		row : function(){
			console.log(3);
			$('.btn-delete').unbind('click');
			$('.btn-delete').click(function(){
				console.log(4)
				$(this).parents('.layout-row').remove();
				//target.remove();
			});
		},
		col : function(){
			//console.log(1);
			$('.btn-delete').unbind('click');
			$('.btn-delete').click(function(){
				//target.remove();
			});
		},
	};
	*/
	
	var prepend_toolbar = function(target, type){
		
		switch(type){
			default:
			case 'section':
				target.prepend(template.section_toolbar);
				target.prepend(template.section_toolbar_bottom);
				//btn_delete.section();
				btn_modal.section(target);
				break;
			case 'row':
				target.prepend(template.row_toolbar);
				target.prepend(template.row_toolbar_bottom);
				//btn_delete.row();
				btn_modal.row(target);
				break;
			case 'col':
				target.prepend(template.col_toolbar);
				target.prepend(template.col_toolbar_bottom);
				//btn_delete.col();
				/*
				$('.btn-modal-col').unbind('click');
				$('.btn-modal-col').click(function(){
					//target = $(this).parents('.layout-section');
					$('#modal_col').modal();
					modal_col(target);
				});
				*/
				break;
		}
		
		btn_delete();
	};
	
	//var mouse_hover_on;
	var prepend_outline = function(target){
		if(p_dragging === true){ return;}
		
		target.prepend(template.outline);
		//target.find(classes.dot_outline).animate({opacity:'1'}, 3000);
		target.find('.padding-block-top').css('height', target.css('padding-top'));
		target.find('.padding-block-top, .padding-handle-top')
			.mouseenter(function(){
				if(p_dragging === true){ return;}
				$('body').disableTextSelect();
				/*
				if(mouse_hover_on!='top' && block_target){
					block_target.animate({opacity:'0'}, 100);
				}
				mouse_hover_on = 'top';
				*/
			
				block_target = $(this).parent('.outline').find('.padding-block-top');
				block_target.animate({opacity:'1'}, 100);
				padding_block_height = block_target.height();
				padding_block_height_ori = padding_block_height;
				block_target.find('.padding-block-value').text(padding_block_height+'px').animate({opacity:'1'}, 200);
			
				$('.padding-handle-top, .padding-block-top')
					.mousedown(function(e){
						p_dragging = true;
						block_type = 'top';
						Y_start = e.pageY;
						padding_block_height = block_target.height();
						block_target.find('.padding-block-value').text(padding_block_height+'px').animate({opacity:'1'}, 200);
					});
				padding_block_mouse();
			})
			.mouseleave(function(){
				if(p_dragging === true){ return;}
				$('body').enableTextSelect();
				/*
				if(mouse_hover_on!='top'){
					block_target.animate({opacity:'0'}, 100);
				}
				*/
				block_target.animate({opacity:'0'}, 100);
			});
		
		target.find('.padding-block-bottom').css('height', target.css('padding-bottom'));
		target.find('.padding-block-bottom, .padding-handle-bottom')
			.mouseenter(function(){
				if(p_dragging === true){ return;}
				$('body').disableTextSelect();
				//console.log(1)
				/*
				if(mouse_hover_on!='bottom' && block_target){
					block_target.animate({opacity:'0'}, 100);
				}
				mouse_hover_on = 'bottom';
				*/
			
				block_target = $(this).parent('.outline').find('.padding-block-bottom');
				block_target.animate({opacity:'1'}, 100);
				padding_block_height = block_target.height();
				padding_block_height_ori = padding_block_height;
				block_target.find('.padding-block-value').text(padding_block_height+'px').animate({opacity:'1'}, 200);
				
				$('.padding-handle-bottom, .padding-block-bottom')
					.mousedown(function(e){
						p_dragging = true;
						block_type = 'bottom';
						Y_start = e.pageY;
						padding_block_height = block_target.height();
						block_target.find('.padding-block-value').text(padding_block_height+'px').animate({opacity:'1'}, 200);
					});
				padding_block_mouse();
			})
			.mouseleave(function(){
				if(p_dragging === true){ return;}
				$('body').enableTextSelect();
				
				/*
				if(mouse_hover_on!='bottom'){
					block_target.animate({opacity:'0'}, 100);
				}
				*/
				block_target.animate({opacity:'0'}, 100);
			});
		
		target.find('.padding-block-right').css('width', target.css('padding-right'));
		target.find('.padding-block-right, .padding-handle-right')
			.mouseenter(function(){
				if(p_dragging === true){ return;}
				$('body').disableTextSelect();
				/*
				if(mouse_hover_on!='right' && block_target){
					block_target.animate({opacity:'0'}, 100);
				}
				mouse_hover_on = 'right';
				*/
				
				block_target = $(this).parent('.outline').find('.padding-block-right');
				block_target.animate({opacity:'1'}, 100);
				padding_block_width = block_target.width();
				padding_block_width_ori = padding_block_width;
				block_target.find('.padding-block-value').text(padding_block_width+'px').animate({opacity:'1'}, 200);
				
				$('.padding-handle-right, .padding-block-right')
					.mousedown(function(e){
						p_dragging = true;
						block_type = 'right';
						X_start = e.pageX;
						padding_block_width = block_target.width();
						block_target.find('.padding-block-value').text(padding_block_width+'px').animate({opacity:'1'}, 200);
					});
				padding_block_mouse();
			})
			.mouseleave(function(){
				if(p_dragging === true){ return;}
				$('body').enableTextSelect();
				
				/*
				console.log(mouse_hover_on)
				if(mouse_hover_on!='right'){
					block_target.animate({opacity:'0'}, 100);
				}
				*/
				block_target.animate({opacity:'0'}, 100);
			});
		
		target.find('.padding-block-left').css('width', target.css('padding-left'));
		target.find('.padding-block-left, .padding-handle-left')
			.mouseenter(function(){
				if(p_dragging === true){ return;}
				$('body').disableTextSelect();
				/*
				if(mouse_hover_on!='left' && block_target){
					block_target.animate({opacity:'0'}, 100);
				}
				mouse_hover_on = 'left';
				*/
				
				block_target = $(this).parent('.outline').find('.padding-block-left');
				block_target.animate({opacity:'1'}, 100);
				padding_block_width = block_target.width();
				padding_block_width_ori = padding_block_width;
				block_target.find('.padding-block-value').text(padding_block_width+'px').animate({opacity:'1'}, 200);

				$('.padding-handle-left, .padding-block-left')
					.mousedown(function(e){
						p_dragging = true;
						block_type = 'left';
						X_start = e.pageX;
						padding_block_width = block_target.width();
						block_target.find('.padding-block-value').text(padding_block_width+'px').animate({opacity:'1'}, 200);
					});
				padding_block_mouse();
			})
			.mouseleave(function(){
				if(p_dragging === true){ return;}
				$('body').enableTextSelect();
				
				/*
				if(mouse_hover_on!='left'){
					block_target.animate({opacity:'0'}, 100);
				}
				*/
				block_target.animate({opacity:'0'}, 100);
			});
	};
	
	var padding_block_mouse = function(){
		$(document)
			.mouseup(function(){
				if(p_dragging === false){ return;}
				$('body').enableTextSelect();
				console.log(1);
				p_dragging = false;
				
				if((block_type==='top' || block_type==='bottom') && !padding_block_height_ori){
					block_target.animate({opacity:'0'}, 100);
				}
				if((block_type==='left' || block_type==='right') && !padding_block_width_ori){
					block_target.animate({opacity:'0'}, 100);
				}
			})
			.mousemove(function(e){
				if(p_dragging === false){ return;}
				var distance;
				var distance_px;

				switch(block_type){
					case 'top':
					case 'bottom':
						distance = padding_block_height+e.pageY-Y_start;
						distance = distance>=0 ? distance : 0;
						distance_px = distance+'px';
						block_target.css('height', distance_px);
						break;
					case 'right':
						distance = padding_block_width+X_start-e.pageX;
						distance = distance>=0 ? distance : 0;
						distance_px = distance+'px';
						block_target.css('width', distance_px);
						break;
					case 'left':
						distance = padding_block_width+e.pageX-X_start;
						distance = distance>=0 ? distance : 0;
						distance_px = distance+'px';
						block_target.css('width', distance_px);
						break;
				}
			
				block_target.find('.padding-block-value').text(distance_px);
				block_target.parents('.outline').parent().css('padding-'+block_type, distance_px);
			});
	};
	
	var init = function(){
		
		//return 0;
		
		$(classes.dot_section+', '+classes.dot_row+', '+classes.dot_col).unbind('mouseenter');
		$(classes.dot_section+', '+classes.dot_row+', '+classes.dot_col).unbind('mouseleave');
		$(classes.dot_section+', '+classes.dot_row+', '+classes.dot_col)
			.mouseenter(function(e){
				if(p_dragging === true){ return;}
				var target = e.currentTarget;
				
				if($(target).hasClass(classes.section)){
					if(!$(target).find(classes.dot_toolbar).length){
						//$(target).prepend(template.section_toolbar);
						prepend_toolbar($(target), 'section');
						prepend_outline($(target));
						$(target).find(classes.dot_toolbar+', '+classes.dot_outline).animate({opacity:'1'}, animate_speed);
					}
				}
			
				if($(target).hasClass(classes.row)){
					if(!$(target).parents(classes.dot_section).children(classes.dot_toolbar).length){
						//$(target).parents(classes.dot_section).prepend(template.row_toolbar);
						prepend_toolbar($(target).parents(classes.dot_section), 'section');
						$(target).parents(classes.dot_section).find(classes.dot_toolbar).animate({opacity:'1'}, animate_speed);
					}
					$(target).parents(classes.dot_section).find(classes.dot_outline).remove();

					if(!$(target).find(classes.dot_toolbar).length){
						//$(target).prepend(template.section_toolbar);
						prepend_toolbar($(target), 'row');
						prepend_outline($(target));
						$(target).find(classes.dot_toolbar+', '+classes.dot_outline).animate({opacity:'1'}, animate_speed);
					}
				}
			
				if($(target).hasClass(classes.col)){
					if(!$(target).parents(classes.dot_section).children(classes.dot_toolbar).length){
						//$(target).parents(classes.dot_section).prepend(template.col_toolbar);
						prepend_toolbar($(target).parents(classes.dot_section), 'section');
						$(target).parents(classes.dot_section).find(classes.dot_toolbar).animate({opacity:'1'}, animate_speed);
					}
					$(target).parents(classes.dot_section).find(classes.dot_outline).remove();

					if(!$(target).parents(classes.dot_row).children(classes.dot_toolbar).length){
						//$(target).parents(classes.dot_row).prepend(template.col_toolbar);
						prepend_toolbar($(target).parents(classes.dot_row), 'row');
						$(target).parents(classes.dot_row).find(classes.dot_toolbar).animate({opacity:'1'}, animate_speed);
					}
					$(target).parents(classes.dot_row).find(classes.dot_outline).remove();

					if(!$(target).find(classes.dot_toolbar).length){
						//$(target).prepend(template.col_toolbar);
						prepend_toolbar($(target), 'col');
						$(target).find(classes.dot_toolbar).animate({opacity:'1'}, animate_speed);
					}
				}

			})
			.mouseleave(function(e){
				if(p_dragging === true){ return;}
				var target = e.currentTarget;
				
				if($(target).hasClass(classes.section)){
					//$('.btn-insert-section').popoverButton('destroy');
					$(target).find(classes.dot_toolbar+', '+classes.dot_toolbar_bottom+', '+classes.dot_outline).animate({opacity:'0'}, animate_speed, element_remove);
					//$(target).find(classes.dot_toolbar+', '+classes.dot_outline).animate({opacity:'0'}, animate_speed, element_remove);
					//console.log(2)
				}
			
				if($(target).hasClass(classes.row)){
					$(target).find(classes.dot_toolbar+', '+classes.dot_toolbar_bottom+', '+classes.dot_outline).animate({opacity:'0'}, animate_speed, element_remove);
					//$(target).find(classes.dot_toolbar+', '+classes.dot_outline).animate({opacity:'0'}, animate_speed, element_remove);

					$(target).parents(classes.dot_section).find(classes.dot_toolbar).animate({opacity:'1'}, animate_speed);
					prepend_outline($(target).parents(classes.dot_section));
				}
			
				if($(target).hasClass(classes.col)){
					$(target).find(classes.dot_toolbar+', '+classes.dot_toolbar_bottom).animate({opacity:'0'}, animate_speed, element_remove);
					//$(target).find(classes.dot_toolbar).animate({opacity:'0'}, animate_speed, element_remove);
					prepend_outline($(target).parents(classes.dot_row));

					$(target).parents(classes.dot_section).find(classes.dot_toolbar).animate({opacity:'1'}, animate_speed);
					$(target).parents(classes.dot_row).find(classes.dot_outline).animate({opacity:'1'}, animate_speed);
				}
			});
	};

	return {
		init: function () {
			init();
		}
	};

}();

$(document).ready(function(){
	'use strict';
	Layout.init();
});