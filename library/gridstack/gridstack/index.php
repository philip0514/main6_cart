
<!DOCTYPE html>
<html lang="en">
<head>
	<base href="<?=base_url();?>"/>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Serialization demo</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="library/gridstack/gridstack.css"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="library/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
    <script src="library/gridstack/gridstack.js"></script>
    <script src="library/gridstack/gridstack.jQueryUI.js"></script>

    <style type="text/css">
        .grid-stack {
            background: lightgoldenrodyellow;
        }

        .grid-stack-item-content {
            color: #2c3e50;
            text-align: center;
            background-color: #18bc9c;
        }
		
		
		
        .grid-stack {
            background: lightgoldenrodyellow;
        }

        .grid-stack-item-content {
            color: #2c3e50;
            text-align: center;
            background-color: #18bc9c;
        }

        .grid-stack .grid-stack {
            /*margin: 0 -10px;*/
            background: rgba(255, 255, 255, 0.3);
        }

        .grid-stack .grid-stack .grid-stack-item-content {
            background: lightpink;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div>
            <a class="btn btn-default" id="save-grid" href="#">Save Grid</a>
            <a class="btn btn-default" id="load-grid" href="#">Load Grid</a>
            <a class="btn btn-default" id="clear-grid" href="#">Clear Grid</a>
        </div>

        <br/>

        <div class="grid-stack"></div>
        
        <!--
        <div class="grid-stack">
            <div class="grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="4" data-gs-height="3">
                <div class="grid-stack-item-content">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius eligendi eos fuga magnam numquam perferendis provident quos rem. Asperiores assumenda dolor error eveniet impedit nihil numquam provident repellat ullam.
                </div>
            </div>
            <div class="grid-stack-item" data-gs-x="4" data-gs-y="0" data-gs-width="4" data-gs-height="4">
                <div class="grid-stack-item-content">

                        <div class="grid-stack">
                            <div class="grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">1</div></div>
                            <div class="grid-stack-item" data-gs-x="3" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">2</div></div>
                            <div class="grid-stack-item" data-gs-x="6" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">3</div></div>
                            <div class="grid-stack-item" data-gs-x="9" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">4</div></div>

                            <div class="grid-stack-item" data-gs-x="0" data-gs-y="1" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">5</div></div>
                            <div class="grid-stack-item" data-gs-x="3" data-gs-y="1" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">6</div></div>
                        </div>

                </div>
            </div>
        </div>
		-->
        <hr/>

        <textarea id="saved-data" cols="100" rows="20" readonly="readonly"></textarea>
    </div>


    <script type="text/javascript">
        $(function () {
            var options = {
            };
            $('.grid-stack').gridstack(options);

            new function () {
                this.serializedData = [
                    {x: 0, y: 0, width: 4, height: 1, id: 1, parentid: 0},
                    {x: 4, y: 0, width: 4, height: 1, id: 2, parentid: 0},
                    {x: 8, y: 0, width: 4, height: 1, id: 3, parentid: 0},
                    {x: 4, y: 1, width: 1, height: 1, id: 4, parentid: 0},
                    {x: 2, y: 3, width: 3, height: 1, id: 5, parentid: 0},
                    {x: 1, y: 4, width: 1, height: 1, id: 6, parentid: 0},
                    {x: 1, y: 3, width: 1, height: 1, id: 7, parentid: 0},
                    {x: 2, y: 4, width: 1, height: 1, id: 8, parentid: 0},
                    {x: 2, y: 5, width: 1, height: 1, id: 9, parentid: 0}
                ];

                this.grid = $('.grid-stack').data('gridstack');

                this.loadGrid = function () {
                    this.grid.removeAll();
                    var items = GridStackUI.Utils.sort(this.serializedData);
                    _.each(items, function (node) {
						console.log(node);
                        this.grid.addWidget($('<div><div class="grid-stack-item-content" /><div/>'),
                            node
						);
						
                    }, this);
                    return false;
                }.bind(this);

                this.saveGrid = function () {
                    this.serializedData = _.map($('.grid-stack > .grid-stack-item:visible'), function (el) {
                        el = $(el);
                        var node = el.data('_gridstack_node');
                        return {
                            x: node.x,
                            y: node.y,
                            width: node.width,
                            height: node.height,
                            id: node.id,
                            parentid: node.parentid
                        };
                    }, this);
                    $('#saved-data').val(JSON.stringify(this.serializedData, null, '    '));
                    return false;
                }.bind(this);

                this.clearGrid = function () {
                    this.grid.removeAll();
                    return false;
                }.bind(this);

                $('#save-grid').click(this.saveGrid);
                $('#load-grid').click(this.loadGrid);
                $('#clear-grid').click(this.clearGrid);

                this.loadGrid();
            };
        });
    </script>
</body>
</html>
