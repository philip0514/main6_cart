// JavaScript Document

//手機驗證
jQuery.validator.addMethod("is_mobile", function(value, element) {
	var str = value;
	var result = false;
	if(str.length > 0){
		
		if(str.substr(0,1)=='+'){
			str = str.substr(1,str.length-1);
		}
		
		//是否只有數字;
		var patt_mobile = /^[\d]{1,}$/;
		result = patt_mobile.test(str);
 
		if(result){
			//檢查前兩個字是否為 09
			//檢查前四個字是否為 8869
			var firstTwo = str.substr(0,2);
			var firstFour = str.substr(0,4);
			var afterFour = str.substr(4,str.length-1);
			if(firstFour == '8869'){
				$(element).val('09'+afterFour);
				if(afterFour.length == 8){
					result = true;
				} else {
					result = false;
				}
			} else if(firstTwo == '09'){
				if(str.length == 10){
					result = true;
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		}
	} else {
		result = true;
	}
	return result;
	
}, "請正確填寫您的手機號碼");

// 中文驗證
jQuery.validator.addMethod("is_chinese", function(value, element) {
	var chinese = /^[\u4e00-\u9fa5]+$/;
	return this.optional(element) || (chinese.test(value));
}, "只能輸入中文");

// 網址名稱
jQuery.validator.addMethod("url_name", function(value, element) {
	//var url_name = '/[^a-zA-Z0-9~%.:_\-]+$/';
	var url_name = /^[a-zA-Z0-9_\-]+$/;
	return this.optional(element) || (url_name.test(value));
}, "只能輸入英文、數字、與符號（ 包含： _ - ）");


function form_highlight(element, errorClass, validClass){
	$(element).parents('.form-group').find('.help').show('fast');
	$(element).parents(".form-group").removeClass("has-success");
	$(element).parents(".form-group").addClass("has-error");
}

function form_unhighlight(element, errorClass, validClass){
	$(element).parents('.form-group').find('.help').hide('fast');
	$(element).parents('.form-group').addClass('has-success');
	$(element).parents(".form-group").removeClass("has-error");
}
function form_error_text($error, $element){
	var text = $error[0].innerText;
	if(text){
		$element.parents('.form-group').find('.help').html(text);
	}
}
