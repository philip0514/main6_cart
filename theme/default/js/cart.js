var Cart = function(){
	
	var next_step = false;
	
	var title = $('title').text();
	
	var history_state;
	
	var $wizard = $('.wizard');
	
	var $form = $("#form1");
	
	var init = function(){
		history_url();
		wizard.init();
	};
	
	var history_url = function(){
		history_state = History.getState();
		History.log('initial:', history_state.data, history_state.title, history_state.url);
		
		History.Adapter.bind(window, 'statechange', function(){
			history_state = History.getState();
			History.log('statechange:', history_state.data, history_state.title, history_state.url);
			
			switch(history_state.data.step){
				default:
					/*
					if(main_host){
						ga('ec:setAction','checkout', {
							'step': 1,
						});
						ga('send', 'pageview', 'cart/');
					}
					*/
					break;
				case 'shipping':
					/*
					if(main_host){
						ga('ec:setAction','checkout', {
							'step': 2,
						});
						ga('send', 'pageview', 'cart/?step=shipping');
					}
					*/
					
					break;
				case 'payment':
					/*
					if(main_host){
						ga('ec:setAction','checkout', {
							'step': 3,
						});
						ga('send', 'pageview', 'cart/?step=payment');
					}
					*/
					break;
			}
		});
	};
	
	var insert = function(){
		$('.btn-cart-insert').click(function(){
			$('#form1').ajaxSubmit(function(response){
				//console.log(response);
			});
		});
	};
	
	var wizard = {
		init: function(){
			var $prototype = this;
			
			$wizard.steps({
				headerTag: "h5",
				bodyTag: "section",
				transitionEffect: "slideLeft",
				autoFocus: true,
				onStepChanging: function (event, currentIndex, newIndex){
					if(currentIndex==0 && newIndex==1){
						
						if(next_step===false){
							$.ajax({
								url: 'member/ajax_login_status/',
								type: 'GET',
								data: {},
								error: function(){},
								success: function(response){
									if(response==0){
										//modal
										$('#login-modal').addClass('modal-active').modal('show');

										validate.login();
										validate.register();
									}else{
										$prototype.calculate();

										next_step = true;
										$wizard.steps("next");
									}
								}
							});
						}
						
						return next_step;
					}
					
					if(currentIndex==1 && newIndex==2){
						var status = $form.valid();
						$prototype.resize();
						
						if(status){
							//購買人與收件人資訊
							$prototype.set_info();
						}
						return status;
					}
					
					//go back
					if(currentIndex-newIndex==1){
						return true;
					}
				},
				onStepChanged: function (event, currentIndex, priorIndex){
					//更改網址，然後內容由historyjs 內處理
					if(currentIndex>priorIndex){
						switch(currentIndex){
							case 0:
								break;
							case 1:
								//運送 shipping
								var url = "?step=shipping";
								History.pushState(
									{
										step:'shipping',
										rand:Math.random()
									}, 
									title, 
									url
								);
								
								$.ajax({
									url: 'cart/shipping/',
									type: 'POST',
									data: {},
									error: function(){},
									success: function(response){
										$('.shipping_block').html(response);
										$prototype.resize();
										
										//發票
										$('.tabs > li').click(function(){
											$('#invoice').val($(this).data('id'));
										});
										
										$prototype.address();
										$prototype.onchange();
										
										validate.buyer_add();
										validate.recipient_remove();
										
										
									}
								});
								
								break;
							case 2:
								//付款
								var url = "?step=payment";
								
								History.pushState(
									{
										step:'payment',
										rand:Math.random()
									}, 
									title, 
									url
								);
								
								$.ajax({
									url: 'cart/discount/',
									type: 'POST',
									data: {},
									error: function(){},
									success: function(response){
										$('.discount_block').html(response);
										$prototype.resize();
										$prototype.onchange();
									}
								});
								
								break;
						}
					}else{
						History.back();
					}
					
				},
				onFinishing: function (event, currentIndex){
					return true;
				},
				onFinished: function (event, currentIndex){
					//alert("Submitted!");
					$form.submit();
				}
			}).addClass('active');
			
			
			$('.quantity').unbind('change');
			$('.quantity').change(function(){
				$prototype.calculate();
			});
			
			$('.btn-delete').unbind('click');
			$('.btn-delete').click(function(){
				$(this).parents('.product_item').remove();
				
				$prototype.calculate();
			});
			
			validate.main();
			
		},
		onchange: function(){
			var $prototype = this;
			
			$('#same').unbind('click');
			$('#same').click(function(){
				if(!this.checked) {
					$('.recipient').show();
					validate.recipient_add();
				}else{
					$('.recipient').hide();
					validate.recipient_remove();
				}
				$prototype.resize();
				$prototype.set_info();
			});
			
			$('.discount_type').unbind('click');
			$('.discount_type').click(function(){
				$prototype.calculate();
			});
			
			$('.coupon_select, #coupon_manual').unbind('change');
			$('.coupon_select, #coupon_manual').change(function(){
				$prototype.calculate();
			});
			
			$('#buyer_name, #buyer_city, #buyer_area, #buyer_address, #buyer_mobile, #buyer_phone, #recipient_name, #recipient_city, #recipient_area, #recipient_address, #recipient_mobile, #recipient_phone').unbind('change');
			$('#buyer_name, #buyer_city, #buyer_area, #buyer_address, #buyer_mobile, #buyer_phone, #recipient_name, #recipient_city, #recipient_area, #recipient_address, #recipient_mobile, #recipient_phone').change(function(){
				$prototype.set_info();
			});
			
			$('.payment_type').unbind('click');
			$('.payment_type').click(function(){
				$prototype.calculate();
			});
		},
		calculate: function(){
			var $prototype = this;
			
			//商品
			var row_id = $('input[name="row_id[]"]').map(function(i, e) {
				return $(e).val();
			}).get();
			
			//數量
			var quantity = $('input[name="quantity[]"]').map(function(i, e) {
				return $(e).val();
			}).get();
			
			//付款方式
			var payment_type = $('input[name="payment_type"]:checked').val();
			if( typeof(payment_type) == "undefined"){
				payment_type = 0;
			}
			
			//運送方式
			var shipping_type = $('input[name="shipping_type"]:checked').val();
			if( typeof(shipping_type) == "undefined"){
				shipping_type = 0;
			}
			
			//折扣方式
			var discount_type = $('input[name="discount_type"]:checked').val();
			if( typeof(discount_type) == "undefined"){
				discount_type = 0;
			}
			
			//選擇coupon
			var coupon_select = $('#coupon_select').val();
			if( typeof(coupon_select) == "undefined"){
				coupon_select = 0;
			}

			//輸入coupon
			var coupon_manual = $('#coupon_manual').val();
			if( typeof(coupon_manual) == "undefined"){
				coupon_manual = 0;
			}
			
			//需傳送的資料
			var data= {
				row_id: row_id,
				quantity: quantity,
				payment_type: payment_type,
				shipping_type: shipping_type,
				discount_type: discount_type,
				coupon_select: coupon_select,
				coupon_manual: coupon_manual,
			};
			
			$.ajax({
				url: 'cart/calculate/',
				type: 'POST',
				data: data,
				error: function(){
				},
				success: function(response){
					//console.log(response);
					
					var res = $.parseJSON(response);
					$('.subtotal_price').html(res.subtotal_price);
					$('.freight').html(res.freight);
					$('.discount_price').html(res.discount_price);
					$('.total_price').html(res.total_price);
				}
			});
		},
		set_info: function(){
			//購買人資訊
			var buyer = {
				name: $('#buyer_name').val(),
				city: $('#buyer_city').val(),
				area: $('#buyer_area').val(),
				address: $('#buyer_address').val(),
				mobile: $('#buyer_mobile').val(),
				phone: $('#buyer_phone').val(),
			};
			
			//收件人資訊
			var recipient = {
				name: $('#recipient_name').val(),
				city: $('#recipient_city').val(),
				area: $('#recipient_area').val(),
				address: $('#recipient_address').val(),
				mobile: $('#recipient_mobile').val(),
				phone: $('#recipient_phone').val(),
			};
			
			//同購買人
			var same = $('#same:checked').val();
			if( typeof(same) == "undefined"){
				same = 0;
			}
			
			var data= {
				buyer: buyer,
				recipient: recipient,
				same: same,
			};
			
			$.ajax({
				url: 'cart/set_info/',
				type: 'POST',
				data: data,
				error: function(){
				},
				success: function(response){
					//console.log(response);
					
					//var res = $.parseJSON(response);
				}
			});
		},
		resize: function(){
			$wizard.find('.content').animate({ height: $('.body.current').outerHeight() }, "slow");
		},
		address: function(){
			zip_code('#buyer_city', '#buyer_area');
			zip_code('#recipient_city', '#recipient_area');
		},
	};
	
	var validate = {
		main: function(){
			$form.validate({
				onblur: true,
				onkeyup: false,
				onsubmit: true,
				rules: {
				},
				messages: {
				},
				highlight: function(element, errorClass, validClass) {
					form_highlight(element, errorClass, validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					form_unhighlight(element, errorClass, validClass);
				},
				errorElement: 'span',
				errorPlacement: function($error, $element) {
					form_error_text($error, $element);
				},
				submitHandler: function(form){
					$form.ajaxSubmit(function(response){
						var res = $.parseJSON(response);
						//console.log(res);return 0;
						
						if(res.redirect){
							window.location = res.location;
						}else{
							$('.payment_gateway').html(res.html);
							$('#form2').submit();
						}
						
						return 0;
					});
				}
			});
		},
		buyer_add: function(){
			$("#buyer_name").rules("add", {
				required: true,
				is_chinese: true,
				messages: {
					required: "請輸入姓名",
					is_chinese: "只能輸入中文"
				}
			});
			$("#buyer_address").rules("add", {
				required: true,
				messages: {
					required: "請輸入地址"
				}
			});
			$("#buyer_mobile").rules("add", {
				required: true,
				is_mobile: true,
				messages: {
					required: "請輸入行動電話"
				}
			});
		},
		recipient_add: function(){
			$("#recipient_name" ).rules("add", {
				required: true,
				is_chinese: true,
				messages: {
					required: "請輸入姓名",
					is_chinese: "只能輸入中文"
				}
			});
			$("#recipient_address").rules("add", {
				required: true,
				messages: {
					required: "請輸入地址"
				}
			});
			$("#recipient_mobile").rules("add", {
				required: true,
				is_mobile: true,
				messages: {
					required: "請輸入行動電話"
				}
			});
		},
		recipient_remove: function(){
			$("#recipient_name").rules("remove");
			$("#recipient_address").rules("remove");
			$("#recipient_mobile").rules("remove");
		},
		login: function(){
			$('#form-modal-login').validate({
				onblur: true,
				onkeyup: false,
				onsubmit: true,
				rules: {
					modal_login_account: {
						required: true,
						email: true
					},
					modal_login_password: {
						required: true,
						minlength: 6
					}
				},
				messages: {
					modal_login_account: {
						required: "請輸入帳號，為email格式",
						email: "格式錯誤，應為email格式"
					},
					modal_login_password: {
						required: "請輸入密碼",
						minlength: "密碼至少 6 個字元"
					}
				},
				highlight: function(element, errorClass, validClass) {
					form_highlight(element, errorClass, validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					form_unhighlight(element, errorClass, validClass);
				},
				errorElement: 'span',
				errorPlacement: function($error, $element) {
					form_error_text($error, $element);
				},
				submitHandler: function(form){
					$('#form-modal-login').ajaxSubmit(function(response){
						var res = $.parseJSON(response);
						switch(res.status){
							case 1:
								$('#login-modal').removeClass('modal-active').modal('hide');
								
								$wizard.steps("next");
								break;
							case 2:
								$('#form-modal-login .error .error_message').html(res.message);
								$('#form-modal-login .error').removeClass('hidden').show();
								break;
							default:
							case 0:
								$('#form-modal-login .error .error_message').html(res.message);
								$('#form-modal-login .error').removeClass('hidden').show();
								break;
						}
						return 0;
					});
				}
			});
		},
		register: function(){
			$('#form-modal-register').validate({
				onblur: true,
				onkeyup: false,
				onsubmit: true,
				rules: {
					modal_register_name: {
						required: true
					},
					modal_register_account: {
						required: true,
						email: true,
						remote: {
							url: "request/member_account",
							type: "post",
							data: {
								account: function() {
									return $("#modal_register_account").val();
								}
							}
						}
					},
					modal_register_password: {
						required: true,
						minlength: 6
					},
					modal_register_password_confirm: {
						equalTo: "#modal_register_password"
					}
				},
				messages: {
					modal_register_name: {
						required: "請輸入姓名"
					},
					modal_register_account: {
						required: "請輸入帳號，為email格式",
						email: "格式錯誤，應為email格式",
						remote: "帳號已存在"
					},
					modal_register_password: {
						required: "請輸入密碼",
						minlength: "密碼至少 6 個字元"
					},
					modal_register_password_confirm: {
						equalTo: "密碼不相同"
					}
				},
				highlight: function(element, errorClass, validClass) {
					form_highlight(element, errorClass, validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					form_unhighlight(element, errorClass, validClass);
				},
				errorElement: 'span',
				errorPlacement: function($error, $element) {
					form_error_text($error, $element);
				},
				submitHandler: function(form){
					$('#form-modal-register').ajaxSubmit(function(response){
						var res = $.parseJSON(response);
						switch(res.status){
							case 1:
								$('#login-modal').removeClass('modal-active').modal('hide');
								
								$wizard.steps("next");
								break;
							default:
							case 0:
								$('.error .error_message').html(res.message);
								$('.error').removeClass('hidden').show();
								break;
						}
						return 0;
					});
				}
			});
		}
	}
	
	return {
		init: function(){
			init();
		},
		insert: function(){
			insert();
		},
	};
}();