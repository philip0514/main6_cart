// JavaScript Document
function validate_form(type){
	switch(type){
		case 'admin_login':
			$('#form1').validate({
				onblur: true,
				onkeyup: false,
				onsubmit: true,
				rules: {
					account: {
						required: true
					},
					password: {
						required: true
					}
				},
				messages: {
					account: {
						required: "請輸入帳號"
					},
					password: {
						required: "請輸入密碼"
					}
				},
				highlight: function(element, errorClass, validClass) {
					form_highlight(element, errorClass, validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					form_unhighlight(element, errorClass, validClass);
				},
				errorElement: 'span',
				errorPlacement: function($error, $element) {
					form_error_text($error, $element);
				}
			});
			break;
		
		case 'list':
			$('#form1').validate({
				onblur: true,
				onkeyup: false,
				onsubmit: true,
				submitHandler: function(form){
					deleted_item_checked(form);
				}
			});
			break;
		case 'setting':
			$('#form1').validate({
				onblur: true,
				onkeyup: false,
				onsubmit: true,
				rules: {
					title:{
						required: true
					},
					description:{
						required: true
					},
					keyword:{
						required: true
					}
				},
				messages: {
					title:{
						required: "請輸入網站名稱"
					},
					description:{
						required: "請輸入網站敘述"
					},
					keyword:{
						required: "請輸入關鍵字"
					}
				},
				highlight: function(element, errorClass, validClass) {
					form_highlight(element, errorClass, validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					form_unhighlight(element, errorClass, validClass);
				},
				errorElement: 'span',
				errorPlacement: function($error, $element) {
					form_error_text($error, $element);
				}
			});
			//console.log(mail_on)
			setting_mail();
			setting_mail_type();
			setting_mail_test();
			var changeCheckbox = document.querySelector('#mail_on');
			changeCheckbox.onchange = function() {
				setting_mail();
			};
			
			break;
	}
}

function setting_mail(){
	var mail_column = [
		['#mail_host', '請輸入信件主機網址', '必填'],
		['#mail_port', '請輸入信件主機port', '必填'],
		['#mail_username', '請輸入信件主機登入帳號', '必填'],
		//['#mail_password', '請輸入信件主機登入密碼', '必填'],
		['#mail_charset', '請輸入信件編碼，預設為 utf-8', '必填'],
		['#mail_encoding', '請輸入信件加密方式', '必填'],
		['#mail_from', '請輸入寄件者電子郵件', '必填'],
		['#mail_fromname', '請輸入寄件者姓名', '必填']
	];
	var $i;

	var mail_on = $('#mail_on').prop("checked");
	if(mail_on){
		//true
		for($i=0; $i<mail_column.length; $i++){
			$(mail_column[$i][0]).rules('add', {
				required: true,
				messages: {
					required: mail_column[$i][1]
				}
			});
			$(mail_column[$i][0]).prop('disabled', false).addClass('required').parents('.form-input').addClass('required').find('.help').text(mail_column[$i][2]);
		}
	}else{
		//false
		for($i=0; $i<mail_column.length; $i++){
			$(mail_column[$i][0]).rules('remove');
			$(mail_column[$i][0]).prop('disabled', true).removeClass('required').parents('.form-input').removeClass('required').find('.help').text('');
		}
	}
}
function setting_mail_type(){
	$('.mail_type').click(function(){
		var mail_type = $('.mail_type:checked').val();
		var mail_on = $('#mail_on').prop("checked");
		if(mail_type==1 && mail_on){
			$('#mail_host').val('smtp.gmail.com');
			$('#mail_port').val('465');
			$('#mail_charset').val('utf-8');
			$('#mail_encoding').val('base64');
			if($(".mail_secure:checked").val()!=1){
				$("#mail_secure1").prop("checked", true);
			}
		}
	});
}
function setting_mail_test(){
	$('.btn-mailtest').click(function(){
		$('.mailtest_result').html('<div class="progress-circle-indeterminate progress-circle-primary m-t-45" ></div>');
		$.ajax({
			url: 'admin/setting/mailtest/',	//檔案位置
			type: 'POST',	//or POST
			data: {
				mail_host: $('#mail_host').val(),
				mail_port: $('#mail_port').val(),
				mail_username: $('#mail_username').val(),
				mail_password: $('#mail_password').val(),
				mail_charset: $('#mail_charset').val(),
				mail_encoding: $('#mail_encoding').val(),
				mail_from: $('#mail_from').val(),
				mail_fromname: $('#mail_fromname').val(),
				mail_secure: $(".mail_secure:checked").val()
			},
			error : function(xhr, textStatus, errorThrown){
				alert(xhr.status+' '+errorThrown);
			},
			success: function(response) {
				$('.mailtest_result').html('<pre>'+response+'</pre>');
			}
		});
	});
}

//手機驗證
jQuery.validator.addMethod("is_mobile", function(value, element) {
	var str = value;
	var result = false;
	if(str.length > 0){
		
		if(str.substr(0,1)=='+'){
			str = str.substr(1,str.length-1);
		}
		
		//是否只有數字;
		var patt_mobile = /^[\d]{1,}$/;
		result = patt_mobile.test(str);
 
		if(result){
			//檢查前兩個字是否為 09
			//檢查前四個字是否為 8869
			var firstTwo = str.substr(0,2);
			var firstFour = str.substr(0,4);
			var afterFour = str.substr(4,str.length-1);
			if(firstFour == '8869'){
				$(element).val('09'+afterFour);
				if(afterFour.length == 8){
					result = true;
				} else {
					result = false;
				}
			} else if(firstTwo == '09'){
				if(str.length == 10){
					result = true;
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		}
	} else {
		result = true;
	}
	return result;
	
}, "請正確填寫您的手機號碼");

// 中文驗證
jQuery.validator.addMethod("chinese", function(value, element) {
	var chinese = /^[\u4e00-\u9fa5]+$/;
	return this.optional(element) || (chinese.test(value));
}, "只能輸入中文");

// 網址名稱
jQuery.validator.addMethod("url_name", function(value, element) {
	//var url_name = '/[^a-zA-Z0-9~%.:_\-]+$/';
	var url_name = /^[a-zA-Z0-9_\-]+$/;
	return this.optional(element) || (url_name.test(value));
}, "只能輸入英文、數字、與符號（ 包含： _ - ）");


function deleted_item_checked(form){
	var num = $('input:checkbox[name="delete[]"][checked="checked"]').length;
	if(num>0){
		var confirmed = confirm('您確定要刪除這 '+num+' 個項目？');
		if(confirmed){
			form.submit();
		}
		return false;
	}else{
		form.submit();
	}
}
function form_highlight(element, errorClass, validClass){
	$(element).parents('.form-group').find('.help-inline').show('fast');
	$(element).parents(".form-group").removeClass("has-success");
	$(element).parents(".form-group").addClass("has-error");
}

function form_unhighlight(element, errorClass, validClass){
	$(element).parents('.form-group').find('.help-inline').hide('fast');
	$(element).parents('.form-group').addClass('has-success');
	$(element).parents(".form-group").removeClass("has-error");
}
function form_error_text($error, $element){
	var text = $error[0].innerText;
	if(text){
		$element.parents('.form-group').find('.help-inline').html(text);
	}
}

function validate_method(method){
	switch(method){
		case 'ticket':
			jQuery.validator.addMethod("ticket", function(value, element) {
			}, "請輸入資料");
			
			break;
	}
}

function handler_admin_member(form){
	if($('#here').val()==3){
		form.submit();
	}else{
		$('#form1').ajaxSubmit(function(response){
			var res = $.parseJSON(response);
			if(res.id){
				$('#form1 #id').val(res.id);

				if(res.temp_html){
					$('#form1 .temp_html').prepend(res.temp_html);
				}

				$('.submit_saved').show();

				$(".submit_saved").show( 'drop', {}, 500, function(){
					setTimeout(function() {
						$( ".submit_saved" ).fadeOut();
					}, 1000 );
				});
			}

			return 0;
		});
	}
}

function handler_admin_category(form){
	$('#form2').ajaxSubmit(function(response){
		var name = $('#form2 #name').val();
		var id = $('#form2 #id').val();
		var new_id = $('#form2 #new_id').val();
		var display = $('#form2 #display').prop("checked");

		$('ol.sortable li#list_'+id+' div .text').first().html(name);
		if(new_id){
			$('ol.sortable li#list_'+new_id+' div .text').first().html(name);
		}

		if(display){
			$('ol.sortable li#list_'+id+'').removeClass('alert').removeClass('alert-danger');
		}else{
			$('ol.sortable li#list_'+id+'').addClass('alert').addClass('alert-danger');
		}

		res = $.parseJSON(response);
		if(res.id){
			if(res.new_id){
				$('ol.sortable li#list_'+new_id).first().prop('id', 'list_'+res.id);
			}
			$('#category_edit').modal('hide');
		}
	});
}

function handler_admin_product(form){
	tinyMCE.triggerSave();

	if($('#here').val()==3){
		//return 0
		form.submit();
	}else{
		$('#form1').ajaxSubmit(function(response){
			var res = $.parseJSON(response);
			if(res.id){
				$('#form1 #id').val(res.id);
				$('.submit_saved').show();
				$(".submit_saved").show( 'drop', {}, 500, function(){
					setTimeout(function() {
						$( ".submit_saved" ).fadeOut();
					}, 1000 );
				});
			}

			return 0;
		});
	}
}