var dtable,
	dt_count	=	1, 
	first_load	=	1,
	sdom;
$.fn.DataTable.ext.pager.numbers_length = 8;
(function(window, document, undefined) {
	
	//default has filter
	sdom = "<'row min-height-150'"+
				"<'col-md-6 datatable-header-left'"+
					"<'adv_search'>"+
				">"+
				"<'col-md-6 datatable-header-right'"+
					"<'submit-btn-group pull-right p-t-10 p-l-10'>"+
					"<'pull-right p-t-10 p-l-5 clear-both md-clear-none'l>"+
					"<'pull-right sm-pull-left p-t-10'f>"+
					"<'datatable-header-right-pagination'p>"+
				">r"+
			">"+
			
			"<'row'"+
				"<'col-md-12'"+
					"t"+
				">"+
			">"+
			"<'row'<'col-md-4'i><'col-md-8'p>>";
	
	if(settings.adv_search_status){
		//without filter
		sdom = "<'row min-height-150'"+
				"<'col-md-6 datatable-header-left'"+
					"<'adv_search'>"+
				">"+
				"<'col-md-6 datatable-header-right'"+
					"<'submit-btn-group pull-right p-t-10 p-l-10'>"+
					"<'pull-right p-t-10 p-l-5 clear-both md-clear-none'l>"+
					//"<'pull-right sm-pull-left p-t-10'f>"+
					"<'datatable-header-right-pagination'p>"+
				">r"+
			">"+
			"<'row'"+
				"<'col-md-12'"+
					"t"+
				">"+
			">"+
			"<'row'<'col-md-4'i><'col-md-8'p>>";
	}
	
	if(settings.sdom){
		//custom sdom
		sdom = settings.sdom;
	}
	
    var factory = function($, DataTable) {
        "use strict";
        $.extend(true, DataTable.defaults, {
			dom: sdom,
			//fixedHeader: true,
            renderer: "bootstrap",
			pagingType: "full_numbers",
			columnDefs: settings.column, //[{"width":'150px', 'targets': [0]}],
			lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			aaSorting: settings.orderby,
			
			//ajax
			bProcessing: true,
			bServerSide: true,
			sAjaxSource: settings.path.ajax,
			fnServerData: function ( sSource, aoData, fnCallback, oSettings ) {
				
				aoData = datatable_functions.server_data(aoData);
					
				$.ajax({
					"dataType": 'json',
					"type": "GET",
					"url": sSource,
					"data": aoData,
					"error": function(){
						datatable_functions.loading_error();
					},
					"success":fnCallback
				});
			},
			fnDrawCallback: function(oSettings){
				datatable_functions.callback(oSettings, settings);
			},
			fnInitComplete: function(oSettings, json) {
				datatable_functions.init_complete();
			},
			oLanguage: {
				sSearch: "",
				sLengthMenu: "_MENU_",
				sZeroRecords: settings.datatable.zero,
				sInfo: settings.datatable.info,
				sInfoEmpty: "0 ~ 0",
				sInfoFiltered: settings.datatable.info_filtered,
				searchPlaceholder: settings.datatable.search_placeholder,
				oPaginate: {
					sFirst: settings.datatable.first,
					sLast: settings.datatable.last,
				}
			},
			language: {
				"processing": "<div class='processing_alert alert alert-warning'>Loading. Please wait...</div>"
			},

			//save state
			bStateSave: true,
			iCookieDuration: 60*60	// 1 hour
        });
		
        $.extend(DataTable.ext.classes, {
            sWrapper: "dataTables_wrapper dt-bootstrap",
            sFilterInput: "form-control",
            sLengthSelect: "form-control selectpicker",
        });
		
        DataTable.ext.renderer.pageButton.bootstrap = function(settings, host, idx, buttons, page, pages) {
            var api = new DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass, counter = 0;
            var attach = function(container, buttons) {
                var i, ien, node, button;
                var clickHandler = function(e) {
                    e.preventDefault();
                    if (!$(e.currentTarget).hasClass('disabled')) {
                        api.page(e.data.action).draw('page');
                    }
                };
                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];
                    if ($.isArray(button)) {
                        attach(container, button);
                    } else {
                        btnDisplay = '';
                        btnClass = '';
                        switch (button) {
                            case 'ellipsis':
                                btnDisplay = '…';
                                btnClass = 'disabled';
                                break;
                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;
                            case 'previous':
                                btnDisplay = '<i class="pg-arrow_left"></i>';
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;
                            case 'next':
                                btnDisplay = '<i class="pg-arrow_right"></i>';
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;
                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;
                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ? 'active' : '';
                                break;
                        }
                        if (btnDisplay) {
                            node = $('<li>', {
                                'class': classes.sPageButton + ' ' + btnClass,
                                'id': idx === 0 && typeof button === 'string' ? settings.sTableId + '_' + button : null
                            }).append($('<a>', {
                                'href': '#',
                                'aria-controls': settings.sTableId,
                                'data-dt-idx': counter,
                                'tabindex': settings.iTabIndex
                            }).html(btnDisplay)).appendTo(container);
                            settings.oApi._fnBindAction(node, {
                                action: button
                            }, clickHandler);
                            counter++;
                        }
                    }
                }
            };
            var activeEl;
            try {
                activeEl = $(host).find(document.activeElement).data('dt-idx');
            } catch (e) {}
            attach($(host).empty().html('<ul class=""/>').children('ul'), buttons);
            if (activeEl) {
                $(host).find('[data-dt-idx=' + activeEl + ']').focus();
            }
        };
    };
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'datatables'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'), require('datatables'));
    } else if (jQuery) {
        factory(jQuery, jQuery.fn.dataTable);
    }
	
})(window, document);

var datatable_functions = function () {
	var loading_error = function(){
		$('.processing_alert').removeClass('alert-warning').addClass('alert-danger').html('Opps, Something goes wrong...');
	}
	
	var checkbox = function(){
		//上下架
		$('.checkbox_display').unbind('click');
		$('.checkbox_display').click(function(e) {
			if($(this).children('input').attr('checked')=='checked'){
				$(this).children('input').removeAttr('checked');
				$(this).removeClass('display_checked');
				$(this).addClass('display_unchecked');
			}else{
				$(this).children('input').attr('checked', 'checked');
				$(this).removeClass('display_unchecked');
				$(this).addClass('display_checked');
			}
		});

		//刪除
		$('.checkbox_delete').unbind('click');
		$('.checkbox_delete').click(function(e) {
			if($(this).children('input').attr('checked')=='checked'){
				$(this).children('input').removeAttr('checked');
				$(this).removeClass('delete_checked');
				$(this).addClass('delete_unchecked');
			}else{
				$(this).children('input').attr('checked', 'checked');
				$(this).removeClass('delete_unchecked');
				$(this).addClass('delete_checked');
			}
		});
	}
	
	var callback = function(oSettings, settings){
		
		checkbox();
		
		if(settings.icon.sort>0){
			$('#limit_start').val(oSettings._iDisplayStart+1);
			$('#epp').val(oSettings._iDisplayLength);
			$('#page_no').val(Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ));
		}

		if(dt_count==1){
			dt_count++;
			if($('.sort_icon').length>0){
				dtable.rowReordering({iIndexColumn:settings.orderby[0][0]});
				$('.action_group').css('cursor', 'ns-resize');
			}
		}

		if($(".sort_icon").length==0){
			$('input[name="sort"]').hide();
		}else{
			$('input[name="sort"]').show();
		}
		
		switch(settings.function){
			default:
				break;
			case 'member':
				use_member();
				break;
			case 'product':
				//product_copy();
				break;
		}
	}
	
	var init_complete = function(){
		if($(".sort_icon").length==0){
			$('button[name="sort"]').hide();
		}else{
			$('button[name="sort"]').show();
		}
		
		var visible_none = [], visible_all = [];
		for(var $i=1; $i<settings.column_visible.length; $i++){
			visible_all.push($i);
			if(!settings.column_visible[$i]){
				visible_none.push($i);
			}
		}
		dtable.api().columns(visible_all).visible(true);
		dtable.api().columns(visible_none).visible(false);
		//console.log(settings.column_visible);
		
		//var $column_visible = $('#column_visible');
		//console.log($column_visible.val())
	}
	
	var server_data = function(aoData){
		
		if(settings.add_vars){
			//後端增加參數
			for(var i=0; i<settings.add_vars.length; i++){
				aoData.push( settings.add_vars[i] );
			}
		}


		$(".submit-btn-group").html(settings.button);
		
		if(settings.adv_search_status){
			//進階搜尋
			if(first_load==1){
				$('.adv_search').html(settings.adv_search.html);

				$('#s').click(function(e){
					e.preventDefault();
					dtable.fnDraw();
				});

				$('#cancel').click(function(){
					form_reset('.adv_search');
					dtable.fnDraw();
				});
				
				var $column_visible = $('#column_visible');
				if($column_visible.length){
					$column_visible.on('change', function (e) {
						var column_length = settings.table_columns.length;
						var value = $(this).val();
						var is_inArray = 0;
						var visible_none = [], visible_all = [];
						
						for(var $i=1; $i<column_length; $i++){
							visible_all.push($i);
							is_inArray = $.inArray($i.toString(), value);
							
							if(is_inArray==-1){
								//dtable.fnSetColumnVis( $i, false );
								visible_none.push($i);
							}else{
								//dtable.fnSetColumnVis( $i, true );
							}
						}
						dtable.api().columns(visible_all).visible(true);
						dtable.api().columns(visible_none).visible(false);
						//dtable.fnSetColumnVis( visible_all, true );
						//dtable.fnSetColumnVis( visible_none, false );
						//console.log(settings.path.list);
						//return 0;
						$.ajax({
							url: settings.path.list+'column_visible/',	//檔案位置
							type: 'GET',	//or POST
							data: {column: value},
							error : function(xhr, textStatus, errorThrown){
								console.log(xhr.status+' '+errorThrown);
							},
							success: function(response) {
								//$(DivId).html(response);
							}
						});
					});
				}

				first_load++;
			}

			for(var i=0; i<settings.adv_search.vars.length; i++){
				var input_name = settings.adv_search.vars[i];

				if(input_name.indexOf("[]")!=-1){
					$('input[name="'+input_name+'"]:checked').each(function(index, element) {
						aoData.push( { "name": input_name, "value": $(this).val() } );
					});
				}else{
					var value = $('.form-control[name="'+input_name+'"]').val();
					if(value){
						aoData.push( { "name": input_name, "value": value } );
					}
				}
			}
		}
		
		return aoData;
	}
	
	return {
		loading_error: function(){
			loading_error();
		},
		checkbox: function(){
			checkbox();
		},
		callback: function(oSettings, settings){
			callback(oSettings, settings);
		},
		init_complete(){
			init_complete();
		},
		server_data(aoData){
			return server_data(aoData);
		}
	}
}();