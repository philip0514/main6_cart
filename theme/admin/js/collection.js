// JavaScript Document

var Collection = function(){
	var init = function(){
		bs_datetimepicker('#start_date');
		bs_datetimepicker('#end_date');
		product.insert();
		product.delete();
		product.delete_all();
		product.sortable();
		product.reindex();
	};
	
	var product = {
		insert: function(){
			var $prototype = this;
			
			$('.product_insert').unbind('click');
			$('.product_insert').click(function(e) {
				var collection_id = $('#id').val();
				if(!collection_id){
					alert('請先儲存基本資料');
					return 0;
				}
				$.ajax({
					type: 'POST',
					url: 'admin/collection/product_insert/',
					data: {
						collection_id: collection_id,
						product_id: $('#product_id').val()
					},
					success: function(response){
						var res = $.parseJSON(response);
						$('.product_sortable .row').prepend(res.html);
						$prototype.reindex();
						$('#product_id').val('');
					}
				});
			});
		},
		delete: function(){
			var $prototype = this;
			
			$('.product_delete').unbind('click');
			$('.product_delete').click(function(){
				var $confirm = confirm('您確定要刪除商品？');
				var $this = $(this);
				var product_id = $this.data('product_id');
				if($confirm){
					$.ajax({
						type: 'POST',
						url: 'admin/collection/product_delete/',
						data: {
							collection_id: $('#id').val(),
							product_id: product_id
						},
						success: function(response){
							$this.parents('.product_item').remove();
						}
					});
				}
			});
			
		},
		delete_all: function(){
			var $prototype = this;
			
			$('.product_delete_all').unbind('click');
			$('.product_delete_all').click(function(){
				var $confirm = confirm('您確定要刪除全部商品？');
				if($confirm){
					$.ajax({
						type: 'POST',
						url: 'admin/collection/product_delete_all/',
						data: {
							collection_id: $('#id').val()
						},
						success: function(response){
							$('.product_sortable .row').html('');
						}
					});
				}
			});
		},
		sortable: function(){
			var $prototype = this;
			
			$('.product_sortable .row').sortable({
				start: function(e, ui){
					ui.placeholder.height(ui.item.outerHeight());
				},
				update: function( event, ui ) {
					$prototype.reindex();
				}
			});
		},
		reindex: function(){
			$('.input-sortable').each(function(index, element) {
				$(this).val(index+1);
			});
		}

	};
	
	return {
		init: function(){
			init();
		},
	};
}();