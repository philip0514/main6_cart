var nested_sortable = function () {
	
	
	var edit_link = function (){
		$('.edit_link').unbind('click');
		$('.edit_link').click(function(){
			id = $(this).parents('li').attr('id');

			$.ajax({
				type: 'GET',
				url: 'admin/category/ajax_item/',
				data: {
					'id': id,
				},
				success: function(data){
					$('.modal_parent').html(data);
					$('#category_edit').modal();
				}
			});
		});
	}

	var delete_link = function (){
		$('.delete').unbind('click');
		$('.delete').click(function(){
			var i = $(this).parents('li');
			id = i.attr('id');
			var c = confirm('您確定要刪除此分類，底下的分類也會一併被刪除！');
			if(c){
				content = '<input name="delete_id[]" id="delete_id[]" type="hidden" value="'+id+'" />';
				$('.btn-save-group').append(content);
				$(this).parent().parent().find('li').each(function(index, element) {
					content = '<input name="delete_id[]" id="delete_id[]" type="hidden" value="'+$(this).attr('id')+'" />';
					$('.btn-save-group').append(content);
				});
				$('#'+id).remove();
			}
		});
	}
	
	var init = function(){
		$('ol.sortable').nestedSortable({
			disableNestingClass: 'no-nest',
			forcePlaceholderSize: true,
			//handle: 'div',
			helper:	'clone',
			items: 'li:not(.unsortable)',
			opacity: .6,
			placeholder: 'placeholder',
			revert: 250,
			tabSize: 25,
			tolerance: 'pointer',
			toleranceElement: '> div',
			maxLevels: 2,

			isTree: true,
			expandOnHover: 700,
			startCollapsed: false,
			handle: ".handler"
		});

		$('#new_item').on('click', function() {
			var new_time = new Date().getTime();
			var content = 
			'<li id="list_'+new_time+'" class="alert alert-success"><div>'+
			'<span class="disclose"><span></span></span>'+
			'<span class="text">New Item</span>'+
			'<a href="javascript:;" class="pull-right sortable_icon delete" title="刪除"><i class="fa fa-trash"></i></a>'+
			'<a href="javascript:;" class="pull-left sortable_icon handler" title="拖曳排序"><i class="fa fa-sort"></i></a>'+
			'<a href="javascript:;" class="pull-right sortable_icon edit_link" title="修改此分類"><i class="fa fa-pencil"></i></a>'+
			'</div></li>';
			$('ol.sortable > li:first-child').before(content);

			edit_link();
			delete_link();
		});

		edit_link();
		delete_link();

		$('.disclose').on('click', function() {
			$(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
		});

		$('#expanded').on('click', function() {
			$('ol.sortable li').removeClass('mjs-nestedSortable-collapsed').addClass('mjs-nestedSortable-expanded');
		});

		$('#collapsed').on('click', function() {
			$('ol.sortable li').removeClass('mjs-nestedSortable-expanded').addClass('mjs-nestedSortable-collapsed');
		});

		$('.save_normal').click(function(e){
			arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
			$('#ssort').val(JSON.stringify(arraied));
			$('#form1').submit();
		});
	}
	
	return {
		init(){
			init();
		},
	}
}();