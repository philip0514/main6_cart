// JavaScript Document
var toolbar_top,		//item page submit toolbar
	toolbar_width;

$.fn.scrollBottom = function() { 
	return $(document).height() - this.scrollTop() - this.height(); 
};


$(document).ready(function(){
	$(document).on('show.bs.modal', '.modal', function (event) {
		var zIndex = 1040 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function() {
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	});
});

function form_reset(element) {
	var $form = $(element);
    $form.find('input:text, input:password, input:file, textarea').val('');
    $form.find('input:radio, input:checkbox')
         .removeAttr('checked').removeAttr('selected');
	$form.find('select option:first').attr('selected','selected');
	$form.find('select').not('#column_visible').each(function(){
		var value = $(this).children('option').first().val();
		$(this).val(value);
	}).selectpicker('refresh');
}

function toolbar_position(){
	if($(window).scrollBottom() > 80) {
		$('.sticky_bottom').removeClass('sticky_bottom_stop');
	}
	if($(window).scrollBottom() < 79){
		$('.sticky_bottom').addClass('sticky_bottom_stop');
	}

	if(toolbar_top<$(window).scrollTop()+$('#toolbar').height()){
		$('#toolbar').css({
			'position': 'absolute',
			'width'	:	toolbar_width+'px'
		})
	}else if(toolbar_top<$(window).scrollTop()){
		$('#toolbar').css({
			'position': 'fixed',
			'width'	:	toolbar_width+'px'
		})
	}else{
		$('#toolbar').css({
			'position': 'absolute',
			'width'	:	toolbar_width+'px'
		})
	}
}

function tinymce_config(selector, language='english'){
	var base_url = $('base').attr('href');
	return {
		selector: selector,
		theme: 'modern',
		skin: 'light',
		language: language,
		skin_url: base_url+'library/tinymce/js/tinymce/skins/light/',
		height: 500,
		plugins: [
			'advlist autolink lists link image charmap preview anchor',//imagetools 
			'searchreplace visualblocks fullscreen',
			'insertdatetime media table contextmenu paste',
			'codemirror'
		],
		menubar: 'edit insert view format table',
		removed_menuitems: 'newdocument',
		toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link media_modal | code',
		
  		document_base_url: base_url,
		relative_urls: false,
		remove_script_host: false,
		convert_urls: false,
		
		content_css: '//www.tinymce.com/css/codepen.min.css',
		setup: function (editor) {
			editor.addButton('media_modal', {
				text: '',
				icon: 'mce-ico mce-i-image',
				onclick: function(){
					$(this).media_mce({editor: editor});
				}
			});
		},
		codemirror: {
			indentOnInit: true, 				// Whether or not to indent code on init.
			fullscreen: false,   				// Default setting is false
			path: 'codemirror-5.21.0', 			// Path to CodeMirror distribution
			config: {           				// CodeMirror config object
				mode: 'htmlmixed',
				//theme: 'mdn-like',
				
				theme: 'default',
				lineNumbers: true,
				lineWrapping: true,
				indentUnit: 2,
				tabSize: 2,
				indentWithTabs: true,
				matchBrackets: true,
				saveCursorPosition: true,
				styleActiveLine: true
			},
			width: 1000,         				// Default value is 800
			height: 600,        				// Default value is 550
			jsFiles: [          				// Additional JS files to load
				'mode/clike/clike.js',
				'mode/php/php.js'
			]
		}
	};
}

function datetimepicker_active(){
	var oldMethod = $.datepicker._generateMonthYearHeader;
	$.datepicker._generateMonthYearHeader = function(){
		var html = $("<div />").html(oldMethod.apply(this,arguments));
		var monthselect = html.find(".ui-datepicker-month");
		monthselect.insertAfter(monthselect.next());
		return html.html();
	}
	
	$.datepicker.regional['zh-TW'] = {
		clearText: '清除', clearStatus: '清除已選日期',
		closeText: '關閉', closeStatus: '取消選擇',
		prevText: '<上一月', prevStatus: '顯示上個月',
		nextText: '下一月>', nextStatus: '顯示下個月',
		currentText: '今天', currentStatus: '顯示本月',
		monthNames: ['一月','二月','三月','四月','五月','六月',
		'七月','八月','九月','十月','十一月','十二月'],
		monthNamesShort: ['一月','二月','三月','四月','五月','六月',
		'七月','八月','九月','十月','十一月','十二月'],
		monthStatus: '選擇月份', yearStatus: '選擇年份',
		weekHeader: '周', weekStatus: '',
		dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
		dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
		dayNamesMin: ['日','一','二','三','四','五','六'],
		dayStatus: '設定每周第一天', dateStatus: '選擇 m月 d日, DD',
		dateFormat: 'yy-mm-dd', firstDay: 0, 
		initStatus: '請選擇日期', isRTL: false
	};
	$.datepicker.setDefaults($.datepicker.regional['zh-TW']);
	
	$.timepicker.regional['zh-TW']={
		timeOnlyTitle:"選擇時分秒",
		timeText:"時間",
		hourText:"時",
		minuteText:"分",
		secondText:"秒",
		millisecText:"毫秒",
		timezoneText:"時區",
		currentText:"現在時間",
		closeText:"確定",
		amNames:["上午","AM","A"],
		pmNames:["下午","PM","P"],
		isRTL: false
	};
	$.timepicker.setDefaults($.timepicker.regional['zh-TW']);
}

function custom_time(choose){
	$(choose).datepicker({
		changeYear: true,
		changeMonth: true,
		dateFormat: "yy/mm/dd",
		addSliderAccess: true,
		sliderAccessArgs: { touchonly: false },
		yearRange: '-1:+1'
	}).inputmask("y/m/d", { "placeholder": "____/__/__" });
}

function birth_time(choose){
	$(choose).datepicker({
		changeYear: true,
		changeMonth: true,
		dateFormat: "yy/mm/dd",
		addSliderAccess: true,
		sliderAccessArgs: { touchonly: false }
	}).inputmask("y/m/d", { "placeholder": "____/__/__" });
}

function start_end_time(){
	
	$('.start_time').datetimepicker({
		changeYear: true,
		changeMonth: true,
		dateFormat: "yy/mm/dd",
		timeFormat: 'HH:mm',
		addSliderAccess: true,
		sliderAccessArgs: { touchonly: false },
		yearRange: '-1:+1',
		onSelect: function(selectedDate){
			var end_time = $(this).parent().parent().children('.time_selector').children('.end_time');
			var end = end_time.val();
			if(new Date(end) < $(this).datetimepicker('getDate')){
				end_time.datetimepicker('option', 'minDate', $(this).datetimepicker('getDate'));
				end_time.val($(this).val());
			}
		},
		onClose: function(dateText, inst) {
			var end_time = $(this).parent().parent().children('.time_selector').children('.end_time');
			if (!end_time.val()){
				end_time.val(dateText);
			}else{
				var start_date = $(this).datetimepicker('getDate');
				var end_date = end_time.datetimepicker('getDate');
				if (start_date > end_date)
					end_time.datetimepicker('setDate', start_date);
			}
		}
	}).inputmask("y/m/d h:s", { "placeholder": "____/__/__ __:__" });
	
	$('.end_time').datetimepicker({
		changeYear: true,
		changeMonth: true,
		dateFormat: "yy/mm/dd",
		timeFormat: 'HH:mm',
		addSliderAccess: true,
		sliderAccessArgs: { touchonly: false },
		yearRange: '-1:+1',
		onSelect: function(selectedDate){
			var start_time = $(this).parent().parent().children('.time_selector').children('.start_time');
			var start = start_time.val();
			if(new Date(start) > $(this).datetimepicker('getDate')){
				start_time.datetimepicker('option', 'maxDate', $(this).datetimepicker('getDate'));
				start_time.val($(this).val());
			}
		},
		onClose: function(dateText, inst) {
			var start_time = $(this).parent().parent().children('.time_selector').children('.start_time');
			if (!start_time.val()){
				start_time.val(dateText);
			}else{
				var start_date = start_time.datetimepicker('getDate');
				var end_date = $(this).datetimepicker('getDate');
				if (start_date > end_date)
					start_time.datetimepicker('setDate', end_date);
			}
		}
	}).inputmask("y/m/d h:s", { "placeholder": "____/__/__ __:__" });
	
	
	
}


function bs_datetimepicker(element){
	$(element).datetimepicker({
		sideBySide: true,
		format: 'YYYY/MM/DD HH:mm',
		showTodayButton: true,
		showClear: true,
		showClose: true,
		icons:{
			today: 'glyphicon glyphicon-time'
		}
	});
}

function date_datetimepicker(element){
	$(element).datetimepicker({
		format: 'YYYY/MM/DD',
		showTodayButton: true,
		showClear: true,
		showClose: true,
		icons:{
			today: 'glyphicon glyphicon-time'
		}
	});
}

function bs_datetimepicker_relate(e1, e2){
	$(e1).on("dp.hide", function (e) {
		//console.log($(e2).val());
		var e2_value = $(e2).val();
		$(e2).data("DateTimePicker").minDate(e.date);
		if(!e2_value){
			$(e2).val('');
		}
	});
	$(e2).on("dp.hide", function (e) {
		var e1_value = $(e1).val();
		$(e1).data("DateTimePicker").maxDate(e.date);
		if(!e1_value){
			$(e1).val('');
		}
	});
}

function user_item(){
	$('.rights_editall').click(function(){
		var checked = $(this).is(':checked');
		if(checked){
			$(this).parents('li').children('label').children('.rights_edit').prop('checked', true);
			$(this).parents('li').children('label').children('.rights_readonly').removeAttr('checked');
		}else{
			$(this).parents('li').children('label').children('.rights_edit').removeAttr('checked');
		}
	});
	
	$('.rights_edit').click(function(){
		var checked = $(this).is(':checked');
		if(checked){
			if($(this).parents('li').children('label').children('.rights_edit[checked="checked"]').length==3){
				$(this).parents('li').children('label').children('.rights_editall').prop('checked', true);
			}
			$(this).parents('li').children('label').children('.rights_readonly').removeAttr('checked');
		}else{
			$(this).parents('li').children('label').children('.rights_editall').removeAttr('checked');
		}
	});
	
	$('.change_rights').click(function(){
		var checked = $(this).is(':checked');
		if(checked){
			$(this).parents('li').children('label').children('.rights_edit').prop('checked', true);
			$(this).parents('li').children('label').children('.rights_editall').prop('checked', true);
		}else{
			$(this).parents('li').children('label').children('.rights_edit').removeAttr('checked');
			$(this).parents('li').children('label').children('.rights_editall').removeAttr('checked');
		}
		
	});
	
	$('.rights_readonly').click(function(e) {
		var checked = $(this).is(':checked');
		if(checked){
			$(this).parents('li').children('label').children('.rights_edit').removeAttr('checked');
			$(this).parents('li').children('label').children('.rights_editall').removeAttr('checked');
		}else{
		}
	});
}

/*
 *	ajax Sample
 */
function sample_ajax(DivId, urladres) {
	$.ajax({
		url: urladres,	//檔案位置
		type: 'GET',	//or POST
		data: {gen1: $('#gen1').val(), gen2:$('#gen2').val()},
		error : function(xhr, textStatus, errorThrown){
			alert(xhr.status+' '+errorThrown);
		},
		success: function(response) {
			$(DivId).html(response);
		}
	});
}

/*
	使用會員身份
*/
function use_member(){
	$('.use_member').unbind('click');
	$('.use_member').click(function(){
		//e.preventDefault();
		var id = $(this).attr('value');
		$.ajax({
			url: 'request/use_member/'+id+'/',	//檔案位置
			type: 'POST',	//or POST
			data: {},
			error: function(xhr) {
				alert('Ajax request 發生錯誤');
			},
			success: function(response) {
				//$(DivId).html(response);
				if(!response){
					alert('已切換會員');
				}
			}
		});
		
		return false;
	});
}
function admin_rights(){
	$('.rights_editall').click(function(){
		var checked = $(this).is(':checked');
		if(checked){
			$(this).parents('li').children('label').children('.rights_edit').prop('checked', true);
			$(this).parents('li').children('label').children('.rights_readonly').removeAttr('checked');
		}else{
			$(this).parents('li').children('label').children('.rights_edit').removeAttr('checked');
		}
	});
	
	$('.rights_edit').click(function(){
		var checked = $(this).is(':checked');
		if(checked){
			if($(this).parents('li').children('label').children('.rights_edit[checked="checked"]').length==3){
				$(this).parents('li').children('label').children('.rights_editall').prop('checked', true);
			}
			$(this).parents('li').children('label').children('.rights_readonly').removeAttr('checked');
		}else{
			$(this).parents('li').children('label').children('.rights_editall').removeAttr('checked');
		}
	});
	
	$('.change_rights').click(function(){
		var checked = $(this).is(':checked');
		if(checked){
			$(this).parents('li').children('label').children('.rights_edit').prop('checked', true);
			$(this).parents('li').children('label').children('.rights_editall').prop('checked', true);
		}else{
			$(this).parents('li').children('label').children('.rights_edit').removeAttr('checked');
			$(this).parents('li').children('label').children('.rights_editall').removeAttr('checked');
		}
		
	});
	
	$('.rights_readonly').click(function(e) {
		var checked = $(this).is(':checked');
		if(checked){
			$(this).parents('li').children('label').children('.rights_edit').removeAttr('checked');
			$(this).parents('li').children('label').children('.rights_editall').removeAttr('checked');
		}else{
		}
	});
}


function zip_code(element_city, element_area, empty_area){
	$(element_city).selectpicker();
	$(element_area).selectpicker();

	$(element_city).change(function(){
		var id = $(this).val();
		$.ajax({
			url: 'request/zip_code/',	//檔案位置
			type: 'POST',	//or POST
			data: {id: id},
			error: function(xhr) {
				alert('Ajax request 發生錯誤');
			},
			success: function(response){
				var res = $.parseJSON(response);
					//console.log(res);
				$(element_area).empty();

				if(empty_area){
					$(element_area)
						.append($("<option></option>")
						.attr("value",0)
						.text('--- 無 ---')); 
				}

				$.each(res, function(key, value) {  
					if(key==0){
						$(element_area)
						.append($("<option></option>")
						.attr("value",res[key]['id'])
						.attr('selected', 'selected')
						.text(res[key]['name'])); 
					}else{
						$(element_area)
						.append($("<option></option>")
						.attr("value",res[key]['id'])
						.text(res[key]['name'])); 
					}
				});

				$(element_area).selectpicker('refresh');
			}
		});
	});
}

function media_ogimage(selectable_limit){
	$('.ogimage_manager').media_multiple({
		area: 					'.ogimage_area',
		input_field: 			'.ogimage_input',
		selectable_limit:		selectable_limit,
		selectable_multiple:	1,
	});
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function ssort(){
	var sorting=[];
	$(".sort_icon").each(function(){
		sorting.push($(this).attr('value'));
	});
	$('#orderby').val(sorting);
	$('#here').val(2);
}

function csrf_ajax(){
	$.ajax({
		url: 'csrf/',	//檔案位置
		type: 'GET',	//or POST
		data: {
			
		},
		error : function(xhr, textStatus, errorThrown){
			console.log(xhr.status+' '+errorThrown);
		},
		success: function(response) {
			var res = $.parseJSON(response);//console.log(res);
			$('#csrf_site').val(res.hash);
			
			$.ajax({
				url: 'csrf/',	//檔案位置
				type: 'POST',	//or POST
				data: {
					csrf_site: $('#csrf_site').val()
				},
				error : function(xhr, textStatus, errorThrown){
					console.log(xhr.status+' '+errorThrown);
				},
				success: function(response) {
					var res = $.parseJSON(response);//console.log(res);
					$('#csrf_site').val(res.hash);
				}
			});
		}
	});
}

function csrf(){
	setInterval(csrf_ajax, 7000000);//7000000
}