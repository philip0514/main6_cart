// JavaScript Document
var Coupon = function () {
	
	var event = {
		type_change: function(value){
			var $prototype = this;
			
			switch(value){
				default:
				case 0:
					$('.serial_type1').show();
					$('.serial_type2').hide();
					$('.serial_type3').hide();

					$( "#serial" ).rules( "remove" );
					$( "#limit_count" ).rules( "remove" );

					$( "#amount" ).rules( "add", {
						required: true
					});
					
					$( "#length" ).rules( "add", {
						required: true
					});

					break;
				case 1:
					$('.serial_type1').show();
					$('.serial_type2').show();
					$('.serial_type3').hide();

					$( "#serial" ).rules( "remove" );
					$( "#limit_count" ).rules( "remove" );

					$( "#amount" ).rules( "add", {
						required: true
					});
					
					$( "#length" ).rules( "add", {
						required: true
					});

					break;
				case 2:
					$('.serial_type1').hide();
					$('.serial_type2').hide();
					$('.serial_type3').show();

					$( "#amount" ).rules( "remove" );
					$( "#length" ).rules( "remove" );

					$( "#serial" ).rules( "add", {
						required: true,
						remote: {
							url: 'admin/coupon/ajax_coupon_check/',
							type: "POST",
							data: {
								name: function() {
									return $("#serial").val();
								}
							}
						}
					});
					
					$( "#limit_count" ).rules( "add", {
						required: true
					});

					break;
			}
			
			$prototype.serial_preview();
		},
		
		serial_preview: function(){
			var $prototype = this;
			
			var serial_type = parseInt($('.serial_type:checked').val()),
				number_start = parseInt($('#number_start').val()),
				prefix = $('#prefix').val(),
				length = $('#length').val(),
				amount = $('#amount').val(),
				serial = $('#serial').val(),
				prefix_length = prefix.length,
				string = '', 
				strings = [],
				loop = true,
				option;
			
			$('.typed_prefix').html(prefix);

			switch(serial_type){
				case 1:
					var code = [];
					for(var i=0; i<amount; i++){
						var str = $prototype.str_pad_left(number_start+i, length-prefix_length);
						strings.push(str);
						code.push(prefix+str);
					}
					if(amount==1){
						loop = false;
					}
					
					$.ajax({
						url: 'admin/coupon/ajax_serial_type1_check/',	//檔案位置
						type: 'POST',	//or POST
						data: {
							code: code
						},
						error: function(xhr) {
							//alert('Ajax request 發生錯誤');
						},
						success: function(response) {
							var res = $.parseJSON(response);
							var status = parseInt(res.status);
							if(status==1){
								$('.serial_check').removeClass('alert-warning alert-danger alert-success').addClass('alert-success').html('全部的序號皆可使用');
							}else if(status==0 && res.size==amount){
								$('.serial_check').removeClass('alert-warning alert-danger alert-success').addClass('alert-danger').html(res.size+' 組序號皆無法使用！');
							}else{
								$('.serial_check').removeClass('alert-warning alert-danger alert-success').addClass('alert-warning').html(res.size+' 組序號無法使用，如下：<br>'+res.conflict.join('、'));
							}
						}
					});
					
					
					break;
				case 2:
					$('.typed_prefix').html('');
					loop = false;
					strings.push(serial);
					break;
				default:
				case 0:
					for(var i=0; i<amount; i++){
						strings.push($prototype.random_string(length-prefix_length));
					}
					if(amount==1){
						loop = false;
					}
					break;
			}
			
			option = {
				strings: 	strings,
				typeSpeed: 	50,
				backSpeed: 	0,
				backDelay: 	1000,
				loop: 		loop,
			};

			$(".typed_element").typed(option);
		},
		
		random_string: function($count){
			var text = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			for(var i=0; i<$count; i++){
				text += possible.charAt(Math.floor(Math.random() * possible.length));
			}

			return text;
		},

		str_pad_left:function(text, len) {
			var $prototype = this;
			
			text = '' + text;
			
			if(text.length >= len){
				return text;
			}else{
				return $prototype.str_pad_left("0" + text, len);
			}
		}
	};
	
	var insert_relationship = function(){
		$('#insert_relationship').click(function(e) {
			$.ajax({
				url: 'admin/coupon/ajax_insert_relationship/',	//檔案位置
				type: 'POST',	//or POST
				data: {
					email: $('#email').val(),
					coupon_id: $('#id').val()
				},
				error: function(xhr) {
					alert('Ajax request 發生錯誤');
				},
				success: function(response) {
					//console.log(response);return 0;
					var res = $.parseJSON(response);
					if(res.content){
						$('.rows_zero').hide();
						$('.relation_table').append(res.content);

						if(res.limit_count=="1"){
							$('.insert_relationship_block').hide()
						}
						
						$('#email').val('');
					}
				}
			});

			return false;
		});
	};
	
	var init = function(){
		bs_datetimepicker('#start_date');
		bs_datetimepicker('#end_date');
		bs_datetimepicker_relate('#start_date', '#end_date');
		
		$('.serial_type').click(function(){
			var value = parseInt($(this).val());
			event.type_change(value);
		});

		$('.serial_type[value="0"]').trigger('click');

		$('#prefix, #length, #amount, #number_start, #serial').change(function(){
			event.serial_preview();
		});
	};

	return {
		init: function(){
			init();
		},
		insert_relationship: function(){
			insert_relationship();
		}
	};
}();