// JavaScript Document
var tags = new Bloodhound({
	datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
	queryTokenizer: Bloodhound.tokenizers.whitespace,
	limit: 10,
	remote: {
		url: 'request/tag_search/?q=%QUERY',
		filter: function(list){
			if(list){
				return $.map(list, function(result){
					//console.log(result);
					//return { id: result.id, name: result.name  }; 
					return { name: result };
				});
			}
		}
	}
});
tags.initialize();
tags.clearRemoteCache();

var product_gifts = new Bloodhound({
	datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
	queryTokenizer: Bloodhound.tokenizers.whitespace,
	limit: 10,
	remote: {
		url: 'request/product_gift_search/?q=%QUERY',
		filter: function(list){
			return $.map(list, function(product_gifts){
				return { id: product_gifts.id, name: product_gifts.name  }; 
			});
		}
	}
});
product_gifts.initialize();
product_gifts.clearRemoteCache();

function product_gift(){
	$('.btn-gift').unbind('click');
	$('.btn-gift').click(function(){
		var product_id = $('#id').val();
		var gift_id = $(this).data('value');
		if(product_id){
			$.ajax({
				url: 'request/product_gift/',	//檔案位置
				type: 'POST',	//or POST
				data: {
					product_id: product_id, 
					gift_id: gift_id
				},
				error: function(xhr) {
					alert('Ajax request 發生錯誤');
				},
				success: function(res) {//console.log(res);
					$('.modal_parent').html(res);
					$('#myModal').modal('show');
				}
			});
		}
	});
	$('.gift_group').sortable({
		start: function(e, ui){
			var height = ui.item.outerHeight();
			ui.placeholder.css({
				'height': height+'px'
			});
		}
	});
}