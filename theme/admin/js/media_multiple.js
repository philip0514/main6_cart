(function($){
	'use strict';
	
	var during_retrieve = false;
	
	var options = {};

	var default_options = {
		is_image:				1,
		selectable_limit:		0,
		selectable_multiple:	1,
		max_position:			0,
		modal_btn:				'.media_manager',
		modal_url:				'media/manager/',
		modal_container:		'.media_modal_content',
		modal_content:			'#media_modal',
		modal_body:				'.modal-body',
		input_field: 			'.media_input',
		area: 					'.media_area',
		element_selected:		'#media_selected',
		element_selectable:		'.media_selectable',
		element_single:			'.media_single',
		element_id:				'.media_id',
		delete_btn:				'.media_delete',
		update_btn:				'.media_update',
		template_upload_id:		'template_upload',
		template_download_id:	'template_download',
		full_column:			false
	};
 	
	function mediaMultiple(e, config){
		var $prototype = this;
		
		$(e).on('click', function(){
			options = $.extend({}, default_options, config );
			$prototype.manager();
		});
		
		options = $.extend({}, default_options, config);
		$prototype.sortable(options);
		$prototype.image.delete(options);
		//$prototype.image.edit(options);
	}
	
	mediaMultiple.prototype = {
		constructor: mediaMultiple,
		manager: function(){
      		var $prototype = this;
			$.ajax({
				type: 'POST',
				url: options.modal_url,
				data:{
					is_image: 			options.is_image,
					selectable_limit: 	options.selectable_limit,
					input_field: 		$(options.input_field).val()
				},
				error: function(xhr, textStatus) {
					alert(textStatus);
				},
				success: function(data, textStatus, jqXHR){
					$(options.modal_container).html(data);
					$(options.modal_content).modal();
					$prototype.modal();
					$prototype.upload();
					$prototype.search();
				}
			});
		},
		modal: function(){
      		var $prototype = this;
			during_retrieve = false;
			
			$prototype.selectable();
			
			//存檔
			$('.btn-save').click(function(){
				var $this,
					link,
					value,
					title,
					html 		= '',
					html_col_class = '',
					ids 		= [];
				
				$(options.element_selected+' '+options.element_selectable+' a').each(function(){
					$this 		= $(this);
					link 		= $this.data('link');
					value 		= $this.data('value');
					title 		= $this.data('title');
					
					//if(options.full_column===true){
						//html_col_class = 'col-xs-12';
					//}else{
						html_col_class = 'col-xl-3 col-lg-3 col-md-3 col-sm-4 col-xs-6';
					//}
					
					html +=	'<div class="'+html_col_class+' padding_v_10 '+options.element_single.substr(1)+'" data-value="'+value+'">'+
								'<div class="media_preview">'+
									'<img src="'+link+'" alt="'+title+'" class="img-rounded" />'+
								'</div>'+
								'<div class="media_btn_group btn-group" role="group">'+
									'<button type="button" class="btn btn-danger '+options.delete_btn.substr(1)+'">'+
										'<i class="glyphicon glyphicon-remove"></i>'+
										'刪除'+
									'</button>'+
								'</div>'+
							'</div>';
					ids.push(value);
				});
				
				$(options.input_field).val(ids.join());
				$(options.area).html(html);
				$prototype.image.delete(options);
				//$prototype.image.edit();
				$(options.modal_content).modal('hide');
			});
			
			//無限捲軸
			$prototype.infinity_scroll('mine');
			$prototype.infinity_scroll('all');
			
		},
		
		search: function(){
			var $prototype = this;
			
			$('#btn-search').click(function(){
				$prototype.search_ajax();
			});
			$('#input_search').keypress(function(e) {
				if(e.which == 13) {
					$prototype.search_ajax();
				}
			});
		},
		
		search_ajax: function(){
			var $prototype = this;
			
			$.ajax({
				type: 'GET',
				url: 'media/file_list_infinite/4/0/',
				data:{
					search: $('#input_search').val()
				},
				error: function(xhr) {
					alert(xhr);
				},
				success: function(data, textStatus, jqXHR){
					$('.media_search').html(data);
					$prototype.selectable();
					$prototype.infinity_scroll('search');
				}
			});
		},
		
		selectable: function(){
			var $prototype = this;
			
			var ids = $(options.input_field).val();
			if(ids){
				ids = ids.split(',');
			}else{
				ids = [];
			}
			
			$(options.element_selectable).selectable();
	
			//如可多選，點選就可以選擇
			if(options.selectable_multiple){
				$(options.element_selectable).on("selectablestart", function(event, ui){
					event.originalEvent.ctrlKey = true;
				});
			}
			
			//選擇
			$(options.element_selectable).on("selectableselected", function(event, ui){
				var id;
				var item_value = $(ui.selected).children('a').data('value');
				
				if(item_value){
					id = item_value.toString();
				}else{
					return 0;
				}
				
				//選擇時，順便加入已選擇區
				$('a[data-value="'+id+'"]').parent().addClass('ui-selected');
				
				if($.inArray(id, ids)==-1){
					ids.push(id);
					$(ui.selected).clone().appendTo(options.element_selected+' '+options.element_selectable);
				}
				$(options.input_field).val(ids.toString());
			});
			
			//取消選擇
			$(options.element_selectable).on("selectableunselected", function(event, ui){
				var id;
				var item_value = $(ui.unselected).children('a').data('value');
				
				if(item_value){
					id = item_value.toString();
				}else{
					return 0;
				}
				
				//取消選擇時，順便移除已選擇區
				$('a[data-value="'+id+'"]').parent().removeClass('ui-selected');
				
				$(ids).each(function(index, element) {
					if(ids[index]==id){
						ids.splice(index, 1);
						$(options.element_selected+' '+options.element_selectable).find('a[data-value="'+id+'"]').parent().remove();
					}
				});
				
				$(options.input_field).val(ids.toString());
			});
			
			if(options.selectable_limit>1){
				//數量限制
				$(options.element_selectable).on("selectableselecting", function(event, ui){
					if(ids.length >= options.selectable_limit){
						$(ui.selecting).removeClass("ui-selecting");
					}
				});
			}
		},
		
		/*
			無限捲軸
		*/
		infinity_scroll: function(type){
      		//var $prototype = this;
			var scroll_area_id,
				scroll_type,
				url;
			switch(type){
				default:
				case 'mine':
					scroll_area_id = '#media_mine';
					scroll_type = 3;
					break;
				case 'all':
					scroll_area_id = '#media';
					scroll_type = 1;
					break;
				case 'search':
					scroll_area_id = '#media_search';
					scroll_type = 4;
					break;
			}
			
			$(scroll_area_id+' .file_area').scroll(function() {
				var height = $(scroll_area_id+" .media_selectable").height();
				var scroll_position = 0-$(scroll_area_id+" .media_selectable").position().top+$(scroll_area_id+' .file_area').height();
				
				if(scroll_position+200 >= height && during_retrieve===false){
					during_retrieve = true;
					options.max_position = scroll_position;
					if(during_retrieve){
						$(scroll_area_id+' .media_selectable').infinitescroll('retrieve');
					}
				}
				
				if(options.max_position+800 <= height){
					during_retrieve = false;
				}
			});
			
			$(scroll_area_id+' .media_selectable').infinitescroll({
				navSelector  	: scroll_area_id+" .media_selectable #next:last",
				nextSelector 	: scroll_area_id+" .media_selectable a#next:last",
				itemSelector 	: ".media_selectable .file_single",
				debug		 	: false,
				dataType	 	: 'html',
				path: function(index){
					url = "media/file_list_infinite/"+scroll_type+"/"+index+"/";
					if(scroll_type==4){
						url += "?search="+$('#input_search').val();
						$(scroll_area_id+' .media_selectable a#next').remove();
					}else{
						$(scroll_area_id+' .media_selectable a#next').parent().remove();
					}
					return url;
				},
				errorCallback: function(e){
					console.log(e)
				}
				
			}, function(newElements, data, url){console.log(1)
				during_retrieve = false;
			});
			
			$(window).unbind('.infscr');
			
			$(scroll_area_id+" .media_selectable a#next:last").click(function(){
				$(scroll_area_id+' .media_selectable').infinitescroll('retrieve');
				return false;
			});
			
			$(document).ajaxError(function(e,xhr,opt){
				if(xhr.status==404){
					$(scroll_area_id+" .media_selectable a#next:last").remove();
				}
			});
		},
		
		/*
			上傳檔案
		*/
		upload: function(){
			// Initialize the jQuery File Upload widget:
			$('.fileupload').fileupload({
				url: 'media/do_upload/',
				filesContainer: '#media_mine '+options.element_selectable,
				uploadTemplateId: options.template_upload_id,
				downloadTemplateId: options.template_download_id,
				done: function (e, data) {
					 if(data.context) {
						data.context.each(function(){
							var node = $(this);
							var template = tmpl(options.template_download_id, data.result);
							$(template).replaceAll(node);
							$('#media '+options.element_selectable).prepend(template);
						});
					}
				}
			});
		},
	
		/*
			排序
		*/
		sortable: function(options){
			$(options.area).sortable({
				start: function(e, ui){
					var height = ui.item.outerHeight();
					ui.placeholder.css({
						'background':'#ebf6fb',
						'height': height+'px'
					});
				},
				stop: function( event, ui ) {
					var id_array = [];
					$(options.area+' '+options.element_single).each(function(index, element){
						id_array.push($(this).data('value'));
					});
					$(options.input_field).val(id_array.join());
				}
			});
		},
		
		/*
			已經選擇的圖片
		*/
		image: {
			delete: function(options){
				$(options.area+' '+options.delete_btn).unbind('click');
				$(options.area+' '+options.delete_btn).click(function(){
					var $this = $(this);
					var media_id = $this.parents(options.element_single).data('value');
					var input_field = $(options.input_field).val().split(',');
					var id_array = [];
					var c = confirm("確定刪除此圖片？");
					if(c){
						$.each(input_field, function( index, value ) {
							if(value==media_id){
								$this.parents(options.element_single).remove();
							}else{
								id_array.push(value);
							}
							$(options.input_field).val(id_array)
						});
					}

				});
			}
		},
			
	};
	
    $.fn.media_multiple = function(config){
		var results = [];
		
		return this.each(function(){
			new mediaMultiple(this, config);
		});
    };
	
	$.fn.media_multiple.Constructor = mediaMultiple;
 
}(jQuery));