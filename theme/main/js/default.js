function date_datetimepicker(element){
	$(element).datetimepicker({
		format: 'YYYY/MM/DD',
		showTodayButton: true,
		showClear: true,
		showClose: true,
		icons:{
			today: 'fa fa-clock-o'
		}
	});
}
function zip_code(element_city, element_area, empty_area){
	$(element_city).change(function(){
		var id = $(this).val();
		$.ajax({
			url: 'request/zip_code/',	//檔案位置
			type: 'POST',	//or POST
			data: {id: id},
			error: function(xhr) {
				alert('Ajax request 發生錯誤');
			},
			success: function(response){
				var res = $.parseJSON(response);
					//console.log(res);
				$(element_area).empty();

				if(empty_area){
					$(element_area)
						.append($("<option></option>")
						.attr("value",0)
						.text('--- 無 ---')); 
				}

				$.each(res, function(key, value) {  
					if(key==0){
						$(element_area)
						.append($("<option></option>")
						.attr("value",res[key]['id'])
						.attr('selected', 'selected')
						.text(res[key]['name'])); 
					}else{
						$(element_area)
						.append($("<option></option>")
						.attr("value",res[key]['id'])
						.text(res[key]['name'])); 
					}
				});
			}
		});
	});
}
function facebook_login(){
	FB.login(function (response) {
		FB.api('/me?fields='+facebook_scope, function(response) {
			
			var csrf_val = $('.csrf').val();
			
			$.ajax({
				url: 'member/facebook_login/',	//檔案位置
				type: 'POST',	//or POST
				data: {
					response: response,
					csrf_site: csrf_val
				},
				error: function(xhr) {
					alert('Ajax request 發生錯誤');
				},
				success: function(response){
					var res = $.parseJSON(response);
					location.href = res.location;
					
				}
			});
		}//,{scope: 'email,public_profile'}
		);
	});
}