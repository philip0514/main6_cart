! function($) {
    "use strict";
    var Pages = function() {
		this.pageScrollElement = "html, body", 
		this.$body = $("body"), 
		this.setUserOS(), 
		this.setUserAgent()
    };
    Pages.prototype.setUserOS = function() {
        var OSName = ""; - 1 != navigator.appVersion.indexOf("Win") && (OSName = "windows"), -1 != navigator.appVersion.indexOf("Mac") && (OSName = "mac"), -1 != navigator.appVersion.indexOf("X11") && (OSName = "unix"), -1 != navigator.appVersion.indexOf("Linux") && (OSName = "linux"), this.$body.addClass(OSName);
    }, 
	Pages.prototype.setUserAgent = function() {
        navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i) ? this.$body.addClass("mobile") : (this.$body.addClass("desktop"), navigator.userAgent.match(/MSIE 9.0/) && this.$body.addClass("ie9"));
    }, 
	Pages.prototype.isVisibleXs = function() {
        return !$("#pg-visible-xs").length && this.$body.append('<div id="pg-visible-xs" class="visible-xs" />'), $("#pg-visible-xs").is(":visible");
    }, 
	Pages.prototype.isVisibleSm = function() {
        return !$("#pg-visible-sm").length && this.$body.append('<div id="pg-visible-sm" class="visible-sm" />'), $("#pg-visible-sm").is(":visible");
    }, 
	Pages.prototype.isVisibleMd = function() {
        return !$("#pg-visible-md").length && this.$body.append('<div id="pg-visible-md" class="visible-md" />'), $("#pg-visible-md").is(":visible");
    }, 
	Pages.prototype.isVisibleLg = function() {
        return !$("#pg-visible-lg").length && this.$body.append('<div id="pg-visible-lg" class="visible-lg" />'), $("#pg-visible-lg").is(":visible");
    }, 
	Pages.prototype.getUserAgent = function() {
        return $("body").hasClass("mobile") ? "mobile" : "desktop";
    }, 
	Pages.prototype.initSidebar = function(context) {
        $('[data-pages="sidebar"]', context).each(function() {
            var $sidebar = $(this);
            $sidebar.sidebar($sidebar.data());
        });
    },
	Pages.prototype.getColor = function(color, opacity) {
        opacity = parseFloat(opacity) || 1;
        var elem = $(".pg-colors").length ? $(".pg-colors") : $('<div class="pg-colors"></div>').appendTo("body"),
            colorElem = elem.find('[data-color="' + color + '"]').length ? elem.find('[data-color="' + color + '"]') : $('<div class="bg-' + color + '" data-color="' + color + '"></div>').appendTo(elem),
            color = colorElem.css("background-color"),
            rgb = color.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/),
            rgba = "rgba(" + rgb[1] + ", " + rgb[2] + ", " + rgb[3] + ", " + opacity + ")";
        return rgba
    }, 
	Pages.prototype.initSwitcheryPlugin = function(context) {
        window.Switchery && $('[data-init-plugin="switchery"]', context).each(function() {
            var el = $(this);
            new Switchery(el.get(0), {
                color: null != el.data("color") ? $.Pages.getColor(el.data("color")) : $.Pages.getColor("success"),
                size: null != el.data("size") ? el.data("size") : "default"
            });
        });
	},
	Pages.prototype.initProgressBars = function() {
        $(window).on("load", function() {
            $(".progress-bar-indeterminate, .progress-circle-indeterminate, .mapplic-pin").hide().show(0);
        });
	}, 
	Pages.prototype.initHorizontalMenu = function() {
		$(".content").on("click", function(e) {
			if(!$(e.toElement).parents('.horizontal-menu').find('.bar-inner > ul > li').hasClass('open')){
            	$(".horizontal-menu .bar-inner > ul > li").removeClass("open");
			}
        });
        $(document).on("click", ".horizontal-menu .bar-inner > ul > li", function() {
			$(this).toggleClass("open").siblings().removeClass("open");
        });
		$('[data-pages="horizontal-menu-toggle"]').on("click touchstart", function(e) {
            e.preventDefault();
			$("body").toggleClass("menu-opened");
        });
    }, 
	Pages.prototype.init = function() {
        this.initSidebar(),
		this.initProgressBars(),
		this.initHorizontalMenu(), 
		this.initSwitcheryPlugin()
    }, $.Pages = new Pages, $.Pages.Constructor = Pages
}(window.jQuery),
	
function($) {
    "use strict";

    function Plugin(option) {
        return this.filter(":input").each(function() {
            var $this = $(this),
                data = $this.data("pg.circularProgress"),
                options = "object" == typeof option && option;
            data || $this.data("pg.circularProgress", data = new Progress(this, options)), "string" == typeof option ? data[option]() : options.hasOwnProperty("value") && data.value(options.value)
        });
    }

    function perc2deg(p) {
        return parseInt(p / 100 * 360);
    }
    var Progress = function(element, options) {
        this.$element = $(element), this.options = $.extend(!0, {}, $.fn.circularProgress.defaults, options), this.$container = $('<div class="progress-circle"></div>'), this.$element.attr("data-color") && this.$container.addClass("progress-circle-" + this.$element.attr("data-color")), this.$element.attr("data-thick") && this.$container.addClass("progress-circle-thick"), this.$pie = $('<div class="pie"></div>'), this.$pie.$left = $('<div class="left-side half-circle"></div>'), this.$pie.$right = $('<div class="right-side half-circle"></div>'), this.$pie.append(this.$pie.$left).append(this.$pie.$right), this.$container.append(this.$pie).append('<div class="shadow"></div>'), this.$element.after(this.$container), this.val = this.$element.val();
        var deg = perc2deg(this.val);
        this.val <= 50 ? this.$pie.$right.css("transform", "rotate(" + deg + "deg)") : (this.$pie.css("clip", "rect(auto, auto, auto, auto)"), this.$pie.$right.css("transform", "rotate(180deg)"), this.$pie.$left.css("transform", "rotate(" + deg + "deg)"))
    };
    Progress.VERSION = "1.0.0", Progress.prototype.value = function(val) {
        if ("undefined" != typeof val) {
            var deg = perc2deg(val);
            this.$pie.removeAttr("style"), this.$pie.$right.removeAttr("style"), this.$pie.$left.removeAttr("style"), 50 >= val ? this.$pie.$right.css("transform", "rotate(" + deg + "deg)") : (this.$pie.css("clip", "rect(auto, auto, auto, auto)"), this.$pie.$right.css("transform", "rotate(180deg)"), this.$pie.$left.css("transform", "rotate(" + deg + "deg)"))
        }
    };
    var old = $.fn.circularProgress;
    $.fn.circularProgress = Plugin, $.fn.circularProgress.Constructor = Progress, $.fn.circularProgress.defaults = {
        value: 0
    }, $.fn.circularProgress.noConflict = function() {
        return $.fn.circularProgress = old, this
    }, $(window).on("load", function() {
        $('[data-pages-progress="circle"]').each(function() {
            var $progress = $(this);
            $progress.circularProgress($progress.data())
        })
    })
}(window.jQuery),
	
function($) {
    "use strict";

    function Plugin(option) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data("pg.sidebar"),
                options = "object" == typeof option && option;
            data || $this.data("pg.sidebar", data = new Sidebar(this, options)), "string" == typeof option && data[option]()
        })
    }
	
    var Sidebar = function(element, options) {
        function sidebarMouseEnter(e) {
            var _sideBarWidthCondensed = _this.$body.hasClass("rtl") ? -_this.sideBarWidthCondensed : _this.sideBarWidthCondensed,
                menuOpenCSS = 1 == this.css3d ? "translate3d(" + _sideBarWidthCondensed + "px, 0,0)" : "translate(" + _sideBarWidthCondensed + "px, 0)";
            return $.Pages.isVisibleSm() || $.Pages.isVisibleXs() ? !1 : void($(".close-sidebar").data("clicked") || _this.$body.hasClass("menu-pin") || (_this.cssAnimation ? (_this.$element.css({
                transform: menuOpenCSS
            }), _this.$body.addClass("sidebar-visible")) : _this.$element.stop().animate({
                left: "0px"
            }, 400, $.bez(_this.bezierEasing), function() {
                _this.$body.addClass("sidebar-visible");
            })));
        }

        function sidebarMouseLeave(e) {
            var menuClosedCSS = 1 == _this.css3d ? "translate3d(0, 0,0)" : "translate(0, 0)";
            if ($.Pages.isVisibleSm() || $.Pages.isVisibleXs()) return !1;
            if ("undefined" != typeof e) {
                var target = $(e.target);
                if (target.parent(".page-sidebar").length) return
            }
            _this.$body.hasClass("menu-pin") || ($(".sidebar-overlay-slide").hasClass("show") && ($(".sidebar-overlay-slide").removeClass("show"), $("[data-pages-toggle']").removeClass("active")), _this.cssAnimation ? (_this.$element.css({
                transform: menuClosedCSS
            }), _this.$body.removeClass("sidebar-visible")) : _this.$element.stop().animate({
                left: "-" + _this.sideBarWidthCondensed + "px"
            }, 400, $.bez(_this.bezierEasing), function() {
                _this.$body.removeClass("sidebar-visible"), setTimeout(function() {
                    $(".close-sidebar").data({
                        clicked: !1
                    })
                }, 100)
            }))
        }
		
        if(this.$element = $(element), 
		   this.$body = $("body"), 
		   this.options = $.extend(!0, {}, $.fn.sidebar.defaults, options), 
		   this.bezierEasing = [.05, .74, .27, .99], 
		   this.cssAnimation = !0, 
		   this.css3d = !0, 
		   this.sideBarWidth = 280, 
		   this.sideBarWidthCondensed = 210, 
		   this.$sidebarMenu = 
		   this.$element.find(".sidebar-menu > ul"), 
		   this.$pageContainer = $(this.options.pageContainer), 
		   this.$sidebarMenu.length
		){
            "desktop" == $.Pages.getUserAgent(), 
			Modernizr.csstransitions || (this.cssAnimation = !1), 
			Modernizr.csstransforms3d || (this.css3d = !1), 
			"undefined" == typeof angular && $(document).on("click", ".sidebar-menu a", function(e) {
                if ($(this).parent().children(".sub-menu") !== !1) {
                    var el = $(this),
                        parent = $(this).parent().parent(),
                        li = $(this).parent(),
                        sub = $(this).parent().children(".sub-menu");
                    li.hasClass("open active") ? (el.children(".arrow").removeClass("open active"), sub.slideUp(200, function() {
                        li.removeClass("open active")
                    })) : (parent.children("li.open").children(".sub-menu").slideUp(200), parent.children("li.open").children("a").children(".arrow").removeClass("open active"), parent.children("li.open").removeClass("open active"), el.children(".arrow").addClass("open active"), sub.slideDown(200, function() {
                        li.addClass("open active")
                    }))
                }
            }), $(".sidebar-slide-toggle").on("click touchend", function(e) {
                e.preventDefault(), $(this).toggleClass("active");
                var el = $(this).attr("data-pages-toggle");
                null != el && $(el).toggleClass("show")
            });
            var _this = this;
            this.$element.bind("mouseenter mouseleave", sidebarMouseEnter), this.$pageContainer.bind("mouseover", sidebarMouseLeave)
        }
    };
    Sidebar.prototype.toggleSidebar = function(toggle) {
        var timer, bodyColor = $("body").css("background-color");
        $(".page-container").css("background-color", bodyColor), this.$body.hasClass("sidebar-open") ? (this.$body.removeClass("sidebar-open"), timer = setTimeout(function() {
            this.$element.removeClass("visible")
        }.bind(this), 400)) : (clearTimeout(timer), this.$element.addClass("visible"), setTimeout(function() {
            this.$body.addClass("sidebar-open")
        }.bind(this), 10), setTimeout(function() {
            $(".page-container").css({
                "background-color": ""
            })
        }, 1e3))
    }, 
	Sidebar.prototype.togglePinSidebar = function(toggle) {
        "hide" == toggle ? this.$body.removeClass("menu-pin") : "show" == toggle ? this.$body.addClass("menu-pin") : this.$body.toggleClass("menu-pin")
    };
	
    var old = $.fn.sidebar;
    $.fn.sidebar = Plugin, $.fn.sidebar.Constructor = Sidebar, $.fn.sidebar.defaults = {
        pageContainer: ".page-container"
    }, 
	$.fn.sidebar.noConflict = function() {
        return $.fn.sidebar = old, this
    }, 
	$(document).on("click.pg.sidebar.data-api", '[data-toggle-pin="sidebar"]', function(e) {
        e.preventDefault();
        var $target = ($(this), $('[data-pages="sidebar"]'));
        return $target.data("pg.sidebar").togglePinSidebar(), !1
    }), 
	$(document).on("click.pg.sidebar.data-api touchstart", '[data-toggle="sidebar"]', function(e) {
        e.preventDefault();
        var $target = ($(this), $('[data-pages="sidebar"]'));
        return $target.data("pg.sidebar").toggleSidebar(), !1
    })
}(window.jQuery),

function($) {
    "use strict";
    "undefined" == typeof angular && $.Pages.init()
}(window.jQuery);